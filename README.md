# GFS Mobile web application

> We use *YARN* instead of *NPM* as our package manager.
>
> Refer to documentation for [installation instructions][yarn-install]

## Component blueprints

To use component blueprints install [redux-cli][redux-cli] *globally* into your system:
```bash
yarn global add redux-cli
```
Following blueprints are available for use in the project:

* `redux g dumb SomeComponent` — generates a *dumb* component named `SomeComponent` in `./source/components` folder.
* `redux g smart SomeComponent` — generates a *container* component named `SomeComponentContainer` in `./source/containers` folder.

For additional information please refer to [redux-cli documentation][redux-cli] .

[yarn-install]: https://yarnpkg.com/docs/install "YARN installation"
[redux-cli]: https://github.com/SpencerCDixon/redux-cli#readme "Redux CLI"
