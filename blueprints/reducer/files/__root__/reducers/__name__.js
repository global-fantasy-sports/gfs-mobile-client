import { ACTION_NAME } from '../actions/<%= camelEntityName %>';

const initialState = {
    value: null,
};

function <%= camelEntityName %>Reducer ( state = initialState, action ) {
    switch ( action.type ) {
        case ACTION_NAME:
            return {
                ...state,
                ...payload,
            };
        default:
            return state;
    }
}

export default <%= camelEntityName %>Reducer;
