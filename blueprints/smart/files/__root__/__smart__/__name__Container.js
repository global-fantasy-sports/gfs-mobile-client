import { connect } from 'react-redux';

import <%= pascalEntityName %> from '../components/<%= dashesEntityName %>';

const mapStateToProps = state => {
    return {
        // TODO: state,
    };
};

const mapActionsToProps = {
    // TODO: actionCreators
};

export default connect(
    mapStateToProps,
    mapActionsToProps,
)( <%= pascalEntityName %> );
