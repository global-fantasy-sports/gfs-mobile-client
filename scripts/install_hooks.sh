#!/usr/bin/env bash

if [ -d .git ]; then
    echo "Installing git hooks"
    cp -vf ./scripts/git-hooks/pre-push.sh ./.git/hooks/pre-push
    chmod +x ./.git/hooks/pre-push
fi
