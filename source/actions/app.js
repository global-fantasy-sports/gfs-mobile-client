import RestHelper from './rest';
import Types from '../constants/app';
import { API } from '../constants/api';

export default {
    started: () => ( { type: Types.started } ),
    initialised: () => ( { type: Types.initialised } ),
    sportChanged: payload => ( { type: Types.sportChanged, payload } ),
    leagueChanged: payload => ( { type: Types.leagueChanged, payload } ),
    getInfo: () => RestHelper.get( API.appInfo, Types.getInfo ),
    preload: () => ( { type: Types.preload } ),
};
