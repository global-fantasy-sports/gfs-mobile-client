import RestHelper from './rest';
import { API } from '../constants/api';
import Types from '../constants/athlete';

export default {
    getAll: () => RestHelper.get( API.sportAthletes, Types.getAll ),
    getFavorites: () => RestHelper.get( API.sportFavoriteAthletes, Types.getFavorites ),
    history: ( athleteId ) => {
        let route = API.sportAthleteHistory.replace( ':athleteId', athleteId );
        return RestHelper.get( route, Types.history );
    },
};
