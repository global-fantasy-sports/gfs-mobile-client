import {
    SET_COMPARISON,
    RESET_COMPARISON,
    REMOVE_COMPARISON,
    SELECT_COMPARISON_ATHLETE,
} from '../constants/comparison';

export const setComparison = ( sport, league, athleteId, position ) => {
    return ( { sport, league, type: SET_COMPARISON, payload: { athleteId, position } } );
};

export const resetComparison = ( sport, league ) => {
    return ( { sport, league, type: RESET_COMPARISON } );
};

export const removeComparison = ( sport, league, athleteId ) => {
    return ( { sport, league, type: REMOVE_COMPARISON, payload: athleteId } );
};

export const selectComparisonAthlete = ( sport, league, athleteId ) => {
    return ( { sport, league, type: SELECT_COMPARISON_ATHLETE, payload: athleteId } );
};
