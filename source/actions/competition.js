import RestHelper from './rest';
import { API } from '../constants/api';
import Types from '../constants/competition';

import {
    GET_COMPETITIONS_REQUEST,
    GET_COMPETITIONS_REQUEST_FAIL,
    GET_COMPETITIONS_REQUEST_SUCCESS,
    GET_COMPETITION_ROOM_REQUEST,
    GET_COMPETITION_ROOM_SUCCESS,
    RESET_COMPETITION_ROOMS_FILTER,
    SET_MY_GAMES_FILTER,
    TOGGLE_COMPETITION_ROOMS_FILTER_PANEL,
    UPDATE_COMPETITIONS_ROOMS,
    UPDATE_COMPETITION_ROOM,
    UPDATE_COMPETITION_ROOMS_ENTRY_FEE_RANGE,
    UPDATE_COMPETITION_ROOMS_FILTER,
    UPDATE_COMPETITION_ROOM_REQUEST,
} from '../constants/competition';

export const updateCompetitionsRooms = ( payload, sport, league ) => (
    { type: UPDATE_COMPETITIONS_ROOMS, payload, sport, league }
);

export const updateCompetitionRoom = ( payload, sport, league ) => (
    { type: UPDATE_COMPETITION_ROOM, payload, sport, league }
);
export const updateCompetitionRoomRequest = payload => (
    { type: UPDATE_COMPETITION_ROOM_REQUEST, payload }
);

export const updateCompetitionRoomsFilter = ( payload, sport, league ) => (
    { type: UPDATE_COMPETITION_ROOMS_FILTER, payload, sport, league }
);

export const resetCompetitionRoomsFilter = ( sport, league ) => (
    { type: RESET_COMPETITION_ROOMS_FILTER, sport, league }
);

export const updateCompetitionRoomsEntryFeeRange = ( payload, sport, league ) => (
    { type: UPDATE_COMPETITION_ROOMS_ENTRY_FEE_RANGE, payload, sport, league }
);

export const setMyGamesFilter = ( payload, sport, league ) => (
    { type: SET_MY_GAMES_FILTER, payload, sport, league }
);

export const toggleCompetitionRoomFilterPanel = ( sport, league ) => (
    { type: TOGGLE_COMPETITION_ROOMS_FILTER_PANEL, sport, league }
);

export const getCompetitionsRequest = payload => (
    { type: GET_COMPETITIONS_REQUEST, payload }
);
export const getCompetitionsRequestFail = ( payload, sport, league ) => (
    { type: GET_COMPETITIONS_REQUEST_FAIL, payload, sport, league }
);
export const getCompetitionsRequestSuccess = ( payload, sport, league ) => (
    { type: GET_COMPETITIONS_REQUEST_SUCCESS, payload, sport, league }
);

export const getCompetitionRoomRequest = ( payload, sport, league ) => (
    { type: GET_COMPETITION_ROOM_REQUEST, payload, sport, league }
);
export const getCompetitionRoomSuccess = ( payload, sport, league ) => (
    { type: GET_COMPETITION_ROOM_SUCCESS, payload, sport, league }
);

export default {
    getAll: () => RestHelper.get( API.sportRooms, Types.getAll ),
    getWithGames: ( roomId, gameId ) => {
        let route = API.sportRoomGames.replace( ':roomId', roomId );
        if ( gameId ) {
            route += '?game_id=' + gameId;
        }
        return RestHelper.get( route, Types.getWithGames );
    },
};
