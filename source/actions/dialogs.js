import {
    SHOW_CONFIRM_DIALOG,
    HIDE_DIALOG,
    OPEN_BOTTOM_SIDE_MENU,
    CLOSE_BOTTOM_SIDE_MENU } from '../constants/dialogs';


export const showModalDialog = ( payload ) => (
    { type: SHOW_CONFIRM_DIALOG, payload }
);

export const hideModalDialog = ( ) => (
    { type: HIDE_DIALOG }
);

export const showRulesDialog = () => showModalDialog( {
    rules: true,
} );

export const showScoringDialog = () => showModalDialog( {
    scoring: true,
} );

export const openBottomSideMenu = () => {
    return ( { type: OPEN_BOTTOM_SIDE_MENU } );
};

export const closeBottomSideMenu = () => {
    return ( { type: CLOSE_BOTTOM_SIDE_MENU } );
};
