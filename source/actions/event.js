import Types from '../constants/event';
import { API } from '../constants/api';
import RestHelper from './rest';

export default {
    open: () => RestHelper.get( API.sportEvent, Types.open ),
};
