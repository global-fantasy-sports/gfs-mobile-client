import {
    FACEBOOK_CHALLENGE,
    FACEBOOK_CHALLENGE_SUCCESS,
    FACEBOOK_FRIENDS_FETCHED,
    FACEBOOK_INITIALIZED,
    FACEBOOK_INVITE,
    FACEBOOK_INVITE_FAIL,
    FACEBOOK_INVITE_SUCCESS,
    FACEBOOK_LOGIN,
    FACEBOOK_SEARCH,
    FACEBOOK_STATUS,
} from '../constants/facebook';

export const fbChallenge = ( id ) => ( { type: FACEBOOK_CHALLENGE, payload: id } );
export const fbChallengeSuccess = ( id ) => ( { type: FACEBOOK_CHALLENGE_SUCCESS, payload: id } );
export const fbFriendsFetched = ( payload ) => ( { type: FACEBOOK_FRIENDS_FETCHED, payload } );
export const fbInitialized = () => ( { type: FACEBOOK_INITIALIZED } );
export const fbInvite = ( id ) => ( { type: FACEBOOK_INVITE, payload: id } );
export const fbInviteFail = ( id ) => ( { type: FACEBOOK_INVITE_FAIL, payload: id } );
export const fbInviteSuccess = ( id ) => ( { type: FACEBOOK_INVITE_SUCCESS, payload: id } );
export const fbLogin = () => ( { type: FACEBOOK_LOGIN } );
export const fbRegisteredFriendsFetched = ( payload ) => ( { type: FACEBOOK_FRIENDS_FETCHED, payload } );
export const fbStatus = ( status ) => ( { type: FACEBOOK_STATUS, payload: status } );
export const fbSearch = ( search = '' ) => ( { type: FACEBOOK_SEARCH, payload: search } );
