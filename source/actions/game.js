import Types from '../constants/game';
import { API } from '../constants/api';
import RestHelper from './rest';

export default {
    getMy: () => RestHelper.get( API.sportMyGames, Types.getMy ),
    edit: ( sport, league, gameId ) => ( {
        type: Types.edit,
        payload: { sport, league, gameId },
    } ),
    remove: ( sport, league, gameId ) => ( {
        type: Types.remove,
        payload: { sport, league, gameId },
    } ),
    forgetNewGames: ( sport, league ) => ( {
        type: Types.forgetNewGames,
        sport,
        league,
    } ),
};
