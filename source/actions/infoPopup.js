import { HIDE_INFO_POPUP, SHOW_INFO_POPUP } from '../constants/infoPopups';

export const hideInfoPopup = ( name ) => {
    return ( { type: HIDE_INFO_POPUP, payload: name } );
};

export const showInfoPopup = ( name ) => {
    return ( { type: SHOW_INFO_POPUP, payload: name } );
};
