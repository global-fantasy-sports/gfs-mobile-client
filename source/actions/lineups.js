import { API } from '../constants/api';
import rest from './rest';

export const LINEUPS_DELETE = 'LINEUPS::DELETE';
export const LINEUPS_DUPLICATE = 'LINEUPS::DUPLICATE';
export const LINEUPS_EDIT_CANCEL = 'LINEUPS::EDIT_CANCEL';
export const LINEUPS_EDIT_START = 'LINEUPS::EDIT_START';
export const LINEUPS_FETCH = 'LINEUPS::FETCH';
export const LINEUPS_REMOVE_ATHLETE = 'LINEUPS::REMOVE_ATHLETE';
export const LINEUPS_SAVE = 'LINEUPS::SAVE';
export const LINEUPS_SAVE_WO_REDIRECT = 'LINEUPS::SAVE_WO_REDIRECT';
export const LINEUPS_SET_FAVORITE = 'LINEUPS::SET_FAVORITE';
export const LINEUPS_SET_NAME = 'LINEUPS::SET_NAME';
export const LINEUPS_SET_POSITION = 'LINEUPS::SET_POSITION';

export const getLineupURL = ( id ) => {
    const url = API.lineups;
    return ( id && id !== 'new' ) ? `${url}${id}/` : url;
};

export const fetchLineups = () => rest.get( API.lineups, LINEUPS_FETCH );

export const saveLineup = ( sport, league, { id, ...lineup } ) => {
    const method = id ? 'put' : 'post';
    const url = getLineupURL( id );
    return rest[method]( url, lineup, LINEUPS_SAVE );
};

export const saveLineupWORedirect = ( sport, league, { id, ...lineup } ) => {
    const method = id ? 'put' : 'post';
    const url = getLineupURL( id );
    return rest[method]( url, lineup, LINEUPS_SAVE_WO_REDIRECT );
};

export const deleteLineup = ( sport, league, id ) => ( {
    type: LINEUPS_DELETE,
    payload: { sport, league, id },
} );

export const duplicateLineup = ( sport, league, id ) => ( {
    type: LINEUPS_DUPLICATE,
    payload: { id, sport, league },
} );

export const editLineup = ( sport, league, id ) => ( {
    type: LINEUPS_EDIT_START,
    payload: { id, sport, league },
} );

export const cancelLineupEdit = () => ( {
    type: LINEUPS_EDIT_CANCEL,
} );

export const setLineupName = ( lineupId, name ) => ( {
    type: LINEUPS_SET_NAME,
    payload: { name },
} );

export const setLineupPosition = ( lineup, position, value ) => ( {
    type: LINEUPS_SET_POSITION,
    payload: { lineup, position, value },
} );

export const undraftAthlete = ( lineup, id ) => ( {
    type: LINEUPS_REMOVE_ATHLETE,
    payload: { lineup, id },
} );
