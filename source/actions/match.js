import Types from '../constants/match';
import { API } from '../constants/api';
import RestHelper from './rest';

export default {
    getAll: ( sport, league, eventId ) => {
        let route = API.sportMatches;
        if ( eventId ) {
            route += '?sport_event=' + eventId;
        }
        return RestHelper.get( route, Types.getAll );
    },
};
