import Types from '../constants/notification';
import { API } from '../constants/api';
import RestHelper from './rest';

export const toggleNotifications = () => {
    return ( { type: Types.toggleNotifications } );
};

export default {
    markAll: () => RestHelper.post( API.notificationsMarkAll, {}, Types.markAll ),
    next: ( id ) => {
        const route = API.notificationsNext.replace( ':id', id );
        return RestHelper.get( route, Types.root );
    },
    recent: () => RestHelper.get( API.notificationsRecent, Types.root ),
};
