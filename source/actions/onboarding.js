import { HIDE_ONBOARDING } from '../constants/onboarding';

export const hideOnboarding = ( name ) => ( {
    type: HIDE_ONBOARDING,
    payload: name,
} );


