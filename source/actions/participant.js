import Types from '../constants/participant';
import { API } from '../constants/api';
import RestHelper from './rest';

export default {
    getAll: () => RestHelper.get( API.sportParticipants, Types.getAll ),
    getAllResults: () => RestHelper.get( API.sportParticipantsResults, Types.getAllResults ),
    getResultsLegend: () => RestHelper.get( API.sportParticipantsResultsLegend, Types.getResultsLegend ),
};
