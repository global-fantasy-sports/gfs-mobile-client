export const REGISTRATION_REQUEST = 'REGISTRATION::REQUEST';
export const REGISTRATION_FAIL = 'REGISTRATION::FAIL';
export const REGISTRATION_SUCCESS = 'REGISTRATION::SUCCESS';
export const REGISTRATION_OFF = 'REGISTRATION::OFF';
export const REGISTRATION_REDIRECTED = 'REGISTRATION::REDIRECTED';

export const registrationRequest = ( payload ) => ( { type: REGISTRATION_REQUEST, payload } );
export const registrationRequestSuccess = ( payload ) => ( { type: REGISTRATION_SUCCESS, payload } );
export const registrationClean = ( payload ) => ( { type: REGISTRATION_OFF, payload } );
export const registrationRequestFail = ( payload ) => ( { type: REGISTRATION_FAIL, payload } );
export const registrationRedirect = ( payload ) => ( { type: REGISTRATION_REDIRECTED, payload } );
