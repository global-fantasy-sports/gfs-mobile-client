import { makeRequestActionTypes } from '../utils/actions';

export const REST_REQUEST = 'REST::REQUEST';

/**
 * Creates an Action object that REST Worker can understand
 * @param {object} options
 * @param {string} options.method
 * @param {string} options.url
 * @param {object} options.actions
 * @param {?object} options.params
 * @param {?object} options.data
 * @return {object} fetch action for REST Worker
 */
export function request ( options ) {
    return {
        type: REST_REQUEST,
        payload: {
            method: 'get',
            ...options,
        },
    };
}

const parseOptions = ( input ) =>
    ( typeof input === 'string' || Array.isArray( input ) )
        ? { actions: makeRequestActionTypes( input ) }
        : input;

export default {
    /**
     * GET request shortcut
     * @param {string} url
     * @param {String|Array|object} [options]
     */
    get: ( url, options ) => request( {
        url,
        method: 'get',
        ...parseOptions( options ),
    } ),
    /**
     * POST request shortcut
     * @param {string} url
     * @param {*} data
     * @param {String|Array|object} options
     */
    post: ( url, data, options ) => request( {
        url, data,
        method: 'post',
        ...parseOptions( options ),
    } ),
    /**
     * PUT request shortcut
     * @param {string} url
     * @param {*} data
     * @param {String|Array|object} options
     */
    put: ( url, data, options ) => request( {
        url, data,
        method: 'put',
        ...parseOptions( options ),
    } ),
    /**
     * DELETE request shortcut
     * @param {string} url
     * @param {*} data
     * @param {String|Array|object} options
     */
    delete: ( url, data, options ) => request( {
        url, data,
        method: 'delete',
        ...parseOptions( options ),
    } ),
};
