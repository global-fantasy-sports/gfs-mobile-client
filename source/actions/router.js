import RoutesConstants from '../constants/routes';
import RouterTypes from '../constants/router';


export function goTo ( path ) {
    return { type: RouterTypes.LEAGUE_GOTO, payload: path };
}

export function redirectTo ( path ) {
    return { type: RouterTypes.LEAGUE_REDIRECT_TO, payload: path };
}

export function goToCompetitionRoom ( iRoomId ) {
    return goTo( `${ RoutesConstants.get( 'competitionPage' ) }${ iRoomId }/` );
}
