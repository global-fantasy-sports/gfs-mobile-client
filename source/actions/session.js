import { API } from '../constants/api';
import Types from '../constants/user';
import RestHelper from './rest';

export default {
    create: ( payload ) => RestHelper.post( API.session, payload, Types.login.root ),
    remove: () => RestHelper.delete( API.session, {}, Types.logout.root ),

    clean: () => ( { type: Types.logout.clean } ),
    get: () => RestHelper.get( API.session, Types.session.get ),
    error: ( payload ) => ( { type: Types.session.error, payload } ),
};
