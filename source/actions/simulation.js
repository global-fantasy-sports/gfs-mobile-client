import { API } from '../constants/api';
import Types from '../constants/simulation';
import RestHelper from './rest';

export const SIM_CLEAR_ERROR = 'SIMULATION::CLEAR_ERROR';

export default {
    restart: () => {
        if ( process.env.WITH_SIMULATION !== 'yes' ) {
            return { type: '' };
        }
        return RestHelper.post( API.sportSimulationRestart, {}, Types.restart );
    },
    setDate: ( time ) => {
        if ( process.env.WITH_SIMULATION !== 'yes' ) {
            return { type: '' };
        }
        return RestHelper.post( API.sportSimulationSetDate, { time }, Types.setDate );
    },
    getDate: () => RestHelper.get( API.sportSimulationGetDate, Types.getDate ),
    setPoint: ( point ) => {
        if ( process.env.WITH_SIMULATION !== 'yes' ) {
            return { type: '' };
        }
        return RestHelper.post( API.sportSimulationSetPoint, { point }, Types.setPoint );
    },
    clearError: () => ( { type: SIM_CLEAR_ERROR } ),
};
