import { makeRequestActionTypes } from '../utils/actions';

export const SOCKET_CONNECTED = 'SOCKET::CONNECTED';
export const SOCKET_DISCONNECTED = 'SOCKET::DISCONNECTED';
export const SOCKET_ERROR = 'SOCKET::ERROR';
export const SOCKET_MESSAGE = 'SOCKET::MESSAGE';
export const SOCKET_SEND = 'SOCKET::SEND';
export const SOCKET_SUBSCRIBE = 'SOCKET::SUBSCRIBE';
export const SOCKET_UNSUBSCRIBE = 'SOCKET::UNSUBSCRIBE';

export const send = ( action, channel, value, actions ) => ( {
    type: SOCKET_SEND,
    payload: {
        action, channel, value,
        actions: makeRequestActionTypes( actions ),
    },
} );

export default {
    get: ( channel, value, actions ) => send( 'get', channel, value, actions ),
    set: ( channel, value, actions ) => send( 'set', channel, value, actions ),
    delete: ( channel, value, actions ) => send( 'delete', channel, value, actions ),
    subscribe: ( sport, league ) => send( 'set', 'subscribe-broadcast', { sport, league }, SOCKET_SUBSCRIBE ),
    unsubscribe: () => send( 'set', 'unsubscribe-broadcast', {}, SOCKET_UNSUBSCRIBE ),
};
