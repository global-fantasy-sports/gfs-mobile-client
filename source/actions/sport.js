import {
    UPDATE_SPORTSMEN,
    UPDATE_PARTICIPANTS,
    UPDATE_GAMES,
    UPDATE_REAL_GAMES,
    UPDATE_CURRENT_DATE,
    UPDATE_LOCAL_DATE,
    SPORT_SOCKET_LOGGED,
    SPORT_TOGGLE_FAVORITE,
    SPORT_ADD_FAVORITE,
    SPORT_DELETE_FAVORITE,
    UPDATE_TEAMS,
    UPDATE_DST,
    UPDATE_PARTICIPANT_EVENT_SPORTSMAN_INDEX,
    UPDATE_DEFENCE_TEAM_EVENT_TEAM_INDEX,
} from '../constants/sport';
import Types from '../constants/sport';
import { API } from '../constants/api';
import RestHelper from './rest';


export const updateSportsmen = ( payload, sport ) => ( { type: UPDATE_SPORTSMEN, payload, sport } );

export const updateParticipants = ( payload, sport ) => ( { type: UPDATE_PARTICIPANTS, payload, sport } );

export const updateParticipantEventSportsmanIndex = ( payload, sport ) => (
    { type: UPDATE_PARTICIPANT_EVENT_SPORTSMAN_INDEX, payload, sport }
);

export const updateDefenceTeamEventTeamIndex = ( payload, sport ) => (
    { type: UPDATE_DEFENCE_TEAM_EVENT_TEAM_INDEX, payload, sport }
);

export const updateGames = ( payload, sport ) => ( { type: UPDATE_GAMES, payload, sport } );

export const updateRealGames = ( payload, sport ) => ( { type: UPDATE_REAL_GAMES, payload, sport } );

export const updateCurrentDate = ( payload, sport ) => ( { type: UPDATE_CURRENT_DATE, payload, sport } );

export const updateLocalDate = ( payload, sport ) => ( { type: UPDATE_LOCAL_DATE, payload, sport } );

export const sportSocketLogged = ( sport ) => ( { type: SPORT_SOCKET_LOGGED, payload: sport } );

export const updateTeams = ( payload, sport ) => ( { type: UPDATE_TEAMS, payload, sport } );

export const updateDST = ( payload, sport ) => ( { type: UPDATE_DST, payload, sport } );

export const toggleFavoriteAthlete = ( id ) => ( { type: SPORT_TOGGLE_FAVORITE, payload: { athleteId: id } } );

export const addFavoriteAthlete = ( aId ) => {
    return RestHelper.post( API.sportFavoriteAthletes, { athleteId: aId }, SPORT_ADD_FAVORITE );
};

export const deleteFavoriteAthlete = ( aId ) => {
    const route = API.sportFavoriteAthleteDelete.replace( ':id', aId );
    return RestHelper.delete( route, { }, SPORT_DELETE_FAVORITE );
};

export default {
    info: () => RestHelper.get( API.sportInfo, Types.info.root ),
};
