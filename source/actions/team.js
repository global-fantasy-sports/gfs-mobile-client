import Types from '../constants/team';
import { API } from '../constants/api';
import RestHelper from './rest';

export default {
    getAll: () => RestHelper.get( API.sportTeams, Types.getAll ),
};
