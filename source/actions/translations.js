import Types from '../constants/translations';
import { API } from '../constants/api';
import RestHelper from './rest';

export default {
    getAll: () => RestHelper.get( API.translations, Types.getAll ),
};
