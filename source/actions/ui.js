import { REPORT_STEP, GO_BACK, SET_VARIABLE } from '../constants/ui';

export const reportStep = ( step, url ) => ( { type: REPORT_STEP, payload: { step, url } } );

export const goPrevStep = ( name ) => ( { type: GO_BACK, payload: name } );

export const setVariable = ( variable ) => ( { type: SET_VARIABLE, payload: variable } );
