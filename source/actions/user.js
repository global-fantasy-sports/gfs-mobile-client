import RestHelper from './rest';
import { API } from '../constants/api';
import Types from '../constants/user';

export default {
    clean: () => ( { type: Types.user.clean } ),
    error: () => ( { type: Types.session.error } ),
    info: () => RestHelper.get( API.userInfo, Types.user.info ),
};
