export const CLEAR_ERROR = 'WIZARD::CLEAR_ERROR';

export const CREATE_GAME = 'WIZARD::CREATE_GAME';
export const CREATE_GAME_FAIL = 'WIZARD::CREATE_GAME_FAIL';
export const CREATE_GAME_SUCCESS = 'WIZARD::CREATE_GAME_SUCCESS';

export const WIZARD_INIT = 'WIZARD::INIT';
export const WIZARD_SAVE = 'WIZARD::SAVE';
export const WIZARD_CLEAR = 'WIZARD::CLEAR';
export const WIZARD_ERROR = 'WIZARD::ERROR';

export const WIZARD_DRAFT_CLEAR = 'WIZARD::DRAFT_CLEAR';

export const INIT_FROM_SCRATCH = 'WIZARD::INIT_FROM_SCRATCH';
export const INIT_FROM_COMPETITION = 'WIZARD::INIT_FROM_COMPETITION';
export const INIT_FROM_LINEUP = 'WIZARD::INIT_FROM_LINEUP';

export const SELECT_COMPETITION = 'WIZARD::SELECT_COMPETITION';
export const SELECT_PARTICIPANT = 'WIZARD::SELECT_PARTICIPANT';
export const UNSELECT_PARTICIPANT = 'WIZARD::UNSELECT_PARTICIPANT';
export const REPLACE_PARTICIPANT = 'WIZARD::REPLACE_PARTICIPANT';
export const CLEAR_POSITION = 'WIZARD::CLEAR_POSITION';

export const LINEUP_AUTOFILL = 'WIZARD::LINEUP_AUTOFILL';
export const LINEUP_HIDE_SAVER = 'WIZARD::LINEUP_HIDE_SAVER';

export const CONFIRM_REDIRECT_TO_LINEUP = 'WIZARD::CONFIRM_REDIRECT_TO_LINEUP';


export const initFromScratch = ( sport, league ) => ( {
    type: INIT_FROM_SCRATCH,
    payload: { sport, league },
} );

export const joinCompetition = ( sport, league, competitionId ) => ( {
    type: INIT_FROM_COMPETITION,
    payload: { sport, league, competitionId },
} );

export const initFromLineup = ( sport, league, lineupId ) => ( {
    type: INIT_FROM_LINEUP,
    payload: { sport, league, lineupId },
} );

export const saveGame = () => ( { type: WIZARD_SAVE } );

export const clearWizard = () => ( { type: WIZARD_CLEAR } );

export const clearDraft = () => ( { type: WIZARD_DRAFT_CLEAR } );

export const selectCompetition = ( id ) => ( { type: SELECT_COMPETITION, payload: { id } } );

export const selectParticipant = ( position, id ) => ( { type: SELECT_PARTICIPANT, payload: { position, id } } );

export const unselectParticipant = ( id ) => ( { type: UNSELECT_PARTICIPANT, payload: { id } } );

export const replaceParticipant = ( position, replaceId, replaceWithId ) => (
    {
        type: REPLACE_PARTICIPANT,
        payload: { position, replaceId, replaceWithId },
    }
);

export const clearPosition = ( position ) => ( { type: CLEAR_POSITION, payload: { position } } );

export const createGame = () => ( { type: CREATE_GAME } );

export const clearError = () => ( { type: CLEAR_ERROR } );

export const autofill = () => ( { type: LINEUP_AUTOFILL } );

export const wizardError = ( error ) => ( { type: WIZARD_ERROR, payload: { error } } );

export const hideLineUpSaver = () => ( { type: LINEUP_HIDE_SAVER } );

export const confirmRedirect = () => ( { type: CONFIRM_REDIRECT_TO_LINEUP } );

