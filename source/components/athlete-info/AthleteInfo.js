import React, { Component, PropTypes } from 'react';

import Page from '../page';
import ScrollLoader from '../scroll-loader';
import AthleteInfoHeader from './AthleteInfoHeader';
import AthleteInfoSummary from './AthleteInfoSummary';
import TabPanel, { Tab } from './TabPanel';
import GameLogTable from './GameLogTable';
import SalaryFPPGChart from './SalaryFPPGChart';

import './AthleteInfo.scss';

const AthleteInfoStats = ( { _t, stats = {}, statsByPosition } ) => (
    <section className="AthleteInfoStats">
        { statsByPosition.map( ( key, index ) =>
            <div className="AthleteInfoStats__column" key={ index }>
                <div className="label">
                    { _t( `athlete_card.stats.${key}` ) }
                </div>
                <div className="value">{ stats[key] }</div>
            </div>
        ) }
    </section>
);

AthleteInfoStats.propTypes = {
    stats: PropTypes.object.isRequired,
    statsByPosition: PropTypes.array.isRequired,
    _t: PropTypes.func,
};

class AthleteInfo extends Component {

    static propTypes = {
        athlete: PropTypes.object.isRequired,
        averageStats: PropTypes.object.isRequired,
        compared: PropTypes.bool.isRequired,
        match: PropTypes.object.isRequired,
        dataLoaded: PropTypes.bool.isRequired,
        gameLogs: PropTypes.array.isRequired,
        getAthleteHistory: PropTypes.func.isRequired,
        isFavorite: PropTypes.bool.isRequired,
        isSelected: PropTypes.bool.isRequired,
        onCompare: PropTypes.func.isRequired,
        onDraft: PropTypes.func.isRequired,
        onGoBack: PropTypes.func.isRequired,
        onUndraft: PropTypes.func.isRequired,
        onUncompare: PropTypes.func.isRequired,
        onHandleFavorite: PropTypes.func.isRequired,
        selectedTeams: PropTypes.object.isRequired,
        statsByPosition: PropTypes.array.isRequired,
        statsHistory: PropTypes.array.isRequired,
        team: PropTypes.object.isRequired,
        _t: PropTypes.func,
    };

    getHistory ( props ) {
        if ( props.athlete.id && !this.loadedHistory ) {
            props.getAthleteHistory();
            this.loadedHistory = true;
            return false;
        }
        return true;
    }

    componentDidMount () {
        this.getHistory( this.props );
    }

    shouldComponentUpdate ( nextProps ) {
        return this.getHistory( nextProps );
    }

    handleCompare = () => {
        const { compared, athlete } = this.props;
        if ( compared ) {
            this.props.onUncompare( athlete.athleteId );
        } else {
            this.props.onCompare( athlete );
        }
    };

    render () {
        if ( !this.props.dataLoaded ) {
            return <ScrollLoader />;
        }

        const {
            _t,
            athlete,
            averageStats,
            gameLogs,
            match,
            compared,
            isFavorite,
            isSelected,
            selectedTeams,
            statsByPosition,
            statsHistory,
            team,
            onDraft,
            onGoBack,
            onHandleFavorite,
            onUndraft,
        } = this.props;

        const teamFull = selectedTeams[athlete.teamId] >= 4;
        return (
            <Page className="AthleteInfo">
                <AthleteInfoHeader
                    _t={ _t }
                    athlete={ athlete }
                    compared={ compared }
                    isFavorite={ isFavorite }
                    isSelected={ isSelected }
                    onCompare={ this.handleCompare }
                    onDraft={ onDraft }
                    onGoBack={ onGoBack }
                    onHandleFavorite={ onHandleFavorite }
                    onUndraft={ onUndraft }
                    photo={ athlete.largePhoto }
                    teamFull={ teamFull }
                    teamLogo={ team.largeIcon }
                />
                <AthleteInfoSummary
                    _t={ _t }
                    athlete={ athlete }
                    isSelected={ isSelected }
                    match={ match }
                    pointsHistory={ statsHistory.map( info => info.fantasyPoints ) }
                    teamFull={ teamFull }
                    teamName={ team.name }
                />
                <AthleteInfoStats
                    _t={ _t }
                    stats={ averageStats }
                    statsByPosition={ statsByPosition }
                />
                <TabPanel>
                    <Tab label={ _t( 'athlete_card.tab.salary_fppg' ) }>
                        <SalaryFPPGChart _t={ _t } data={ statsHistory } />
                    </Tab>
                    <Tab label={ _t( 'athlete_card.tab.game_log' ) }>
                        <GameLogTable _t={ _t } data={ gameLogs } />
                     </Tab>
                </TabPanel>
            </Page>
        );
    }
}

export default AthleteInfo;
