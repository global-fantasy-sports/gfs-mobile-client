import React, { PropTypes } from 'react';
import cx from 'classnames';

import Btn from '../../ui/btn/Btn';
import PlaceholderImage from '../common/PlaceholderImage';

import './AthleteInfo.scss';
import playerPlaceholder from '../../images/room/player-placeholder.png';

const DraftAthleteButton = ( { _t, selected, onDraft, onUndraft, teamFull } ) => {
    let label = _t( 'athlete_card.header.draft.btn_draft' );
    if ( selected ) {
        label = _t( 'athlete_card.header.draft.btn_undraft' );
    }
    return (
        <Btn
            disabled={ teamFull && !selected }
            label={ label }
            onClick={ selected ? onUndraft : onDraft }
            size="normal"
            type={ selected ? 'warning' : 'payment' }
        />
    );
};

DraftAthleteButton.propTypes = {
    onDraft: PropTypes.func.isRequired,
    onUndraft: PropTypes.func.isRequired,
    selected: PropTypes.bool,
    teamFull: PropTypes.bool.isRequired,
    _t: PropTypes.func.isRequired,
};

const CompareAthleteButton = ( { _t, compared, onClick } ) => (
        <Btn
            label={ compared ?
                _t( 'athlete_card.header.btn_discard' ) :
                _t( 'athlete_card.header.btn_compare' )
            }
            onClick={ onClick }
            size="normal"
            type="default"
        />
);

CompareAthleteButton.propTypes = {
    _t: PropTypes.func.isRequired,
    compared: PropTypes.bool.isRequired,
    onClick: PropTypes.func.isRequired,
};

const AthleteInfoHeader = ( {
        _t,
        athlete,
        compared,
        isSelected,
        isFavorite,
        onGoBack,
        onHandleFavorite,
        onCompare,
        onDraft,
        onUndraft,
        photo,
        teamLogo,
        teamFull,
        } ) => {
    return (
        <div className="AthleteInfoHeader">
            <div className="AthleteInfoHeader__inner">
                <div
                    className="AthleteInfoHeader__bg"
                    style={ { backgroundImage: `url(${teamLogo})` } }
                />
                <div className="AthleteInfoHeader__content">
                    <PlaceholderImage
                        className="AthleteInfoHeader__image"
                        height={ 145 }
                        placeholder={ playerPlaceholder }
                        src={ photo }
                        width={ 200 }
                    />
                    <div className="AthleteInfoHeader__buttons">
                        <Btn
                            icon="arrow-left"
                            label={ _t( 'athlete_card.header.btn_back' ) }
                            onClick={ onGoBack }
                            type="transparent"
                        />
                        <div className="AthleteInfoHeader__right-btn">
                            <DraftAthleteButton
                                _t={ _t }
                                onDraft={ onDraft }
                                onUndraft={ onUndraft }
                                selected={ isSelected }
                                teamFull={ teamFull }
                            />
                            <CompareAthleteButton
                                _t={ _t }
                                compared={ compared }
                                onClick={ onCompare }
                            />
                        </div>
                    </div>
                </div>
            </div>
            <div className="AthleteInfoHeader__favorites">
                <i
                    className={ cx( 'icon-fav', { 'icon-fav-active': isFavorite } ) }
                    onClick={ onHandleFavorite }
                />
            </div>
            <h1
                className="AthleteInfoHeader__name"
                data-position={ _t( `athlete.position.short.${ athlete.position }` ) }
            >
                { _t( athlete.name ) }
            </h1>
        </div>
    );
};

AthleteInfoHeader.propTypes = {
    isSelected: PropTypes.bool.isRequired,
    athlete: PropTypes.object.isRequired,
    compared: PropTypes.bool.isRequired,
    isFavorite: PropTypes.bool.isRequired,
    onCompare: PropTypes.func.isRequired,
    onDraft: PropTypes.func.isRequired,
    onUndraft: PropTypes.func.isRequired,
    onGoBack: PropTypes.func.isRequired,
    onHandleFavorite: PropTypes.func.isRequired,
    photo: PropTypes.string.isRequired,
    teamFull: PropTypes.bool.isRequired,
    teamLogo: PropTypes.string.isRequired,
    _t: PropTypes.func.isRequired,
};

export default AthleteInfoHeader;
