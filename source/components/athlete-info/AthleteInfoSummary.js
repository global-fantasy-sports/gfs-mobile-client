import React, { PropTypes } from 'react';

import Money from '../common/Money';
import TinyLineChart from '../tiny-line-chart';
import { format, formatHeight } from '../../utils/number';

import './AthleteInfo.scss';

const Entry = ( { _t, label, children } ) => (
    children ?
        <span className="entry">
            { label ? _t( `athlete_card.summary.entry.${label}` ) + ': ' : '' }
            <b>{ children }</b>
        </span>
        : null
);

Entry.propTypes = {
    children: PropTypes.node,
    label: PropTypes.string,
    _t: PropTypes.func,
};

/** @return {XML|null} */
const Warning = ( props ) => {
    if ( props.isSelected ) {
        return (
            <section className="warning">
                <i className="locked-icon" />
                <p className="text">
                    { props._t( 'athlete_card.summary.warning.already_filled' ) }
                </p>
            </section>
        );
    } else if ( props.teamFull ) {
        return (
            <section className="warning">
                <div className="count">4/4</div>
                <p className="text">
                    { props._t( 'athlete_card.summary.warning.too_many' ) }
                </p>
            </section>
        );
    } else if ( props.unavailable ) {
        return (
            <section className="warning">
                <i className="exclamation-icon" />
                <p className="text">
                    { props._t( 'athlete_card.summary.warning.unavailable' ) }
                </p>
            </section>
        );
    }

    return null;
};

Warning.propTypes = {
    isSelected: PropTypes.bool.isRequired,
    teamFull: PropTypes.bool.isRequired,
    unavailable: PropTypes.bool,
    _t: PropTypes.func,
};


const AthleteInfoSummary = ( {
        _t,
        athlete,
        isSelected,
        pointsHistory = [],
        teamName,
        teamFull,
        match,
    } ) => {

    let pointsHistoryForChart = pointsHistory;
    if ( pointsHistory.length >= 12 ) {
        pointsHistoryForChart = pointsHistory.slice( -12 );
    }
    return (
        <section className="AthleteInfoSummary">
            <main>
                <Warning
                    _t={ _t }
                    isSelected={ isSelected && !athlete.unavailable }
                    teamFull={ teamFull }
                    unavailable={ athlete.unavailable }
                />
                <section className="AthleteInfoSummary__content">
                    <section>
                        <p className="common-info">
                            <Entry>{ _t( match.homeTeam ) }@{ _t( match.awayTeam ) }</Entry>
                            <br />
                            <Entry>{ match.time }</Entry>
                        </p>
                        <p className="common-info">
                            <Entry>{ _t( teamName ) }</Entry>
                            <br />
                            <Entry>
                                { _t( `athlete_card.summary.position.${athlete.position}` ) }&nbsp;
                                #{ athlete.jerseyNumber }
                            </Entry>
                        </p>
                        <p className="common-info">
                            <Entry _t={ _t } label="h">{ formatHeight( athlete.height ) }</Entry>,&nbsp;
                            <Entry _t={ _t } label="w">{ athlete.weight }</Entry>,&nbsp;
                            <Entry _t={ _t } label="age">{ athlete.age }</Entry>
                            <br />
                            <Entry _t={ _t } label="college">{ athlete.college }</Entry>
                            <Entry _t={ _t } label="country">{ athlete.country }</Entry>
                        </p>
                    </section>
                    <section>
                        <TinyLineChart
                            color="#3c5ca5"
                            data={ pointsHistoryForChart }
                            fill="#d2d8e5"
                            height={ 40 }
                            width={ 130 }
                        />
                        <div className="legend">
                            <div className="legend-item">
                                <span className="label">
                                    { _t( 'athlete_card.legend.fppg' ) }
                                </span>
                                <span className="value">{ format( athlete.FPPG, 2 ) }</span>
                            </div>
                            <div className="legend-item">
                                <span className="label">
                                    { _t( 'athlete_card.legend.salary' ) }
                                </span>
                                <Money className="value" currency="usd" value={ athlete.salary || 0 } />
                            </div>
                        </div>
                    </section>
                </section>
            </main>
        </section>
    );
};

AthleteInfoSummary.propTypes = {
    athlete: PropTypes.object.isRequired,
    isSelected: PropTypes.bool.isRequired,
    match: PropTypes.shape( {
        homeTeam: PropTypes.string.isRequired,
        awayTeam: PropTypes.string.isRequired,
        time: PropTypes.string.isRequired,
    } ).isRequired,
    teamFull: PropTypes.bool.isRequired,
    pointsHistory: PropTypes.arrayOf( PropTypes.number ).isRequired,
    teamName: PropTypes.string.isRequired,
    _t: PropTypes.func,
};

export default AthleteInfoSummary;
