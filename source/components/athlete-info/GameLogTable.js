import React, { Component, PropTypes } from 'react';
import moment from 'moment';

import { TIME_FORMAT } from '../../constants/app';

class GameLogTable extends Component {

    static propTypes = {
        _t: PropTypes.func,
        data: PropTypes.arrayOf( PropTypes.shape( {
            date: PropTypes.number.isRequired,
            gameLog: PropTypes.array.isRequired,
        } ) ).isRequired,
    };

    renderTitleColumn = ( _t, data ) => {
        const date = moment( data[0].date ).format( TIME_FORMAT.athlete_info_log_year );
        const labels = data[0].gameLog.map( ( row, i ) =>
            <div className="game-log__cell game-log__cell--title" key={ i }>{ _t( row[0] ) }</div>
        );

        return (
            <div className="game-log__column game-log__column--title">
                <div className="game-log__cell game-log__cell--caption game-log__cell--title">
                    { date }
                </div>
                { labels }
            </div>
        );
    };

    renderColumns = ( data ) => {
        return data.map( ( stat, j ) => {
            const statDate = moment( stat.date ).format( TIME_FORMAT.athlete_info_log_date );
            const stats = stat.gameLog.map( ( row, i ) =>
                <div className="game-log__cell" key={ i }>{ row[1] }</div>
            );

            return (
                <div className="game-log__column" key={ j }>
                    <div className="game-log__cell game-log__cell--caption">{ statDate }</div>
                    { stats }
                </div>
            );
        } );
    }

    render () {
        const { _t, data } = this.props;

        return (
            <div className="game-log">
                { data.length ? this.renderTitleColumn( _t, data ) : null }
                <div className="game-log__column-scroller">
                    { this.renderColumns( data ) }
                </div>
            </div>
        );
    }
}

export default GameLogTable;
