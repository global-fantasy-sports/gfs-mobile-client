import _ from 'lodash';
import moment from 'moment';
import React, { Component, PropTypes } from 'react';
import AutoSizer from 'react-virtualized/dist/commonjs/AutoSizer';
import { CartesianGrid, Legend, Line, LineChart, ReferenceLine, XAxis, YAxis } from 'recharts';
import { TIME_FORMAT } from '../../constants/app';


const formatDate = value => moment( value ).format( TIME_FORMAT.athlete_info_salary );
const formatPoints = value => value ? _.round( value, 1 ) : '0';
const formatSalary = value => {
    if ( value >= 1000 ) {
        return `$${_.round( value / 1000, 2 )}k`;
    } else if ( value >= 0 ) {
        return `$${_.round( value, 2 )}`;
    }
    return null;
};

function getChartDomains ( data ) {
    let pointsMin = 0;
    let pointsMax = 10;
    let salaryMin = 0;
    let salaryMax = 5000;

    data.forEach( ( { salary, fantasyPoints } ) => {
        pointsMin = Math.min( pointsMin, fantasyPoints );
        pointsMax = Math.max( pointsMax, fantasyPoints );
        salaryMin = Math.min( salaryMin, salary );
        salaryMax = Math.max( salaryMax, salary );
    } );

    pointsMin = Math.floor( pointsMin / 10 ) * 10;
    pointsMax = Math.ceil( pointsMax / 10 ) * 10;
    salaryMax = Math.ceil( salaryMax / 1000 ) * 1000;

    if ( pointsMin < 0 ) {
        salaryMin = -( _.ceil( salaryMax / ( pointsMax / Math.abs( pointsMin ) ), -3 ) );
    } else {
        pointsMin = 0;
    }

    return {
        salary: [salaryMin, salaryMax],
        points: [pointsMin, pointsMax],
    };
}

const CustomizedAxisTick = ( { x, y, payload } ) => {
    return (
        <g transform={ `translate(${x},${y})` } >
            <text
                dy={ 16 }
                fill="#666"
                fontFamily={ 'latoblack' }
                fontSize={ 11 }
                textAnchor="end"
                transform="rotate(-35)"
                x={ 0 }
                y={ 0 }
            >
                { formatDate( payload.value ) }
            </text>
        </g>
    );
};
CustomizedAxisTick.propTypes = {
    payload: PropTypes.object,
    x: PropTypes.number,
    y: PropTypes.number,
};


class SalaryFPPGStatsChart extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        data: PropTypes.arrayOf( PropTypes.shape( {
            match: PropTypes.object.isRequired,
            date: PropTypes.number.isRequired,
            fantasyPoints: PropTypes.number.isRequired,
            salary: PropTypes.number.isRequired,
        } ) ).isRequired,
    };

    constructor ( props ) {
        super( props );
        this.renderChart = ::this.renderChart;
    }

    renderChart ( { width, height } ) {
        const { _t, data } = this.props;
        let chartData = data;
        if ( data.length >= 12 ) {
            chartData = data.slice( -12 );
        }
        const domains = getChartDomains( chartData );
        return (
            <LineChart
                data={ chartData }
                height={ Math.max( height, 190 ) }
                margin={ { top: 35, right: 0, bottom: 30, left: 0 } }
                width={ width }
            >
                <XAxis
                    dataKey="date"
                    interval={ 0 }
                    padding={ { left: 20, right: 20 } }
                    stroke="#666"
                    strokeWidth={ 1 }
                    tick={ <CustomizedAxisTick /> }
                />
                <YAxis
                    domain={ domains.salary }
                    orientation="left"
                    stroke="#666"
                    strokeWidth={ 1 }
                    tick={ { fontSize: 11, fontFamily: 'latoblack' } }
                    tickFormatter={ formatSalary }
                    width={ 50 }
                    yAxisId="salary"
                />
                <YAxis
                    domain={ domains.points }
                    orientation="right"
                    stroke="#666"
                    strokeWidth={ 1 }
                    tick={ { fontSize: 11, fontFamily: 'latoblack' } }
                    tickFormatter={ formatPoints }
                    width={ 50 }
                    yAxisId="points"
                />
                <Legend
                    align="left"
                    height={ 20 }
                    iconSize={ 10 }
                    verticalAlign="top"
                    wrapperStyle={ {
                        color: '#666',
                        fontFamily: 'latoblack',
                        fontSize: 11,
                        paddingLeft: 50,
                        textTransform: 'uppercase',
                        top: 7,
                    } }
                />
                <CartesianGrid
                    stroke="#eee"
                    strokeDasharray="3 3"
                />
                <Line
                    dataKey="salary"
                    isAnimationActive={ false }
                    legendType="circle"
                    name={ _t( 'athlete_card.legend.salary' ) }
                    stroke="#2bcb8c"
                    strokeWidth={ 2 }
                    type="linear"
                    yAxisId="salary"
                />
                <Line
                    dataKey="fantasyPoints"
                    isAnimationActive={ false }
                    legendType="circle"
                    name={ _t( 'athlete_card.legend.fppg' ) }
                    stroke="#49547f"
                    strokeWidth={ 2 }
                    type="linear"
                    yAxisId="points"
                />
                <ReferenceLine
                    stroke="#cccccc"
                    y={ 0 }
                    yAxisId="points"
                />
            </LineChart>
        );
    }

    render () {
        return <AutoSizer>{ this.renderChart }</AutoSizer>;
    }
}

export default SalaryFPPGStatsChart;
