import _ from 'lodash';
import React from 'react';
import { storiesOf } from '@kadira/storybook';
import { withKnobs } from '@kadira/storybook-addon-knobs';
import SalaryFPPGChart from './SalaryFPPGChart';

const stories = storiesOf( 'SalaryFPPGChart', module );

stories.addDecorator( withKnobs );

const getData = () => [
    { salary: _.random( 29, 90 ) * 100, fantasyPoints: _.random( -5, 30 ), date: +new Date( 2016, 1, 1 ) },
    { salary: _.random( 29, 90 ) * 100, fantasyPoints: _.random( -5, 30 ), date: +new Date( 2016, 2, 2 ) },
    { salary: _.random( 29, 90 ) * 100, fantasyPoints: _.random( -5, 30 ), date: +new Date( 2016, 3, 3 ) },
    { salary: _.random( 29, 90 ) * 100, fantasyPoints: _.random( -5, 30 ), date: +new Date( 2016, 4, 4 ) },
    { salary: _.random( 29, 90 ) * 100, fantasyPoints: _.random( -5, 30 ), date: +new Date( 2016, 5, 5 ) },
    { salary: _.random( 29, 90 ) * 100, fantasyPoints: _.random( -5, 30 ), date: +new Date( 2016, 6, 6 ) },
    { salary: _.random( 29, 90 ) * 100, fantasyPoints: _.random( -5, 30 ), date: +new Date( 2016, 7, 7 ) },
];

stories.add( 'default', () => (
    <section style={ { backgroundColor: 'white', height: 200 } }>
        <SalaryFPPGChart
            data={ getData() }
        />
    </section>
) );
