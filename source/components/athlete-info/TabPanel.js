import React, { Children, Component, PropTypes } from 'react';
import { StickyContainer, Sticky } from 'react-sticky';
import cx from 'classnames';


export const Tab = ( { children, className, ...props } ) => (
    <article
        { ...props }
        className={ cx( 'TabContainer', className ) }
        label={ null }
    >
        { children }
    </article>
);

Tab.propTypes = {
    children: PropTypes.node.isRequired,
    className: PropTypes.string,
    label: PropTypes.oneOfType( [
        PropTypes.string,
        PropTypes.func,
        PropTypes.node,
    ] ).isRequired,
};


class TabButton extends Component {

    static propTypes = {
        label: PropTypes.oneOfType( [
            PropTypes.string,
            PropTypes.func,
            PropTypes.node,
        ] ).isRequired,
        onClick: PropTypes.func.isRequired,
        selected: PropTypes.bool.isRequired,
        value: PropTypes.any.isRequired,
    };

    constructor ( props ) {
        super( props );

        this.handleClick = ::this.handleClick;
    }

    handleClick ( e ) {
        e.preventDefault();
        this.props.onClick( this.props.value );
    }

    render () {
        const { label, selected } = this.props;
        return (
            <a
                className={ cx( 'TabBar__Tab', { selected } ) }
                href="#"
                onClick={ this.handleClick }
            >
                { typeof label === 'function' ? label() : label }
            </a>
        );
    }
}

class TabPanel extends Component {

    static propTypes = {
        children: PropTypes.node.isRequired,
        mod: PropTypes.string,
        scrollTop: PropTypes.bool,
        topStick: PropTypes.number,
    };

    constructor ( props ) {
        super( props );
        this.state = {
            activeIndex: 0,
        };

        this.handleTabClick = ::this.handleTabClick;
    }

    handleTabClick ( activeIndex ) {
        this.setState( { activeIndex } );
        if ( this.props.scrollTop ) {
            document.body.scrollTop = 0;
        }
    }

    render () {
        const mod = this.props.mod || '';
        const children = Children.toArray( this.props.children );
        const labels = children.map( tab => tab.props.label );
        const { activeIndex } = this.state;

        const topStick = this.props.topStick;

        const tabs = (
            <nav className="TabBar">
                { labels.map( ( label, index ) =>
                    <TabButton
                        key={ index }
                        label={ label }
                        onClick={ this.handleTabClick }
                        selected={ index === activeIndex }
                        value={ index }
                    />
                ) }
            </nav>
        );

        if ( !topStick ) {
            return (
                <section className={ 'TabPanel ' + mod }>
                    { tabs }
                    { children[activeIndex] }
                </section>
            );
        }

        return (
            <StickyContainer className={ 'TabPanel ' + mod }>
                <Sticky
                    stickyStyle={ { top: topStick + 'px', zIndex: 2 } }
                    topOffset={ -topStick }
                >
                    {tabs}
                </Sticky>
                { children[activeIndex] }
            </StickyContainer>
        );
    }
}

export default TabPanel;
