import React, { Component, PropTypes } from 'react';  // eslint-disable-line
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import { getChosenSport } from '../selectors/sport';
import { getChosenLeague } from '../selectors/league';
import UserActions from '../actions/user';
import NotificationActions from '../actions/notification';
import { getSessionId } from '../selectors/user';
import AppActions from '../actions/app';
import SessionActions from '../actions/session';

class AuthContainer extends Component {

    static propTypes = {
        children: PropTypes.object.isRequired,
        getUserInfo: PropTypes.func.isRequired,
        getNotifications: PropTypes.func.isRequired,
        preloadData: PropTypes.func.isRequired,
        requestSession: PropTypes.func.isRequired,
        session: PropTypes.string,
    };

    componentDidMount () {
        this.checkSession();
    }

    componentDidUpdate ( prevProps ) {
        if ( prevProps.session !== this.props.session ) {
            this.checkSession();
        }
    }

    checkSession () {
        if ( this.props.session ) {
            this.props.getUserInfo();
            this.props.getNotifications();
        } else {
            this.props.requestSession();
        }
    }

    render () {
        return this.props.children;
    }
}

const mapStateToPops = createStructuredSelector( {
    currentSport: getChosenSport,
    currentLeague: getChosenLeague,
    session: getSessionId,
} );

const mapDispatchToProps = {
    getUserInfo: UserActions.info,
    getNotifications: NotificationActions.recent,
    preloadData: AppActions.preload,
    requestSession: SessionActions.get,
};

export default connect(
    mapStateToPops,
    mapDispatchToProps,
)( AuthContainer );
