import React, { Component, PropTypes } from 'react';

import './Avatar.scss';

class Avatar extends Component {

    static propTypes = {
        name: PropTypes.string.isRequired,
        image: PropTypes.string,
        onClick: PropTypes.func,
    };

    handleClick ( event ) {
        const { onClick } = this.props;
        if ( onClick ) {
            event.preventDefault();
            this.props.onClick();
        }
    }

    render () {
        const { name, image } = this.props;
        return (
            <div className="Avatar" onTouchTap={ this.handleClick }>
                <img alt={ name } height={ 40 } src={ image } width={ 40 } />
                <span className="Avatar-Label">{ name.charAt( 0 ).toUpperCase() }</span>
            </div>
        );
    }
}

export default Avatar;
