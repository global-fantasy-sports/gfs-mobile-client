import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { withKnobs, text } from '@kadira/storybook-addon-knobs';
import Avatar from './Avatar';

const stories = storiesOf( 'Avatar', module );

stories.addDecorator( withKnobs );

stories.add( 'default', () => (
    <Avatar
        image={ text( 'image', 'https://api.adorable.io/avatars/50/piratus@adorable.io.png' ) }
        name={ text( 'name', 'User Name' ) }
        onClick={ action( 'click' ) }
    />
) );
