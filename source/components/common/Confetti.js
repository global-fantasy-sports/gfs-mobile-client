import _ from 'lodash';
import React, { Component } from 'react';
import cx from 'classnames';

import './_confetti.scss';

// BASED ON: https://codepen.io/salihDemircan/pen/JXBvYa

class Particle {
    constructor ( context, x, y ) {
        this.context = context;
        this.x = x;
        this.y = y;
        this.vx = _.random( -4, 4 );
        this.vy = _.random( -10, -0 );
        this.isCircle = _.sample( [true, false] );

        this.w = _.random( 10, 15 );
        this.h = _.random( 10, 15 );

        this.r = _.random( 5, 5 );

        this.angle = _.random( 0, 360 ) / 180 * Math.PI;
        this.anglespin = _.random( -0.2, 0.2 );

        this.color = _.sample( [
            '#f44336', '#e91e63', '#9c27b0', '#673ab7', '#3f51b5', '#2196f3',
            '#03a9f4', '#00bcd4', '#009688', '#4CAF50', '#8BC34A', '#CDDC39',
            '#FFEB3B', '#FFC107', '#FF9800', '#FF5722', '#795548',
        ] );

        this.rotateY = _.random( 0, 1 );
    }

    update () {
        this.x += this.vx;
        this.y += this.vy;
        this.vy += 0.05; // gravity
        this.vx += 0; // wind
        this.vx *= 0.99; // friction
        this.vy *= 0.99; // friction

        if ( this.rotateY < 1 ) {
            this.rotateY += 0.1;
        } else {
            this.rotateY = -1;

        }
        this.angle += this.anglespin;

        this.context.save();
        this.context.translate( this.x, this.y );
        this.context.rotate( this.angle );

        this.context.scale( 1, this.rotateY );

        this.context.rotate( this.angle );
        this.context.beginPath();
        this.context.fillStyle = this.color;
        this.context.strokeStyle = this.color;
        this.context.lineCap = 'round';
        this.context.lineWidth = 2;

        if ( this.isCircle ) {
            this.context.beginPath();
            this.context.arc( 0, 0, this.r, 0, 2 * Math.PI );
            this.context.fill();
        } else {
            this.context.fillRect( -this.w / 2, -this.h / 2, this.w, this.h );
        }

        this.context.closePath();
        this.context.restore();
    }
}

class Confetti extends Component {

    constructor () {
        super();
        this.particles = [];
    }

    generateParticles ( w, h ) {
        const context = this.confettiCanvas.getContext( '2d' );
        context.clearRect( 0, 0, w, h );

        const contextW = w;
        const contextH = h;
        const axisX = 0;
        const axisY = 0;
        const contextW2 = w;
        const contextH2 = 0;

        if ( this.particles.length < 100 ) {
            this.particles.push( new Particle(
                context,
                _.clamp( _.random( axisX, contextW2 + axisX ), axisX, contextW2 + axisX ),
                _.clamp( _.random( axisY, contextH2 + axisY ), axisY, contextH2 + axisY )
                )
            );
        }

        for ( let i = 0; i < this.particles.length; i++ ) {
            const p = this.particles[i];
            p.update();
            if ( p.y > contextH || p.y < -100 || p.x > contextW + 100 || p.x < -100 ) {
                // a brand new particle replacing the dead one
                this.particles[i] = new Particle(
                    context,
                    _.clamp( _.random( axisX, contextW2 + axisX ), axisX, contextW2 + axisX ),
                    _.clamp( _.random( axisY, contextH2 + axisY ), axisY, contextH2 + axisY )
                );
            }
        }
    }


    componentDidMount () {
        let startTime = null;
        const update = ( time ) => {
            if ( !startTime ) {
                startTime = time;
            }
            if ( this.confettiCanvas ) {
                this.confettiCanvas
                .getContext( '2d' )
                .clearRect( 0, 0, this.confettiCanvas.width, this.confettiCanvas.height );
                if ( time - startTime < 20000 ) {
                    this.generateParticles( this.confettiCanvas.width, this.confettiCanvas.height );
                    requestAnimationFrame( update );
                } else {
                    startTime = null;
                }
            }
        };
        requestAnimationFrame( update );
    }


    render () {
        return (
            <canvas
                className={ cx( 'confetti', { rules: false } ) }
                ref={ ( canvas ) => {
                    this.confettiCanvas = canvas;
                } }
            />
        );
    }
}

export default Confetti;
