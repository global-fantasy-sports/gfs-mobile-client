import React, { PropTypes } from 'react';
import cx from 'classnames';

import { format } from '../../utils/number';

import './_money.scss';

const Money = ( { className, currency, value, ...props } ) => (
    <span { ...props } className={ cx( 'Money', className, `currency--${currency}` ) }>
        { format( value, 0, '0' ) }
    </span>
);

Money.propTypes = {
    className: PropTypes.string,
    currency: PropTypes.oneOf( ['usd', 'token', 'bean'] ).isRequired,
    value: PropTypes.number.isRequired,
};

export default Money;
