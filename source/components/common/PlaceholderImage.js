import React, { Component, PropTypes } from 'react';
import cx from 'classnames';

const EMPTY = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';

class PlaceholderImage extends Component {

    static propTypes = {
        className: PropTypes.string,
        placeholder: PropTypes.string.isRequired,
        src: PropTypes.string,
    };

    constructor ( props ) {
        super( props );
        this.imageInstance = new Image();
        this.state = {
            displayedImage: EMPTY,
            opacity: false,
        };
    }

    componentDidMount () {
        this.setDisplayedImage( this.props.src );
    }

    componentWillReceiveProps ( { src } ) {
        if ( src !== this.props.src ) {
            this.setDisplayedImage( src );
        }
    }

    componentWillUnmount () {
        this.imageInstance.onerror = null;
        this.imageInstance.onload = null;
    }

    setDisplayedImage ( src ) {
        this.imageInstance.onerror = () => {
            this.setState( { displayedImage: this.props.placeholder, opacity: true } );
        };

        this.imageInstance.onload = () => {
            this.setState( { displayedImage: src } );
        };

        this.imageInstance.src = src;

        this.setState( { displayedImage: EMPTY } );
    }

    render () {
        const { displayedImage, opacity } = this.state;
        return (
            <img
                { ...this.props }
                className={ cx( this.props.className, { ghost: opacity } ) }
                placeholder={ null }
                src={ displayedImage }
            />
        );
    }
}

export default PlaceholderImage;
