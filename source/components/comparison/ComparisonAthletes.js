import React, { Component, PropTypes } from 'react';
import cx from 'classnames';

import Icon from '../../ui/icon/Icon';
import Btn from '../../ui/btn/Btn';
import BtnGroup from '../../ui/btn/BtnGroup';
import PlaceholderImage from '../../components/common/PlaceholderImage';
import playerPlaceholder from '../../images/room/player-placeholder.png';

import './style.scss';


class ComparisonAthlete extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        athlete: PropTypes.object,
        count: PropTypes.number.isRequired,
        drafted: PropTypes.bool.isRequired,
        mod: PropTypes.string.isRequired,
        onAddFavoriteAthlete: PropTypes.func.isRequired,
        onDeleteFavoriteAthlete: PropTypes.func.isRequired,
        onDraft: PropTypes.func.isRequired,
        onSelect: PropTypes.func.isRequired,
        onUndraft: PropTypes.func.isRequired,
        removeComparison: PropTypes.func.isRequired,
        selected: PropTypes.bool.isRequired,
        team: PropTypes.object,
    };

    handleDraft = () => {
        const participantId = this.props.athlete.currentParticipantId;
        const athleteId = this.props.athlete.athleteId;

        if ( this.props.drafted ) {
            this.props.onUndraft( { participantId, athleteId } );
        } else {
            this.props.onDraft( this.props.mod );
        }
    };

    handleFavoritize = () => {
        const { athleteId, isFavorite } = this.props.athlete;
        if ( isFavorite ) {
            this.props.onDeleteFavoriteAthlete( athleteId );
        } else {
            this.props.onAddFavoriteAthlete( athleteId );
        }
    };

    handleEmptyClick = () => {
        const searchBar = document.querySelector( '.page.comparison > .wizard-search-wrap' );
        const cards = document.querySelector( '.page.comparison .top .comparison-athletes' ).getBoundingClientRect();
        const scrollTo = searchBar.offsetTop - ( cards.top + cards.height + 10 );

        window.scrollTo( 0, scrollTo );
    };

    componentDidMount = () => {
        this.handleEmptyClick();
    }

    handleClose = ( event ) => {
        event.preventDefault();
        this.props.removeComparison( this.props.athlete.id );
    };

    handleSelect = ( event ) => {
        if ( event.isDefaultPrevented() ) {
            return;
        }
        this.props.onSelect( this.props.athlete.id );
    };


    render () {
        const {
            _t,
            athlete,
            count,
            drafted,
            mod,
            selected,
            team,
        } = this.props;
        const cs = cx( `${mod}-athlete`, { selected } );

        if ( !athlete || !athlete.id ) {
            return (
                <div className={ `${mod}-athlete empty` } onClick={ this.handleEmptyClick }>
                    <p className={ `select-athlete ${mod}` }>
                        { _t( 'comparison.athlete_card.empty_text' ) }
                    </p>
                </div>
            );
        }

        return (
            <div className={ cs } onClick={ this.handleSelect }>
                <BtnGroup>
                    <Btn
                        className="btn-fav"
                        icon={ athlete.isFavorite ? 'favorite-active' : 'favorite' }
                        onClick={ this.handleFavoritize }
                        type="white"
                    />
                    <Btn
                        disabled={ count >= 4 && !drafted }
                        onClick={ this.handleDraft }
                        type={ drafted ? 'warning' : 'payment' }
                    >
                        <div className="draft">
                            <Icon
                                icon={ drafted ? 'minus-circle' : 'plus-circle' }
                                mod={ count >= 4 && !drafted ? 'disabled' : '' }
                            />
                            { drafted ?
                                _t( 'comparison.athlete_card.btn_undraft' ) :
                                _t( 'comparison.athlete_card.btn_draft' )
                            }
                        </div>
                    </Btn>
                </BtnGroup>
                <div className="info">
                    { process.env.BUILD_TARGET !== 'og'
                    ? <img
                        alt={ `${ _t( team.name ) || ''}` }
                        className="team-logo"
                        src={ team.smallIcon }
                      />
                    : <div /> }
                    <p className="name">{ _t( athlete.name ) }</p>
                    <p className="budget">{ athlete.salary ? `$${athlete.salary}` : ''}</p>
                </div>
                <div className="photo">
                    <PlaceholderImage
                        alt={ _t( athlete.name ) }
                        className="player-photo"
                        height={ 100 }
                        placeholder={ playerPlaceholder }
                        src={ athlete.largePhoto }
                        width={ 140 }
                    />
                </div>
                {selected &&
                <i
                    className={ `icon-cross ${mod}` }
                    onClick={ this.handleClose }
                />
                }
                {drafted && <p className={ `in-draft ${mod}` }>
                    { _t( 'comparison.athlete_card.in_draft' ) }
                </p>}
            </div>
        );
    }
}


class ComparisonAthletes extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        leftAthlete: PropTypes.object.isRequired,
        leftAthleteTeam: PropTypes.object.isRequired,
        onAddFavoriteAthlete: PropTypes.func.isRequired,
        onDeleteFavoriteAthlete: PropTypes.func.isRequired,
        onDraft: PropTypes.func.isRequired,
        onSelect: PropTypes.func.isRequired,
        onUndraft: PropTypes.func.isRequired,
        removeComparison: PropTypes.func.isRequired,
        rightAthlete: PropTypes.object.isRequired,
        rightAthleteTeam: PropTypes.object.isRequired,
        selectedComparison: PropTypes.string,
        selectedIds: PropTypes.object.isRequired,
        selectedTeams: PropTypes.object,
    };

    render () {
        const {
            _t,
            leftAthlete,
            leftAthleteTeam,
            onAddFavoriteAthlete,
            onDeleteFavoriteAthlete,
            onDraft,
            onSelect,
            onUndraft,
            removeComparison,
            rightAthlete,
            rightAthleteTeam,
            selectedComparison,
            selectedIds,
            selectedTeams,
        } = this.props;
        const left = leftAthlete || {};
        const right = rightAthlete || {};

        const leftDrafted = selectedIds.has( ( left.id || 0 ).toString() )
                    || selectedIds.has( ( left.currentParticipantId || 0 ).toString() );
        const rightDrafted = selectedIds.has( ( right.id || 0 ).toString() )
                    || selectedIds.has( ( right.currentParticipantId || 0 ).toString() );

        return (
            <div className="comparison-athletes">
                <ComparisonAthlete
                    _t={ _t }
                    athlete={ leftAthlete }
                    count={ selectedTeams[left.teamId] || 0 }
                    drafted={ leftDrafted }
                    mod="left"
                    onAddFavoriteAthlete={ onAddFavoriteAthlete }
                    onDeleteFavoriteAthlete={ onDeleteFavoriteAthlete }
                    onDraft={ onDraft }
                    onSelect={ onSelect }
                    onUndraft={ onUndraft }
                    removeComparison={ removeComparison }
                    selected={ selectedComparison && selectedComparison === left.id }
                    team={ leftAthleteTeam }
                />
                <div className="vs-circle">
                    <div>{ _t( 'comparison.athletes.vs' ) }</div>
                </div>
                <ComparisonAthlete
                    _t={ _t }
                    athlete={ rightAthlete }
                    count={ selectedTeams[right.teamId] || 0 }
                    drafted={ rightDrafted }
                    mod="right"
                    onAddFavoriteAthlete={ onAddFavoriteAthlete }
                    onDeleteFavoriteAthlete={ onDeleteFavoriteAthlete }
                    onDraft={ onDraft }
                    onSelect={ onSelect }
                    onUndraft={ onUndraft }
                    removeComparison={ removeComparison }
                    selected={ selectedComparison && selectedComparison === right.id }
                    team={ rightAthleteTeam }
                />
            </div>
        );
    }
}

export default ComparisonAthletes;
