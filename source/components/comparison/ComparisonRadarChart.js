import React, { Component, PropTypes } from 'react';
import _ from 'lodash';
import { PolarAngleAxis, PolarRadiusAxis, PolarGrid, Radar, RadarChart } from 'recharts';
import { STATS_LABELS } from '../../constants/wizard';

import './style.scss';

const styles = {
    left: {
        animationDuration: 200,
        animationEasing: 'ease-in',
        dataKey: 'leftRatio',
        fill: '#a7b6d7',
        fillOpacity: 0.5,
        stroke: 'none',
        dot: {
            fill: '#9eb0d6',
            fillOpacity: 1,
            stroke: 'none',
        },
    },
    right: {
        animationDuration: 200,
        animationEasing: 'ease-in',
        dataKey: 'rightRatio',
        fill: '#37447e',
        fillOpacity: 0.7,
        stroke: 'none',
        dot: {
            fill: '#37447e',
            fillOpacity: 1,
            stroke: 'none',
        },
    },
};

function angleOffset ( angle, { x, y } ) {
    switch ( angle ) {
        case 30:
        case -30:
            return { x: x + 35, y };
        case -90:
            return { x, y: y + 12 };
        case -150:
        case -210:
            return { x: x - 35, y };
        default:
            return { x, y: y - 15 };
    }
}


const Tick = ( props ) => {
    const _t = props._t;
    const { angle, value } = props.payload;
    const labelText = STATS_LABELS[value.type] || value.type;
    const label = _t( `comparison.stats.label.${ labelText }` ).toUpperCase().split( ', ' );
    const unit = ( value.unit && value.unit !== 'pct' ) ? _t( `comparison.stats.unit.${ value.unit }` ) : null;

    return (
        <g
            fill="white"
            fontFamily="latoblack"
            fontSize="9"
            stroke="none"
            textAnchor="middle"
        >
            { label.map( ( text, index ) =>
                <text
                    dy={ 11 * index }
                    key={ index }
                    { ...angleOffset( angle, props ) }
                >
                    { text }
                </text>
            ) }
            { unit
            ? <text
                className="unit-labels"
                dy={ 11 * label.length }
                key={ label.length }
                { ...angleOffset( angle, props ) }
              >{ unit }</text>
            : null }
        </g>
    );
};

Tick.propTypes = {
    _t: PropTypes.func.isRequired,
    x: PropTypes.number,
    y: PropTypes.number,
    payload: PropTypes.object,
};

class ComparisonRadarChart extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        data: PropTypes.array.isRequired,
        leftId: PropTypes.string,
        rightId: PropTypes.string,
        selected: PropTypes.string.isRequired,
    };

    shouldComponentUpdate ( props ) {
        return !_.isEqual( props, this.props );
    }

    render () {
        const { _t, leftId, rightId, selected, data } = this.props;

        const [leftOptions, rightOptions] = leftId === selected
            ? [styles.right, styles.left]
            : [styles.left, styles.right];

        const width = window.innerWidth;
        const height = Math.ceil( width / 1.4 );

        return (
            <div className="radar-chart-wrap">
                <RadarChart
                    className="comparison-radar-chart"
                    data={ data }
                    height={ height }
                    innerRadius={ 0 }
                    margin={ { bottom: 10, left: 10, right: 10, top: 15 } }
                    width={ width }
                >
                    <Radar
                        { ...leftOptions }
                        hide={ !leftId }
                    />
                    <Radar
                        { ...rightOptions }
                        hide={ !rightId }
                    />
                    <PolarRadiusAxis
                        axisLine={ false }
                        domain={ [0, 1] }
                        tick={ false }
                    />
                    <PolarGrid
                        gridType="circle"
                        stroke="#ccc"
                        strokeOpacity={ 0.6 }
                    />
                    <PolarAngleAxis
                        axisLineType="circle"
                        dataKey="label"
                        tick={ <Tick _t={ _t } /> }
                        tickLine={ false }
                    />
                </RadarChart>
            </div>
        );
    }
}

export default ComparisonRadarChart;
