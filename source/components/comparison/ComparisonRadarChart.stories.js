import _ from 'lodash';
import React from 'react';
import { storiesOf } from '@kadira/storybook';
import { withKnobs, select } from '@kadira/storybook-addon-knobs';
import ComparisonRadarChart from './ComparisonRadarChart';
import { RADAR_CHART_STATS } from '../../constants/wizard';

const stories = storiesOf( 'ComparisonRadarChart', module );

stories.addDecorator( withKnobs );

const getData = ( position ) => RADAR_CHART_STATS[position].map( ( type ) => ( {
    type,
    left: _.random( 0, 100 ),
    right: _.random( 0, 100 ),
} ) );

Object.keys( RADAR_CHART_STATS ).forEach( ( position ) => {
    stories.add( position, () => (
        <ComparisonRadarChart
            data={ getData( position ) }
            leftId="1"
            rightId="2"
            selected={ select( 'selected', ['1', '2'], '1' ) }
        />
    ) );
} );
