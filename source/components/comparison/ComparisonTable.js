import cx from 'classnames';
import React, { PropTypes } from 'react';
import _ from 'lodash';

import { STATS_LABELS } from '../../constants/wizard';

function formatValue ( value, unit ) {
    if ( typeof value === 'number' ) {
        return unit === 'pct' ? `${_.round( value, 2 )} %` : _.round( value, 2 );
    }
    return '—';
}

const ComparisonTableRow = ( { _t, stat, left, right, unit } ) => (
    <div className="row">
        <span className={ cx( 'stats-num', { better: left > right } ) }>
            { formatValue( left, unit ) }
        </span>
        <span className="stats-name">
            { _t( `comparison.stats.label.${ ( STATS_LABELS[stat] || stat ) }` ).replace( ',', '' ) }&nbsp;
            <span className="stats-unit">{ unit !== 'pct' ? _t( `comparison.stats.unit.${unit}` ) : null }</span>
        </span>
        <span className={ cx( 'stats-num right', { better: right > left } ) }>
            { formatValue( right, unit ) }
        </span>
    </div>
);

ComparisonTableRow.propTypes = {
    _t: PropTypes.func.isRequired,
    left: PropTypes.number,
    right: PropTypes.number,
    stat: PropTypes.string.isRequired,
    unit: PropTypes.string.isRequired,
};

const ComparisonTable = ( { stats, _t } ) => (
    <div className="table">
        { stats.map( ( values, index ) =>
            <ComparisonTableRow
                _t={ _t }
                key={ index }
                left={ values.left }
                right={ values.right }
                stat={ values.type }
                unit={ values.unit }
            />,
        ) }
    </div>
);

ComparisonTable.propTypes = {
    _t: PropTypes.func.isRequired,
    stats: PropTypes.array.isRequired,
};

export default ComparisonTable;
