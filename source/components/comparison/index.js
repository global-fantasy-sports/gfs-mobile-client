import React, { Component, PropTypes } from 'react';
import Page from '../../components/page';
import ParticipantsList from '../../wizard/components/ParticipantsList';
import SearchComponent from '../../wizard/components/SearchComponent';
import ComparisonAthletes from './ComparisonAthletes';
import ComparisonRadarChart from './ComparisonRadarChart';
import ComparisonTable from './ComparisonTable';
import ScrollLoaderComponent from '../scroll-loader';
import { SORTING_OPTIONS } from '../../constants/wizard';
import Btn from '../../ui/btn/Btn';

import './style.scss';


class ComparisonComponent extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        addComparison: PropTypes.func.isRequired,
        athletes: PropTypes.array.isRequired,
        budget: PropTypes.number.isRequired,
        comparisonAthletes: PropTypes.shape( {
            left: PropTypes.object,
            right: PropTypes.object,
        } ).isRequired,
        currentSport: PropTypes.string.isRequired,
        favorites: PropTypes.array.isRequired,
        goPrevRouterStep: PropTypes.func.isRequired,
        goPrevStep: PropTypes.func.isRequired,
        isReady: PropTypes.bool.isRequired,
        leftAthleteTeam: PropTypes.object.isRequired,
        onParticipantClick: PropTypes.func.isRequired,
        onParticipantDraft: PropTypes.func.isRequired,
        onParticipantUndraft: PropTypes.func.isRequired,
        onAddFavoriteAthlete: PropTypes.func.isRequired,
        onDeleteFavoriteAthlete: PropTypes.func.isRequired,
        position: PropTypes.string.isRequired,
        removeComparison: PropTypes.func.isRequired,
        rightAthleteTeam: PropTypes.object.isRequired,
        selectComparisonAthlete: PropTypes.func.isRequired,
        selectedComparison: PropTypes.string.isRequired,
        selectedIds: PropTypes.object.isRequired,
        selectedTeams: PropTypes.object.isRequired,
        stats: PropTypes.array.isRequired,
        statsTable: PropTypes.array.isRequired,
    };

    constructor ( props ) {
        super( props );
        this.state = {
            search: '',
            showFavorite: false,
            sorting: 'salary desc',
        };
    }

    componentDidMount () {
        this.maybeRedirect();
    }

    componentDidUpdate () {
        this.maybeRedirect();
    }

    maybeRedirect () {
        const { comparisonAthletes, position } = this.props;
        if ( !comparisonAthletes.left && !comparisonAthletes.right && !position ) {
            this.props.goPrevRouterStep();
        }
    }

    handleFilterByFavorites = () => {
        this.setState( { showFavorite: !this.state.showFavorite } );
    };

    handleExit = () => {
        this.props.goPrevStep();
    };

    handleCompare = ( ...args ) => {
        window.scrollTo( 0, 0 );
        this.props.addComparison( ...args );
    };

    handleSearch = ( search ) => {
        this.setState( { search } );
    };

    handleSort = ( sorting ) => {
        this.setState( { sorting } );
    };

    render () {
        if ( !this.props.isReady ) {
            return <ScrollLoaderComponent />;
        }

        const {
            _t,
            athletes,
            comparisonAthletes: { left = {}, right = {} },
            budget,
            currentSport,
            favorites,
            leftAthleteTeam,
            onParticipantClick,
            onParticipantDraft,
            onParticipantUndraft,
            onAddFavoriteAthlete,
            onDeleteFavoriteAthlete,
            position,
            removeComparison,
            rightAthleteTeam,
            selectComparisonAthlete,
            selectedComparison,
            selectedIds,
            selectedTeams,
            stats,
            statsTable,
        } = this.props;

        const { search, showFavorite, sorting } = this.state;

        return (
            <Page className="comparison">
                <div className="top">
                    <div className="top-row">
                        <Btn
                            icon="arrow-left"
                            label={ _t( 'comparison.top.btn_exit' ) }
                            onClick={ this.handleExit }
                            type="transparent"
                        />
                        <h3 className="title">
                            <span>{ _t( 'comparison.title.comparing' ) }</span>
                            <span>{ _t( `comparison.title.position.${ position }` ) }</span>
                        </h3>
                    </div>
                    <ComparisonAthletes
                        _t={ _t }
                        draftedIds={ selectedIds }
                        leftAthlete={ left }
                        leftAthleteTeam={ leftAthleteTeam }
                        onAddFavoriteAthlete={ onAddFavoriteAthlete }
                        onDeleteFavoriteAthlete={ onDeleteFavoriteAthlete }
                        onDraft={ onParticipantDraft }
                        onSelect={ selectComparisonAthlete }
                        onUndraft={ onParticipantUndraft }
                        removeComparison={ removeComparison }
                        rightAthlete={ right }
                        rightAthleteTeam={ rightAthleteTeam }
                        selectedComparison={ selectedComparison }
                        selectedIds={ selectedIds }
                        selectedTeams={ selectedTeams }
                    />
                </div>
                <ComparisonRadarChart
                    _t={ _t }
                    data={ stats }
                    leftId={ left.athleteId }
                    rightId={ right.athleteId }
                    selected={ selectedComparison }
                />
                <ComparisonTable _t={ _t } stats={ statsTable } />
                <SearchComponent
                    _t={ _t }
                    favoritesOnly={ showFavorite }
                    onFilterFavorites={ this.handleFilterByFavorites }
                    onSearch={ this.handleSearch }
                    onSort={ this.handleSort }
                    position={ position }
                    search={ search }
                    sorting={ sorting }
                    sortingOptions={ SORTING_OPTIONS }
                    sport={ currentSport }
                />
                <ParticipantsList
                    _t={ _t }
                    budgetLeft={ budget }
                    comparison={ this.props.comparisonAthletes }
                    favorites={ favorites }
                    favoritesOnly={ showFavorite }
                    onAddFavoriteAthlete={ onAddFavoriteAthlete }
                    onClick={ onParticipantClick }
                    onCompare={ this.handleCompare }
                    onDeleteFavoriteAthlete={ onDeleteFavoriteAthlete }
                    onDraft={ onParticipantDraft }
                    onUncompare={ removeComparison }
                    onUndraft={ onParticipantUndraft }
                    participants={ athletes }
                    search={ search }
                    selectedIds={ selectedIds }
                    selectedTeams={ selectedTeams }
                    sorting={ sorting }
                />
            </Page>
        );
    }
}

export default ComparisonComponent;
