import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';
import SwipeableViews from 'react-swipeable-views';

import CompetitionRoom from '../competition-room';
import { COMPETITION_ROOM_GROUPS } from '../../constants/competition';
import './style.scss';

const CompetitionRoomExplanation = ( { _t, explanation, page, type } ) => {
    return (
        <section className={ classnames( 'competition-room-ns info-onboarding', page ) }>
            <div className="info">
                <h4 className="info-title">
                    { _t( `onboarding.room_type.title.${ explanation.title }` ) }
                </h4>
                <p>{ _t( `onboarding.room_type.text.${ explanation.text }` ) }</p>
                { type === '50/50' || type === 'Free-roll' ?
                    <p className="swipe">
                        <i className="left-arrow" />
                        <span>{ _t( 'onboarding.room_type.swipe' ) }</span>
                    </p>
                    : null
                }
            </div>
        </section>
    );
};
CompetitionRoomExplanation.propTypes = {
    _t: PropTypes.func,
    explanation: PropTypes.object,
    page: PropTypes.string,
    type: PropTypes.string,
};


class CompetitionRoomGroup extends Component {
    constructor ( props ) {
        super( props );

        this.state = {
            index: ( !props.explanation || props.showExplanation ? 0 : 1 ),
        };
    }

    renderViews = () => {
        const { _t, selected, rooms, onRoomClick, explanation, type, page = '' } = this.props;

        const selectable = Array.isArray( selected );
        let renderedRooms = rooms.map( iRoom =>
            <CompetitionRoom
                _t={ _t }
                key={ iRoom.id }
                onClick={ onRoomClick }
                page={ page || '' }
                room={ iRoom }
                selectable={ selectable }
                selected={ selectable && selected.includes( iRoom.id ) }
            />
        );

        if ( explanation ) {
            renderedRooms = [
                <CompetitionRoomExplanation
                    _t={ _t }
                    explanation={ explanation }
                    key={ 0 }
                    page={ page }
                    type={ type }
                />,
                renderedRooms,
            ];
        }

        if ( page === 'lobby' ) {
            return ( <SwipeableViews
                index={ this.state.index }
                style={ { padding: '0 14px' } }
                     >
                {renderedRooms}
            </SwipeableViews> );
        }

        return ( <div>
                {renderedRooms}
            </div> );

    }

    render () {
        const { _t, type, rooms, season, page = '' } = this.props;
        const color = COMPETITION_ROOM_GROUPS[type];

        if ( !rooms.length ) {
            return season && season.period === 'weekrun' ? null : (
                <section className={ `competition-room-group-ns ${ color } ${ page } empty` }>
                    <div className="group-header">
                        <div className="room-type">
                            { _t( `common.competition_type.${ type }` ) }
                        </div>
                    </div>
                </section>
            );
        }

        return (
            <section className={ `competition-room-group-ns ${ color } ${ page }` }>
                <div className="group-header">
                    <div className="room-type">
                        { _t( `common.competition_type.${ type }` ) }
                    </div>
                    <div className="room-counter">
                        { _t( 'competition_room_group.header.room', { count: rooms.length } ) }
                    </div>
                </div>
                {this.renderViews()}
            </section>
        );
    }
}

CompetitionRoomGroup.propTypes = {
    explanation: PropTypes.object,
    type: PropTypes.string.isRequired,
    rooms: PropTypes.array.isRequired,
    onRoomClick: PropTypes.func.isRequired,
    page: PropTypes.string,
    season: PropTypes.object,
    selected: PropTypes.array,
    showExplanation: PropTypes.bool,
    _t: PropTypes.func.isRequired,
};

export default CompetitionRoomGroup;
