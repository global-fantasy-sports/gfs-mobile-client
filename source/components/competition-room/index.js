import _ from 'lodash';
import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';

import Money from '../common/Money';
import { unixDateToLocal } from '../../utils/date';
import { TIME_FORMAT } from '../../constants/app';

import './style.scss';

const Entries = ( { _t, room: { gamesCount, maxEntries } } ) => {
    if ( gamesCount < maxEntries ) {
        return (
            <div className="circle">
                <span className="room-entries">{ gamesCount }</span>
                <span className="circle-slash">/</span>
                <span className="room-maxEntries">{ maxEntries }</span>
            </div>
        );
    }

    return (
        <div className="circle">
            <div className="full">{ _t( 'competition_room.entries.full' ) }</div>
        </div>
    );
};
Entries.propTypes = {
    _t: PropTypes.func.isRequired,
    room: PropTypes.object.isRequired,
};


const EntryFee = ( { _t, room, selected } ) => {
    if ( room.entryFee === 0 ) {
        return (
            <div className={ classnames( 'params-free', { selected } ) } >
                { _t( 'competition_room.label.free-entry' ) }
            </div>
        );
    }
    return (
        <div className="params-column">
            <label className="label">
                { _t( 'competition_room.label.entry' ) }
            </label>
            <Money
                className="value"
                currency={ room.currency }
                value={ room.entryFee }
            />
        </div>
    );
};
EntryFee.propTypes = {
    _t: PropTypes.func.isRequired,
    room: PropTypes.object.isRequired,
    selected: PropTypes.bool,
};


class CompetitionRoom extends Component {

    constructor ( props ) {
        super( props );
        this.handleClick = ::this.handleClick;
    }

    handleClick () {
        this.props.onClick( this.props.room.id );
    }

    render () {
        const { _t, page, room, selected } = this.props;
        const roomClass = classnames( 'competition-room-ns', page, room.prize_thumb ? '' : 'noprizebg' );
        const countPrizes = room ? _.size( room.prizeList ) : null;

        if ( selected ) {
            return (
                <section
                    className={ classnames( 'selected', roomClass ) }
                    onClick={ this.handleClick }
                >
                    <div className="params-row">
                        <div className="params-column">
                            <label className="label">
                                { _t( 'competition_room.label.prize' ) }
                            </label>
                            <Money
                                className="prize"
                                currency={ room.currency }
                                value={ room.pot }
                            />
                        </div>
                        <EntryFee _t={ _t } room={ room } selected={ selected } />
                        <div className="params-column">
                            <label className="label">
                                { _t( 'competition_room.label.stats' ) }
                            </label>
                            <div className="date-value">
                                { unixDateToLocal( room.startDate, TIME_FORMAT.competition_room_card ) }
                            </div>
                        </div>
                        <div className="params-colum">
                            <Entries _t={ _t } room={ room } />
                        </div>
                    </div>
                    <p className="selected-text">
                        { _t( 'competition_room.text_selected' ) }
                    </p>
                </section>
            );
        }

        return (
            <section
                className={ roomClass }
                onClick={ this.handleClick }
                style={ room.prize_thumb ? { backgroundImage: `url(${room.prize_thumb})` } : {} }
            >
                <div className="badge">
                    { _t( 'competition_room.label.prize' ) }
                    &nbsp;
                    <Money
                        currency={ room.currency }
                        value={ room.pot }
                    />
                </div>
                <div className="winners-row winners">
                    { _t( 'competition_room.winners', { count: countPrizes } ) }
                </div>
                <div className="params-row">
                    { room.prizeList[1] ?
                        <div className="params-column">
                            <label className="label">
                                { _t( 'competition.prize-list.place', { range: '1' } ) }
                            </label>
                            <Money
                                className="prize"
                                currency={ room.currency }
                                value={ room.prizeList[1] }
                            />
                        </div>
                    : null }
                    <EntryFee _t={ _t } room={ room } />
                    <div className="params-column">
                        <label className="label">
                            { _t( 'competition_room.label.stats' ) }
                        </label>
                        <div className="date-value">
                            { unixDateToLocal( room.startDate, TIME_FORMAT.competition_room_card ) }
                        </div>
                    </div>
                </div>
                <div className="entries-row">
                    <div className="entries with-prizes">
                        <label className="label">
                            { _t( 'competition_room.entries.label' ) }
                        </label>
                        <Entries _t={ _t } room={ room } />
                    </div>
                </div>
            </section>
        );
    }
}

CompetitionRoom.propTypes = {
    onClick: PropTypes.func.isRequired,
    page: PropTypes.string,
    room: PropTypes.object.isRequired,
    selectable: PropTypes.bool,
    selected: PropTypes.bool,
    _t: PropTypes.func,
};

export default CompetitionRoom;
