import React, { PropTypes } from 'react';

export const CompetitionRoomCounter = ( { _t, competitionState, time, className } ) => {
    let block;
    switch ( competitionState ) {
        case 'live':
            block = <p className="live-status">{ _t( 'competition.status.live' ) }</p>;
            break;
        case 'finished':
            block = <p className="finished">{ _t( 'competition.status.final' ) }</p>;
            break;
        case 'countdown':
            block = <p className="countdown">{ time }</p>;
            break;
        default:
            block = <p className="day-status">{ time }</p>;
    }
    return (
        <div className={ 'timer ' + ( className || '' ) } >{ block }</div>
    );
};
CompetitionRoomCounter.propTypes = {
    _t: PropTypes.func.isRequired,
    competitionState: PropTypes.string.isRequired,
    time: PropTypes.string,
    className: PropTypes.string,
};
