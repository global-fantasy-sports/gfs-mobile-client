import React, { Component, PropTypes } from 'react';

import playerPhoto from '../../images/player-placeholder-small.png';

import './style.scss';


class CompetitionRoomEntriesList extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        competition: PropTypes.object.isRequired,
        entriesCount: PropTypes.number,
        games: PropTypes.array.isRequired,
    };

    constructor ( props ) {
        super( props );
        this.renderUser = ::this.renderUser;
    }

    setImagePlaceholder ( iEvent ) {
        iEvent.target.src = playerPhoto;
    }

    renderUser ( iGame, iIndex ) {
        const user = iGame.userData;
        return (
            <div className={ `player-photo photo-${ iIndex + 1 }` } key={ iIndex }>
                <img
                    alt={ user.name }
                    onError={ this.setImagePlaceholder }
                    src={ user.avatar }
                />
            </div>
        );
    }

    renderEmptyUser ( iGamesList, iUserCountAvailable ) {
        if ( iGamesList.length <= 6 ) {
            return '';
        }
        return (
            <div className="player-photo photo-7 last" key={ -1 }>
                <img alt="Placeholder user photo" src={ playerPhoto } />
                <span>+{ iGamesList.length - iUserCountAvailable }</span>
            </div>
        );
    }

    renderEntriesCount ( iEntriesCount, iEntriesMax ) {
        if ( iEntriesCount === iEntriesMax ) {
            return (
                <p className="entries-full">{ this.props._t( 'competition.entries.full' ) }</p>
            );
        }

        return (
            <p className="entries-count">
                <span> { iEntriesCount }/ </span>
                <span> { iEntriesMax } </span>
            </p>
        );
    }

    render () {
        const { _t, competition, entriesCount, games } = this.props;
        if ( !( games && games.length ) ) {
            return (
            <div className="entries">
                <p className="entries-title">{ _t( 'competition.main-info.entries' ) }</p>
                <div className="entries-list flex">
                    <div className="player-photo empty" />
                </div>
                { this.renderEntriesCount( entriesCount, competition.maxEntries ) }
            </div>
            );
        }

        const userCountAvailable = 6;

        let userBlocks = games.map( this.renderUser );

        let entriesListStyle;
        if ( entriesCount === 1 ) {
            entriesListStyle = '25%';
        } else if ( userBlocks.length <= 7 ) {
            entriesListStyle = userBlocks.length * 17 + '%';
        } else {
            entriesListStyle = '100%';
        }

        return (
            <div className="entries">
                <p className="entries-title">{ _t( 'competition.main-info.entries' ) }</p>
                <div className="entries-list">
                    <div className="entries-list-wrap" style={ { width: entriesListStyle } }>
                        { userBlocks.slice( 0, userCountAvailable ) }
                        { this.renderEmptyUser( games, userCountAvailable ) }
                    </div>
                </div>
                { this.renderEntriesCount( entriesCount, competition.maxEntries ) }
            </div>
        );
    }
}

export default CompetitionRoomEntriesList;
