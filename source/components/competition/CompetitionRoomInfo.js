import React, { Component, PropTypes } from 'react';

import './style.scss';


const StatusBlock = ( { _t, isGameLive, isGameFinished, leftDays, smallStatusIcon } ) => {
    if ( isGameLive ) {
        return (
            <div>
                <p><i className={ smallStatusIcon() } /></p>
                <p className="icon-text">{ _t( 'competition.status.live' ) }</p>
            </div>
        );
    } else if ( isGameFinished ) {
        return (
            <div>
                <p><i className={ smallStatusIcon() } /></p>
                <p className="icon-text">{ _t( 'competition.status.final' ) }</p>
            </div>
        );
    }

    return (
        <div>
            <p><i className={ smallStatusIcon() } /><span> { leftDays }</span></p>
            <p className="icon-text">{ _t( 'competition.status.days_left' ) }</p>
        </div>
    );
};
StatusBlock.propTypes = {
    _t: PropTypes.func.isRequired,
    isGameFinished: PropTypes.bool.isRequired,
    isGameLive: PropTypes.bool.isRequired,
    leftDays: PropTypes.number.isRequired,
    smallStatusIcon: PropTypes.func.isRequired,
};


class CompetitionRoomInfo extends Component {

    static propTypes = {
        _t: PropTypes.func,
        competition: PropTypes.object.isRequired,
        isGameFarBefore: PropTypes.bool.isRequired,
        isGameFinished: PropTypes.bool.isRequired,
        isGameLive: PropTypes.bool.isRequired,
        isGameRightBefore: PropTypes.bool.isRequired,
        leftDays: PropTypes.number.isRequired,
        winnersCount: PropTypes.number.isRequired,
    };

    constructor ( props ) {
        super( props );
        this.smallStatusIcon = ::this.smallStatusIcon;
    }

    smallStatusIcon () {
        let icon;
        if ( this.props.isGameRightBefore || this.props.isGameFarBefore ) {
            icon = 'soon';
        } else if ( this.props.isGameLive ) {
            icon = 'live';
        } else if ( this.props.isGameFinished ) {
            icon = 'finished';
        }

        return `icon ${ icon }`;
    }

    render () {
        const { _t, competition, isGameLive, isGameFinished, leftDays, winnersCount } = this.props;
        return (
            <article className="room-info">
                <div>
                    <p><i className="icon player" /><span> { competition.maxEntries }</span></p>
                    <p className="icon-text">{ _t( 'competition.main-info.players' ) }</p>
                </div>
                <hr />
                <div>
                    <p><i className="icon winner" /><span> { winnersCount }</span></p>
                    <p className="icon-text">{ winnersCount > 1
                        ? _t( 'competition.main-info.winners' )
                        : _t( 'competition.main-info.winner' ) }
                    </p>
                </div>
                <hr />
                <StatusBlock
                    _t={ _t }
                    isGameFinished={ isGameFinished }
                    isGameLive={ isGameLive }
                    leftDays={ leftDays }
                    smallStatusIcon={ this.smallStatusIcon }
                />
            </article>
        );
    }
}

export default CompetitionRoomInfo;
