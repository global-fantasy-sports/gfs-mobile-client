import React, { Component, PropTypes } from 'react';
import classname from 'classnames';

import Money from '../common/Money';

import './style.scss';


const PrizeCell = ( { _t, competition, item } ) => (
    <div className="cell">
        <div className="cell-inner">
            <p>
                <Money
                    className={
                        classname( {
                            bean: competition.currency === 'bean',
                        } )
                    }
                    currency={ competition.currency }
                    value={ item[1] }
                />
            </p>
            <p className="prize-text">{ _t( 'competition.prize-list.place', { range: item[0] } ) }</p>
        </div>
    </div>
);
PrizeCell.propTypes = {
    _t: PropTypes.func.isRequired,
    competition: PropTypes.object.isRequired,
    item: PropTypes.array.isRequired,
};

class CompetitionRoomPrizeOverview extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        competition: PropTypes.object.isRequired,
        groupedPrizeList: PropTypes.array.isRequired,
    };

    constructor ( props ) {
        super( props );
    }

    render () {
        const { _t, competition, groupedPrizeList } = this.props;
        const firstThreePlace = groupedPrizeList.slice( 0, 3 );
        return (
            <article className="room-prize-overview">
                { firstThreePlace.map( ( item, index ) =>
                    <PrizeCell
                        _t={ _t }
                        competition={ competition }
                        item={ item }
                        key={ index }
                    />
                ) }
            </article>
        );
    }
}

export default CompetitionRoomPrizeOverview;
