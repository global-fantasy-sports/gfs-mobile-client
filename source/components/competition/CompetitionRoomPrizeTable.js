import React, { Component, PropTypes } from 'react';

import Money from '../common/Money';

import './style.scss';

class CompetitionRoomPrizeTable extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        competition: PropTypes.object.isRequired,
        entriesCount: PropTypes.number.isRequired,
        groupedPrizeList: PropTypes.array.isRequired,
    };

    constructor ( props ) {
        super( props );
    }

    renderPrizeList () {
        const { _t, competition, groupedPrizeList } = this.props;

        return groupedPrizeList.map( ( item, index ) => {
            return (
                <div className="row" key={ index }>
                    <p className="row__label">
                        { _t( 'competition.prize-list.place', { range: item[0] } ) }
                    </p>
                    <Money
                        className="row__value"
                        currency={ competition.currency }
                        value={ item[1] }
                    />
                </div>
            );
        } );
    }
    render () {
        const { _t } = this.props;
        return (
            <div className="table">
                <p className="prize-title">{ _t( 'competition.prize_distribution' ) }</p>
                <div>
                    { this.renderPrizeList() }
                </div>
            </div>
        );
    }
}

export default CompetitionRoomPrizeTable;
