import React, { Component, PropTypes } from 'react';
import { Sticky } from 'react-sticky';

import Btn from '../../ui/btn/Btn';

import { StatusIcon } from './StatusIcon';
import { GameStatusOrDateTime } from './GameStatusOrDateTime';

// League images
import leagueEPL from '../../images/room/league-epl.png';
import leagueCSL from '../../images/room/league-csl.png';

const LEAGUE_LOGO_TYPES = {
    epl: {
        type: 'EPL',
        url: leagueEPL,
        height: 145,
        width: 145,
    },
    csl: {
        type: 'SCL',
        url: leagueCSL,
        height: 100,
        style: {
            paddingBottom: 15,
            paddingTop: 30,
        },
        width: 210,
    },
};

import './style.scss';

const initialState = {
    isPlayButtonVisible: false,
};

class CompetitionRoomTop extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        competition: PropTypes.object.isRequired,
        competitionState: PropTypes.string.isRequired,
        disablePlayButton: PropTypes.bool.isRequired,
        finishDate: PropTypes.string.isRequired,
        games: PropTypes.array.isRequired,
        headerDate: PropTypes.string.isRequired,
        isParticipatingAvailable: PropTypes.bool,
        league: PropTypes.string.isRequired,
        onExit: PropTypes.func.isRequired,
        onPlay: PropTypes.func.isRequired,
        timerValue: PropTypes.string.isRequired,
        winnersCount: PropTypes.number,
    };

    constructor ( props ) {
        super( props );
        this.state = initialState;

        this.handleStickyStateChange = :: this.handleStickyStateChange;
        this.onExit = :: this.onExit;
    }

    handleStickyStateChange () {
        this.setState( { isPlayButtonVisible: !this.state.isPlayButtonVisible } );
    }

    onExit () {
        this.props.onExit();
    }

    render () {
        const {
            _t,
            competitionState,
            disablePlayButton,
            finishDate,
            isParticipatingAvailable,
            headerDate,
            league,
            onPlay,
            timerValue,
        } = this.props;

        return (
            <div className="status-bg">
                <div className="controls">
                    <Btn
                        icon="arrow-left"
                        label={ _t( 'competition.header.btn_back' ) }
                        onClick={ this.onExit }
                        type="transparent"
                    />
                    <StatusIcon
                        _t={ _t }
                        competitionState={ competitionState }
                        disablePlayButton={ disablePlayButton }
                        isParticipatingAvailable={ isParticipatingAvailable }
                        onPlay={ onPlay( 'Play (header button)' ) }
                        showPlayButton={ this.state.isPlayButtonVisible }
                    />
                </div>
                <img
                    alt={ LEAGUE_LOGO_TYPES[league].type }
                    className="img-league"
                    height={ LEAGUE_LOGO_TYPES[league].height }
                    src={ LEAGUE_LOGO_TYPES[league].url }
                    style={ LEAGUE_LOGO_TYPES[league].style }
                    width={ LEAGUE_LOGO_TYPES[league].width }
                />
                <div className="top">
                    <div className="room-play-btn-wrap">
                        { competitionState === 'before' || competitionState === 'countdown' ?
                            <Btn
                                disabled={ !isParticipatingAvailable || disablePlayButton }
                                label={ _t( 'competition.status.btn_play' ) }
                                onClick={ onPlay( 'Play (big button)' ) }
                                size="monster"
                                type="primary"
                            />
                            : null
                        }
                    </div>
                    <div className="game-status-wrap">
                        <Sticky
                            onStickyStateChange={ this.handleStickyStateChange }
                            stickyClassName="game-status-sticky"
                            stickyStyle={ { left: 0, top: '60px', width: '100%' } }
                            topOffset={ -120 }
                        >
                            <GameStatusOrDateTime
                                _t={ _t }
                                competitionState={ competitionState }
                                date={ headerDate }
                                finishDate={ finishDate }
                                isGameFinished={ competitionState === 'finished' }
                                isGameLive={ competitionState === 'live' }
                                isGameRightBefore={ competitionState === 'countdown' }
                                time={ timerValue }
                            />
                        </Sticky>
                    </div>
                </div>
            </div>
        );
    }
}

export default CompetitionRoomTop;
