import React, { PropTypes } from 'react';
import { CompetitionRoomCounter } from './CompetitionRoomCounter';

export const GameStatusOrDateTime = (
    {
        _t,
        competitionState,
        date,
        finishDate,
        time,
        isGameLive,
        isGameFinished,
        isGameRightBefore,
    } ) => {
    let block = ( <div className="day-status">
        <span className="day-status__text">{ date } - <nobr>{ finishDate }</nobr></span>
    </div> );
    if ( isGameRightBefore ) {
        block = ( <div className="before-status">
            <CompetitionRoomCounter _t={ _t } competitionState={ competitionState } time={ time } />
            <p className="before-status__text">{ _t( 'competition.status.till' ) } <nobr>{ finishDate }</nobr></p>
        </div> );
    }
    if ( isGameLive ) {
        block = ( <div className="live-status">
            <p className="live-status__badge">{ _t( 'competition.status.live' ) }</p>
            <p className="live-status__text">{ _t( 'competition.status.till' ) } <nobr>{ finishDate }</nobr></p>
        </div> );
    }
    if ( isGameFinished ) {
        block = ( <div className="finished-status">
            <p className="finished-status__badge">{ _t( 'competition.status.final' ) }</p>
            <p className="finished-status__text">{ date } - <nobr>{ finishDate }</nobr></p>
        </div> );
    }
    return block;
};
GameStatusOrDateTime.propTypes = {
    _t: PropTypes.func.isRequired,
    competitionState: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    finishDate: PropTypes.string.isRequired,
    isGameFinished: PropTypes.bool.isRequired,
    isGameLive: PropTypes.bool.isRequired,
    isGameRightBefore: PropTypes.bool.isRequired,
    time: PropTypes.string.isRequired,
};
