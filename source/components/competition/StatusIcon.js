import React, { PropTypes } from 'react';

import Btn from '../../ui/btn/Btn';


export const StatusIcon = ( {
        competitionState,
        disablePlayButton,
        isParticipatingAvailable,
        onPlay,
        showPlayButton,
        _t,
    } ) => {
    let block;
    switch ( competitionState ) {
        case 'live':
            block = <i className="status-icon live" />;
            break;
        case 'finished':
            block = <i className="status-icon finished" />;
            break;
        case 'countdown':
        default:
            if ( showPlayButton ) {
                block = (
                    <Btn
                        disabled={ !isParticipatingAvailable || disablePlayButton }
                        label={ _t( 'competition.status.btn_play' ) }
                        onClick={ onPlay }
                        size="normal"
                        type="primary"
                    />
                );
            } else {
                block = <div />;
            }
    }
    return (
        <div>{ block }</div>
    );
};
StatusIcon.propTypes = {
    _t: PropTypes.func.isRequired,
    competitionState: PropTypes.string.isRequired,
    disablePlayButton: PropTypes.bool.isRequired,
    isParticipatingAvailable: PropTypes.bool,
    onPlay: PropTypes.func.isRequired,
    showPlayButton: PropTypes.bool.isRequired,
};
