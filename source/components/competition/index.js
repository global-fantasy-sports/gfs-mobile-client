import React, { Component, PropTypes } from 'react';
import { StickyContainer } from 'react-sticky';
import classname from 'classnames';

import Page from '../page';
import GameCard from '../game-card';
import { unixDateToLocal } from '../../utils/date';
import Timer from '../../utils/timer';
import { getDescriptionBlock } from './types';
import ScrollLoaderComponent from '../scroll-loader';
import Money from '../common/Money';
import Btn from '../../ui/btn/Btn';

import CompetitionRoomTop from './CompetitionRoomTop';
import { CompetitionRoomCounter } from './CompetitionRoomCounter';
import CompetitionRoomEntriesList from './CompetitionRoomEntriesList';
import CompetitionRoomInfo from './CompetitionRoomInfo';
import CompetitionRoomPrizeTable from './CompetitionRoomPrizeTable';
import CompetitionRoomPrizeOverview from './CompetitionRoomPrizeOverview';
import PoweredBy from '../footer/PoweredBy';

import { TIME_FORMAT } from '../../constants/app';

import './style.scss';

const initialState = {
    countdown: {
        hours: 0,
        minutes: 0,
        seconds: 0,
    },
    isInfoPanelDisplayed: true,
    isInfoPanelInit: false,
    isPrizeTableDisplayed: false,
};

class CompetitionComponent extends Component {

    constructor ( props ) {
        super( props );

        this.competitionState = 'before';  // before -> countdown -> live -> finished
        this.timer = null;
        this.competitionUpdateRequestSent = false;
        this.competitionGamesUpdateRequestSent = false;

        this.state = initialState;

        this.timerUpdate = ::this.timerUpdate;
        this.toggleGamesPanel = ::this.toggleGamesPanel;
        this.isGameRightBefore = ::this.isGameRightBefore;
        this.isGameLive = ::this.isGameLive;
        this.isGameFinished = ::this.isGameFinished;
        this.togglePrizeTable = ::this.togglePrizeTable;
    }

    componentWillMount () {
        this.props.getParticipantResults();
        this.props.getResultsLegend();
    }

    componentWillUnmount () {
        if ( this.timer ) {
            this.timer.destroy();
        }
    }

    shouldComponentUpdate ( iNextProps ) {
        return iNextProps.dataLoaded;
    }

    componentWillUpdate ( iNextProps ) {
        if ( this.isPreparedPage( iNextProps ) && !this.state.isInfoPanelInit ) {
            if ( !iNextProps.games.length ) {
                return false;
            }
            const isOpenedPanel = !this.isCurrentUserParticipated( iNextProps.games, iNextProps.currentUserId );
            this.setState( { isInfoPanelDisplayed: isOpenedPanel, isInfoPanelInit: true } );
        }
        return true;
    }

    isPreparedPage ( iProps ) {
        return iProps.dataLoaded && iProps.competition && iProps.competition.id && iProps.games;
    }

    timerSubscribe ( iProps ) {
        if ( !this.timer && this.isGameRightBefore() ) {
            this.timer = new Timer( iProps.currentDate, iProps.localDate, iProps.competition.startDate );
            this.timer.subscribe( this.timerUpdate );
        }
    }

    timerUpdate ( iTime ) {
        this.setState( { countdown: iTime } );
    }

    getNumberOfWinners ( iCompetition ) {
        if ( !iCompetition.id ) {
            return 1;
        }
        return Object.keys( iCompetition.prizeList ).length;
    }

    isGameFinished () {
        return this.competitionState === 'finished';
    }

    isGameLive () {
        return this.competitionState === 'live';
    }

    isGameFarBefore () {
        return this.competitionState === 'before';
    }

    isGameRightBefore () {
        return this.competitionState === 'countdown';
    }

    getRoomEntries ( iCompetition, iGames ) {
        return iGames.length ? iGames.length : iCompetition.gamesCount;
    }

    getPrizeList ( iPrizeList ) {
        if ( !iPrizeList ) {
            return [];
        }
        return Object.values( iPrizeList ).map( iValue => iValue );
    }

    getDaysToStart ( iCompetitionStartDate, iCurrentDate ) {
        const startDate = unixDateToLocal( iCompetitionStartDate );
        const currentDate = unixDateToLocal( iCurrentDate );
        const days = startDate.diff( currentDate, 'days' );
        if ( isNaN( days ) ) {
            return null;
        }
        return days > 0 ? days : 0;
    }

    initCompetitionState ( iCompetition, iDays, iCurrentDate ) {
        if ( iDays > 0 || iDays === null ) {
            return 'before';
        }

        const startDate = unixDateToLocal( iCompetition.startDate );
        const endDate = unixDateToLocal( iCompetition.endDate );
        const currentDate = unixDateToLocal( iCurrentDate );

        if ( currentDate.isAfter( startDate ) && currentDate.isBefore( endDate ) ) {
            return 'live';
        }

        if ( iCompetition.finished ) {
            return 'finished';
        }

        if ( iDays === 0 ) {
            return 'countdown';
        }

        return this.competitionState;
    }

    toggleGamesPanel () {
        this.setState( { isInfoPanelDisplayed: !this.state.isInfoPanelDisplayed } );
    }

    togglePrizeTable () {
        this.setState( { isPrizeTableDisplayed: !this.state.isPrizeTableDisplayed } );
    }

    isCurrentUserParticipated ( iGames, iCurrentUserId ) {
        return iGames.some( iGame => iGame.userData.id === iCurrentUserId );
    }

    isParticipatingAvailable ( iCompetition, iGames ) {
        return ( this.isGameRightBefore() || this.isGameFarBefore() )
                && this.getRoomEntries( iCompetition, iGames ) < iCompetition.maxEntries;
    }

    prepareTimerValue () {
        const { countdown } = this.state;

        if ( !( this.isGameRightBefore() && this.timer && Object.keys( countdown ).length ) ) {
            return '';
        }

        return `${ countdown.hours } : ${ countdown.minutes } : ${ countdown.seconds }`;
    }

    renderGameRows ( iCompetition, iProps ) {
        let gameBlockList = [], index = 0, isShowInvitation;

        const { currentUserId, games, onPlay, playerPositions, playerPositionsNames } = iProps;
        for ( index = 0; index < iCompetition.maxEntries; index++ ) {
            isShowInvitation = ( games.length === index )
                && !this.isCurrentUserParticipated( games, currentUserId )
                && this.isParticipatingAvailable( iCompetition, games );
            let isUserGame = games[index] && games[index].userData.id === currentUserId;

            gameBlockList.push(
                <GameCard
                    _t={ iProps._t }
                    competition={ iCompetition }
                    game={ iProps.games[index] }
                    index={ index + 1 }
                    isShowInvitation={ isShowInvitation }
                    isUserGame={ isUserGame }
                    key={ index }
                    matches={ iProps.matches }
                    onPlay={ onPlay }
                    positionsList={ playerPositions }
                    positionsNamesList={ playerPositionsNames }
                    type="competition"
                />
            );
        }
        return ( gameBlockList );
    }

    renderTimerBeforeGameStart ( iTimerValue ) {
        if ( !this.isGameRightBefore() ) {
            return '';
        }

        return (
            <div className="counter">
                <p>{ this.props._t( 'competition.timer.game_start' ) }</p>
                <CompetitionRoomCounter
                    _t={ this.props._t }
                    className="big"
                    competitionState={ this.competitionState }
                    time={ iTimerValue }
                />
            </div>
        );

    }
    getGroupedPrizeList () {
        const { competition } = this.props;
        const prizeList = this.getPrizeList( competition.prizeList );
        const last = ( array ) => {
            if ( Array.isArray( array ) && array.length ) {
                return array[array.length - 1];
            }
            return null;
        };
        return prizeList
            .map( ( prize, index ) => ( { prize, place: index + 1 } ) )
            .reduce( ( agg, item ) => {
                let group = last( agg );
                if ( !group || last( group ).prize !== item.prize ) {
                    agg.push( [item] );
                } else {
                    group.push( item );
                }
                return agg;
            }, [] )
            .map( entry => ( entry.length === 1 )
                ? [`${entry[0].place}`, entry[0].prize]
                : [`${entry[0].place} — ${last( entry ).place}`, entry[0].prize]
            );
    }

    render () {
        if ( !this.isPreparedPage( this.props ) ) {
            return (
                <ScrollLoaderComponent />
            );
        }

        const { _t, currentDate, competition, disablePlayButton, games, league, onPlay, onExit } = this.props;

        const headerDate = unixDateToLocal( competition.startDate, TIME_FORMAT.competition_room_header );
        const finishDate = unixDateToLocal( competition.endDate, TIME_FORMAT.competition_room_header );
        const winnersCount = this.getNumberOfWinners( competition );
        const entriesCount = this.getRoomEntries( competition, games );
        const leftDays = this.getDaysToStart( competition.startDate, currentDate );
        const description = getDescriptionBlock( competition, winnersCount, _t );
        this.competitionState = this.initCompetitionState( competition, leftDays, currentDate );

        this.timerSubscribe( this.props );
        const timerValue = this.prepareTimerValue();

        return (
            <StickyContainer>
                <Page className="competition">
                    <CompetitionRoomTop
                        _t={ _t }
                        competition={ competition }
                        competitionState={ this.competitionState }
                        disablePlayButton={ disablePlayButton }
                        finishDate={ finishDate }
                        games={ games }
                        headerDate={ headerDate }
                        isParticipatingAvailable={ this.isParticipatingAvailable( competition, games ) }
                        league={ league }
                        onExit={ onExit }
                        onPlay={ onPlay }
                        timerValue={ timerValue }
                        winnersCount={ winnersCount }
                    />
                    <h1 className="room-type-title">
                        { _t( `common.competition_type.${ competition.type }` ) }
                        </h1>
                    <p className="room-type-subtitle">
                        { _t( 'competition.top.salary-cap_text' ) }&nbsp;
                        { winnersCount > 1
                            ? _t( 'competition.top.winners', { count: winnersCount } )
                            : _t( 'competition.top.winner', { count: winnersCount } )
                        }
                    </p>
                    <div className="room-entry-fee">
                        <p>{ _t( 'common.entry_fee' ) }</p>
                        <div className="money-wrap">
                            <Money
                                className={
                                    classname( {
                                        bean: competition.currency === 'bean',
                                    } )
                                }
                                currency={ competition.currency }
                                value={ competition.entryFee }
                            />
                        </div>
                    </div>
                    <CompetitionRoomPrizeOverview
                        _t={ _t }
                        competition={ competition }
                        groupedPrizeList={ this.getGroupedPrizeList() }
                    />
                    { this.state.isPrizeTableDisplayed ?
                        <CompetitionRoomPrizeTable
                            _t={ _t }
                            competition={ competition }
                            entriesCount={ entriesCount }
                            groupedPrizeList={ this.getGroupedPrizeList() }
                        />
                        : null
                    }
                    <div className="room-table-toggler">
                        <Btn
                            className="toggle-arrow-label"
                            icon={ this.state.isPrizeTableDisplayed ? 'arrow-center-top' : 'arrow-center-bottom' }
                            label={ _t( 'All Places' ) }
                            onClick={ this.togglePrizeTable }
                        />
                    </div>
                    <CompetitionRoomEntriesList
                        _t={ _t }
                        competition={ competition }
                        entriesCount={ entriesCount }
                        games={ games }
                    />
                    <CompetitionRoomInfo
                        _t={ _t }
                        competition={ competition }
                        isGameFarBefore={ this.isGameFarBefore() }
                        isGameFinished={ this.isGameFinished() }
                        isGameLive={ this.isGameLive() }
                        isGameRightBefore={ this.isGameRightBefore() }
                        leftDays={ leftDays }
                        winnersCount={ winnersCount }
                    />
                    <article className="information">
                        <h2>{ _t( 'competition.information.week' ) } { competition.week }</h2>
                        <h3>{ _t( 'competition.information.season' ) }</h3>
                        { this.renderTimerBeforeGameStart( timerValue ) }
                        <div className={ `table-wrapper ${ this.state.isInfoPanelDisplayed ? 'open' : '' }` }>
                            { description }
                            <p className="game-desc">
                                { _t( 'competition.description' ) }
                            </p>
                        </div>
                    </article>
                    <article className="games">
                        <Btn
                            icon={ this.state.isInfoPanelDisplayed ? 'arrow-center-top' : 'arrow-center-bottom' }
                            onClick={ this.toggleGamesPanel }
                        />
                        <div className="panel" >
                            { this.renderGameRows( competition, this.props ) }
                        </div>
                    </article>
                    <PoweredBy className="powered-by" />
                </Page>
            </StickyContainer>
        );
    }
}

CompetitionComponent.propTypes = {
    _t: PropTypes.func,
    competition: PropTypes.object.isRequired,
    competitionId: PropTypes.number.isRequired,
    currentDate: PropTypes.number,
    currentUserId: PropTypes.number,
    dataLoaded: PropTypes.bool.isRequired,
    disablePlayButton: PropTypes.bool.isRequired,
    games: PropTypes.array.isRequired,
    getCompetitionGames: PropTypes.func.isRequired,
    getParticipantResults: PropTypes.func.isRequired,
    getResultsLegend: PropTypes.func.isRequired,
    localDate: PropTypes.number,
    matches: PropTypes.object.isRequired,
    onExit: PropTypes.func.isRequired,
    onPlay: PropTypes.func.isRequired,
    playerPositions: PropTypes.array.isRequired,
    playerPositionsNames: PropTypes.array.isRequired,
    sport: PropTypes.string.isRequired,
    league: PropTypes.string.isRequired,
};

export default CompetitionComponent;
