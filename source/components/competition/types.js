import React from 'react';

const getTop15Block = ( iMaxEntries, iWinners, _t ) => {
    return (
        <div>
            <p className="text-head">
                { _t( 'competition.description.head.top_15', { iMaxEntries, iWinners } ) }
            </p>
            <p className="text-winner">
                { _t( 'competition.description.winner.top_15' ) }
            </p>
        </div>
    );
};

const getTop20Block = ( iMaxEntries, iWinners, _t ) => {
    return (
        <div>
            <p className="text-head">
                { _t( 'competition.description.head.top_20', { iMaxEntries, iWinners } ) }
            </p>
            <p className="text-winner">
                { _t( 'competition.description.winner.top_20' ) }
            </p>
        </div>
    );
};

const getH2HBlock = ( _t ) => {
    return (
        <div>
            <p className="text-head">
                { _t( 'competition.description.head.h2h' ) }
            </p>
            <p className="text-winner">
                { _t( 'competition.description.winner.h2h' ) }
            </p>
        </div>
    );
};

const get50Block = ( iMaxEntries, iWinners, _t ) => {
    return (
        <div>
            <p className="text-head">
                { _t( 'competition.description.head.50_50', { iMaxEntries, iWinners } ) }
            </p>
            <p className="text-winner">
                { _t( 'competition.description.winner.50_50' ) }
            </p>
        </div>
    );
};

const getDoableUpBlock = ( iMaxEntries, iWinners, _t ) => {
    return (
        <div>
            <p className="text-head">
                { _t( 'competition.description.head.double_up', { iMaxEntries, iWinners } ) }
            </p>
            <p className="text-winner">
                { _t( 'competition.description.winner.double_up' ) }
            </p>
        </div>
    );
};

const getQuadrupleBlock = ( iMaxEntries, iWinners, _t ) => {
    return (
        <div>
            <p className="text-head">
                { _t( 'competition.description.head.quadruple', { iMaxEntries, iWinners } ) }
            </p>
            <p className="text-winner">
                { _t( 'competition.description.winner.quadruple' ) }
            </p>
        </div>
    );
};

export const getDescriptionBlock = ( iCompetition, iWinnersNumber, _t ) => {
    switch ( iCompetition.type ) {
        case 'Top 20%':
            return getTop20Block( iCompetition.maxEntries, iWinnersNumber, _t );
        case 'Top 15%':
            return getTop15Block( iCompetition.maxEntries, iWinnersNumber, _t );
        case 'Head 2 head':
            return getH2HBlock( _t );
        case '50/50':
            return get50Block( iCompetition.maxEntries, iWinnersNumber, _t );
        case 'Double Up':
            return getDoableUpBlock( iCompetition.maxEntries, iWinnersNumber, _t );
        case 'Quadruple':
            return getQuadrupleBlock( iCompetition.maxEntries, iWinnersNumber, _t );
        default:
            return '';
    }
};

const currencyLabels = {
    usd: 'DOLLARS',
    token: 'TOKENS',
};

export const getGameTypes = ( competition ) => {
    let types = [];
    if ( competition.type ) {
        types.push( competition.type );
    }

    const currency = currencyLabels[competition.currency];
    if ( currency ) {
        types.push( currency );
    }

    return types;
};
