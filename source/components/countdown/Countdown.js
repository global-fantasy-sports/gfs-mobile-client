import React, { Component, PropTypes } from 'react';
import moment from 'moment';

import { numberWithPadding as pad } from '../../utils/number';
import './Countdown.scss';

const CountdownItem = ( { value, label, horizontal } ) => (
    horizontal ?
    <span className="num">{ pad( value ) }<sub>{ label }</sub></span>
    :
    <div className="CountdownItem">
        <div className="CountdownItem_value">{ pad( value ) }</div>
        <div className="CountdownItem_label">{ label }</div>
    </div>
);

CountdownItem.propTypes = {
    horizontal: PropTypes.bool,
    label: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired,
};

class Countdown extends Component {

    static propTypes = {
        from: PropTypes.number.isRequired,
        horizontal: PropTypes.bool,
        labels: PropTypes.shape( {
            day: PropTypes.string.isRequired,
            hour: PropTypes.string.isRequired,
            minute: PropTypes.string.isRequired,
            second: PropTypes.string.isRequired,
        } ).isRequired,
        to: PropTypes.number.isRequired,
    };

    state = {
        duration: 0,
    };

    componentDidMount () {
        this.setDuration( this.props );
        this.startTimer();
    }

    componentWillUnmount () {
        this.stopTimer( );
    }

    componentWillReceiveProps ( nextProps ) {
        const { from, to } = this.props;
        if ( from !== nextProps.from || to !== nextProps.to ) {
            this.setDuration( nextProps );
        }
    }

    setDuration ( { from, to } ) {
        let duration = 0;
        if ( from && to ) {
            duration = moment.duration( to - from ).valueOf();
        }
        this.setState( { duration: Math.max( duration, 0 ) } );
    }

    startTimer () {
        this._interval = setInterval( () => {
            const duration = Math.max( this.state.duration - 1000, 0 );
            this.setState( { duration } );
            if ( duration === 0 ) {
                clearInterval( this._interval );
            }
        }, 1000 );
    }

    stopTimer () {
        clearInterval( this._interval );
    }

    render () {
        const { horizontal, labels } = this.props;
        const d = moment.duration( this.state.duration );
        return (
            <p className={ horizontal ? 'clock' : 'Countdown' }>
                {d.get( 'd' )
                    ? <CountdownItem
                        horizontal={ horizontal }
                        label={ labels.day || 'Days' }
                        value={ d.get( 'd' ) }
                      />
                    : null
                }
                <CountdownItem
                    horizontal={ horizontal }
                    label={ labels.hour || 'Hours' }
                    value={ d.get( 'h' ) }
                />
                <CountdownItem
                    horizontal={ horizontal }
                    label={ labels.minute || 'Minutes' }
                    value={ d.get( 'm' ) }
                />
                {!d.get( 'd' )
                    ? <CountdownItem
                        horizontal={ horizontal }
                        label={ labels.second || 'Seconds' }
                        value={ d.get( 's' ) }
                      />
                    : null
                }
            </p>
        );
    }
}

export default Countdown;
