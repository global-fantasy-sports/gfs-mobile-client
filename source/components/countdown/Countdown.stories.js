import React from 'react';
import { storiesOf } from '@kadira/storybook';
import { withKnobs, date } from '@kadira/storybook-addon-knobs';
import Countdown from './Countdown';

const stories = storiesOf( 'Countdown', module );

stories.addDecorator( withKnobs );

let start = new Date();
let now = new Date( start - ( 1000 * 60 * 60 ) );

stories.add( 'with value', () => (
    <Countdown
        from={ +date( 'From', now ) }
        to={ +date( 'To', start ) }
    />
) );
