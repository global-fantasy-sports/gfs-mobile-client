import cx from 'classnames';
import React, { Component, PropTypes } from 'react';
import Btn from '../../ui/btn/Btn';
import RulesDialog from './rules/RulesDialog';
import { SCORING_RULES } from '../../constants/rules';

import './style.scss';

const ScoringRulesTable = ( { _t, rules } ) => (
    <div className="scoring-table">
        <div className="content">
            <div className="row titles">
                <div className="left-column">
                    <span>{ _t( `game-rules.scoring-rules-table.title.${ rules.positionTitle }` ) }</span>
                    <span>{ _t( `game-rules.scoring-rules-table.title.${ rules.labelTitle }` ) }</span>
                </div>
                <div className="right-column">
                    { _t( `game-rules.scoring-rules-table.title.${ rules.valueTitle }` ) }
                </div>
            </div>
            { rules.scores.map( ( [pos, category, points], index ) => (
                <div className="row" key={ index }>
                    <div className="left-column">
                        <span>{ _t( `game-rules.scoring-rules-table.pos-type_${ pos }` ) }</span>
                        <span>{ _t( `game-rules.scoring-rules-table.category_${ category }` ) }</span>
                    </div>
                    <div className="right-column">
                        { category === 'Minutes played' || category === 'Duel won (tackle, head or sprint)' ?
                            _t( `game-rules.scoring-rules-table.points_${ points }` )
                            :
                            points
                        }

                    </div>
                </div>
            ) ) }
        </div>
    </div>
);
ScoringRulesTable.propTypes = {
    _t: PropTypes.func.isRequired,
    rules: PropTypes.object.isRequired,
};


class Dialog extends Component {

    static propTypes = {
        _t: PropTypes.func,
        actionLabel: PropTypes.node,
        buttons: PropTypes.array,
        cancelLabel: PropTypes.node,
        chosenLeague: PropTypes.string,
        content: PropTypes.node,
        text: PropTypes.string,
        title: PropTypes.string,
        onHide: PropTypes.func.isRequired,
        okCancel: PropTypes.bool,
        onClick: PropTypes.func,
        onCancel: PropTypes.func,
        rules: PropTypes.bool,
        scoring: PropTypes.bool,
    };
    constructor () {
        super();
        this.state = {
            openedRulesItemId: 0,
        };
    }

    componentDidMount () {
        document.body.classList.toggle( 'no-scroll' );
    }

    componentWillUnmount () {
        document.body.classList.toggle( 'no-scroll' );
    }

    toggleRulesItem = ( itemId ) => {
        this.setState( { openedRulesItemId: this.state.openedRulesItemId === itemId ? 0 : itemId } );
    };

    renderBaseDialog () {
        return (
            <div>
                <header className="title">
                    { this.props.title }
                    <i
                        className="icon-cross"
                        onClick={ this.props.onHide }
                    />
                </header>
                <div className="content">
                    { this.props.text }
                </div>
                <div className="buttons">
                    { this.props.buttons.map( ( props, key ) =>
                        <button
                            className={ cx( 'btn btn-dialog', props.className ) }
                            key={ key }
                            onClick={ props.onClick }
                        >
                            { props.label }
                        </button>
                    ) }
                </div>
            </div>
        );
    }

    renderScoringDialog () {
        return (
            <article>
                <header className="title rules">
                    <Btn
                        label={ this.props._t( 'dialog_scoring.title.btn_exit' ) }
                        onClick={ this.props.onHide }
                    />
                    <h3>{ this.props._t( 'dialog_scoring.title.scoring_rules' ) }</h3>
                    <div />
                </header>
                <ScoringRulesTable _t={ this.props._t } rules={ SCORING_RULES[this.props.chosenLeague] } />
            </article>
        );
    }

    renderRulesDialog () {
        const { openedRulesItemId } = this.state;
        const { _t } = this.props;
        return (
            <RulesDialog
                _t={ _t }
                onHide={ this.props.onHide }
                openedRulesItemId={ openedRulesItemId }
                toggleRulesItem={ this.toggleRulesItem }
            />
        );
    }

    renderOkCancelDialog () {
        return (
            <div>
                <header className="title">
                    { this.props.title }
                    <i
                        className="icon-cross"
                        onClick={ this.props.onCancel }
                    />
                </header>
                <div className="content">
                    { this.props.text }
                </div>
                <div className="buttons">
                    <button
                        className="btn btn-dialog btn-dialog-blue"
                        onClick={ this.props.onCancel }
                    >
                        { this.props.cancelLabel }
                    </button>
                    <button
                        className="btn btn-dialog btn-dialog-red"
                        onClick={ this.props.onClick }
                    >
                        { this.props.actionLabel }
                    </button>
                </div>
            </div>
        );
    }

    render () {
        const { rules, scoring, okCancel } = this.props;

        let dialog = '';
        if ( okCancel ) {
            dialog = this.renderOkCancelDialog();
        } else if ( !rules && !scoring ) {
            dialog = this.renderBaseDialog();
        } else if ( scoring && !rules ) {
            dialog = this.renderScoringDialog();
        } else {
            dialog = this.renderRulesDialog();
        }

        return (
            <section className={ cx( 'dialog', { rules: rules || scoring } ) }>
                { dialog }
            </section>
        );
    }
}

export default Dialog;
