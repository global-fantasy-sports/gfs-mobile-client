import React, { Component } from 'react';
import { storiesOf } from '@kadira/storybook';
import { withKnobs } from '@kadira/storybook-addon-knobs';
import DialogWrapper from './DialogWrapper';

const stories = storiesOf( 'Dialog', module );

stories.addDecorator( withKnobs );

const buttons = [
    { label: 'Cancel', className: 'btn btn-dialog btn-dialog-blue', value: 'cancel' },
    { label: 'Delete', className: 'btn btn-dialog btn-dialog-red', value: 'delete' },
];

class DialogStory extends Component {

    state = { dialogActive: false };

    handleToggleDialog = ( event ) => {
        event.preventDefault();
        this.setState( { dialogActive: !this.state.dialogActive } );
    };

    render () {
        const { dialogActive } = this.state;
        return (
            <div>
                <p style={ { padding: 20 } }>
                    <button
                        className="btn btn-expand btn-blue-filter"
                        onClick={ this.handleToggleDialog }
                    >
                        { dialogActive ? 'HIDE DIALOG' : 'SHOW DIALOG' }
                    </button>
                </p>
                <DialogWrapper
                    buttons={ buttons }
                    onHide={ this.handleToggleDialog }
                    showDialog={ dialogActive }
                    text={ <p>Are you sure want to delete this line-up?</p> }
                    title="Delete line-up"
                />
            </div>
        );
    }
}

stories.add( 'default', () => (
    <DialogStory />
) );
