import cx from 'classnames';
import React, { PropTypes } from 'react';
import CSSTransitionGroup from 'react-addons-css-transition-group';

import Dialog from './Dialog';

const DialogWrapper = ( { showDialog, ...props } ) => (
    <CSSTransitionGroup
        component="div"
        transitionEnterTimeout={ 200 }
        transitionLeaveTimeout={ 200 }
        transitionName="dialog-overlay"
    >
        {showDialog ? <div className={ cx( 'dialog-wrapper', { active: showDialog } ) }>
            <Dialog { ...props } />
        </div> : null}
    </CSSTransitionGroup>
);

DialogWrapper.propTypes = {
    _t: PropTypes.func,
    buttons: PropTypes.array,
    onHide: PropTypes.func.isRequired,
    showDialog: PropTypes.bool.isRequired,
    text: PropTypes.string,
    title: PropTypes.string,
};

export default DialogWrapper;
