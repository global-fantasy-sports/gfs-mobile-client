import React, { PropTypes, Component } from 'react';
import cx from 'classnames';


class DialogRulesItem extends Component {

    handleToggle = () => {
        this.props.onToggle( this.props.itemId );
    };

    render () {
        const { open, table, title, description } = this.props;
        return (
            <section className="rules-item">
                <header
                    className={ cx( 'rules-title', { opened: open } ) }
                    onClick={ this.handleToggle }
                >
                    <h4> { title } </h4>
                    <i className="rules-arrow" />
                </header>
                <div
                    className={ cx( 'rules-block', { opened: open, table } ) }
                >
                    { description }
                </div>
            </section>
        );
    }
}

DialogRulesItem.propTypes = {
    itemId: PropTypes.number.isRequired,
    onToggle: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    table: PropTypes.bool,
    title: PropTypes.string,
    description: PropTypes.oneOfType( [
        React.PropTypes.node,
    ] ),
};


export default DialogRulesItem;
