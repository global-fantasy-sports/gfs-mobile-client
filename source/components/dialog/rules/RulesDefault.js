import React, { PropTypes } from 'react';
import { SCORING_SYSTEMS_NFL, SCORING_RULES } from '../../../constants/rules';


export const GlossaryFootball = ( { _t } ) => (
    <article>
        <section className="rules-text">
            <header><strong>Lobby</strong></header>
            <p>The first page of a specified sport that represent all the information
                for a specific sport, e.g. list of competition rooms, analytical tools, match schedule, etc.</p>
        </section>
        <section className="rules-text">
            <header><strong>Fantasy player</strong></header>
            <p>a user that plays fantasy games on the GFS platform.</p>
        </section>
        <section className="rules-text">
            <header><strong>Player</strong></header>
            <p>a real world sportsman / athlete.</p>
        </section>
        <section className="rules-text">
            <header><strong>Salary / Player's cost</strong></header>
            <p>the virtual cost of a player inside of the SALARY CAP game.</p>
        </section>
        <section className="rules-text">
            <header><strong>{ _t( 'game-rules.item-glossary-football.title.budget' ) }</strong></header>
            <p>{ _t( 'game-rules.item-glossary-football.budget_text' ) }</p>
        </section>
        <section className="rules-text">
            <header><strong>Line-Up</strong></header>
            <p>selected group of fantasy players expected to participate in a competition room.</p>
        </section>
        <section className="rules-text">
            <header><strong>Drafting</strong></header>
            <p>the process of spending the virtual budget to draft players to the line-up.</p>
        </section>
        <section className="rules-text">
            <header><strong>My Line-up</strong></header>
            <p>saved line-up to use for future competitions.</p>
        </section>
        <section className="rules-text">
            <header><strong>{ _t( 'game-rules.item-glossary-football.title.comp_room' ) }</strong></header>
            <p>{ _t( 'game-rules.item-glossary-football.comp_room_text' ) }</p>
        </section>
        <section className="rules-text">
            <header><strong>Entrance Fee</strong></header>
            <p>the amount of GOLDEN BEANS that user pays to enter a specific competition room.</p>
        </section>
        <section className="rules-text">
            <header><strong>GOLDEN BEANS</strong></header>
            <p>
                virtual game currency that can be used to enter a competition room.
                GOLDEN BEANS can be used to roll to win gifts in the STORE.
            </p>
        </section>
        <section className="rules-text">
            <header><strong>Store</strong></header>
            <p>in-game shop, where users can exchange GOLDEN BEANS
                for real items (e.g. iPad, smartphones, other equipment etc.).
                These can change price and availability at any time.
            </p>
        </section>
        <section className="rules-text">
            <header><strong>Prize Pool</strong></header>
            <p>the total amount of GOLDEN BEANS in a COMPETITION ROOM.</p>
        </section>
        <section className="rules-text">
            <header><strong>Prize Distribution</strong></header>
            <p>the rule or set of rules that defines how the prize pool will be shared amongst the winners.</p>
        </section>
        <section className="rules-text">
            <header><strong>My Games</strong></header>
            <p>The collection of all of a user's games on one easily accessible page.</p>
        </section>
        <section className="rules-text">
            <header><strong>{ _t( 'game-rules.item-glossary-football.title.fp' ) }</strong></header>
            <p>{ _t( 'game-rules.item-glossary-football.fp_text' ) }</p>
        </section>
        <section className="rules-text">
            <header><strong>{ _t( 'game-rules.item-glossary-football.title.fppg' ) }</strong></header>
            <p>{ _t( 'game-rules.item-glossary-football.fppg_text' ) }</p>
        </section>
        <section className="rules-text">
            <header><strong>{ _t( 'game-rules.item-glossary-football.title.oprk' ) }</strong></header>
            <p>{ _t( 'game-rules.item-glossary-football.oprk_text' ) }</p>
        </section>
        <section className="rules-text">
            <header><strong>Spider Chart</strong></header>
            <p>a chart where a user can, in a visual way, compare the performance of a player
                on various metrics.</p>
        </section>
        <section className="rules-text">
            <header><strong>Scoring System</strong></header>
            <p>the system for a particular sport that defines how the real performance of
                a player converts to FANTASY POINTS.</p>
        </section>
        <section className="rules-text">
            <header><strong>Games Pool</strong></header>
            <p>
                is the active matches being played a specific time period.
                Each competition room is tied to a Game Pool.
            </p>
        </section>
        <section className="rules-text">
            <header><strong>Pool of players</strong></header>
            <p>
                all athletes that are available for drafting as defined by the GAME POOL
                period available to a competition room.
            </p>
        </section>
        <section className="rules-text">
            <header><strong>Playing Period</strong></header>
            <p>period of time (day or week) for which competition rooms are created.</p>
        </section>
    </article>
);
GlossaryFootball.propTypes = {
    _t: PropTypes.func,
};

export const SalaryCapGameFormatFootball = ( { _t } ) => (
    <article>
        <section className="rules-text">{ _t( 'game-rules.item-salary-cap.text' ) }</section>
    </article>
);
SalaryCapGameFormatFootball.propTypes = {
    _t: PropTypes.func,
};

export const InGameCurrenciesFootball = () => (
    <article>
        <section className="rules-text">
            The platform uses GOLDEN BEANS as its currency systems. GOLDEN BEANS can be purchased for real money.
        </section>
        <section className="rules-text">
            Players in competition rooms compete for GOLDEN BEANS.
            GOLDEN BEANS can be used to pay entry fees. GOLDEN BEANS are used to purchase items from the STORE.
        </section>
    </article>
);

export const CompetitionRoomsRulesFootball = () => (
    <article>
        <section className="rules-text">
            All competition room entrances must be paid by an ENTRY FEE.
            GFS offers many different competition rooms where users can play against other users and win prizes.
            The competition room defines the PRIZE POOL structure, PRIZE DISTRIBUTION structure, the type of game, the
            size of the ENTRY FEE, and how many players can play in each competition room.
        </section>
        <section className="rules-text">
            <header><strong>4.1. Competition room creation and statuses</strong></header>
            <p>COMPETITION ROOMS will be created, no later than 24 hours after the last match from the previous
                PLAYING PERIOD.
            </p>
            <p>
                The PLAYING PERIOD  can be a week or a day. More information can be found in the section
                for each specific sport.
            </p>
            <p>
                COMPETITIONS ROOMS for the first playing period of the season for each individual sport will be
                created in a custom order defined by GFS.
            </p>
        </section>
        <section className="rules-text">
            <header><strong>4.2. Competition room status</strong></header>
            <p><strong>Soon</strong> - Indicates that a match is starting soon.
                Before the first match of a PLAYING PERIOD starts these competition
                rooms are still possible to enter and play within.
            </p>
            <p><strong>Live</strong> - Indicates that the first match of the COMPETITION ROOM has begun. These rooms
                are now unavailable for any further user entry.
            </p>
            <p><strong>Final</strong> - Once the last match of a game pool is over, an additional 24 hours will be
                reserved for changes to the result and winners receive their prizes.
                This is due to pending referee or data provider corrections.
            </p>
        </section>
        <section className="rules-text">
            <header><strong>4.3. Labels on competition rooms</strong></header>
            <p>The platform uses various labels to explain key criteria for a competition room.</p>
            <p>
                <strong>Beginner</strong> - Only for players who have played less than 50 games.
                This helps to separate new users from skilled fantasy users for the first 50 games.
            </p>
            <p>
                <strong>Guaranteed</strong> - No matter how many users enter a competition room,
                this competition will run and all winners will get a prize; unless real world events dictate otherwise.
            </p>
            <p>
                <strong>Featured</strong> - Exciting and popular competition rooms that are
                recommended by the platform to play in.
            </p>
        </section>
        <section className="rules-text">
            <header><strong>4.4. Competition rooms distribution based on difficulty</strong></header>
            <p>GFS provides two labels to describe the difficulty of a room.</p>
            <p><strong>Pro rooms</strong> - rooms with big prizes and fewer winners.</p>
            <p><strong>Casual rooms</strong> - rooms with smaller prizes shared between a larger group of winners.</p>
        </section>
        <section className="rules-text">
            <header><strong>4.5. Prize distribution types</strong></header>
            <p>There are two types of prize distribution: </p>
            <ul className="prize-distribution-list">
                <li>1) Equal prize distribution for all winners. Example -
                    you, as a user participate in a room with 5 winners
                    that share 50 GOLDEN BEANS PRIZE POOL. Each winner will get a
                    10 GOLDEN BEANS prize as they share the prize pool equally.
                </li>
                <li>2) Descending prize distribution based on user’s final result in a competition room (distribution
                    follows a descending curve). I.e. a user participates
                    in a competition room with a total 50 GOLDEN BEANS
                    PRIZE POOL with 5 winners. Prize distribution will be as in the table below
                    (with descending prize structure).
                </li>
            </ul>
            <div className="rules-prizes">
                <div>
                    <span>Place</span>
                    <span>Prize</span>
                </div>
                <div>
                    <span>1</span>
                    <span>15 Golden beans</span>
                </div>
                <div>
                    <span>2</span>
                    <span>12 Golden beans</span>
                </div>
                <div>
                    <span>3</span>
                    <span>10 Golden beans</span>
                </div>
                <div>
                    <span>4</span>
                    <span>7 Golden beans</span>
                </div>
                <div>
                    <span>5</span>
                    <span>6 Golden beans</span>
                </div>
            </div>
        </section>
        <section className="rules-text">
            <header><strong>4.6. Competition rooms types</strong></header>
            <p><strong>Multiplier rooms</strong> - For multiplier rooms the goal is to multiply the ENTRY FEE by a
                factor of e.g. 2x, 3x, 4x. E.g. a user has entered a Double Up competition room with
                an ENTRY FEE of 5 GOLDEN BEANS. All winners get the PRIZE of 5 multiplied by 3 = 15 GOLDEN BEANS.
            </p>
            <p><strong>Double Up</strong> - A winning fantasy player doubles (2x) their GOLDEN BEANS.</p>
            <p><strong>Triple Up</strong> - A winning fantasy player triples (3x) their GOLDEN BEANS.</p>
            <p><strong>Quadruple</strong> - A winning fantasy player quadruples (4x) their GOLDEN BEANS.</p>
            <p><strong>Top % - rooms</strong><br />
                These competition rooms award prizes to a fixed percentage of the best players.
                Prizes are distributed following a distribution curve (see below for more info).
            </p>
            <p><strong>50 / 50</strong> - Means that 50% of the users win.</p>
            <p><strong>Top 15</strong> - Means that top 15 % of the users win.</p>
            <p><strong>Top 20</strong> - Means that 20 % of the users win.</p>
            <p><strong>Head to Head (abbreviation H2H)</strong> - Fantasy player plays vs another Fantasy player
                and the winner takes all.</p>
        </section>
        <section className="rules-text">
            <header><strong>4.7. Additional competition rooms types</strong></header>
            <p><strong>Free roll</strong> - are free COMPETITION ROOMS (PLAYERs don’t pay a ENTRY FEE or
                in other words ENTRY FEE is equal to zero) but, winners with can win GOLDEN BEANS,
                tournament tickets, etc.
            </p>
            <p><strong>Qualifiers</strong> - the general idea of a qualifier is the user is trying to win their
                way into a big time game. So, for example, if there is a 150 GOLDEN BEANS buy-in daily fantasy game
                happening the following weekend, there may be a
                tournament that has just a 10 GOLDEN BEANS buy-in, and you’ll
                compete against the rest of the field for a spot in the action.
            </p>
            <p><strong>Satellites</strong> - are similar to qualifiers. They give the winner – or winners –
                entry into more expensive contests.
            </p>
        </section>
    </article>
);

export const RosterSizesFootball = () => (
    <article>
        <section className="rules-text">
            <p>The 9 line-up positions are:</p>
            <p>
                The 9 roster positions are: GK, DF1, DF2, DF3 MF1, MF2, MF3, FW1, FW2.<br />
                GK = Goalkeeper.<br />
                DF = Defender.<br />
                MF = Midfielder.<br />
                FW = forward.<br />
            </p>
        </section>
    </article>
);

export const ScoringSystemFootball = ( { league } ) => (
    <article>
        <section className="rules-text">
            <div className="scoring-system-table">
                <div className="scoring-content">
                    <div className="row big">
                        <span><strong>{ SCORING_RULES[league].titleLeft }</strong></span>
                        <span><strong>{ SCORING_RULES[league].titleCenter }</strong></span>
                        <span><strong>{ SCORING_RULES[league].titleRight }</strong></span>
                    </div>
                    { SCORING_RULES[league].scores.map( ( [leftCell, centerCell, rightCell], index ) => (
                        <ScoringSystemsRow
                            centerCell={ centerCell }
                            key={ index }
                            leftCell={ leftCell }
                            rightCell={ rightCell }
                        />
                    ) ) }
                </div>
            </div>
        </section>
    </article>
);

ScoringSystemFootball.propTypes = {
    league: PropTypes.string,
};

export const AdditionalFootball = () => (
    <article>
        <section className="rules-text">
            <header><strong>8.1. FPPG</strong></header>
            <p>Average Fantasy Points the athlete has scored for the current season (on a per game basis)</p>
        </section>
        <section className="rules-text">
            <header><strong>8.2. OPRK</strong></header>
            <p>
                The Opponent Rank (OPRK) is a calculation of how difficult the opponent team’s rank is versus that
                player. The lower the OPRK is, the more difficult it is for the selected player versus that team.
            </p>
        </section>
        <section className="rules-text">
            <header><strong>8.3. SPIDER CHART</strong></header>
            <p>
                It is possible to compare all players on their most important key skills. The visual comparison tool
                is a radar chart view that allows comparison of PLAYERS.
            </p>
        </section>
    </article>
);


export const Glossary = () => (
    <article>
        <section className="rules-text">
            <header><strong>Lobby</strong></header>
            <p>The first page of a specified sport that represent all the information
                for a specific sport, e.g. list of competition rooms, analytical tools, match schedule, etc.</p>
        </section>
        <section className="rules-text">
            <header><strong>Fantasy player</strong></header>
            <p>a user that plays fantasy games on the GFS platform.</p>
        </section>
        <section className="rules-text">
            <header><strong>Player</strong></header>
            <p>a real world sportsman / athlete.</p>
        </section>
        <section className="rules-text">
            <header><strong>Salary / Player's cost</strong></header>
            <p>the virtual cost of a player inside of the SALARY CAP game.</p>
        </section>
        <section className="rules-text">
            <header><strong>Salary cap / Budget</strong></header>
            <p>
                limitation of the virtual money budget available to a user from which to draft and buy all virtual
                fantasy players for the user’s line-up.
            </p>
        </section>
        <section className="rules-text">
            <header><strong>Line-Up</strong></header>
            <p>selected group of fantasy players expected to participate in a competition room.</p>
        </section>
        <section className="rules-text">
            <header><strong>Drafting</strong></header>
            <p>the process of spending the virtual budget to draft players to the line-up.</p>
        </section>
        <section className="rules-text">
            <header><strong>My Line-Up</strong></header>
            <p>saved line-up to use for future competitions.</p>
        </section>
        <section className="rules-text">
            <header><strong>Competition room</strong></header>
            <p>a contest in which users plays against each other in fantasy sport
                games and compete for prizes specific
                to the room. To enter the room, users should pay an ENTRANCE FEE.
            </p>
        </section>
        <section className="rules-text">
            <header><strong>Entrance Fee</strong></header>
            <p>the amount of money/tokens that user pays to enter a specific competition room.</p>
        </section>
        <section className="rules-text">
            <header><strong>Tokens</strong></header>
            <p>
                virtual in-game currency that  can be used to enter a competition room. Tokens can be given as a bonus,
                purchased for real money or won.
            </p>
        </section>
        <section className="rules-text">
            <header><strong>Gold</strong></header>
            <p>virtual in-game currency that users win. Can be used to purchase real items in the STORE.</p>
        </section>
        <section className="rules-text">
            <header><strong>Store</strong></header>
            <p>in-game shop, where users can exchange GOLD for real items (e.g. iPad, smartphones, other equipment etc).
                These can change price and availability at any time.</p>
        </section>
        <section className="rules-text">
            <header><strong>Prize Pool</strong></header>
            <p>the total amount of the prize(s) in a COMPETITION ROOM</p>
        </section>
        <section className="rules-text">
            <header><strong>Prize Distribution</strong></header>
            <p>the rule or set of rules that defines how the prize pool will be shared amongst the winners.</p>
        </section>
        <section className="rules-text">
            <header><strong>My Games</strong></header>
            <p>The collection of all of a user's games on one easily accessible page.</p>
        </section>
        <section className="rules-text">
            <header><strong>Fantasy Points (FP)</strong></header>
            <p>total points scored by the player according to the scoring system.</p>
        </section>
        <section className="rules-text">
            <header><strong>FPPG</strong></header>
            <p>fantasy points per game, the average of player’s fantasy points per
                game for the current sports season.</p>
        </section>
        <section className="rules-text">
            <header><strong>OPRK</strong></header>
            <p>Opponent rank versus player’s position. Displays how well an upcoming opponent performs against the
                player’s position. See analytics section for more explanations.</p>
        </section>
        <section className="rules-text">
            <header><strong>Spider Chart</strong></header>
            <p>a chart where a user can, in a visual way, compare the seasonal performance of a player
                on various metrics.</p>
        </section>
        <section className="rules-text">
            <header><strong>Scoring System</strong></header>
            <p>the system for a particular sport that defines how the real performance of
                a player converts to FANTASY POINTS.</p>
        </section>
        <section className="rules-text">
            <header><strong>Games Pool</strong></header>
            <p>is a set of games used in competition rooms; each competition room is tied to one Game Pool.
                Competition rooms tied to Game Pool can be created at different times. Therefore,
                game policies are based on the timing of a Game Pool becoming available,
                not by each individual competition room.</p>
        </section>
        <section className="rules-text">
            <header><strong>Pool of players</strong></header>
            <p>all athletes that are available for drafting during the current week for a selected competition room.
                NOTE - for rooms with Game Pool, the set of players will be different.</p>
        </section>
        <section className="rules-text">
            <header><strong>Playing Period</strong></header>
            <p>period of time (day or week) for which competition rooms are created.</p>
        </section>
    </article>
);

export const SalaryCapGameFormat = () => (
    <article>
        <section className="rules-text">
            In a SALARY CAP game, the fantasy player acts as a virtual team manager, complete with a virtual bank
            account from which to spend money to draft players. The match performances of these players translate
            into FANTASY POINTS. These points are awarded for the player’s activity on the field.
            (More information about scoring can be found in the SCORING SYSTEM section relevant to a specific sport.)
        </section>
        <section className="rules-text">
            Players' salaries rise and fall according to their daily/weekly performances, and the goal of the user,
            as the virtual team manager, is to build the strongest possible line-up without exhausting
            the BUDGET (also known as the SALARY CAP). GFS’ standard Salary Cap is 65,000 virtual USD.
        </section>
    </article>
);

export const InGameCurrenciesSystems = () => (
    <article>
        <section className="rules-text">
            GFS offers two types of in-game currency systems.
        </section>
        <section className="rules-text">
            <header><strong>11.1. Money, gold and tokens</strong><br /></header>
            <p>GFS offers two different types of ENTRY FEE payments: </p>
            <p> - play for real money or <br />
                - play for virtual currency (gold or tokens).</p>
        </section>
        <section className="rules-text">
            <header><strong>11.2. Gold and tokens</strong></header>
            <p>Players in competition rooms can win either money, gold or tokens.
                Tokens can be used to pay entry fees, but not gold. Gold can be used, however,
                to purchase items in a STORE.
            </p>
        </section>
    </article>
);

export const CompetitionRoomsRules = () => (
    <article>
        <section className="rules-text">
            All competition room entrances must be paid by an ENTRY FEE.
            GFS offers many different competition rooms where users can play against other users and win prizes.
            The competition room defines the PRIZE POOL structure, PRIZE DISTRIBUTION structure, the type of game, the
            size of the ENTRY FEE, and how many players can play in each competition room.
        </section>
        <section className="rules-text">
            <header><strong>12.1. Competition room creation and statuses</strong></header>
            <p>
                COMPETITION ROOMS will be created no later than 24 hours after the last match from the previous
                PLAYING PERIOD.
            </p>
            <p>
                The PLAYING PERIOD can be a week or a day. More information can be found in the section
                for each specific sport.
            </p>
            <p>
                COMPETITIONS ROOMS for the first playing period of the season for each individual sport will be
                created in a custom order defined by GFS (users will be notified via email).
            </p>
        </section>
        <section className="rules-text">
            <header><strong>12.2. Competition room status</strong></header>
            <p>
                <strong>Soon</strong> - Indicates that a match is starting soon.
                Before the first match of a PLAYING PERIOD starts these competition
                rooms are still possible to enter and play within.
            </p>
            <p><strong>Live</strong> - Indicates that the first match of the COMPETITION ROOM has begun. These rooms
                are now unavailable for any further user entry.</p>
            <p><strong>Final</strong> - Once the last match of a game pool is over, an additional 24 hours will be
                reserved for changes to the result and winners receive their prizes. This is due to pending referee or
                data provider corrections.
            </p>
        </section>
        <section className="rules-text">
            <header><strong>12.3. Labels on competition rooms</strong></header>
            <p>The platform uses various labels to explain key criteria for a competition room.</p>
            <p>
                <strong>Beginner</strong> - Only for players who have played less than 50 games.
                This helps to separate new users from skilled fantasy users for the first 50 games.
            </p>
            <p>
                <strong>Guaranteed</strong> - No matter how many users enter a competition room,
                this competition will run and all winners will get a prize; unless real world events dictate otherwise.
            </p>
            <p>
                <strong>Featured</strong> - Exciting and popular competition rooms that
                recommended by the platform to play in.
            </p>
        </section>
        <section className="rules-text">
            <header><strong>12.4. Competition rooms distribution based on difficulty</strong></header>
            <p>GFS provides two labels to describe the difficulty of a room.</p>
            <p><strong>Pro rooms</strong> - rooms with big prizes and fewer winners.</p>
            <p><strong>Casual rooms</strong> - rooms with smaller prizes shared between a larger group of winners.</p>
        </section>
        <section className="rules-text">
            <header><strong>12.5. Prize distribution types</strong></header>
            <p>There are two types of prize distribution: </p>
            <ul className="prize-distribution-list">
                <li>1) Equal prize distribution for all winners. Example -
                    you, as a user participate in a room with 5 winners
                    that share 50 USD PRIZE POOL. Each winner will get a
                    10 USD prize as they share the prize pool equally.
                </li>
                <li>2) Descending prize distribution based on user’s final result in a competition room (distribution
                    follows a descending curve). I.e a user participates in a competition room with a total 50 USD
                    PRIZE POOL with 5 winners. Prize distribution will be as in the table below
                    (with descending prize structure)
                </li>
            </ul>
            <div className="rules-prizes">
                <div>
                    <span>Place</span>
                    <span>Prize</span>
                </div>
                <div>
                    <span>1</span>
                    <span>$15</span>
                </div>
                <div>
                    <span>2</span>
                    <span>$12</span>
                </div>
                <div>
                    <span>3</span>
                    <span>$10</span>
                </div>
                <div>
                    <span>4</span>
                    <span>$7</span>
                </div>
                <div>
                    <span>5</span>
                    <span>$6</span>
                </div>
            </div>
        </section>
        <section className="rules-text">
            <header><strong>12.6. Competition rooms types</strong></header>
            <p><strong>Multiplier rooms</strong> - For multiplier rooms the goal is to multiply the ENTRY FEE by a
                factor of e.g. 2x, 3x, 4x. E.g. a user has entered a Double Up competition room with
                an ENTRY FEE of 5 USD. All winners get the PRIZE of 5 multiplied by 3 = 15 USD.
            </p>
            <p><strong>Double Up</strong> - A winning fantasy player doubles (2x) their money.</p>
            <p><strong>Triple Up</strong> - A winning fantasy player triples (3x) their money.</p>
            <p><strong>Quadruple</strong> - A winning fantasy player quadruples (4x) their money.</p>
            <p><strong>Top % - rooms</strong><br />
                These competition rooms award prizes to a fixed percentage of the best players.
                Prizes are distributed following a distribution curve (see below for more info).
            </p>
            <p><strong>50 / 50</strong> - Means that 50% of the users win.</p>
            <p><strong>Top 15</strong> - Means that top 15 % of the users win.</p>
            <p><strong>Top 20</strong> - Means that 20 % of the users win.</p>
            <p><strong>Head to Head (abbreviation H2H)</strong> - Fantasy player plays vs another Fantasy player
                and the winner takes all.</p>
        </section>
        <section className="rules-text">
            <header><strong>12.7. Additional competition rooms types</strong></header>
            <p><strong>Free roll</strong> - are free COMPETITION ROOMS (PLAYERs don’t pay a ENTRY FEE or
                in other words ENTRY FEE is equal to zero) but, winners with can win real money, virtual tokens,
                tournament tickets, etc.
            </p>
            <p><strong>Qualifiers</strong> - the general idea of a qualifier is the user is trying to win their
                way into a big time game. So, for example, if there is a $150 buy-in daily fantasy game
                happening the following weekend, there may be a tournament that has just a $10 buy-in, and you’ll
                compete against the rest of the field for a spot in the action.
            </p>
            <p><strong>Satellites</strong> - are similar to qualifiers. They give the winner – or winners –
                entry into more expensive contests.
            </p>
        </section>
    </article>
);

export const LineupDraftingRules = ( { _t } ) => (
    <article>
        <section className="rules-text">
            <ul>
                <li>{ _t( 'game-rules.item-lineup-rules.text_1' ) }</li>
                <li>{ _t( 'game-rules.item-lineup-rules.text_2' ) }</li>
                <li>{ _t( 'game-rules.item-lineup-rules.text_3' ) }</li>
                <li>{ _t( 'game-rules.item-lineup-rules.text_4' ) }</li>
                <li>{ _t( 'game-rules.item-lineup-rules.text_5' ) }</li>
                <li>{ _t( 'game-rules.item-lineup-rules.text_6' ) }</li>
            </ul>
        </section>
    </article>
);
LineupDraftingRules.propTypes = {
    _t: PropTypes.func,
};

export const RosterSizes = () => (
    <article>
        <section className="rules-text">
            <header><strong>14.1. NFL</strong></header>
            <p>
                The 9 roster positions are: QB, RB, RB, WR, WR, WR, TE, FLEX (RB/WR/TE), and DST.<br />
                QB = Quarterback.<br />
                RB = Running Back.<br />
                WR = Wide Receiver.<br />
                TE = Tight End.<br />
                FLEX = a flexible position that could be filled with a Running Back or Wide
                Receiver or Tight End.<br />
                DST = Defensive and Special teams.<br />
            </p>
        </section>
        <section className="rules-text">
            <header><strong>14.2. SOCCER</strong></header>
            <p>
                The 9 roster positions are: GK, DF1, DF2, DF3 MF1, MF2, MF3, FW1, FW2.<br />
                GK = Goalkeeper.<br />
                DF = Defender.<br />
                MF = Midfielder.<br />
                FW = forward.<br />
            </p>
        </section>
    </article>
);

const ScoringSystemsRow = ( { centerCell, leftCell, rightCell } ) => {
    if ( centerCell ) {
        return (
            <div className="row big">
                <span>{ leftCell }</span>
                <span>{ centerCell }</span>
                <span>{ rightCell }</span>
            </div>
        );
    }
    return (
        <div className="row">
            <span>{ leftCell }</span>
            <span>{ rightCell }</span>
        </div>
    );
};
ScoringSystemsRow.propTypes = {
    leftCell: PropTypes.string,
    centerCell: PropTypes.string,
    rightCell: PropTypes.string,
};
export const ScoringSystems = () => (
    <article>
        <section className="rules-text">
            <header><strong>15.1. NFL</strong></header>
            <div className="scoring-system-table">
                <div className="scoring-title"><strong>Offense</strong></div>
                <div className="scoring-content">
                    <div className="row">
                        <span><strong>{ SCORING_SYSTEMS_NFL.titleLeft }</strong></span>
                        <span><strong>{ SCORING_SYSTEMS_NFL.titleRight }</strong></span>
                    </div>
                    { SCORING_SYSTEMS_NFL.offense.map( ( [leftCell, rightCell], index ) => (
                        <ScoringSystemsRow
                            key={ index }
                            leftCell={ leftCell }
                            rightCell={ rightCell }
                        />
                    ) ) }
                </div>
            </div>
            <div className="scoring-system-table">
                <div className="scoring-title"><strong>Defense</strong></div>
                <div className="scoring-content">
                    <div className="row">
                        <span><strong>{ SCORING_SYSTEMS_NFL.titleLeft }</strong></span>
                        <span><strong>{ SCORING_SYSTEMS_NFL.titleRight }</strong></span>
                    </div>
                    { SCORING_SYSTEMS_NFL.defense.map( ( [leftCell, rightCell], index ) => (
                        <ScoringSystemsRow
                            key={ index }
                            leftCell={ leftCell }
                            rightCell={ rightCell }
                        />
                    ) ) }
                </div>
            </div>
        </section>
        <section className="rules-text">
            <p>Notes: For purposes of GFS defensive scoring, points allowed are calculated as:</p>
            <p>6 * (Rushing TD + Receiving TD + Own fumbles recovered for TD ) + 2 *
                (Two point conversions) + Extra points + 3 * (Field Goals)
            </p>
        </section>
        <section className="rules-text">
            <header><strong>15.2.1 Soccer EPL</strong></header>
            <div className="scoring-system-table">
                <div className="scoring-content">
                    <div className="row big">
                        <span><strong>{ SCORING_RULES.epl.titleLeft }</strong></span>
                        <span><strong>{ SCORING_RULES.epl.titleCenter }</strong></span>
                        <span><strong>{ SCORING_RULES.epl.titleRight }</strong></span>
                    </div>
                    { SCORING_RULES.epl.scores.map( ( [leftCell, centerCell, rightCell], index ) => (
                        <ScoringSystemsRow
                            centerCell={ centerCell }
                            key={ index }
                            leftCell={ leftCell }
                            rightCell={ rightCell }
                        />
                    ) ) }
                </div>
            </div>
        </section>
        <section className="rules-text">
            <header><strong>15.2.3 Soccer CSL</strong></header>
            <div className="scoring-system-table">
                <div className="scoring-content">
                    <div className="row big">
                        <span><strong>{ SCORING_RULES.csl.titleLeft }</strong></span>
                        <span><strong>{ SCORING_RULES.csl.titleCenter }</strong></span>
                        <span><strong>{ SCORING_RULES.csl.titleRight }</strong></span>
                    </div>
                    { SCORING_RULES.csl.scores.map( ( [leftCell, centerCell, rightCell], index ) => (
                        <ScoringSystemsRow
                            centerCell={ centerCell }
                            key={ index }
                            leftCell={ leftCell }
                            rightCell={ rightCell }
                        />
                    ) ) }
                </div>
            </div>
        </section>
    </article>
);

export const Additional = () => (
    <article>
        <section className="rules-text">
            <header><strong>16.1. FPPG</strong></header>
            <p>Average Fantasy Points the athlete has scored for the current season (on a per game basis).</p>
        </section>
        <section className="rules-text">
            <header><strong>16.2. OPRK</strong></header>
            <p>
                The Opponent Rank (OPRK) is a calculation of how difficult the opponent team’s rank is versus that
                player. The lower the OPRK is, the more difficult it is for the selected player versus that team.
            </p>
        </section>
        <section className="rules-text">
            <header><strong>16.3. SPIDER CHART</strong></header>
            <p>
                It is possible to compare all players on their most important key skills.
                The visual comparison tool is a radar chart view that allows comparison of PLAYERS.
            </p>
        </section>
    </article>
);
