import React from 'react';


const RulesDialog = ( props ) => {
    let Component;

    if ( process.env.BUILD_TARGET === 'og' ) {
        Component = require( './RulesDialogOg' ).default;
    } else {
        Component = require( './RulesDialogDefault' ).default;
    }

    return <Component { ...props } />;
};

export default RulesDialog;
