import React, { PropTypes } from 'react';
import DialogRulesItem from './DialogRulesItem';
import Btn from '../../../ui/btn/Btn';


import {
    AdditionalFootball,
    Additional,
    CompetitionRoomsRulesFootball,
    CompetitionRoomsRules,
    Glossary,
    GlossaryFootball,
    LineupDraftingRules,
    InGameCurrenciesFootball,
    InGameCurrenciesSystems,
    RosterSizesFootball,
    RosterSizes,
    SalaryCapGameFormatFootball,
    SalaryCapGameFormat,
    ScoringSystemFootball,
    ScoringSystems,
} from './RulesDefault';


const RulesDialogDefault = ( { _t, onHide, toggleRulesItem, openedRulesItemId } ) => {
    return (
        <main className="rules-table">
            <header className="title rules">
                <Btn
                    label={ _t( 'dialog_scoring.title.btn_exit' ) }
                    onClick={ onHide }
                />
                <h3>{ _t( 'game-rules.title.game_rules' ) }</h3>
                <div />
            </header>
            <div className="content rules">
                <h3 className="rules-article-title">
                    { _t( 'game-rules.football-salary.football_salary_cap' ) }
                </h3>
                <DialogRulesItem
                    description={ <GlossaryFootball _t={ _t } /> }
                    itemId={ 1 }
                    onToggle={ toggleRulesItem }
                    open={ openedRulesItemId === 1 }
                    title={ _t( 'game-rules.item-title.glossary_football' ) }
                />
                <DialogRulesItem
                    description={ <SalaryCapGameFormatFootball _t={ _t } /> }
                    itemId={ 2 }
                    onToggle={ toggleRulesItem }
                    open={ openedRulesItemId === 2 }
                    title={ _t( 'game-rules.item-title.salary_cap_game' ) }
                />
                <DialogRulesItem
                    description={ <InGameCurrenciesFootball /> }
                    itemId={ 3 }
                    onToggle={ toggleRulesItem }
                    open={ openedRulesItemId === 3 }
                    title="3. In-game currency"
                />
                <DialogRulesItem
                    description={ <CompetitionRoomsRulesFootball /> }
                    itemId={ 4 }
                    onToggle={ toggleRulesItem }
                    open={ openedRulesItemId === 4 }
                    title="4. Competition rooms"
                />
                <DialogRulesItem
                    description={ <LineupDraftingRules _t={ _t } /> }
                    itemId={ 5 }
                    onToggle={ toggleRulesItem }
                    open={ openedRulesItemId === 5 }
                    title={ _t( 'game-rules.item-title.lineup_rules' ) }
                />
                <DialogRulesItem
                    description={ <RosterSizesFootball /> }
                    itemId={ 6 }
                    onToggle={ toggleRulesItem }
                    open={ openedRulesItemId === 6 }
                    title="6. Football lineup size"
                />
                <DialogRulesItem
                    description={ <ScoringSystemFootball league="epl" /> }
                    itemId={ 7 }
                    onToggle={ toggleRulesItem }
                    open={ openedRulesItemId === 7 }
                    title="7.1 Football (EPL) fantasy points scoring system"
                />
                <DialogRulesItem
                    description={ <ScoringSystemFootball league="csl" /> }
                    itemId={ 8 }
                    onToggle={ toggleRulesItem }
                    open={ openedRulesItemId === 8 }
                    title="7.2 Football (CSL) fantasy points scoring system"
                />
                <DialogRulesItem
                    description={ <AdditionalFootball /> }
                    itemId={ 9 }
                    onToggle={ toggleRulesItem }
                    open={ openedRulesItemId === 9 }
                    title="8. Additional analytics tools"
                />


                <h3 className="rules-article-title">
                    Generic DFS salary cap game rules and analytical tools explanation
                </h3>
                <DialogRulesItem
                    description={ <Glossary /> }
                    itemId={ 10 }
                    onToggle={ toggleRulesItem }
                    open={ openedRulesItemId === 10 }
                    title="9. Generic Glossary"
                />
                <DialogRulesItem
                    description={ <SalaryCapGameFormat /> }
                    itemId={ 11 }
                    onToggle={ toggleRulesItem }
                    open={ openedRulesItemId === 11 }
                    title="10. Salary Cap Game Format"
                />
                <DialogRulesItem
                    description={ <InGameCurrenciesSystems /> }
                    itemId={ 12 }
                    onToggle={ toggleRulesItem }
                    open={ openedRulesItemId === 12 }
                    title="11. In-game currency systems"
                />
                <DialogRulesItem
                    description={ <CompetitionRoomsRules /> }
                    itemId={ 13 }
                    onToggle={ toggleRulesItem }
                    open={ openedRulesItemId === 13 }
                    title="12. Competition rooms"
                />
                <DialogRulesItem
                    description={ <LineupDraftingRules _t={ _t } /> }
                    itemId={ 14 }
                    onToggle={ toggleRulesItem }
                    open={ openedRulesItemId === 14 }
                    title="13. Lineup drafting rules and restrictions"
                />
                <DialogRulesItem
                    description={ <RosterSizes /> }
                    itemId={ 15 }
                    onToggle={ toggleRulesItem }
                    open={ openedRulesItemId === 15 }
                    title="14. Roster sizes for different sports"
                />
                <DialogRulesItem
                    description={ <ScoringSystems _t={ _t } /> }
                    itemId={ 16 }
                    onToggle={ toggleRulesItem }
                    open={ openedRulesItemId === 16 }
                    title="15. Scoring systems for different sports"
                />
                <DialogRulesItem
                    description={ <Additional /> }
                    itemId={ 17 }
                    onToggle={ toggleRulesItem }
                    open={ openedRulesItemId === 17 }
                    title="16. Additional analytics tools"
                />
            </div>
        </main>
    );
};

RulesDialogDefault.propTypes = {
    _t: PropTypes.func.isRequired,
    onHide: PropTypes.func.isRequired,
    toggleRulesItem: PropTypes.func.isRequired,
    openedRulesItemId: PropTypes.number,
};

export default RulesDialogDefault;
