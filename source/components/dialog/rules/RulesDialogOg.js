import React, { PropTypes } from 'react';
import DialogRulesItem from './DialogRulesItem';
import Btn from '../../../ui/btn/Btn';


import {
    GlossaryFootball,
    LineupDraftingRules,
    SalaryCapGameFormatFootball,
} from './RulesOg';


const RulesDialogOg = ( { _t, onHide, toggleRulesItem, openedRulesItemId } ) => {
    return (
        <main className="rules-table">
            <header className="title rules">
                <Btn
                    label={ _t( 'dialog_scoring.title.btn_exit' ) }
                    onClick={ onHide }
                />
                <h3>{ _t( 'game-rules.title.game_rules' ) }</h3>
                <div />
            </header>
            <div className="content rules">
                <h3 className="rules-article-title">
                    { _t( 'game-rules.football-salary.football_salary_cap' ) }
                </h3>
                <DialogRulesItem
                    description={ <GlossaryFootball _t={ _t } /> }
                    itemId={ 1 }
                    onToggle={ toggleRulesItem }
                    open={ openedRulesItemId === 1 }
                    title={ _t( 'game-rules.item-title.glossary_football' ) }
                />
                <DialogRulesItem
                    description={ <SalaryCapGameFormatFootball _t={ _t } /> }
                    itemId={ 2 }
                    onToggle={ toggleRulesItem }
                    open={ openedRulesItemId === 2 }
                    title={ _t( 'game-rules.item-title.salary_cap_game' ) }
                />
                <DialogRulesItem
                    description={ <LineupDraftingRules _t={ _t } /> }
                    itemId={ 3 }
                    onToggle={ toggleRulesItem }
                    open={ openedRulesItemId === 3 }
                    title={ _t( 'game-rules.item-title.lineup_rules' ) }
                />
            </div>
        </main>
    );
};

RulesDialogOg.propTypes = {
    _t: PropTypes.func.isRequired,
    onHide: PropTypes.func.isRequired,
    toggleRulesItem: PropTypes.func.isRequired,
    openedRulesItemId: PropTypes.number,
};


export default RulesDialogOg;
