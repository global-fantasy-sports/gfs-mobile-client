import React, { PropTypes } from 'react';


export const GlossaryFootball = ( { _t } ) => (
    <article>
        <section className="rules-text">
            <header><strong>{ _t( 'game-rules.item-glossary-football.title.budget' ) }</strong></header>
            <p>{ _t( 'game-rules.item-glossary-football.budget_text' ) }</p>
        </section>
        <section className="rules-text">
            <header><strong>{ _t( 'game-rules.item-glossary-football.title.comp_room' ) }</strong></header>
            <p>{ _t( 'game-rules.item-glossary-football.comp_room_text' ) }</p>
        </section>
        <section className="rules-text">
            <header><strong>{ _t( 'game-rules.item-glossary-football.title.fp' ) }</strong></header>
            <p>{ _t( 'game-rules.item-glossary-football.fp_text' ) }</p>
        </section>
        <section className="rules-text">
            <header><strong>{ _t( 'game-rules.item-glossary-football.title.fppg' ) }</strong></header>
            <p>{ _t( 'game-rules.item-glossary-football.fppg_text' ) }</p>
        </section>
        <section className="rules-text">
            <header><strong>{ _t( 'game-rules.item-glossary-football.title.oprk' ) }</strong></header>
            <p>{ _t( 'game-rules.item-glossary-football.oprk_text' ) }</p>
        </section>
    </article>
);
GlossaryFootball.propTypes = {
    _t: PropTypes.func,
};

export const SalaryCapGameFormatFootball = ( { _t } ) => (
    <article>
        <section className="rules-text">{ _t( 'game-rules.item-salary-cap.text' ) }</section>
    </article>
);
SalaryCapGameFormatFootball.propTypes = {
    _t: PropTypes.func,
};


export const LineupDraftingRules = ( { _t } ) => (
    <article>
        <section className="rules-text">
            <ul>
                <li>{ _t( 'game-rules.item-lineup-rules.text_1' ) }</li>
                <li>{ _t( 'game-rules.item-lineup-rules.text_2' ) }</li>
                <li>{ _t( 'game-rules.item-lineup-rules.text_3' ) }</li>
                <li>{ _t( 'game-rules.item-lineup-rules.text_4' ) }</li>
                <li>{ _t( 'game-rules.item-lineup-rules.text_5' ) }</li>
                <li>{ _t( 'game-rules.item-lineup-rules.text_6' ) }</li>
            </ul>
        </section>
    </article>
);
LineupDraftingRules.propTypes = {
    _t: PropTypes.func,
};
