import React, { Component, PropTypes } from 'react';
import iconUrl from './images/powered-by-gfs.png';


class PoweredBy extends Component {
    static propTypes = {
        className: PropTypes.string,
    }

    render () {
        const { className } = this.props;

        return ( <section className={ className }>
            <img src={ iconUrl } width={ 120 } />
        </section> );
    }
}

export default PoweredBy;
