import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

import RoutesConstants from '../../constants/routes';

import './style.scss';
import { preparePathWithLeague } from '../../utils/router';

class FooterComponent extends Component {

    static propTypes = {
        currentLocation: PropTypes.string.isRequired,
        sport: PropTypes.string.isRequired,
        league: PropTypes.string.isRequired,
        _t: PropTypes.func.isRequired,
    };

    render () {
        const { _t, sport, league } = this.props;
        const schedulePage = preparePathWithLeague( RoutesConstants.get( 'schedule' ) );
        const gamesPage = preparePathWithLeague( RoutesConstants.get( 'gamesPage' ) );
        const lobbyPage = preparePathWithLeague( RoutesConstants.get( 'lobbyPage' ) );
        const lineupsPage = preparePathWithLeague( RoutesConstants.get( 'lineups' ) );
        const selectorPage = `/${sport}/`;

        return (
            <footer className="footer">
                <Link
                    activeClassName="active"
                    to={ schedulePage }
                >
                    <i className="icon icon-schedule" />
                    <p>{ _t( 'footer.item.schedule' ) }</p>
                </Link>
                <Link
                    activeClassName="active"
                    to={ gamesPage }
                >
                    <i className="icon icon-games" />
                    <p>{ _t( 'footer.item.my_games' ) }</p>
                </Link>
                <Link
                    activeClassName="active"
                    to={ lobbyPage }
                >
                    <i className={ `icon icon-lobby-${this.props.sport}` } />
                    <p>{ _t( 'footer.item.lobby' ) }</p>
                </Link>
                <Link
                    activeClassName="active"
                    to={ lineupsPage }
                >
                    <i className="icon icon-my-lineups" />
                    <p>{ _t( 'footer.item.lineups' ) }</p>
                </Link>
                { process.env.WITH_SELECTOR === 'yes' ?
                    <Link
                        activeClassName="active"
                        to={ RoutesConstants.get( 'sportSelectPage' ) }
                    >
                        <i className="icon icon-sports" />
                        <p>{ _t( 'footer.item.sports' ) }</p>
                    </Link>
                : null }
                { process.env.WITH_SELECTOR_LEAGUES === 'yes' ?
                    <Link
                        activeClassName="active"
                        className="full-height"
                        to={ selectorPage }
                    >
                        <i className={ `icon icon-league-${ league }` } />
                    </Link>
                : null }
            </footer>
        );
    }
}

export default FooterComponent;
