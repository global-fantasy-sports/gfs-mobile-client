import React, { Component } from 'react';
import { Link } from 'react-router';

import Confetti from '../common/Confetti';
import Money from '../common/Money';
import RoutesConstants from '../../constants/routes';
import LineupPhotoBlock from '../lineups/LineupPhotoBlock';
import playerPlaceholder from '../../images/player-placeholder-small.png';
import { preparePathWithLeague } from '../../utils/router';


const initialState = {
    isClosedLineUp: true,
};

class GameCardCompetition extends Component {

    constructor () {
        super();
        this.state = initialState;

        this.toggleLineUp = ::this.toggleLineUp;
    }

    prepareLineupPhotos ( game, playerPositions, room, matches ) {
        if ( !game || !room ) {
            return Array( 9 ).fill( {} );
        }

        let playersPhoto = playerPositions
            .map( pos => {
                if ( !game.sportsmen[pos] ) {
                    return null;
                }
                let sportsman = { name: game.sportsmen[pos].name, thumb: game.sportsmen[pos].smallPhoto };

                if ( !room.finished
                   && ( !matches[game.sportsmen[pos].contestId]
                   || matches[game.sportsmen[pos].contestId].status === 'not_started' ) ) {
                    sportsman.points = null;
                    return sportsman;
                }

                if ( room.finished ) {
                    sportsman.points = room.participantResults[game.sportsmen[pos].athleteId]
                        ? room.participantResults[game.sportsmen[pos].athleteId].fantasyPoints
                        : 0;
                } else {
                    sportsman.points = game.sportsmen[pos].fantasyPoints;
                }
                return sportsman;
            } );

        if ( game.sportsmen.dst ) {
            playersPhoto = playersPhoto.splice( 0, playersPhoto.length - 1 );
            playersPhoto.push( game.sportsmen.dst.logo );
        }

        return playersPhoto;
    }


    toggleLineUp () {
        this.setState( { isClosedLineUp: !this.state.isClosedLineUp } );
    }

    setImagePlaceholder ( iPlaceholder, iEvent ) {
        iEvent.target.src = iPlaceholder;
    }

    renderEmptyCompetitionGames ( room, index ) {
        return room ? this.renderInvitation( index ) : this.renderEmptyRow( index );
    }

    isGameHasLineUp ( game ) {
        return Boolean( game.dst || game.gk );
    }

    renderGameWithEmptyLineUp ( index, game ) {
        const { _t } = this.props;
        return (
            <div
                className={ `lineup ${ this.state.isClosedLineUp ? 'closed' : '' }` }
                onClick={ this.toggleLineUp }
            >
                <article className="game filled">
                    <div className="info">
                        <div className="player">#<span className="number">{ index }</span></div>
                        <div className="photo">
                            <img
                                onError={ this.setImagePlaceholder.bind( this, playerPlaceholder ) }
                                src={ game.userData.avatar }
                            />
                        </div>
                        <div className="name">
                            <p>{ game.userData.name }</p>
                        </div>
                    </div>
                    <div className="data">
                        <p className="data-title" />
                        <p className="data-num green" />
                    </div>
                    <div className="data">
                        <p className="data-title">
                            { _t( 'games.card-competition.game.points' ) }
                        </p>
                        <p className="data-num points">0</p>
                    </div>
                </article>
                { this.renderEmptyLineup() }
            </div>
        );
    }

    renderPrizeBox ( isCompetition, iAmount, iCurrency ) {
        const { _t } = this.props;
        if ( !( isCompetition.finished && iAmount ) ) {
            return '';
        }

        return (
            <div className="data">
                <p className="data-title">
                    { _t( 'games.card-competition.game.prize.' ) }
                </p>
                <p className="data-num prize">
                    <Money currency={ iCurrency } value={ iAmount } />
                </p>
            </div>
        );
    }

    renderGameWithLineUp ( index, game, room, isCurrentUserGame ) {
        const { _t } = this.props;
        let gameBlockClass = room.started && !room.finished && isCurrentUserGame ? ' live' : '';
        gameBlockClass += room.finished ? ' finished' : '';
        gameBlockClass += game.prizeAmount ? ' winner' : '';
        gameBlockClass += game.imWinner ? ' my-winner' : '';
        const cs = `lineup ${ this.state.isClosedLineUp && !isCurrentUserGame ? 'closed' : '' } ${ gameBlockClass }`;
        const place = game.placeRange === '0' ? index : game.placeRange;

        const gameCardPage = preparePathWithLeague( `${ RoutesConstants.get( 'competitionPage' ) }${ room.id }/`
                     + `${ RoutesConstants.get( 'rosterPage' ) }${ game.id }/` );
        return (
            <div
                className={ cs }
                onClick={ this.toggleLineUp }
                style={ { position: 'relative' } }
            >
                <article className={ 'game filled' }>
                    <div className="info">
                        <div className="player">#<span className="number">{ place }</span></div>
                        <div className="photo">
                            <img
                                onError={ this.setImagePlaceholder.bind( this, playerPlaceholder ) }
                                src={ game.userData.avatar }
                            />
                        </div>
                        <div className="name">
                            <p>{ game.userData.name }</p>
                        </div>
                    </div>
                    { this.renderPrizeBox( room, game.prizeAmount, game.prizeCurrency ) }
                    <div className="data">
                        <p className="data-title">
                            { _t( 'games.card-competition.game.points' ) }
                        </p>
                        <p className="data-num points">{ game.points || 0 }</p>
                    </div>
                </article>
                <Link to={ gameCardPage } >
                    { this.renderLineup() }
                </Link>

                { game.imWinner && game.celebrateTime ? <Confetti /> : null }
            </div>
        );
    }

    renderEmptyLineup () {
        const { _t, game, positionsNamesList, playerPositions } = this.props;
        return (
            <div className="team-lineup-wrapper">
                <LineupPhotoBlock
                    _t={ _t }
                    photos={ this.prepareLineupPhotos( game, playerPositions ) }
                    positionsNamesList={ positionsNamesList }
                />
            </div>
        );
    }

    renderInvitation ( index ) {
        const { _t, onPlay } = this.props;
        return (
            <article className="game create-link" onClick={ onPlay( 'Create Game' ) }>
                <span className="player">#<span className="index">{ index }</span></span>
                <Link className="caption">
                    + { _t( 'games.card-competition.game.create_your' ) }
                </Link>
            </article>
        );
    }

    renderEmptyRow ( index ) {
        return (
            <article className="game">
                <div>
                    <span className="player">#<span className="index">{ index }</span></span>
                </div>
            </article>
        );
    }

    renderPositionsNames ( iPositionsNames ) {
        return (
                <div className="team-name">
                    { iPositionsNames.map( ( iPosition, index ) =>
                        <span key={ index }>{ iPosition }</span>
                    ) }
                </div>
        );
    }

    renderLineup () {
        const { _t, competition, game, matches, positionsNamesList, playerPositions } = this.props;

        return (
            <div className="team-lineup-wrapper">
                <LineupPhotoBlock
                    _t={ _t }
                    photos={ this.prepareLineupPhotos( game, playerPositions, competition, matches ) }
                    positionsNamesList={ positionsNamesList }
                    withPoints={ competition.started }
                />
            </div>
        );
    }

    render () {
        const { competition, index, game, isShowInvitation, isUserGame } = this.props;

        if ( !game ) {
            return this.renderEmptyCompetitionGames( isShowInvitation, index );
        }

        if ( this.isGameHasLineUp( game ) ) {
            return this.renderGameWithLineUp( index, game, competition, isUserGame );
        }

        return this.renderGameWithEmptyLineUp( index, game, competition, isUserGame );

    }
}

GameCardCompetition.propTypes = {
    _t: React.PropTypes.func,
    competition: React.PropTypes.object,
    game: React.PropTypes.any,
    index: React.PropTypes.number,
    isShowInvitation: React.PropTypes.bool.isRequired,
    isUserGame: React.PropTypes.bool,
    matches: React.PropTypes.object,
    onPlay: React.PropTypes.func.isRequired,
    playerPositions: React.PropTypes.array,
    positionsNamesList: React.PropTypes.array,
};

export default GameCardCompetition;
