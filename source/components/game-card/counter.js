import React, { Component, PropTypes } from 'react';

import Timer from '../../utils/timer';
import { unixDateToLocal } from '../../utils/date';
import { TIME_FORMAT } from '../../constants/app';


class Counter extends Component {

    static propTypes = {
        localTime: PropTypes.any.isRequired,
        range: PropTypes.array.isRequired,
        serverTime: PropTypes.any.isRequired,
        timestamp: PropTypes.any.isRequired,
    };

    constructor ( props ) {
        super( props );

        this.state = {
            timerStr: '',
            cs: '',
        };

        this.timer = undefined;
        this.createTimer( props );
    }

    componentDidMount () {
        this.createTimer( this.props );
    }

    componentDidUpdate () {
        this.createTimer( this.props );
    }

    componentWillUnmount () {
        if ( this.timer ) {
            this.timer.destroy();
        }
    }

    createTimer ( props ) {
        const { serverTime, localTime, timestamp } = props;
        if ( !this.timer && serverTime > 0 && localTime > 0 && timestamp > 0 ) {
            this.timer = new Timer( serverTime, localTime, timestamp, ::this.tick );
        }
    }

    tick ( { days, hours, minutes, seconds } ) {
        if ( +days > 0 ) {
            const { range } = this.props;
            this.setState( {
                timerStr: `${unixDateToLocal( range[0], TIME_FORMAT.counter )}`,
                cs: 'data-num time',
            } );
        } else {
            this.setState( {
                timerStr: `${hours}:${minutes}:${seconds}`,
                cs: 'data-num counter',
            } );
        }
    }

    render () {
        return (
            <span className={ this.state.cs }>
                {this.state.timerStr}
            </span>
        );
    }
}


export default Counter;
