import React, { Component, PropTypes } from 'react';

import GameCardCompetition from './competition';
import GameCardMyGame from './my-game';

class GameCard extends Component {

    static propTypes = {
        _t: PropTypes.func,
        competition: PropTypes.object,
        game: PropTypes.shape( {
            id: PropTypes.any.isRequired,
        } ),
        index: PropTypes.number,
        isShowInvitation: PropTypes.bool,
        isUserGame: PropTypes.bool,
        matches: PropTypes.object,
        onPlay: PropTypes.func,
        positionsList: PropTypes.array,
        positionsNamesList: PropTypes.array,
        soon: PropTypes.bool,
        sport: PropTypes.string,
        type: PropTypes.string.isRequired,
    };

    prizeClasses () {
        const { competition } = this.props;
        let entriesClass, entryFeeClass, potClass;

        entriesClass = entryFeeClass = potClass = 'data-num ';
        entriesClass += competition.maxEntries > 99 ? 'entries-small' : 'big';
        entryFeeClass += competition.maxEntries > 99 ? 'entry-fee-small' : 'big';
        potClass += competition.maxEntries > 99 ? 'prize-pool-small' : 'big';

        return { entriesClass, entryFeeClass, potClass };
    }

    render () {
        const { _t, competition, game, matches, onPlay, type } = this.props;

        if ( !competition ) {
            return <div>LOADING...</div>;
        }

        let block;
        switch ( type ) {
            case 'competition':
                block = (
                    <GameCardCompetition
                        _t={ _t }
                        competition={ competition }
                        game={ game }
                        index={ this.props.index }
                        isShowInvitation={ this.props.isShowInvitation }
                        isUserGame={ this.props.isUserGame }
                        matches={ matches }
                        onPlay={ onPlay }
                        playerPositions={ this.props.positionsList }
                        positionsNamesList={ this.props.positionsNamesList }
                    />
                );
                break;
            case 'games':
                block = (
                    <GameCardMyGame
                        _t={ _t }
                        game={ game }
                        index={ this.props.index }
                        playerPositions={ this.props.positionsList }
                        positionsNamesList={ this.props.positionsNamesList }
                        soon={ this.props.soon }
                        sport={ this.props.sport }
                    />
                );
                break;
            default:
                block = null;
        }

        return (
            <div className="game-card" >
                { block }
            </div>
        );
    }
}

export default GameCard;
