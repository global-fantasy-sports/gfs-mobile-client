import React, { Component } from 'react';
import { Link } from 'react-router';
import classnames from 'classnames';

import Confetti from '../common/Confetti';
import CounterContainer from '../../containers/CounterContainer';
import LineupPhotoBlock from '../lineups/LineupPhotoBlock';
import Money from '../common/Money';
import RoutesConstants from '../../constants/routes';
import ScrollLoaderComponent from '../scroll-loader';
import { preparePathWithLeague } from '../../utils/router';

const prepareLineupPhotos = ( game, playerPositions ) => {
    if ( !game ) {
        return '';
    }

    let playersPhoto = playerPositions.map( pos => game.sportsmen[pos]
                                            ? { thumb: game.sportsmen[pos].smallPhoto, name: game.sportsmen[pos].name }
                                            : null );
    if ( game.sportsmen.dst ) {
        playersPhoto = playersPhoto.splice( 0, playersPhoto.length - 1 );
        playersPhoto.push( game.sportsmen.dst.logo );
    }

    return playersPhoto;
};

class GameCardMyGame extends Component {

    renderFinishedGame ( game ) {
        return this.showMyGamesRow( game );
    }

    renderBeforeStartGame ( game ) {
        return this.showMyGamesRow( game );
    }

    renderStartedGame ( game ) {
        return this.showMyGamesRow( game );
    }

    renderOngoingGame ( game ) {
        return this.showMyGamesRow( game );
    }

    renderTitle ( room ) {
        const { _t, game } = this.props;
        if ( !room.type ) {
            return (
                <ScrollLoaderComponent />
            );
        }
        return (
            <article className="titles">
                <div className="lineup-item__name">
                    { _t( `common.competition_type.${ room.type }` ) }
                </div>
                <div className="lineup-item__subname">
                    { _t( 'games.game_card.info.salary_cap' ) }
                </div>
                { game.isNew ? <i className="lineup-item__new-game-badge">
                    { _t( 'games.game_card.info.new-game-badge' ) }
                </i> : null }
            </article>
        );
    }

    renderPoints ( room, game ) {
        const placeCs = classnames( 'data__num', {
            red: room.started && !room.finished,
            green: room.finished,
        } );
        const { _t } = this.props;

        return (
            <article className="points">
                <div className="data">
                    <div className="data__title">
                        { _t( 'games.game_card.info.points' ) }
                    </div>
                    <div className="data__num">{ game.points || 0 }</div>
                </div>
                <div className="data">
                    <div className="data__title">
                        { _t( 'games.game_card.info.place' ) }
                    </div>
                    <div className={ placeCs }>
                        { game.place }
                    </div>
                </div>
            </article>
        );
    }

    prizeClasses ( room ) {
        let entriesClass, entryFeeClass, potClass;

        entriesClass = entryFeeClass = potClass = 'data__num ';
        entriesClass += room.maxEntries > 99 ? 'entries-small' : 'big';
        entryFeeClass += room.maxEntries > 99 ? 'entry-fee-small' : 'big';
        potClass += room.maxEntries > 99 ? 'prize-pool-small' : 'big';

        return { entriesClass, entryFeeClass, potClass };
    }

    renderTimer () {
        const { competition } = this.props.game;
        if ( !competition ) {
            return null;
        }

        return (
            <CounterContainer
                range={ [competition.startDate, competition.endDate] }
                timestamp={ competition.startDate || 0 }
            /> );
    }

    renderStatusSection ( competition ) {
        const { _t } = this.props;
        if ( !competition ) {
            return (
                <article className="prizes" />
            );
        }
        return (
            <article className="prizes">
                <div className="data">
                    <p className="data__title">
                        { _t( 'games.game_card.status.entry_fee' ) }
                    </p>
                    <p className={ this.prizeClasses( competition ).entryFeeClass }>
                        { competition.entryFee || competition.entryFee === 0
                         ? <Money currency={ competition.currency } value={ competition.entryFee } />
                         : <span>&ndash;</span>
                        }
                    </p>
                </div>
                <div className="data">
                    <p className="data__title">
                        { _t( 'games.game_card.status.prize_pool' ) }
                    </p>
                    <p className={ this.prizeClasses( competition ).potClass }>
                        { competition.entryFee || competition.entryFee === 0
                         ? <Money currency={ competition.currency } value={ competition.pot } />
                         : <span>&ndash;</span>
                        }
                    </p>
                </div>
            </article>
        );
    }


    renderEntries ( competition ) {
        const { _t } = this.props;

        return (
            <article className="entries">
                <span className="entries__title">
                    { _t( 'games.game_card.status.entries' ) }
                </span>
                <span className="entries__num">
                    <span>{ competition.gamesCount }/</span>
                    <span className="black">{ competition.maxEntries }</span>
                </span>
            </article>
        );
    }

    showMyGamesRow ( game ) {
        const room = game.competition;
        let started = game.competition.started;
        let finished = game.competition.finished;
        let timeStatus;
        if ( !started ) {
            timeStatus = (
                <div className="data starts-on">
                    <span className="data__title">
                        { this.props.soon
                        ? this.props._t( 'games.game_card.status.start_in' )
                        : this.props._t( 'games.game_card.status.start_on' ) }
                    </span>

                    {this.renderTimer()}
                </div>
            );
        } else if ( started && !finished ) {
            timeStatus = (
                <div className="data starts-on live">
                    <span className="data__title">
                    { this.props._t( 'games.game_card.status.live' ) }
                    </span>
                </div>
            );
        } else if ( finished ) {
            timeStatus = (
                <div className="data starts-on finished">
                    <span className="data__title">
                    { this.props._t( 'games.game_card.status.finished' ) }
                    </span>
                </div>
            );
        }

        const competitionPage = preparePathWithLeague( `${RoutesConstants.get( 'competitionPage' )}${ room.id }/` );

        return (
            <Link to={ competitionPage }>
                <article className="lineup-item__row">
                    <div className="lineup-item__info">
                        { this.renderTitle( room, game ) }
                        { this.renderPoints( room, game ) }
                        { this.renderStatusSection( room, game ) }
                        { this.renderEntries( room, game ) }
                    </div>

                    <div className="team-lineup-container">
                        { timeStatus }

                        <LineupPhotoBlock
                            _t={ this.props._t }
                            photos={ prepareLineupPhotos( game, this.props.playerPositions ) }
                        />
                    </div>
                </article>
                { game.imWinner && game.celebrateTime ? <Confetti /> : null }
            </Link>
        );
    }
    render () {
        const { game } = this.props;

        if ( !game.competition.started ) {
            return this.renderBeforeStartGame( game );
        }

        if ( game.competition.started ) {
            return this.renderStartedGame( game );
        }

        if ( game.competition.started && !game.competition.finished ) {
            return this.renderOngoingGame( game );
        }

        if ( game.competition.finished ) {
            return this.renderFinishedGame( game );
        }

        return (
            <ScrollLoaderComponent />
        );
    }
}

GameCardMyGame.propTypes = {
    _t: React.PropTypes.func,
    competition: React.PropTypes.object,
    game: React.PropTypes.any,
    index: React.PropTypes.number,
    playerPositions: React.PropTypes.array,
    soon: React.PropTypes.bool,
    sport: React.PropTypes.string,
};

export default GameCardMyGame;
