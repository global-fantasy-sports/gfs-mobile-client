import React, { PropTypes } from 'react';

import Btn from '../../ui/btn/Btn';

import './style.scss';


const EmptyGames = ( { _t, onPlay } ) => (
    <article className="empty-games">
        <div className="top">
            <h1 className="top-title">
                <span>
                    { _t( 'games.title.my' ) }&nbsp;
                </span>
                    <span>
                    { _t( 'games.title.games' ) }
                </span>
            </h1>
            <p className="text-info">
                { _t( 'games.empty.subtitle' ) }
            </p>
        </div>
        <div className="steps">
            <div className="item">
                <span>1</span>
                <span>{ _t( 'games.empty.draft_team' ) }</span>
            </div>
            <div className="item">
                <span>2</span>
                <span>{ _t( 'games.empty.choose_comp' ) }</span>
            </div>
        </div>
        <Btn
            label={ _t( 'games.empty.btn_create_game' ) }
            onClick={ onPlay( 'Play' ) }
            size="monster"
            type="primary"
        />
    </article>
);

EmptyGames.propTypes = {
    _t: PropTypes.func.isRequired,
    onPlay: PropTypes.func.isRequired,
};

export default EmptyGames;
