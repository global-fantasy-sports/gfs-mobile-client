import React, { Component, PropTypes } from 'react';

import GameCard from '../game-card';
import Swipeable from '../swipeable/Swipeable';
import SwipeBar from '../../ui/swipe-bar/SwipeBar';
import cx from 'classnames';
import Icon from '../../ui/icon/Icon';

import './style.scss';


class Games extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        games: PropTypes.array.isRequired,
        now: PropTypes.number.isRequired,
        onEdit: PropTypes.func.isRequired,
        onRemove: PropTypes.func.isRequired,
        playerPositions: PropTypes.array.isRequired,
        playerPositionsNames: PropTypes.array.isRequired,
        sport: PropTypes.string.isRequired,
    };

    state = {
        active: null,
    };

    handleSwipeEnd = ( gameId ) => {
        return () => {
            this.setState( { active: gameId } );
        };
    };

    handleGameRemove = ( gameId, removable ) => {
        return () => {
            if ( removable ) {
                this.props.onRemove( gameId );
            }
        };
    };

    handleGameEdit = ( gameId, removable ) => {
        return () => {
            if ( removable ) {
                this.props.onEdit( gameId );
            }
        };
    };

    render () {
        const { _t, now, games, playerPositions, playerPositionsNames, sport } = this.props;
        return (
            <article className="games-rows">
                { games.map( ( game ) => {
                    if ( !game.competition ) {
                        return '';
                    }

                    const active = this.state.active === game.id;
                    const diff = game.competition.startDate - now;
                    const soon = diff < 24 * 60 * 60;
                    const removable = diff > 13 * 60;

                    const lineupItemClass = cx( 'lineup-item', {
                        soon,
                        live: game.competition.started && !game.competition.finished,
                        finished: game.competition.finished,
                        disabled: !removable,
                    } );

                    return (
                        <Swipeable
                            action={
                                <SwipeBar vertical>
                                     <div
                                         className={ cx( 'gc__button edit', { disabled: !removable } ) }
                                         onTouchTap={ this.handleGameEdit( game.id, removable ) }
                                     >
                                        <Icon className="icon" icon={ !removable ? 'edit-disabled' : 'edit' } />
                                        { _t( 'games.game_actions.btn_edit' ) }
                                     </div>
                                     <div
                                         className={ cx( 'gc__button remove', { disabled: !removable } ) }
                                         onTouchTap={ this.handleGameRemove( game.id, removable ) }
                                     >
                                        <Icon className="icon" icon={ !removable ? 'delete-disabled' : 'delete' } />
                                        { _t( 'games.game_actions.btn_delete' ) }
                                     </div>
                                </SwipeBar>
                            }
                            containerClassName={ lineupItemClass }
                            height={ 170 }
                            key={ game.id }
                            onSwipeEnd={ this.handleSwipeEnd( game.id ) }
                            open={ active }
                        >
                            <GameCard
                                _t={ _t }
                                competition={ game.competition }
                                game={ game }
                                positionsList={ playerPositions }
                                positionsNamesList={ playerPositionsNames }
                                soon={ soon }
                                sport={ sport }
                                type="games"
                            />
                        </Swipeable>
                    );
                } ) }
            </article>
        );
    }
}

export default Games;
