import cx from 'classnames';
import React, { Component, PropTypes } from 'react';

import Page from '../page';
import Games from './Games';
import EmptyGames from './EmptyGames';
import ScrollLoaderComponent from '../scroll-loader';
import Btn from '../../ui/btn/Btn';

import './style.scss';

class GamesFilterButton extends Component {

    static propTypes = {
        gamesFilter: PropTypes.string.isRequired,
        onFilter: PropTypes.func.isRequired,
        value: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired,
    };

    handleFilter = ( event ) => {
        event.preventDefault();
        this.props.onFilter( this.props.value );
    };

    render () {
        const { gamesFilter, text, value } = this.props;
        const className = cx( 'btn btn-blue-filter', {
            active: gamesFilter === value,
        } );
        return (
            <button
                className={ className }
                onTouchTap={ this.handleFilter }
            >
                { text }
            </button>
        );
    }
}

const FiltersPanel = ( { _t, gamesFilter, onFilter } ) => (
    <div className="filters">
        <GamesFilterButton
            gamesFilter={ gamesFilter }
            onFilter={ onFilter }
            text={ _t( 'games.filter.btn_all' ) }
            value="all"
        />
        <GamesFilterButton
            gamesFilter={ gamesFilter }
            onFilter={ onFilter }
            text={ _t( 'games.filter.btn_soon' ) }
            value="soon"
        />
        <GamesFilterButton
            gamesFilter={ gamesFilter }
            onFilter={ onFilter }
            text={ _t( 'games.filter.btn_live' ) }
            value="live"
        />
        <GamesFilterButton
            gamesFilter={ gamesFilter }
            onFilter={ onFilter }
            text={ _t( 'games.filter.btn_finished' ) }
            value="finished"
        />
    </div>
);

FiltersPanel.propTypes = {
    _t: PropTypes.func.isRequired,
    gamesFilter: PropTypes.string.isRequired,
    onFilter: PropTypes.func.isRequired,
};

const GamesStatusExplanation = ( { _t, gamesFilter } ) => {
    switch ( gamesFilter ) {
        case 'soon':
            return (
                <div className="games-explanation">
                    <i className="icon soon" />
                    <p dangerouslySetInnerHTML={ { __html: _t( 'games.filter.empty_soon' ) } } />
                </div>
            );
        case 'live':
            return (
                <div className="games-explanation">
                    <i className="icon live" />
                    <p dangerouslySetInnerHTML={ { __html: _t( 'games.filter.empty_live' ) } } />
                </div>
            );
        case 'finished':
            return (
                <div className="games-explanation">
                    <i className="icon finished" />
                    <p dangerouslySetInnerHTML={ { __html: _t( 'games.filter.empty_finished' ) } } />
                </div>
            );
        default:
            return <div />;
    }
};

GamesStatusExplanation.propTypes = {
    _t: PropTypes.func.isRequired,
    gamesFilter: PropTypes.string.isRequired,
};

class GamesComponent extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        games: PropTypes.array.isRequired,
        gamesFilter: PropTypes.string.isRequired,
        gamesLoaded: PropTypes.bool.isRequired,
        gamesTotal: PropTypes.number.isRequired,
        getGames: PropTypes.func.isRequired,
        now: PropTypes.number.isRequired,
        onCreate: PropTypes.func.isRequired,
        onEdit: PropTypes.func.isRequired,
        onFilter: PropTypes.func.isRequired,
        onForgetNewGames: PropTypes.func.isRequired,
        onJoinCompetition: PropTypes.func.isRequired,
        onRemove: PropTypes.func.isRequired,
        playerPositions: PropTypes.array.isRequired,
        playerPositionsNames: PropTypes.array.isRequired,
        sport: PropTypes.string.isRequired,
    };

    constructor ( props ) {
        super( props );
    }

    componentWillMount () {
        this.props.getGames();
    }

    componentWillUnmount () {
        this.props.onForgetNewGames();
    }

    componentDidMount () {
        document.body.scrollTop = 0;
    }

    toArray ( games, rooms ) {
        let list = [];
        Object.values( games ).forEach( game => {

            game.competition = rooms[game.competitionRoomId];
            list.push( game );
        } );
        return list;
    }

    sortHandler ( iGame1, iGame2 ) {
        if ( !( iGame1.competition && iGame2.competition ) ) {
            return 0;
        }

        return iGame2.competition.startDate - iGame1.competition.startDate;
    }

    handleEdit = ( gameId ) => {
        this.props.onEdit( gameId );
    }

    handleRemove = ( gameId ) => {
        this.props.onRemove( gameId );
    };

    renderGamesTitle () {
        const { _t, games, gamesFilter, onCreate, onFilter } = this.props;
        const gamesCount = Object.keys( games ).length;

        return (
            <article className="top">
                <h1 className="top-title">
                        <span>
                            { _t( 'games.title.my' ) }&nbsp;
                        </span>
                    <span>
                            { _t( 'games.title.games' ) }
                        </span>
                </h1>
                <h4 className="sub-title">
                    { _t( 'games.title.total' ) } { gamesCount }
                </h4>
               <Btn
                   icon="plus"
                   onClick={ onCreate( 'plus button' ) }
                   round
                   size="normal"
                   type="primary"
               />
                <FiltersPanel
                    _t={ _t }
                    gamesFilter={ gamesFilter }
                    onFilter={ onFilter }
                />
            </article>
        );
    }

    render () {
        const {
            _t, games, gamesLoaded, gamesFilter, gamesTotal, onCreate,
            sport, playerPositionsNames, playerPositions, now,
        } = this.props;

        if ( !gamesLoaded ) {
            return (
                <ScrollLoaderComponent />
            );
        }

        return (
            <Page className={ cx( 'games', { empty: games.length === 0 } ) }>
                { gamesTotal > 0 ? this.renderGamesTitle() : ''}
                { games.length === 0 ? <GamesStatusExplanation _t={ _t } gamesFilter={ gamesFilter } /> : '' }
                { gamesTotal > 0 ?
                    <Games
                        _t={ _t }
                        games={ games }
                        now={ now }
                        onEdit={ this.handleEdit }
                        onRemove={ this.handleRemove }
                        playerPositions={ playerPositions }
                        playerPositionsNames={ playerPositionsNames }
                        sport={ sport }
                    />
                    :
                    <EmptyGames
                        _t={ _t }
                        onPlay={ this.props.onCreate }
                    />
                }
                { gamesTotal > 0 && gamesFilter === 'all'
                ? <div className="create-placeholder" onClick={ onCreate( 'placeholder' ) }>
                        { _t( 'games.empty.btn_create_game' ) }
                  </div>
                : null }
            </Page>
        );
    }
}

export default GamesComponent;
