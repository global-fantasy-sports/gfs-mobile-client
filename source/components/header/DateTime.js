import React, { Component } from 'react';

import { TIME_FORMAT } from '../../constants/app';

import './style.scss';

class DateTime extends Component {
    render () {
        const date = this.props.now.format( TIME_FORMAT.datetime_full );
        const time = this.props.now.format( TIME_FORMAT.datetime_time );

        return (
            <section className="datetime">
                <div className="column">
                    <div className="date">
                        { date }
                    </div>
                    <div className="time">
                        { time }
                    </div>
                </div>
            </section>
        );
    }
}

DateTime.propTypes = {
    now: React.PropTypes.object.isRequired,
};

export default DateTime;
