import React, { Component, PropTypes } from 'react';

import Simulation from '../simulation';
import NotificationsToggle from '../notification/NotificationsToggle';
import photoPlaceholder from '../../images/player-placeholder-small.png';

import './style.scss';

const initialState = {
    menuOpen: false,
};

class HeaderComponent extends Component {

    static propTypes = {
        avatar: PropTypes.string.isRequired,
        balance: PropTypes.objectOf( PropTypes.number ).isRequired,
        changePointSimulationKickOff: PropTypes.func.isRequired,
        changePointsSimulationMid: PropTypes.func.isRequired,
        changePointsSimulationResults: PropTypes.func.isRequired,
        changeTimeSimulationOneDay: PropTypes.func.isRequired,
        changeTimeSimulationMinutes: PropTypes.func.isRequired,
        changeTimeSimulationOneHour: PropTypes.func.isRequired,
        changeTimeSimulationOneWeek: PropTypes.func.isRequired,
        changeTimeSimulationSixHours: PropTypes.func.isRequired,
        currentDateObject: PropTypes.object,
        isFixed: PropTypes.bool,
        simLoader: PropTypes.bool.isRequired,
        simError: PropTypes.string.isRequired,
        onClearSimError: PropTypes.func.isRequired,
        logout: PropTypes.func.isRequired,
        name: PropTypes.string,
        notifications: PropTypes.array.isRequired,
        restartSimulation: PropTypes.func.isRequired,
        showNotifications: PropTypes.bool.isRequired,
        simulation: PropTypes.bool,
        toggleNotifications: PropTypes.func.isRequired,
        _t: PropTypes.func,
    };

    constructor ( props ) {
        super( props );

        this.avatarSize = 40;
        this.state = initialState;
        this.timeout = null;
    }

    handleLogoClick = ( event ) => {
        event.preventDefault();
        this.setState( { menuOpen: !this.state.menuOpen } );
        if ( this.props.showNotifications ) {
            this.props.toggleNotifications();
        }
    };

    handleLogout = ( event ) => {
        event.preventDefault();
        this.props.logout();
    };

    handleNotifications = () => {
        this.props.toggleNotifications();
    };

    renderSimulation ( props ) {
        const {
            restartSimulation,
            currentDateObject,
            simulation,
            changeTimeSimulationMinutes,
            changeTimeSimulationOneHour,
            changeTimeSimulationSixHours,
            changeTimeSimulationOneDay,
            changeTimeSimulationOneWeek,
            changePointSimulationKickOff,
            changePointsSimulationMid,
            changePointsSimulationResults,
            simLoader,
            simError,
            onClearSimError,
        } = props;

        if ( process.env.WITH_SIMULATION === 'yes' && simulation ) {
            return (
                <Simulation
                    changePointSimulationKickOff={ changePointSimulationKickOff }
                    changePointsSimulationMid={ changePointsSimulationMid }
                    changePointsSimulationResults={ changePointsSimulationResults }
                    changeTimeSimulationMinutes={ changeTimeSimulationMinutes }
                    changeTimeSimulationOneDay={ changeTimeSimulationOneDay }
                    changeTimeSimulationOneHour={ changeTimeSimulationOneHour }
                    changeTimeSimulationOneWeek={ changeTimeSimulationOneWeek }
                    changeTimeSimulationSixHours={ changeTimeSimulationSixHours }
                    currentDateObject={ currentDateObject }
                    onClearSimError={ onClearSimError }
                    restartSimulation={ restartSimulation }
                    simError={ simError }
                    simLoader={ simLoader }
                />
            );
        }

        return null;
    }

    renderAuthMenu () {
        const { _t, avatar, name } = this.props;
        if ( !this.state.menuOpen ) {
            return null;
        }

        return (
            <div className="auth-menu">
                <div
                    className="inner"
                    onClick={ this.handleLogoClick }
                >
                    <div className="user-avatar">
                        <img
                            height={ this.avatarSize }
                            src={ avatar || photoPlaceholder }
                            width={ this.avatarSize }
                        />
                    </div>
                    <span className="username">{ name }</span>
                    <i className="cross" />
                </div>
                <div className="separator" />
                <div className="menu-body">
                    { this.renderSimulation( this.props ) }
                    <div className="separator minor" />
                    <div className="logout-wrapper">
                        <span className="logout-button" onClick={ this.handleLogout }>
                            { _t( 'header.auth_menu.btn_logout' ) }
                        </span>
                    </div>
                </div>
            </div>
        );
    }

    render () {
        const { balance, avatar, isFixed, showNotifications, notifications } = this.props;
        const fixed = isFixed || showNotifications ? 'fixed' : '';
        const unreadNotifications = notifications.filter( iNotification => !iNotification.read );

        return (
            <div className={ `header-wrap ${fixed}` }>
                <header className="header">
                    { this.renderAuthMenu() }
                    <div className="info">
                        <div className="header-left">
                            <div className="avatar-wrapper">
                                <div className="user-avatar">
                                    <img
                                        height={ this.avatarSize }
                                        onClick={ this.handleLogoClick }
                                        src={ avatar || photoPlaceholder }
                                        width={ this.avatarSize }
                                    />
                                </div>
                            </div>
                            <NotificationsToggle
                                onToggle={ this.handleNotifications }
                                unreadCount={ unreadNotifications.length }
                            />
                        </div>
                        <div className="balance-info">
                            { Object.entries( balance ).map( ( [currency, amount] ) =>
                                <div
                                    className={ `${currency}-amount` }
                                    key={ currency }
                                >
                                    { amount || 0 }
                                </div>
                            ) }
                        </div>
                    </div>
                </header>
            </div>
        );
    }
}

export default HeaderComponent;
