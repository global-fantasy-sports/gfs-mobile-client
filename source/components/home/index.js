import React, { Component, PropTypes } from 'react';
import { StickyContainer, Sticky } from 'react-sticky';

import Page from '../page';
import LoginContainer from '../../containers/LoginContainer';

import logoImg from '../../images/logos/GFS-white.png';
import logoImgBlack from '../../images/logos/GFS-black.png';
import Btn from '../../ui/btn/Btn';

import './style.scss';

class HomeComponent extends Component {

    static propTypes = {
        session: PropTypes.string,
        onRedirect: PropTypes.func.isRequired,
    };

    componentDidMount () {
        this.maybeRedirect();
    }

    componentDidUpdate () {
        this.maybeRedirect();
    }

    maybeRedirect () {
        if ( this.props.session ) {
            this.props.onRedirect();
        }
    }

    scrollToLogin () {
        this.refs['login-form'].scrollIntoView( { behavior: 'smooth' } );
        this.forceUpdate();
    }

    render () {
        return (
            <StickyContainer>
                <Page className="home">
                    <Sticky stickyStyle={ { left: 0, width: '100%' } } topOffset={ 500 }>
                        <header className="head">
                            <img alt="logo" height={ 25 } src={ logoImgBlack } width={ 82 } />
                            <Btn
                                label="Login"
                                onClick={ ::this.scrollToLogin }
                                size="normal"
                                type="primary"
                            />
                        </header>
                    </Sticky>
                    <article className="first">
                        <img alt="GFS" className="gfs-logo-white" src={ logoImg } />
                        <h1 className="first-title">Think you know <br /> fantasy sports?</h1>
                        <p className="first-sub-title">Prove it! One-day fantasy sports real <br />
                            money. Never stop being a fan!
                        </p>
                        <Btn
                            label="Login"
                            onClick={ ::this.scrollToLogin }
                            size="monster"
                            type="primary"
                        />
                        <p className="bottom-sub-title">free and paid games available</p>
                        <div className="money-back">
                            <div className="money-back-left">
                                <p className="money-back-title">MONEY BACK GUARANTEE</p>
                                <p className="money-back-subtitle">
                                    Love it after your first league or your money back!
                                </p>
                            </div>
                            <div className="money-back-right" />
                        </div>
                    </article>

                    <article className="second">
                        <div>
                            <h1 className="title">Real money <br />is at stake</h1>
                            <p className="sub-title">Lots of winners, lots of payouts – don't let <br />
                                your friends beat you!</p>
                            <div className="reviews">
                                <div className="reviews-item">
                                    <div><i className="reviews-photo joe" /></div>
                                    <div className="reviews-right">
                                        <p className="reviews-name">JOE DRAPE</p>
                                        <p className="reviews-journal">New york times</p>
                                        <p className="reviews-text">
                                            <q>The fantasy sports industry has been growing rapidly,
                                            with hundreds of thousands of dollars can be won
                                            </q>
                                        </p>
                                    </div>
                                </div>
                                <div className="reviews-item">
                                    <div><i className="reviews-photo seth" /></div>
                                    <div className="reviews-right">
                                        <p className="reviews-name">Seth Markman</p>
                                        <p className="reviews-journal">espn</p>
                                        <p className="reviews-text"><q>We actually kind of take the philosophy now
                                            that the fantasy player is our audience.</q></p>
                                    </div>
                                </div>
                                <div className="reviews-item">
                                    <div><i className="reviews-photo lora" /></div>
                                    <div className="reviews-right">
                                        <p className="reviews-name">Lora Kolodny</p>
                                        <p className="reviews-journal">the wall street journal</p>
                                        <p className="reviews-text">
                                            <q>There's a new obsession taking hold in the world of
                                                fantasy sports online: daily games played for cash
                                            </q>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <h1 className="title white right">
                                CHOOSE FROM<br />HUNDREDS OF<br />COMPETITIONS<br />ROOMS
                            </h1>
                            <p className="sub-title gray right">Great sports and many competitions to <br />
                                choose from in unique games with lots<br />of winners. </p>
                            <div className="game-icons">
                                <div className="game-icon-row">
                                    <div className="game-icon">
                                        <i className="icon epl" />
                                        <p>EPL</p>
                                    </div>
                                    <div className="game-icon">
                                        <i className="icon nfl" />
                                        <p>NFL</p>
                                    </div>
                                </div>
                                <div className="game-icon-row">
                                    <div className="game-icon">
                                        <i className="icon nba" />
                                        <p>NBA CBB</p>
                                    </div>
                                    <div className="game-icon">
                                        <i className="icon pga" />
                                        <p>PGA TOUR</p>
                                    </div>
                                </div>
                                <div className="game-icon-row">
                                    <div className="game-icon">
                                        <i className="icon mlb" />
                                        <p>MLB</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article className="third">
                        <div className="phone-block">
                            <h1 className="title">Talk smack<br />with your<br />friends</h1>
                            <p className="sub-title">Unique fantasy games for fans and friends.</p>
                            <p className="sub-title">
                                Play Head-2-Head, privately or with other fans. No season-long commitment.
                            </p>
                            <p className="sub-title">Pick your favorite sport and be off to the races in minutes.</p>
                            <div className="phone-img" />
                        </div>
                    </article>

                    <article className="fourth">
                        <div className="game-steps">
                            <h1 className="title white">GET STARTED!<br />EASILY!</h1>
                            <div>
                                <i className="icon pick" />
                                <p>Pick your<br />favorite game</p>
                            </div>
                            <i className="icon arrow-down" />
                            <div>
                                <i className="icon strategy" />
                                <p>Choose game<br />strategy</p>
                            </div>
                            <i className="icon arrow-down" />
                            <div>
                                <i className="icon friends" />
                                <p>Challenge your<br />friends</p>
                            </div>
                            <i className="icon arrow-down" />
                            <div>
                                <i className="icon watch" />
                                <p>watch the game in<br />real time</p>
                            </div>

                        </div>
                    </article>

                    <article className="fifth">
                        <div className="sports-section-title">
                            <h1 className="title white">Sports</h1>
                            <p className="sub-title gray">Be court-side, in the home stretch<br />
                                or play from the grand stands.</p>
                            <p className="sub-title gray">Your sport, your games.</p>
                        </div>
                        <div className="sport-icons-row">
                            <i className="icon ple" />
                            <i className="icon nfl" />
                        </div>
                        <div className="sport-icons-row">
                            <i className="icon nba" />
                            <i className="icon pga" />
                        </div>
                        <div className="sport-icons-row center">
                            <i className="icon mlb" />
                        </div>
                    </article>

                    <article className="sixth">
                        <div>
                            <h1 className="devices-title">
                                PLAY FROM THE<br />DEVICE OF YOUR<br />CHOICE
                            </h1>
                            <p className="devices-subtitle">Whenever you want to.<br />
                                From wherever you might be.</p>
                        </div>
                    </article>

                    <article className="seventh" ref="login-form">
                        <h1>Join now using</h1>
                        <div className="login-form" ><LoginContainer /></div>
                        <div className="copyrights">
                            <p>
                                Residents of the States of Arizona, Iowa, Illinois,
                                Louisiana, Montana, Nevada, North Dakota, New York,
                                Tennessee, Vermont and Washington, as well as Puerto Rico,
                                and where otherwise prohibited by law, are not eligible to
                                enter Global Fantasy Sports Inc pay to play contest
                                (as defined in the Contest Rules).
                            </p>
                            <a href="#">PRIVACY POLICY</a>
                            <a href="#">PRIVACY RIGHTS</a>
                            <a href="#">TERMS AND CONDITIONS</a>
                            <a href="#">CONTEST RULES</a>
                            <p>© 2014-2016 Global Fantasy Sports Inc.<br />All Rights Reserved.</p>
                        </div>
                    </article>
                </Page>
            </StickyContainer>
        );
    }
}

export default HomeComponent;
