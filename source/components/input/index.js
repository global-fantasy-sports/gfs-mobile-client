import React, { Component } from 'react';
import classnames from 'classnames';

import './style.scss';

export class Input extends Component {
    render () {
        const { className, extraProps, maxLength, placeholder, type } = this.props;
        let emailExhibitor, passwordExhibitor;

        if ( type === 'password' ) {
            let exhibitorToggle = iEvent => {
                iEvent.target.nextSibling.classList.toggle( 'display' );
                iEvent.target.classList.toggle( 'active' );
                iEvent.stopPropagation();
            };

            passwordExhibitor = (
                <div>
                    <div className="hiding-element" onClick={ exhibitorToggle } />
                    <input
                        className="exhibitor-element"
                        maxLength={ maxLength }
                        placeholder={ placeholder }
                        type={ 'text' }
                        { ...extraProps }
                    />
                </div>
            );
        }

        if ( type === 'email' ) {
            let cleanValue = iEvent => {
                extraProps.onChange( '' );
                iEvent.stopPropagation();
                this.forceUpdate();
            };
            emailExhibitor = (
                <div>
                    <div
                        className="clean-element"
                        dangerouslySetInnerHTML={ { __html: '&#10005' } }
                        onClick={ cleanValue }
                    />
                    <input
                        className="exhibitor-element"
                        maxLength={ maxLength }
                        placeholder={ placeholder }
                        type={ 'email' }
                        { ...extraProps }
                    />
                </div>
            );
        }

        return (
            <div className={ `input ${className || ''}` }>
                <input
                    maxLength={ maxLength }
                    placeholder={ placeholder }
                    type={ `${ type || 'text' }` }
                    { ...extraProps }
                />
                { type === 'email' ? emailExhibitor : ''}
                { type === 'password' ? passwordExhibitor : '' }
            </div>
        );
    }
}

export class Checkbox extends Input {

    render () {
        const { className, extraProps, children } = this.props;
        const randomId = Math.random();
        return (
            <div className={ `input ${className || ''}` }>
                <input id={ randomId } type="checkbox" { ...extraProps } />
                <label htmlFor={ randomId } />
                <label htmlFor={ randomId }>{ children }</label>
            </div>
        );
    }
}

export class Toggle extends Component {
    constructor ( props ) {
        super( ...arguments );

        this.state = {
            isChecked: !!props.isChecked,
        };
    }

    onClick () {
        if ( this.props.onCheck ) {
            this.props.onCheck( !this.state.isChecked );
        }

        this.setState( { isChecked: !this.state.isChecked } );
    }

    getToggleLabel () {
        if ( this.state.isChecked ) {
            return this.props.onLabel;
        }

        return this.props.offLabel;
    }

    render () {
        const cs = classnames( 'toggle-input__icon', {
            'toggle-input__icon--checked': this.state.isChecked,
        } );

        const inputCs = classnames( 'toggle-input', {
            'toggle-input--checked': this.state.isChecked,
        } );

        const textCs = classnames( 'toggle-input__text', {
            'toggle-input__text--checked': this.state.isChecked,
        } );

        return (
            <section className={ inputCs } onClick={ ::this.onClick }>
                <span className={ textCs }>
                    {this.getToggleLabel()}
                </span>
                <span className={ cs } />
            </section>
        );
    }
}

export class Select extends Input {

    constructor ( props ) {
        super( props );

        this.state = {
            isListOpen: false,
            currentItem: undefined,
        };
    }

    renderOption ( iOption ) {
        return (
            <li
                dangerouslySetInnerHTML={ { __html: iOption.name } }
                data-value={ JSON.stringify( iOption ) }
                key={ iOption.value }
            />
        );
    }

    chooseListItem ( iEvent, iChangeCb ) {
        const value = iEvent.target.getAttribute( 'data-value' );
        const text = iEvent.target.innerHTML;

        this.setState( { currentItem: text } );
        iChangeCb( value );
        this.toggleList();
    }

    toggleList () {
        const { isListOpen } = this.state;
        this.setState( { isListOpen: !isListOpen } );
    }

    render () {
        const { className, onChange, optionsList, placeholder } = this.props;
        let { isListOpen, currentItem } = this.state;
        let holderWidth = 0;

        if ( this.refs.holder ) {
            holderWidth = this.refs.holder.clientWidth;
        }

        return (
            <div className={ `input select ${className || ''}` }>
                <span className="holder" onClick={ ::this.toggleList } ref="holder">
                    <div style={ { width: holderWidth } }>{ currentItem || placeholder }</div>
                </span>
                <div className={ `list ${ isListOpen ? 'open' : 'close' }` }>
                    <ul
                        onClick={ iEvent => this.chooseListItem( iEvent, onChange ) }
                    >
                        { optionsList.map( iOption => this.renderOption( iOption ) ) }
                    </ul>
                </div>
            </div>
        );
    }
}

Input.propTypes = {
    className: React.PropTypes.string,
    placeholder: React.PropTypes.string,
    maxLength: React.PropTypes.string,
    extraProps: React.PropTypes.object,
    type: React.PropTypes.string,
};

Checkbox.propTypes = {
    className: React.PropTypes.string,
    extraProps: React.PropTypes.object,
    children: React.PropTypes.any,
};

Toggle.propTypes = {
    onLabel: React.PropTypes.string.isRequired,
    offLabel: React.PropTypes.string.isRequired,
    isChecked: React.PropTypes.bool,
    onCheck: React.PropTypes.func,
};

Select.propTypes = {
    onBlur: React.PropTypes.func.isRequired,
    onChange: React.PropTypes.func.isRequired,
    optionsList: React.PropTypes.arrayOf( React.PropTypes.shape( {
        name: React.PropTypes.string.isRequired,
        value: React.PropTypes.string.isRequired,
    } ) ),
    placeholder: React.PropTypes.string,
};
