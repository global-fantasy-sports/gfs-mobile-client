import React, { Component, PropTypes } from 'react';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import App from '../actions/app';
import DialogsContainer from '../containers/DialogsContainer';
import NotificationsContainer from '../containers/NotificationsContainer';
import ScrollLoaderComponent from '../components/scroll-loader';
import SESSION from '../actions/session';
import USER from '../actions/user';
import { translationsReady } from '../selectors/translations';
import Error404Page from '../pages/page404/index';

class LayoutContainer extends Component {

    static propTypes = {
        appStarted: PropTypes.bool.isRequired,
        appState: PropTypes.object.isRequired,
        authState: PropTypes.object.isRequired,
        children: PropTypes.object.isRequired,
        getAppInfo: PropTypes.func.isRequired,
        cleanSession: PropTypes.func.isRequired,
        cleanUser: PropTypes.func.isRequired,
        translationsReady: PropTypes.bool.isRequired,
    };

    componentDidMount () {
        this.validateState();
    }

    componentDidUpdate () {
        this.validateState();
    }

    startApp () {
        this.props.getAppInfo();
    }

    cleanApp () {
        this.props.cleanSession();
        this.props.cleanUser();
    }

    validateState () {
        const { appStarted, appState, authState } = this.props;

        if ( appStarted ) {
            if ( appState.reload ) {
                window.location.reload();
            }

            if ( authState.sessionDied ) {
                this.cleanApp();
            }

            if ( appState.initialised === false ) {
                this.startApp();
            }

            if ( authState.purge === true ) {
                this.cleanApp();
            }
        }
    }

    render () {
        if ( !this.props.translationsReady || !this.props.appState.initialised ) {
            return (
                <ScrollLoaderComponent />
            );
        }
        const { appState } = this.props;
        const chosenSport = appState.chosenSport || '';
        const chosenLeague = appState.chosenLeague || '';
        const sports = appState.settings.sports || {};
        if ( chosenSport ) {
            if ( !Object.keys( sports ).includes( chosenSport ) ) {
                return (
                    <Error404Page showLobbyButton={ false } />
                );
            }

            const leagues = sports[chosenSport].leagues;
            if ( chosenLeague && !Object.keys( leagues ).includes( chosenLeague ) ) {
                return (
                    <Error404Page showLobbyButton={ false } />
                );
            }
        }

        return (
            <section className="layout-component">
                { this.props.children }
                <DialogsContainer />
                <NotificationsContainer />
            </section>
        );
    }
}

const mapStateToPops = createStructuredSelector( {
    translationsReady,
    appStarted: ( state ) => state.reducer.app.started,
    appState: ( state ) => state.reducer.app,
    authState: ( state ) => state.reducer.auth,
} );

const mapDispatchToProps = {
    getAppInfo: ()=> App.getInfo(),
    cleanSession: SESSION.clean,
    cleanUser: USER.clean,
};

export default connect( mapStateToPops, mapDispatchToProps )( LayoutContainer );
