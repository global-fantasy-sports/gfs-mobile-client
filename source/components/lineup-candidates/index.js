import React, { Component, PropTypes } from 'react';
import Page from '../page';
import ParticipantsList from '../../wizard/components/ParticipantsList';
import SearchComponent from '../../wizard/components/SearchComponent';
import DraftComponentHeader from '../../wizard/components/DraftComponentHeader';
import { SORTING_OPTIONS } from '../../constants/wizard';
import './style.scss';

class LineupCandidatesComponent extends Component {

    static propTypes = {
        athletes: PropTypes.array.isRequired,
        favoriteAthletes: PropTypes.array,
        currentSport: PropTypes.string.isRequired,
        lineup: PropTypes.shape( {
            totals: PropTypes.object.isRequired,
            positions: PropTypes.array.isRequired,
        } ).isRequired,
        lineupId: PropTypes.string.isRequired,
        position: PropTypes.string.isRequired,
        salarycapBudget: PropTypes.number.isRequired,
        selectedIds: PropTypes.object.isRequired,
        selectedTeams: PropTypes.object.isRequired,
        onCompare: PropTypes.func.isRequired,
        onUncompare: PropTypes.func.isRequired,
        onParticipantClick: PropTypes.func.isRequired,
        onParticipantDraft: PropTypes.func.isRequired,
        onParticipantUndraft: PropTypes.func.isRequired,
        _t: PropTypes.func.isRequired,
    };

    constructor ( props ) {
        super( props );
        this.state = {
            search: '',
            showFavorite: false,
            sorting: 'salary desc',
        };
    }

    handleFilterByFavorites = () => {
        this.setState( { showFavorite: !this.state.showFavorite } );
    };

    handleSearch = ( search ) => {
        this.setState( { search } );
    };

    handleSort = ( sorting ) => {
        this.setState( { sorting } );
    };

    render () {
        const {
            _t,
            athletes,
            favoriteAthletes,
            lineup: { totals },
            lineupId,
            position,
            salarycapBudget,
            selectedIds,
            selectedTeams,
            currentSport,
            onCompare,
            onUncompare,
            onParticipantClick,
            onParticipantDraft,
            onParticipantUndraft,
        } = this.props;

        const { search, showFavorite, sorting } = this.state;

        return (
            <Page className="wizard lineup-candidates">
                <DraftComponentHeader
                    _t={ _t }
                    avgFPPG={ totals.athletes ? totals.fppg / totals.athletes : 0 }
                    budgetLeft={ salarycapBudget - totals.salary }
                    budgetMax={ salarycapBudget }
                    position={ position }
                    returnTo={ `/lineups/${lineupId}/` }
                />
                <SearchComponent
                    _t={ _t }
                    favoritesOnly={ showFavorite }
                    onFilterFavorites={ this.handleFilterByFavorites }
                    onSearch={ this.handleSearch }
                    onSort={ this.handleSort }
                    position={ position }
                    search={ search }
                    sorting={ sorting }
                    sortingOptions={ SORTING_OPTIONS }
                    sport={ currentSport }
                />
                <ParticipantsList
                    _t={ _t }
                    budgetLeft={ salarycapBudget - totals.salary }
                    favorites={ favoriteAthletes }
                    favoritesOnly={ showFavorite }
                    onClick={ onParticipantClick }
                    onCompare={ onCompare }
                    onDraft={ onParticipantDraft }
                    onUncompare={ onUncompare }
                    onUndraft={ onParticipantUndraft }
                    participants={ athletes }
                    search={ search }
                    selectedIds={ selectedIds }
                    selectedTeams={ selectedTeams }
                    sorting={ sorting }
                />
            </Page>
        );
    }
}

export default LineupCandidatesComponent;
