import React, { Component, PropTypes } from 'react';
import cx from 'classnames';

import LineupInfo from '../../wizard/components/LineupInfo';
import LineupNameInput from './LineupNameInput';
import Btn from '../../ui/btn/Btn';

import './style.scss';

const EditingTitle = ( {
    _t,
    athletesCount,
    icon,
    isNew,
    isValid,
    onCancel,
    onSave,
    onToggleWizardViewMode,
    positions,
    wizardViewMode,
    } ) => (
    <div className="lineup-header__row">
        <Btn
            label={ _t( 'lineup_details.edit_title.btn_cancel' ) }
            onClick={ onCancel }
            type="confirm"
        />
        <h3 className="lineup-header__title">
            { isNew ?
                _t( 'lineup_details.edit_title.create' ) :
                _t( 'lineup_details.edit_title.edit' )
            }
            &nbsp;
            { _t( 'lineup_details.title.lineup' ) }
            <div className="info-section">
                <div className="value">{ athletesCount }<em>/{ positions.length }</em></div>
            </div>
        </h3>
        <div className="wizard-header__controls">
            <Btn
                disabled={ !wizardViewMode }
                icon={ icon }
                onClick={ onToggleWizardViewMode }
            />
            <Btn
                disabled={ !isValid }
                label={ _t( 'lineup_details.edit_title.btn_save' ) }
                onClick={ onSave }
                type="primary"
            />
        </div>
    </div>
);

EditingTitle.propTypes = {
    _t: PropTypes.func.isRequired,
    athletesCount: PropTypes.number.isRequired,
    icon: PropTypes.string.isRequired,
    isNew: PropTypes.bool.isRequired,
    isValid: PropTypes.bool.isRequired,
    onCancel: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired,
    onToggleWizardViewMode: PropTypes.func.isRequired,
    positions: PropTypes.array.isRequired,
    wizardViewMode: PropTypes.oneOf( ['list', 'pitch'] ).isRequired,
};

const ViewingTitle = ( {
    _t,
    athletesCount,
    icon,
    onCreateGame,
    onGoBack,
    onToggleWizardViewMode,
    positions,
    wizardViewMode,
} ) => (
    <div className="lineup-header__row">
        <Btn
            label={ _t( 'lineup_details.view_title.btn_exit' ) }
            onClick={ onGoBack }
            type="confirm"
        />
        <h3 className="lineup-header__title">
            <p>{ _t( 'lineup_details.title.lineup' ) } { _t( 'lineup_details.view_title.details' ) }</p>
            <div className="info-section">
                <div className="value">{ athletesCount }<em>/{ positions.length }</em></div>
            </div>
        </h3>
        <div className="wizard-header__controls">
            <Btn
                disabled={ !wizardViewMode }
                icon={ icon }
                onClick={ onToggleWizardViewMode }
            />
            <Btn
                label={ _t( 'lineup_details.view_title.btn_play' ) }
                onClick={ onCreateGame }
                type="primary"
            />
        </div>
    </div>
);

ViewingTitle.propTypes = {
    _t: PropTypes.func.isRequired,
    athletesCount: PropTypes.number.isRequired,
    icon: PropTypes.string.isRequired,
    onCreateGame: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    onGoBack: PropTypes.func.isRequired,
    onToggleWizardViewMode: PropTypes.func.isRequired,
    positions: PropTypes.array.isRequired,
    wizardViewMode: PropTypes.oneOf( ['list', 'pitch'] ).isRequired,
};


class LineupHeader extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        currentSport: PropTypes.string.isRequired,
        isEditing: PropTypes.bool.isRequired,
        lineup: PropTypes.object.isRequired,
        salarycapBudget: PropTypes.number.isRequired,
        positions: PropTypes.array.isRequired,
        totals: PropTypes.shape( {
            athletes: PropTypes.number.isRequired,
            fppg: PropTypes.number.isRequired,
            salary: PropTypes.number.isRequired,
        } ).isRequired,
        onGoBack: PropTypes.func.isRequired,
        onDelete: PropTypes.func.isRequired,
        onCancel: PropTypes.func.isRequired,
        onCreateGame: PropTypes.func.isRequired,
        onNameChange: PropTypes.func.isRequired,
        onToggleWizardViewMode: PropTypes.func.isRequired,
        onSave: PropTypes.func.isRequired,
        wizardViewMode: PropTypes.oneOf( ['list', 'pitch'] ).isRequired,
    };

    constructor ( props ) {
        super( props );

        this.handleNameChange = ::this.handleNameChange;
    }

    handleNameChange ( value ) {
        this.props.onNameChange( value );
    }

    render () {
        const {
            _t,
            lineup,
            positions,
            totals = {},
            isEditing,
            wizardViewMode,
            onToggleWizardViewMode,
            salarycapBudget,
        } = this.props;
        const budgetLeft = salarycapBudget - totals.salary;
        const avgFPPG = totals.athletes === 0 ? 0 : totals.fppg / totals.athletes;
        const isValid = totals.athletes > 0;
        const icon = cx( {
            'pitch-disabled': !wizardViewMode,
            pitch: wizardViewMode === 'list',
            list: wizardViewMode === 'pitch',
        } );

        return (
            <section className="lineup-create-wrap">
                <div className="lineup-header">
                    {isEditing
                        ? <EditingTitle
                            _t={ _t }
                            athletesCount={ lineup.totals.athletes }
                            icon={ icon }
                            isNew={ lineup.id === 'new' }
                            isValid={ isValid }
                            onCancel={ this.props.onCancel }
                            onSave={ this.props.onSave }
                            onToggleWizardViewMode={ onToggleWizardViewMode }
                            positions={ positions }
                            wizardViewMode={ wizardViewMode }
                          />
                        : <ViewingTitle
                            _t={ _t }
                            athletesCount={ lineup.totals.athletes }
                            icon={ icon }
                            onCreateGame={ this.props.onCreateGame }
                            onDelete={ this.props.onDelete }
                            onGoBack={ this.props.onGoBack }
                            onToggleWizardViewMode={ onToggleWizardViewMode }
                            positions={ positions }
                            wizardViewMode={ wizardViewMode }
                          />
                    }
                    <LineupInfo
                        _t={ _t }
                        avgFPPG={ avgFPPG }
                        budgetLeft={ budgetLeft }
                        budgetMax={ salarycapBudget }
                        className="lineup-header__midrow"
                    />
                </div>
                <LineupNameInput
                    _t={ _t }
                    onChange={ this.handleNameChange }
                    value={ lineup.name }
                />
            </section>
        );
    }
}

export default LineupHeader;
