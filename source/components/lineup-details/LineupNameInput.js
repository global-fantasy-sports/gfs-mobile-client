import React, { Component, PropTypes } from 'react';

class LineupNameInput extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        value: PropTypes.string.isRequired,
        onChange: PropTypes.func.isRequired,
    };

    constructor ( props ) {
        super();
        this.state = { value: props.value };
    }

    componentWillReceiveProps ( nextProps ) {
        if ( this.props.value !== nextProps.value ) {
            this.setState( { value: nextProps.value } );
        }
    }

    handleInputChange = ( event ) => {
        this.setState( { value: event.target.value } );
    };

    handleBlur = () => {
        this.props.onChange( this.state.value );
    };

    handleReset = ( event ) => {
        event.preventDefault();
        this.setState( { value: null } );
    };

    render () {
        return (
            <div className="lineup-header-name-field">
                <input
                    onBlur={ this.handleBlur }
                    onChange={ this.handleInputChange }
                    placeholder={ this.props._t( 'lineup_details.input_name.enter_name' ) }
                    type="text"
                    value={ this.state.value || '' }
                />
                <i onTouchTap={ this.handleReset } />
            </div>
        );
    }
}

export default LineupNameInput;
