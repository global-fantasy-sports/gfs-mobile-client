import React, { Component, PropTypes } from 'react';
import Page from '../page';
import LineupHeader from './LineupHeader';
import LineupList from '../lineup-list';
import PitchView from '../../wizard/components/pitch/PitchView';
import './style.scss';

class LineupDetailsComponent extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        currentSport: PropTypes.string.isRequired,
        isEditing: PropTypes.bool.isRequired,
        lineup: PropTypes.object.isRequired,
        lineupId: PropTypes.string.isRequired,
        positions: PropTypes.array.isRequired,
        onCancel: PropTypes.func.isRequired,
        onCompare: PropTypes.func.isRequired,
        onCreateGame: PropTypes.func.isRequired,
        onDelete: PropTypes.func.isRequired,
        onGoBack: PropTypes.func.isRequired,
        onInitEditor: PropTypes.func.isRequired,
        onToggleWizardViewMode: PropTypes.func.isRequired,
        onNameChange: PropTypes.func.isRequired,
        onPositionClear: PropTypes.func.isRequired,
        onPositionSelect: PropTypes.func.isRequired,
        onSave: PropTypes.func.isRequired,
        onShowAthleteInfo: PropTypes.func.isRequired,
        salarycapBudget: PropTypes.number.isRequired,
        selectedIds: PropTypes.object.isRequired,
        wizardViewMode: PropTypes.oneOf( ['list', 'pitch'] ).isRequired,
    };

    componentWillMount () {
        this.props.onInitEditor();
    }

    handlePositionClear = ( position ) => {
        this.props.onPositionClear( position );
    };

    handlePositionSelect = ( position ) => {
        this.props.onPositionSelect( position );
    };

    handleShowAthleteInfo = ( position ) => {
        this.props.onShowAthleteInfo( position );
    };

    render () {
        const {
            _t,
            currentSport,
            isEditing,
            lineup,
            onCancel,
            onCompare,
            onCreateGame,
            onDelete,
            onGoBack,
            onToggleWizardViewMode,
            onNameChange,
            onSave,
            wizardViewMode,
            selectedIds,
            salarycapBudget,
        } = this.props;

        return (
            <Page className="wizard lineup-create">
                <LineupHeader
                    _t={ _t }
                    currentSport={ currentSport }
                    isEditing={ isEditing }
                    lineup={ lineup }
                    onCancel={ onCancel }
                    onCreateGame={ onCreateGame }
                    onDelete={ onDelete }
                    onGoBack={ onGoBack }
                    onNameChange={ onNameChange }
                    onSave={ onSave }
                    onToggleWizardViewMode={ onToggleWizardViewMode }
                    positions={ lineup.positions }
                    salarycapBudget={ salarycapBudget }
                    totals={ lineup.totals }
                    wizardViewMode={ wizardViewMode }
                />
                { wizardViewMode === 'pitch' ?
                     <PitchView
                         _t={ _t }
                         lineup={ lineup }
                         lineupMode
                         onCompare={ onCompare }
                         onPositionSelect={ this.handlePositionSelect }
                         onShowAthleteInfo={ this.handleShowAthleteInfo }
                         onUndraft={ this.handlePositionClear }
                     />
                :
                    <LineupList
                        _t={ _t }
                        lineup={ lineup }
                        onCompare={ onCompare }
                        onPositionClear={ this.handlePositionClear }
                        onPositionSelect={ this.handlePositionSelect }
                        onShowAthleteInfo={ this.handleShowAthleteInfo }
                        positions={ lineup.positions }
                        selectedIds={ selectedIds }
                    />
                }
            </Page>
        );
    }
}

export default LineupDetailsComponent;
