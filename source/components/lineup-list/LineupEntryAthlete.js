import React, { Component, PropTypes } from 'react';
import Icon from '../../ui/icon/Icon';
import cx from 'classnames';
import PlaceholderImage from '../../components/common/PlaceholderImage';
import { format } from '../../utils/number';
import Money from '../../components/common/Money';
import Swipeable from '../../components/swipeable';
import SwipeBar from '../../ui/swipe-bar/SwipeBar';
import playerPlaceholder from '../../images/room/player-placeholder.png';
import './style.scss';


const AthleteInfoDisplay = ( props ) => {
    const csImage = cx( 'image-wrapper', {
        unavailable: props.unavailable,
    } );

    const csSalary = cx( 'value', {
        unavailable: props.unavailable,
    } );

    return (
        <div className="wizard-entry selected-roster-item">
            <div className="heading">
                <div className={ csImage }>
                    <PlaceholderImage
                        alt={ props.name }
                        className="player-photo"
                        height={ 55 }
                        placeholder={ playerPlaceholder }
                        src={ props.photo }
                        width={ 80 }
                    />
                </div>
                <div className="name">{ props.name }</div>
            </div>
            {!props.unavailable ? [
                <div className="column oprk" key={ 0 }>
                    <div className="label">
                        { props._t( 'wizard.select.lineup_entry.oprk' ) }
                    </div>
                    <div className="value">{ props.oprk }</div>
                </div>,
                <div className="column fppg" key={ 1 }>
                    <div className="label">
                        { props._t( 'wizard.select.lineup_entry.fppg' ) }
                    </div>
                    <div className="value">{ format( props.fantasyPoints, 2 ) }</div>
                </div>,
            ] :
                <div className="unavailable-cell">
                    { props._t( 'wizard.select.lineup_entry.player_unavailable' ) }
                </div>
            }

            <div className="column">
                <div className="label">
                    { props._t( 'wizard.select.lineup_entry.salary' ) }
                </div>
                <div className={ csSalary }>
                    <Money currency="usd" value={ props.salary } />
                </div>
            </div>
        </div>
    );
};

AthleteInfoDisplay.propTypes = {
    fantasyPoints: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    oprk: PropTypes.number.isRequired,
    photo: PropTypes.string.isRequired,
    salary: PropTypes.number.isRequired,
    unavailable: PropTypes.bool,
    _t: PropTypes.func.isRequired,
};

class LineupEntryAthlete extends Component {

    static propTypes = {
        disabled: PropTypes.bool.isRequired,
        active: PropTypes.bool.isRequired,
        athlete: PropTypes.object.isRequired,
        onActiveChanged: PropTypes.func.isRequired,
        onClick: PropTypes.func.isRequired,
        onCompare: PropTypes.func.isRequired,
        onUndraft: PropTypes.func.isRequired,
        position: PropTypes.string.isRequired,
        selectedIds: PropTypes.object,
        _t: PropTypes.func.isRequired,
    };

    handleClick = ( event ) => {
        event.preventDefault();
        this.props.onClick( this.props.position );
    };

    handleUndraft = () => {
        this.props.onUndraft( this.props.position );
    };

    handleSwipeStart = () => {
        this.props.onActiveChanged( null );
    };

    handleSwipeEnd = ( isOpen ) => {
        this.props.onActiveChanged( isOpen ? this.props.position : null );
    };

    handleCompare = () => {
        const { athlete, position, onCompare } = this.props;
        onCompare( {
            athleteId: athlete.athleteId,
            participantId: athlete.participantId,
            pos: position,
            position: athlete.position,
        } );
    };

    render () {
        const {
            _t,
            active,
            athlete = {},
            selectedIds,
        } = this.props;

        const selected = selectedIds.has( athlete.id );

        return (
            <Swipeable
                action={
                    <SwipeBar>
                        <div
                            className="compare-button"
                            onClick={ this.handleCompare }
                        >
                            <Icon icon="compare" mod="draft" />
                            <span>{ _t( 'wizard.draft.list_entry.btn_compare' ) }</span>
                        </div>
                        <div
                            className={ cx( 'draft-button', { undraft: selected } ) }
                            onClick={ this.handleUndraft }
                        >
                            <Icon icon={ selected ? 'minus-circle' : 'plus-circle' } mod="draft" />
                            { selected
                                ? _t( 'wizard.select.lineup_entry.btn_undraft' )
                                : _t( 'wizard.select.lineup_entry.btn_draft' )
                            }
                        </div>
                    </SwipeBar>
                }
                containerClassName="wizard-entry-wrapper"
                height={ 64 }
                onSwipeEnd={ this.handleSwipeEnd }
                onSwipeStart={ this.handleSwipeStart }
                onTouchTap={ this.handleClick }
                open={ active }
            >
                <AthleteInfoDisplay
                    _t={ _t }
                    fantasyPoints={ athlete.FPPG }
                    name={ _t( athlete.name ) }
                    oprk={ athlete.oprk }
                    photo={ athlete.smallPhoto }
                    salary={ athlete.salary }
                    unavailable={ athlete.unavailable }
                />
            </Swipeable>
        );
    }
}

export default LineupEntryAthlete;
