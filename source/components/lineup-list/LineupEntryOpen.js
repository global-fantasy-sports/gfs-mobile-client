import React, { Component, PropTypes } from 'react';
import { POSITION_LABELS } from '../../constants/wizard';


class LineupEntryOpen extends Component {

    static propTypes = {
        position: PropTypes.string.isRequired,
        onClick: PropTypes.func.isRequired,
        _t: PropTypes.func.isRequired,
    };

    constructor () {
        super();

        this.handleClick = ::this.handleClick;
    }

    handleClick ( event ) {
        event.preventDefault();
        this.props.onClick( this.props.position );
    }

    render () {
        const { _t, position } = this.props;

        return (
            <div className="wizard-entry empty" onTouchTap={ this.handleClick }>
                <span className="position">
                    { _t( `common.position_abbr_label.${position}` ) }
                </span>
                <span className="label">
                    { _t( 'wizard.select.lineup_entry.select' ) }&nbsp;
                    { _t( `common.position_label.${ POSITION_LABELS[position] }` ) }
                    </span>
            </div>
        );
    }
}

export default LineupEntryOpen;
