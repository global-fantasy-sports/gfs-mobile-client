import React, { Component, PropTypes } from 'react';
import LineupEntryAthlete from './LineupEntryAthlete';
import LineupEntryOpen from './LineupEntryOpen';

import './style.scss';


class LineupList extends Component {

    static propTypes = {
        lineup: PropTypes.object.isRequired,
        positions: PropTypes.array.isRequired,
        onCompare: PropTypes.func,
        onShowAthleteInfo: PropTypes.func.isRequired,
        onPositionClear: PropTypes.func.isRequired,
        onPositionSelect: PropTypes.func.isRequired,
        selectedIds: PropTypes.object,
        _t: PropTypes.func.isRequired,
    };

    constructor () {
        super();
        this.state = {
            activePosition: null,
        };
    }

    componentDidMount () {
        window.scrollBy( 0, -500 );
    }

    handleActiveChanged = ( activePosition ) => {
        this.setState( { activePosition } );
    };

    render () {
        const {
            _t,
            lineup,
            positions,
            onShowAthleteInfo,
            onPositionClear,
            onPositionSelect,
            selectedIds,
        } = this.props;
        const { activePosition } = this.state;

        return (
            <ul className="wizard-list">
                { positions.map( ( position ) =>
                    <li className="wizard-list__item" key={ position }>
                        { lineup[position]
                            ?
                            <LineupEntryAthlete
                                _t={ _t }
                                active={ position === activePosition }
                                athlete={ lineup[position] }
                                disabled={ false }
                                onActiveChanged={ this.handleActiveChanged }
                                onClick={ onShowAthleteInfo }
                                onCompare={ this.props.onCompare }
                                onUndraft={ onPositionClear }
                                position={ position }
                                selectedIds={ selectedIds }
                            />
                            :
                          <LineupEntryOpen
                              _t={ _t }
                              onClick={ onPositionSelect }
                              position={ position }
                          />
                        }
                    </li>
                ) }
            </ul>
        );
    }
}

export default LineupList;
