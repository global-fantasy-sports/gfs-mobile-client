import React, { PropTypes } from 'react';

import './style.scss';
import Btn from '../../ui/btn/Btn';

export const EmptyLineup = ( { _t, handleNewLineup } ) => {
    return (
        <article className="empty-lineup">
            <div className="top">
                <h1 className="title">
                    <span>
                        { _t( 'lineups.title.my' ) }
                    </span>
                    &nbsp;
                    <span>
                        { _t( 'lineups.title.my_lineups' ) }
                    </span>
                </h1>
                <p className="top-text">
                    { _t( 'lineups.empty_lineups_page.subtitle' ) }
                </p>
                <p className="lineup-explanation">
                    { _t( 'lineups.empty_lineups_page.explanation' ) }
                </p>
            </div>
            <Btn
                label={ _t( 'lineups.empty_lineups_page.btn_create' ) }
                onClick={ handleNewLineup( 'placeholder' ) }
                size="monster"
                type="primary"
            />
        </article>
    );
};

EmptyLineup.propTypes = {
    _t: PropTypes.func.isRequired,
    handleNewLineup: PropTypes.func.isRequired,
};
