import React, { Component, PropTypes } from 'react';

import './style.scss';
import Btn from '../../ui/btn/Btn';
import { format } from '../../utils/number';
import Swipeable from '../swipeable/Swipeable';
import SwipeBar from '../../ui/swipe-bar/SwipeBar';
import Money from '../common/Money';
import LineupPhotoBlock from './LineupPhotoBlock';
import Icon from '../../ui/icon/Icon';

class LineupItem extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        active: PropTypes.bool,
        budget: PropTypes.number.isRequired,
        id: PropTypes.any.isRequired,
        name: PropTypes.string.isRequired,
        onActiveChanged: PropTypes.func.isRequired,
        onCreateGame: PropTypes.func.isRequired,
        onDeleteLineup: PropTypes.func.isRequired,
        onDuplicateLineup: PropTypes.func.isRequired,
        onOpenDetails: PropTypes.func.isRequired,
        photos: PropTypes.array.isRequired,
        totals: PropTypes.object.isRequired,
    };

    handleCreate = () => {
        this.props.onCreateGame( this.props.id );
    };

    handleDelete = () => {
        this.props.onDeleteLineup( this.props.id );
    };

    handleDuplicate = () => {
        this.props.onDuplicateLineup( this.props.id );
    };

    handleTap = ( event ) => {
        event.preventDefault();
        this.props.onOpenDetails( this.props.id );
    };

    handleSwipeStart = () => {
        this.props.onActiveChanged( null );
    };

    handleSwipeEnd = ( isOpen ) => {
        this.props.onActiveChanged( isOpen ? this.props.id : null );
    };

    render () {
        const { _t, active, photos, name, budget, totals } = this.props;

        return (
            <Swipeable
                action={
                    <SwipeBar vertical>
                        <div className="button remove" onTouchTap={ this.handleDelete }>
                            <Icon className="icon" icon="delete" />
                            { _t( 'lineups.lineup_item.btn_delete' ) }
                        </div>
                        <div className="button create" onTouchTap={ this.handleCreate }>
                            <Icon className="icon" icon="create-game" />
                            { _t( 'lineups.lineup_item.btn_create' ) }
                        </div>
                    </SwipeBar>
                }
                containerClassName="lineup-item"
                height={ 170 }
                onSwipeEnd={ this.handleSwipeEnd }
                onSwipeStart={ this.handleSwipeStart }
                onTouchTap={ this.handleTap }
                open={ active }
            >
                <div className="lineup-item__info">
                    <div className="lineup-item__name">{ name }</div>
                    <div className="data">
                        <div className="data__title">{ _t( 'lineups.lineup_item.title.budget' ) }</div>
                        <div className="data__num">
                            <Money currency="usd" value={ budget - totals.salary } />
                        </div>
                    </div>
                    <div className="data">
                        <div className="data__title">{ _t( 'lineups.lineup_item.title.cost' ) }</div>
                        <div className="data__num">
                            <Money currency="usd" value={ totals.salary } />
                        </div>
                    </div>
                    <div className="data">
                        <div className="data__title">{ _t( 'lineups.lineup_item.title.avg_fppg' ) }</div>
                        <div className="data__num">
                            {format( totals && totals.athletes ? totals.fppg / totals.athletes : 0, 2 )}
                        </div>
                    </div>
                </div>
                <div className="team-lineup-container lineup-page">
                    <LineupPhotoBlock _t={ _t } photos={ photos } />
                </div>
            </Swipeable>
        );
    }

}

export const FilledLineups = (
    {
        _t,
        activeId,
        lineups,
        handleNewLineup,
        handleViewLineup,
        handleActiveChanged,
        onCreateGame,
        onDeleteLineup,
        onDuplicateLineup,
        salarycapBudget,
    } ) => {
    return (
        <article className="filled-lineups">
            <div className="top">
                <h1 className="title">
                        <span>
                            { _t( 'lineups.title.my' ) }
                        </span>
                    &nbsp;
                    <span>
                            { _t( 'lineups.title.my_lineups' ) }
                        </span>
                </h1>
                <h4 className="sub-title">{ _t( 'lineups.title.total' ) } { lineups.length }</h4>
                <Btn
                    icon="plus"
                    onClick={ handleNewLineup( '+' ) }
                    round
                    size="normal"
                    type="primary"
                />
            </div>
            <div className="lineup-rows">
                { lineups.map( ( { id, name = '', positions, totals, ...lineup }, key ) =>
                    <LineupItem
                        _t={ _t }
                        active={ activeId === id }
                        budget={ salarycapBudget }
                        id={ id }
                        key={ key }
                        name={ name }
                        onActiveChanged={ handleActiveChanged }
                        onCreateGame={ onCreateGame }
                        onDeleteLineup={ onDeleteLineup }
                        onDuplicateLineup={ onDuplicateLineup }
                        onOpenDetails={ handleViewLineup }
                        photos={ positions.map( pos => lineup[pos]
                                                        ? { thumb: lineup[pos].smallPhoto, name: lineup[pos].name }
                                                        : { thumb: null, name: pos } ) }
                        positions={ positions }
                        totals={ totals }
                    />
                ) }
                <a onTouchTap={ handleNewLineup( 'create lineup' ) }>
                    <article className="empty-row">
                        <p className="title">
                            <span>{ _t( 'lineups.empty_lineup_item.create' ) }</span>
                        </p>
                    </article>
                </a>
            </div>
        </article>
    );
};

FilledLineups.propTypes = {
    activeId: PropTypes.number,
    lineups: PropTypes.array.isRequired,
    handleNewLineup: PropTypes.func.isRequired,
    handleViewLineup: PropTypes.func.isRequired,
    handleActiveChanged: PropTypes.func.isRequired,
    onCreateGame: PropTypes.func.isRequired,
    onDeleteLineup: PropTypes.func.isRequired,
    onDuplicateLineup: PropTypes.func.isRequired,
    salarycapBudget: PropTypes.number.isRequired,
    _t: PropTypes.func.isRequired,
};
