import React, { PropTypes } from 'react';
import cx from 'classnames';

import PlaceholderImage from '../common/PlaceholderImage';
import playerPlaceholder from '../../images/room/player-placeholder.png';

const ScoreBubble = ( { points } ) => {
    return (
        <div className="team-lineup__item__points">{ points }</div>
    );
};
ScoreBubble.propTypes = {
    points: PropTypes.any.isRequired,
};

const LineupPhotoBlock = ( { _t, photos, withPoints } ) => {
    if ( !photos ) {
        photos = new Array( 9 ).fill( null );
    }

    return (
        <article className="team-lineup">
            { photos.map( ( item, i ) =>
                <div className="team-lineup__item" key={ i }>
                    <PlaceholderImage
                        alt="Player Photo"
                        placeholder={ playerPlaceholder }
                        src={ item.thumb }
                    />
                    { withPoints && item.points !== null
                        ? <ScoreBubble points={ item.points || 0 } />
                        : null }
                    <div className={ cx( 'team-lineup__caption', { ghost: !item.thumb } ) }>
                        { item.name ? _t( item.name ) : <span>&mdash;</span> }
                    </div>
                </div>
            ) }
        </article>
    );
};


LineupPhotoBlock.propTypes = {
    _t: PropTypes.func.isRequired,
    photos: PropTypes.arrayOf( PropTypes.shape( {
        thumb: PropTypes.string,
        name: PropTypes.string,
        points: PropTypes.any,
    } ) ).isRequired,
    withPoints: PropTypes.bool,
};

export default LineupPhotoBlock;
