import React, { PropTypes } from 'react';

import PlaceholderImage from '../common/PlaceholderImage';
import playerPlaceholder from '../../images/room/player-placeholder.png';


const LineupPhotos = ( { _t, photos, positionsNamesList } ) => {
    if ( !photos ) {
        photos = new Array( 9 ).fill( null );
    }

    return (
        <article className="team-lineup">
            <ul className="team-lineup__list">
                { photos.map( ( iPhoto, iIndex ) =>
                    <li className={ `team-lineup__item-${ iIndex + 1 }` } key={ iIndex }>
                        { iPhoto ?
                            <PlaceholderImage
                                alt="Player Photo"
                                placeholder={ playerPlaceholder }
                                src={ iPhoto }
                            />
                            :
                            <span className="team-lineup__empty" />
                        }
                        <p key={ iIndex }>
                            { _t( `lineups.lineup_item.position.${ positionsNamesList[iIndex] }` ) }
                        </p>
                    </li>
                ) }
            </ul>
            <div className="team-lineup__line" />
        </article>
    );
};


LineupPhotos.propTypes = {
    photos: PropTypes.array,
    positionsNamesList: PropTypes.array.isRequired,
    _t: PropTypes.func.isRequired,
};

export default LineupPhotos;
