import React, { Component, PropTypes } from 'react';
import classname from 'classnames';

import Page from '../page';
import { EmptyLineup } from './EmptyLineup';
import { FilledLineups } from './FilledLineups';

import './style.scss';


class LineupsComponent extends Component {

    static propTypes = {
        currentSport: PropTypes.string.isRequired,
        lineups: PropTypes.array.isRequired,
        onCreateGame: PropTypes.func.isRequired,
        onCreateLineup: PropTypes.func.isRequired,
        onDeleteLineup: PropTypes.func.isRequired,
        onDuplicateLineup: PropTypes.func.isRequired,
        onViewLineup: PropTypes.func.isRequired,
        salarycapBudget: PropTypes.number.isRequired,
        _t: PropTypes.func.isRequired,
    };

    state = {
        activeId: null,
    };

    handleActiveChanged = ( activeId ) => {
        this.setState( { activeId } );
    };

    handleViewLineup = ( id ) => {
        this.props.onViewLineup( id );
    };

    handleNewLineup = ( button ) => () => {
        this.props.onCreateLineup( button );
    };

    componentDidMount () {
        document.body.scrollTop = 0;
    }

    render () {
        const { activeId } = this.state;
        const { lineups, _t, onCreateGame, onDeleteLineup, onDuplicateLineup, salarycapBudget } = this.props;
        const hasLineups = lineups.length > 0;

        return (
            <Page
                className={ classname( 'lineups', {
                    empty: !hasLineups,
                } ) }
            >
                { hasLineups ?
                    <FilledLineups
                        _t={ _t }
                        activeId={ activeId }
                        handleActiveChanged={ this.handleActiveChanged }
                        handleNewLineup={ this.handleNewLineup }
                        handleViewLineup={ this.handleViewLineup }
                        lineups={ lineups }
                        onCreateGame={ onCreateGame }
                        onDeleteLineup={ onDeleteLineup }
                        onDuplicateLineup={ onDuplicateLineup }
                        salarycapBudget={ salarycapBudget }
                    />
                    :
                    <EmptyLineup
                        _t={ _t }
                        handleNewLineup={ this.handleNewLineup }
                    />
                }
            </Page>
        );
    }
}

export default LineupsComponent;
