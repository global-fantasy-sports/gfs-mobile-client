import React, { Component, PropTypes } from 'react';

import OnboardingContainer from '../../containers/OnboardingContainer';
import LobbyOnboarding from '../../components/onboarding/LobbyOnboarding';
import Page from '../page';
import { COMPETITION_ROOM_GROUPS, COMPETITION_ROOM_TYPE_EXPLANATIONS } from '../../constants/competition';
import CompetitionRoomGroup from '../competition-room-group';
import RoomsFilter from '../../wizard/components/RoomsFilter';
import Btn from '../../ui/btn/Btn';
import ScrollLoader from '../scroll-loader';

import './style.scss';
import './slider.scss';

class LobbyComponent extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        competitionRooms: PropTypes.array.isRequired,
        currencies: PropTypes.arrayOf( PropTypes.string ).isRequired,
        entryFeeRange: PropTypes.object.isRequired,
        filters: PropTypes.object.isRequired,
        isOpen: PropTypes.bool.isRequired,
        league: PropTypes.string.isRequired,
        onEntryFeeChanged: PropTypes.func.isRequired,
        onGetGames: PropTypes.func.isRequired,
        onFilterChanged: PropTypes.func.isRequired,
        onFilterReset: PropTypes.func.isRequired,
        onGoToRoom: PropTypes.func.isRequired,
        onShowRulesDialog: PropTypes.func.isRequired,
        onToggleFilterPanel: PropTypes.func.isRequired,
        season: PropTypes.object.isRequired,
        showExplanation: PropTypes.bool.isRequired,
        sport: PropTypes.string.isRequired,
        hideExplanation: PropTypes.func.isRequired,
    };

    componentDidMount () {
        document.body.scrollTop = 0;
        this.props.onGetGames();
    }

    handleFilterToggle = () => this.props.onToggleFilterPanel( this.props.sport, this.props.league );

    handleRoomClick = ( roomId ) => this.props.onGoToRoom( roomId );

    handleShowRules = () => this.props.onShowRulesDialog();

    handleOnboardingClose = () => this.props.hideExplanation();

    renderCompetitions = () =>
        Object.keys( COMPETITION_ROOM_GROUPS ).map( group => {
            const groupedRooms = this.props.competitionRooms.filter( room => room.type === group );
            const explanation = COMPETITION_ROOM_TYPE_EXPLANATIONS[group];
            return (
                <CompetitionRoomGroup
                    _t={ this.props._t }
                    explanation={ explanation }
                    key={ group }
                    onRoomClick={ this.handleRoomClick }
                    page="lobby"
                    rooms={ groupedRooms }
                    season={ this.props.season }
                    showExplanation={ this.props.showExplanation }
                    type={ group }
                />
            );
        } );

    render () {
        const {
            _t,
            competitionRooms,
            currencies,
            entryFeeRange,
            filters,
            isOpen,
            league,
            onEntryFeeChanged,
            onFilterChanged,
            onFilterReset,
            season,
            sport,
        } = this.props;

        if ( season.period === 'notready' ) {
            return (
                <Page className="lobby">
                    <ScrollLoader />
                </Page>
            );
        }

        const total = Object.keys( competitionRooms ).length;
        const firstRoomToOnboarding =
            competitionRooms.find( ( room ) => room.prize_thumb ) || competitionRooms[0];

        return (
            <Page className="lobby">
                <article className="pane-filters">
                    <div className="pane-header">
                        { season.period === 'weekrun' ?
                            <Btn
                                className="filterOpener"
                                icon={ isOpen ? 'plus cross' : 'competition-filters' }
                                onClick={ this.handleFilterToggle }
                                square
                            />
                        : null }
                        <div className="competition-title">
                            <h2>{ _t( 'lobby.header.title.competition' ) }</h2>
                            { season.period === 'weekrun' ?
                            <h4>
                                { _t( 'lobby.header.title.total' ) }: <span>{ total }</span>
                            </h4>
                            :
                            <div className="emptyrooms">
                                <div className="caption">
                                { season.period === 'midseason' ?
                                    _t( 'lobby.header.emptyrooms.midseason' )
                                :
                                    _t( 'lobby.header.emptyrooms.noresults' )
                                }
                                </div>

                                <Btn
                                    label={ _t( 'info_popup.btn_rules' ) }
                                    onClick={ this.handleShowRules }
                                    size="normal"
                                    type="default"
                                />
                            </div>
                             }
                        </div>
                    </div>
                    { season.period === 'weekrun' ?
                        <RoomsFilter
                            _t={ _t }
                            currencies={ currencies }
                            entryFeeRange={ entryFeeRange }
                            filters={ filters }
                            isOpen={ isOpen }
                            league={ league }
                            onEntryFeeChanged={ onEntryFeeChanged }
                            onFilterChanged={ onFilterChanged }
                            onFilterReset={ onFilterReset }
                            sport={ sport }
                            total={ total }
                        />
                    : null }
                </article>
                <article className="pane-rooms">
                    { this.renderCompetitions() }
                </article>
                { total > 0 &&
                    <OnboardingContainer
                        label={ _t( 'onboarding.btn_got_it' ) }
                        name="lobby"
                        onClose={ this.handleOnboardingClose }
                    >
                        <LobbyOnboarding
                            _t={ _t }
                            room={ firstRoomToOnboarding }
                        />
                    </OnboardingContainer>
                }
            </Page>
        );
    }
}

export default LobbyComponent;
