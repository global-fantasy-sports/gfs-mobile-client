import React, { Component } from 'react';

import { Input } from '../input';
import Btn from '../../ui/btn/Btn';

import './style.scss';

const initialState = {
    renderForm: false,
};

class LoginComponent extends Component {

    static propTypes = {
        fields: React.PropTypes.object.isRequired,
        handleSubmit: React.PropTypes.func.isRequired,
        signIn: React.PropTypes.func.isRequired,
        serverError: React.PropTypes.string,
        facebookAPIPath: React.PropTypes.string.isRequired,
        googleAPIPath: React.PropTypes.string.isRequired,
    };

    constructor ( props ) {
        super( props );
        this.state = initialState;
    }

    renderWithError ( iBlock, iValue ) {
        return (
            <div className={ `${iValue.touched && iValue.invalid ? 'error' : ''}` }>
                <div className="error-text">{ iValue.touched ? iValue.error : '' }</div>
                { iBlock }
            </div>
        );
    }

    toggleForm = () => {
        this.setState( { renderForm: !this.state.renderForm } );
    };

    renderFormPlaceholder () {
        return (
            <div
                className="form-placeholder"
                onClick={ this.toggleForm }
            >
                Your email
            </div>
        );
    }

    renderForm () {
        const { handleSubmit, signIn, serverError } = this.props;
        const { fields: { email, password } } = this.props;
        return (
            <form className="form-block" noValidate onSubmit={ handleSubmit( signIn ) } >
                <div className={ `${ serverError ? 'error' : ''}` }>
                    <div className="error-text">{ serverError || '' }</div>
                </div>
                { this.renderWithError(
                    <Input extraProps={ email } maxLength="50" placeholder="EMAIL" type="email" />,
                    email
                ) }
                { this.renderWithError(
                    <Input
                        extraProps={ password }
                        maxLength="20"
                        placeholder="PASSWORD"
                        type="password"
                    />,
                    password
                ) }
                <Btn
                    block
                    label="Go"
                    size="large"
                    submit
                    type="primary"
                />
            </form>
        );
    }

    render () {
        const { facebookAPIPath, googleAPIPath } = this.props;
        return (
            <section className="login">
                {process.env.WITH_SOCIAL === 'yes' ?
                    <div className="social-buttons">
                        <a className="btn btn-social btn-google" href={ googleAPIPath } />
                        <a className="btn btn-social btn-fb" href={ facebookAPIPath } />
                    </div>
                : null}
                { this.state.renderForm
                    ? this.renderForm()
                    : this.renderFormPlaceholder() }
            </section>
        );
    }
}

export default LoginComponent;
