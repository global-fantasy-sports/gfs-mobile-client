import React, { PropTypes } from 'react';

const NotificationsToggle = ( { unreadCount, onToggle } ) => (
    <article className="message-toggler">
        <div className="message-bell" onTouchTap={ onToggle }>
            { unreadCount > 0 ? <i className="icon-notes-count">{ unreadCount }</i> : null }
        </div>
    </article>
);

NotificationsToggle.propTypes = {
    onToggle: PropTypes.func.isRequired,
    unreadCount: PropTypes.number.isRequired,
};

export default NotificationsToggle;
