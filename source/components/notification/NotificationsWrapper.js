import React, { PropTypes } from 'react';
import CSSTransitionGroup from 'react-addons-css-transition-group';
import cx from 'classnames';

import NotificationsComponent from './index';

const NotificationsWrapper = ( {
           _t,
           loadNextNotifications,
           notifications,
           sendReadNotifications,
           showNotifications,
           toggleNotifications,
        } ) => (
    <CSSTransitionGroup
        component="div"
        transitionEnterTimeout={ 200 }
        transitionLeaveTimeout={ 200 }
        transitionName="notification-overlay"
    >
        { showNotifications ? <div className={ cx( 'notification-wrapper', { active: showNotifications } ) }>
                <NotificationsComponent
                    _t={ _t }
                    loadNextNotifications={ loadNextNotifications }
                    notifications={ notifications }
                    sendReadNotifications={ sendReadNotifications }
                    showNotifications={ showNotifications }
                    toggleNotifications={ toggleNotifications }
                />
            </div> : null }
    </CSSTransitionGroup>
);

NotificationsWrapper.propTypes = {
    _t: PropTypes.func.isRequired,
    loadNextNotifications: PropTypes.func.isRequired,
    notifications: PropTypes.array,
    sendReadNotifications: PropTypes.func.isRequired,
    showNotifications: PropTypes.bool.isRequired,
    toggleNotifications: PropTypes.func.isRequired,
};

export default NotificationsWrapper;
