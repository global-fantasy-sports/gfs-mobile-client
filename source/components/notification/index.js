import React, { Component, PropTypes } from 'react';
import cx from 'classnames';
import _ from 'lodash';

import { unixDateToLocalCalendar } from '../../utils/date';
import Confetti from '../common/Confetti';

import { TIME_FORMAT } from '../../constants/app';

import './style.scss';

const defaultCount = 10;

const initialState = {
    displayed: defaultCount,
    expanded: false,
};

class NotificationsComponent extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        loadNextNotifications: PropTypes.func.isRequired,
        notifications: PropTypes.array.isRequired,
        sendReadNotifications: PropTypes.func.isRequired,
        showNotifications: PropTypes.bool.isRequired,
        toggleNotifications: PropTypes.func.isRequired,
    };

    constructor () {
        super();
        this.state = initialState;
        this.expandList = ::this.expandList;
        this.loadNext = ::this.loadNext;
        this.handleNotifications = ::this.handleNotifications;
        this.timeout = null;
    }

    componentDidMount () {
        document.body.classList.toggle( 'no-scroll' );
    }

    componentWillUnmount () {
        document.body.classList.toggle( 'no-scroll' );
        setTimeout( () => this.unmarkRead() );
    }

    unmarkRead () {
        const ids = this.props.notifications
            .filter( iNotification => !iNotification.read )
            .map( iNotification => iNotification.id );
        this.props.sendReadNotifications( { ids } );
    }

    handleNotifications () {
        if ( this.props.showNotifications ) {
            this.unmarkRead();
        }
        this.props.toggleNotifications();
        this.setState( { displayed: defaultCount, expanded: false } );
    }

    expandList () {
        this.setState( { displayed: this.state.displayed + defaultCount, expanded: true } );
    }

    loadNext () {
        this.props.loadNextNotifications( _.last( this.props.notifications ).id );
        this.expandList();
    }

    renderItem ( iNotification, index ) {
        return (
            <li
                className={ `item ${ iNotification.read ? '' : 'unread' }` }
                key={ index }
                style={ { position: 'relative' } }
            >
                <p className="item-date">
                    { unixDateToLocalCalendar( iNotification.createdAt, TIME_FORMAT.notification ) }
                </p>
                <p className="item-title">{ iNotification.title }</p>
                <p className="item-text" dangerouslySetInnerHTML={ { __html: iNotification.message } } />
                { !iNotification.read && iNotification.name === 'round-finished' ? <Confetti /> : null }
            </li>
        );
    }

    render () {
        const { _t, notifications } = this.props;
        const unreadNotifications = notifications.filter( iNotification => !iNotification.read );

        const { expanded } = this.state;
        let displayedCount = this.state.displayed;
        if ( !expanded && unreadNotifications.length ) {
            displayedCount = unreadNotifications.length;
        }
        const displayedNotifications = unreadNotifications.length && !expanded ? unreadNotifications : notifications;
        const isNotificationsClosed = displayedCount < notifications.length;
        let displayExpander = '';

        displayExpander = (
            <li><button className="btn btn-expand" onClick={ isNotificationsClosed ? this.expandList : this.loadNext }>
                <i />
            </button></li>
        );

        return (
            <article className={ cx( 'notification', { closed: isNotificationsClosed } ) }>
                <header className="head">
                    <p>
                        { _t( `notifications.title.new${ unreadNotifications.length === 1 ? '' : 's' }`,
                            { count: unreadNotifications.length } ) }
                    </p>
                    <i className="icon-cross" onClick={ this.handleNotifications } />
                </header>
                <ul className="list">
                    { displayedNotifications.slice( 0, displayedCount ).map( this.renderItem ) }
                    { displayExpander }
                    <li className={ cx( 'item-bottom-offset', { closed: isNotificationsClosed } ) } />
                </ul>
            </article>
        );
    }
}

export default NotificationsComponent;
