import React, { Component, PropTypes } from 'react';

import LineupInfo from '../../wizard/components/LineupInfo';
import HeaderContainer from '../../containers/HeaderContainer';
import { AvgArrow, BudgetArrow, PositionArrow } from './SvgOnboarding';
import { format } from '../../utils/number';

import './Onboarding.scss';


class DraftOnboarding extends Component {

    static propTypes = {
        salarycapBudget: PropTypes.number,
        _t: PropTypes.func,
    };

    componentDidMount () {
        document.body.classList.toggle( 'no-scroll' );
    }

    componentWillUnmount () {
        document.body.classList.toggle( 'no-scroll' );
    }

    render () {
        const { _t, salarycapBudget } = this.props;
        return (
            <section className="draft-onboarding">
                <HeaderContainer />
                <div className="inner">
                    <div className="wizard-header">
                        <h3 className="title">
                            { _t( 'onboarding.draft.title.draft-player' ) }
                        </h3>
                        <div className="lineup-wrap-onboarding">
                            <LineupInfo
                                _t={ this.props._t }
                                avgFPPG={ 12.9 }
                                budgetLeft={ 30600 }
                                budgetMax={ salarycapBudget }
                                className="wizard-header__row"
                                wizardViewMode="pitch"
                            />
                            <div className="tip tip-budget">
                                <BudgetArrow />
                                <p>{ _t( 'onboarding.draft.tip.budget',
                                    { salarycapBudget: format( salarycapBudget ) } ) }</p>
                            </div>
                            <div className="tip tip-avg">
                                <AvgArrow />
                                <p>{ _t( 'onboarding.draft.tip.avg' ) }</p>
                            </div>
                        </div>
                    </div>
                    <div className="tip tip-position">
                        <i className="position-icon">
                            { _t( 'common.position_abbr_label.fw1' ) }
                        </i>
                        <PositionArrow />
                        <p>{ _t( 'onboarding.draft.tip.position-tap' ) }</p>
                        <p>{ _t( 'onboarding.draft.tip.position-draft' ) }</p>
                    </div>
                    <p className="tip tip-bottom">{ _t( 'onboarding.draft.tip.quick-pick' ) }</p>
                </div>
            </section>
        );
    }
}

export default DraftOnboarding;
