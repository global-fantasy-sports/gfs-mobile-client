import React, { Component, PropTypes } from 'react';

import './Onboarding.scss';
import HeaderContainer from '../../containers/HeaderContainer';
import Money from '../common/Money';
import { format } from '../../utils/number';
import { FPPGArrow, OpponentArrow, OPRKArrow, SalaryArrow, SwipeArrow } from './SvgOnboarding';

const TeamSection = ( { _t, count, teamId, match } ) => {
    if ( !match ) {
        return (
            <div className="team">
                <div className="team-names">
                    <div className="team-names-top">
                        <p>{ _t( teamId ) }</p>
                    </div>
                </div>
            </div>
        );
    }
    const [team, oppTeam] = match.homeTeam === teamId ?
        [match.homeTeam, match.awayTeam] : [match.awayTeam, match.homeTeam];
    return (
        <div className="team">
            <div className="team-names">
                <div className="team-names-top">
                    <p>{ _t( team ) }</p>
                    { count > 0 && <p className="team-quantity">{ count }/4</p> }
                </div>
                <p className="team-names-bottom">
                    { _t( 'onboarding.draft_step.participant.vs' ) } { _t( oppTeam ) }
                </p>
            </div>
        </div>
    );
};
TeamSection.propTypes = {
    _t: PropTypes.func,
    count: PropTypes.number,
    match: PropTypes.object,
    teamId: PropTypes.string,
};


const FirstParticipant = (
    {
        FPPG,
        _t,
        count,
        isFavorite,
        match,
        nameKnown,
        oprk,
        salary,
        teamId,
    }
) => {
    return (
        <div className="wizard-entry">
            <div className="heading">
                { isFavorite ? <i className="favorite" /> : '' }
                <div className="name">
                    <p>{ _t( nameKnown ) }</p>
                </div>
                <TeamSection
                    _t={ _t }
                    count={ count }
                    match={ match }
                    teamId={ teamId }
                />
            </div>
            <div className="column oprk">
                <div className="label">{ _t( 'wizard.draft.list_entry.oprk' ) }</div>
                <div className="value">{ oprk }</div>
            </div>
            <div className="column fppg">
                <div className="label">{ _t( 'wizard.draft.list_entry.fppg' ) }</div>
                <div className="value">{ format( FPPG, 2, 'N/A' ) }</div>
            </div>
            <div className="column">
                <div className="label">{ _t( 'wizard.draft.list_entry.salary' ) }</div>
                <div className="value">
                    <Money currency="usd" value={ salary } />
                </div>
            </div>
        </div>
    );
};
FirstParticipant.propTypes = {
    FPPG: PropTypes.number.isRequired,
    _t: PropTypes.func.isRequired,
    count: PropTypes.number.isRequired,
    isFavorite: PropTypes.bool,
    match: PropTypes.object,
    nameKnown: PropTypes.string,
    oprk: PropTypes.number.isRequired,
    salary: PropTypes.number.isRequired,
    teamId: PropTypes.string.isRequired,
};


class DraftStepOnboarding extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        count: PropTypes.number.isRequired,
        participant: PropTypes.object,
    };

    render () {
        const { _t, count, participant } = this.props;
        const { position, FPPG, teamId, isFavorite, match, nameFirst, nameKnown, nameLast, oprk, salary } = participant;
        return (
            <section className="draftstep-onboarding">
                <HeaderContainer />
                <h3 className="draftstep-onboarding__title">
                    { _t( 'wizard.draft.header.text_select' ) }
                    &nbsp;
                    <span>
                        { _t( `roster.position_label.${ position }` ) }
                    </span>
                </h3>
                <div className="inner">
                    <div className="tip tip-opponent">
                        <p>
                            { match ?
                                _t( 'onboarding.draft_step.tip.opponent' ) :
                                _t( 'onboarding.draft_step.tip.players_team' )
                            }
                        </p>
                        <OpponentArrow />
                    </div>
                    <div className="tip tip-swipe">
                        <p>{ _t( 'onboarding.draft_step.tip.swipe' ) }</p>
                        <SwipeArrow />
                    </div>
                    <div className="tip tip-fppg">
                        <p>{ _t( 'onboarding.draft_step.tip.fppg' ) }</p>
                        <FPPGArrow />
                    </div>
                    <FirstParticipant
                        FPPG={ FPPG }
                        _t={ _t }
                        count={ count }
                        isFavorite={ isFavorite }
                        match={ match }
                        nameFirst={ nameFirst }
                        nameKnown={ nameKnown }
                        nameLast={ nameLast }
                        oprk={ oprk }
                        salary={ salary }
                        teamId={ teamId }
                    />
                    <div className="tip tip-oprk">
                        <p>{ _t( 'onboarding.draft_step.tip.oprk' ) }</p>
                        <OPRKArrow />
                    </div>
                    <div className="tip tip-salary">
                        <p>{ _t( 'onboarding.draft_step.tip.salary' ) }</p>
                        <SalaryArrow />
                    </div>
                </div>
            </section>
        );
    }
}

export default DraftStepOnboarding;
