import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';

import './Onboarding.scss';
import HeaderContainer from '../../containers/HeaderContainer';
import CompetitionRoom from '../competition-room/index';
import {
    EntryCostArrow,
    FirstPlaceArrow,
    GameBeginsArrow,
    TopRightArrow,
    TotalPrizeArrow,
} from './SvgOnboarding';

class EntryFeeOnboarding extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        onboardingRoom: PropTypes.object.isRequired,
    };

    render () {
        const { _t, onboardingRoom } = this.props;
        return (
            <section className="entryfee-oboarding">
                <HeaderContainer />
                <h3 className="entryfee-oboarding__title">
                    { _t( 'onboarding.entryfee.title' ) }
                </h3>
                <p className="entryfee-oboarding__sub-title">
                    { _t( 'onboarding.entryfee.sub-title' ) }
                </p>
                <div className="room-wrap">
                    <div className="tip tip-total-prize">
                        <p>{ _t( 'onboarding.lobby.arrow.prize' ) }</p>
                        <TotalPrizeArrow />
                    </div>
                    <div
                        className={ classnames( 'tip tip-entry-cost', {
                            'no-prize-thumb': !onboardingRoom.prize_thumb,
                        } ) }
                    >
                        <p>{ _t( 'onboarding.lobby.arrow.cost' ) }</p>
                        <EntryCostArrow />
                    </div>
                    <div className="tip tip-game-begins">
                        <p>{ _t( 'onboarding.lobby.arrow.starts' ) }</p>
                        <GameBeginsArrow />
                    </div>
                    <CompetitionRoom
                        _t={ _t }
                        onClick={ () => {} }
                        page="lobby-onboarding"
                        room={ onboardingRoom }
                    />
                    { onboardingRoom.prize_thumb ?
                        <div className="tip tip-get-prize">
                            <TopRightArrow />
                            <p>{ _t( 'onboarding.lobby.arrow.get-prize' ) }</p>
                        </div>
                        : null
                    }
                    <div
                        className={ classnames( 'tip tip-first-place', {
                            'no-prize-thumb': !onboardingRoom.prize_thumb,
                        } ) }
                    >
                        <FirstPlaceArrow />
                        <p>{ _t( 'onboarding.lobby.arrow.first-place_prize' ) }</p>
                    </div>
                    <div className="tip tip-players-count">
                        <TopRightArrow />
                        <p>{ _t( 'onboarding.lobby.arrow.playing' ) }</p>
                    </div>
                </div>
            </section>
        );
    }
}

export default EntryFeeOnboarding;
