import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';

import HeaderContainer from '../../containers/HeaderContainer';
import CompetitionRoom from '../competition-room';

import './Onboarding.scss';
import {
    EntryCostArrow,
    FirstPlaceArrow,
    GameBeginsArrow,
    TopRightArrow,
    TotalPrizeArrow,
} from './SvgOnboarding';


const noop = () => {};

class LobbyOnboarding extends Component {

    componentDidMount () {
        document.body.classList.add( 'no-scroll' );
    }

    componentWillUnmount () {
        document.body.classList.remove( 'no-scroll' );
    }

    render () {
        const { room, _t } = this.props;

        return (
            <section className="lobby-onboarding">
                <HeaderContainer />
                <div className="lobby-onboarding__title">
                    { _t( 'onboarding.lobby.title.competitions' ) }
                </div>
                <p className="lobby-onboarding__para">
                    { _t( 'onboarding.lobby.title.first' ) }
                </p>
                <p className="lobby-onboarding__para">
                    { _t( 'onboarding.lobby.title.second' ) }
                </p>
                <p className="lobby-onboarding__para">
                    { _t( 'onboarding.lobby.title.third' ) }
                </p>
                <div className="room-wrap">
                    <div className="tip tip-total-prize">
                        <p>{ _t( 'onboarding.lobby.arrow.prize' ) }</p>
                        <TotalPrizeArrow />
                    </div>
                    <div
                        className={ classnames( 'tip tip-entry-cost', {
                            'no-prize-thumb': !room.prize_thumb,
                        } ) }
                    >
                        <p>{ _t( 'onboarding.lobby.arrow.cost' ) }</p>
                        <EntryCostArrow />
                    </div>
                    <div className="tip tip-game-begins">
                        <p>{ _t( 'onboarding.lobby.arrow.starts' ) }</p>
                        <GameBeginsArrow />
                    </div>
                    <CompetitionRoom
                        _t={ _t }
                        onClick={ noop }
                        page="lobby-onboarding"
                        room={ room }
                    />
                    { room.prize_thumb ?
                        <div className="tip tip-get-prize">
                            <TopRightArrow />
                            <p>{ _t( 'onboarding.lobby.arrow.get-prize' ) }</p>
                        </div>
                        : null
                    }
                    <div
                        className={ classnames( 'tip tip-first-place', {
                            'no-prize-thumb': !room.prize_thumb,
                        } ) }
                    >
                        <FirstPlaceArrow />
                        <p>{ _t( 'onboarding.lobby.arrow.first-place_prize' ) }</p>
                    </div>
                    <div className="tip tip-players-count">
                        <TopRightArrow />
                        <p>{ _t( 'onboarding.lobby.arrow.playing' ) }</p>
                    </div>
                </div>
            </section>
        );
    }
}

LobbyOnboarding.propTypes = {
    room: PropTypes.object.isRequired,
    _t: PropTypes.func.isRequired,
};

export default LobbyOnboarding;
