import React, { Component, PropTypes } from 'react';
import cx from 'classnames';
import Btn from '../../ui/btn/Btn';
import './Onboarding.scss';


class Onboarding extends Component {
    render () {
        const { visible, children, mod, label, salarycapBudget } = this.props;
        const cs = cx( 'onboarding-container', {
            [`oc-mod-${mod}`]: !!mod,
        } );

        if ( visible ) {
            const childrenWithProps = React.Children.map( children, ( child ) =>
                React.cloneElement( child, {
                    salarycapBudget,
                } )
            );

            return (
                <div className={ cs }>
                    <div className="onboarding-container__content">
                        {childrenWithProps}
                    </div>
                    <div className="onboarding-container__action-bar">
                        <Btn
                            block
                            label={ label }
                            onClick={ this.props.closeOnboarding }
                        />
                    </div>
                </div>
            );
        }

        return null;
    }
}

Onboarding.propTypes = {
    children: PropTypes.any.isRequired,
    closeOnboarding: PropTypes.func.isRequired,
    label: PropTypes.string,
    name: PropTypes.string.isRequired,
    mod: PropTypes.string,
    visible: PropTypes.bool.isRequired,
    salarycapBudget: PropTypes.number.isRequired,
};

export default Onboarding;
