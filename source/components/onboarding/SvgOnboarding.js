import React from 'react';
import './SvgOnboardingStyles.scss';


export const BudgetArrow = () => {
    return (
        <div className="svg-arrow-overlay budget">
            <svg
                style={ { enableBackground: 'new 0 0 24.2 43.1' } }
                version="1.1"
                viewBox="0 0 24.2 43.1"
                x="0px"
                xmlSpace="preserve"
                xmlns="http://www.w3.org/2000/svg"
                y="0px"
            >
                <g>
                    <polyline
                        className="st0"
                        points="9.6,4.1 22.7,1.5 20.3,15.6"
                    />
                    <path
                        className="st0"
                        d="M1.5,41.6c0,0-0.3-19.3,20.3-39.3"
                    />
                </g>
            </svg>
        </div>
    );
};

export const AvgArrow = () => {
    return (
        <div className="svg-arrow-overlay avg">
            <svg
                style={ { enableBackground: 'new 0 0 24.2 43.1' } }
                version="1.1"
                viewBox="0 0 24.2 43.1"
                x="0px"
                xmlSpace="preserve"
                xmlns="http://www.w3.org/2000/svg"
                y="0px"
            >
                <g>
                    <polyline
                        className="st0"
                        points="14.6,4.1 1.5,1.5 3.9,15.6"
                    />
                    <path
                        className="st0"
                        d="M22.7,41.6c0,0,0.3-19.3-20.3-39.3"
                    />
                </g>
            </svg>
        </div>
    );
};

export const PositionArrow = () => {
    return (
        <div className="svg-arrow-overlay position">
            <svg
                style={ { enableBackground: 'new 0 0 66.4 106.1' } }
                version="1.1"
                viewBox="0 0 66.4 106.1"
                x="0px"
                xmlSpace="preserve"
                xmlns="http://www.w3.org/2000/svg"
                y="0px"
            >
                <g>
                    <path className="st0" d="M28.9,104.6C86,80,73.4,13.3,3.4,7.9" />
                    <polyline className="st0" points="11.2,16.9 1.5,7.7 14.3,1.5" />
                </g>
            </svg>
        </div>
    );
};

export const OpponentArrow = () => {
    return (
        <div className="svg-arrow-overlay">
            <svg
                style={ { enableBackground: 'new 0 0 63.8 102.3' } }
                version="1.1"
                viewBox="0 0 63.8 102.3"
                x="0px"
                xmlSpace="preserve"
                xmlns="http://www.w3.org/2000/svg"
                y="0px"
            >
                <g>
                    <polyline className="st0" points="62.3,89.7 54.8,100.8 46.6,89.2" />
                    <path className="st0" d="M1.5,1.5c0,0,53.4,34.4,53.4,98.2" />
                </g>
            </svg>
        </div>
    );
};
export const SwipeArrow = () => {
    return (
        <div className="svg-arrow-overlay">
            <svg
                style={ { enableBackground: 'new 0 0 33.5 169' } }
                version="1.1"
                viewBox="0 0 33.5 169"
                x="0px"
                xmlSpace="preserve"
                xmlns="http://www.w3.org/2000/svg"
                y="0px"
            >
                <g>
                    <polyline className="st0" points="32,151.9 31.8,167.5 16.5,163.5" />
                    <path className="st0" d="M22,1.5c-35.9,45.3-19,129,8,163.5" />
                </g>
            </svg>
        </div>
    );
};
export const FPPGArrow = () => {
    return (
        <div className="svg-arrow-overlay">
            <svg
                style={ { enableBackground: 'new 0 0 23.8 72.3' } }
                version="1.1"
                viewBox="0 0 23.8 72.3"
                x="0px"
                xmlSpace="preserve"
                xmlns="http://www.w3.org/2000/svg"
                y="0px"
            >
                <g>
                    <polyline className="st0" points="22.3,59.7 14.8,70.8 6.6,59.2" />
                    <path className="st0" d="M1.5,1.5c0,0,13.4,26.4,13.4,68.2" />
                </g>
            </svg>
        </div>
    );
};
export const OPRKArrow = () => {
    return (
        <div className="svg-arrow-overlay">
            <svg
                style={ { enableBackground: 'new 0 0 119.3 70.4' } }
                version="1.1"
                viewBox="0 0 119.3 70.4"
                x="0px"
                xmlSpace="preserve"
                xmlns="http://www.w3.org/2000/svg"
                y="0px"
            >
                <g>
                    <polyline className="st0" points="102.3,7.7 116.1,1.5 117.8,16.6" />
                    <path className="st0" d="M1.5,68.9C71.7,68.9,113,13.4,114,4.4" />
                </g>
            </svg>
        </div>
    );
};
export const SalaryArrow = () => {
    return (
        <div className="svg-arrow-overlay">
            <svg
                style={ { enableBackground: 'new 0 0 41.3 60.4' } }
                version="1.1"
                viewBox="0 0 41.3 60.4"
                x="0px"
                xmlSpace="preserve"
                xmlns="http://www.w3.org/2000/svg"
                y="0px"
            >
                <g>
                    <polyline className="st0" points="23.3,10.7 34.1,1.5 39.8,15.6" />
                    <path className="st0" d="M1.5,58.9C22.5,44.9,32,13.4,33,4.4" />
                </g>
            </svg>
        </div>
    );
};

export const ArrowLeftToRight = () => {
    return (
        <div className="svg-arrow-overlay left-to-right">
            <svg
                style={ { enableBackground: 'new 0 0 326.2 123.8' } }
                version="1.1"
                viewBox="0 0 326.2 123.8"
                x="0px"
                xmlSpace="preserve"
                xmlns="http://www.w3.org/2000/svg"
                y="0px"
            >
                <g>
                    <g>
                        <g>
                            <line className="st0" x1="1.5" x2="1.5" y1="6.5" y2="11.5" />
                            <path
                                className="st1"
                                d="M1.5,21.4v18.4c0,14.5,11.8,26.3,26.3,26.3h261.4c14.5,0,26.3,11.8,26.3,26.3v10.1"
                            />
                            <line className="st0" x1="315.5" x2="315.5" y1="107.5" y2="112.5" />
                        </g>
                    </g>
                    <polyline className="st2" points="324.7,113.1 315.5,122.3 306.3,113.1" />
                    <line className="st2" x1="315.5" x2="315.5" y1="109.1" y2="118.8" />
                    <line className="st2" x1="1.5" x2="1.5" y1="1.5" y2="11.2" />
                </g>
            </svg>
        </div>
    );
};

export const ArrowRightToLeft = () => {
    return (
        <div className="svg-arrow-overlay right-to-left">
            <svg
                style={ { enableBackground: 'new 0 0 326.2 123.8' } }
                version="1.1"
                viewBox="0 0 326.2 123.8"
                x="0px"
                xmlSpace="preserve"
                xmlns="http://www.w3.org/2000/svg"
                y="0px"
            >
                <g>
                    <g>
                        <g>
                            <line className="st0" x1="324.7" x2="324.7" y1="11.5" y2="6.5" />
                            <path
                                className="st1"
                                d="M10.7,102.5V92.4c0-14.5,11.8-26.3,26.3-26.3h261.4c14.5,0,26.3-11.8,26.3-26.3V21.4"
                            />
                            <line className="st0" x1="10.7" x2="10.7" y1="112.5" y2="107.5" />
                        </g>
                    </g>
                    <polyline className="st2" points="19.9,113.1 10.7,122.3 1.5,113.1" />
                    <line className="st2" x1="10.7" x2="10.7" y1="118.8" y2="109.1" />
                    <line className="st2" x1="324.7" x2="324.7" y1="11.2" y2="1.5" />
                </g>
            </svg>
        </div>
    );
};

export const ArrowShort = () => {
    return (
        <div className="svg-arrow-overlay short">
            <svg
                style={ { enableBackground: 'new 0 0 146.2 123.8' } }
                version="1.1"
                viewBox="0 0 146.2 123.8"
                x="0px"
                xmlSpace="preserve"
                xmlns="http://www.w3.org/2000/svg"
                y="0px"
            >
                <g>
                    <g>
                        <g>
                            <line className="st0" x1="144.7" x2="144.7" y1="11.5" y2="6.5" />
                            <path
                                className="st1"
                                d="M10.7,102.6V92.5C10.7,78,22.5,66.2,37,66.2h81.4c14.5,0,26.3-11.8,26.3-26.3V21.4"
                            />
                            <line className="st0" x1="10.7" x2="10.7" y1="112.5" y2="107.5" />
                        </g>
                    </g>
                    <polyline className="st2" points="19.9,113.1 10.7,122.3 1.5,113.1" />
                    <line className="st2" x1="10.7" x2="10.7" y1="118.8" y2="109.1" />
                    <line className="st2" x1="144.7" x2="144.7" y1="11.2" y2="1.5" />
                </g>
            </svg>
        </div>
    );
};

export const TotalPrizeArrow = () => {
    return (
        <div className="svg-arrow-overlay">
            <svg
                style={ { enableBackground: 'new 0 0 19.2 33.1' } }
                version="1.1"
                viewBox="0 0 19.2 33.1"
                x="0px"
                xmlSpace="preserve"
                xmlns="http://www.w3.org/2000/svg"
                y="0px"
            >
                <g>
                    <polyline className="st0" points="4.6,29 17.7,31.6 15.3,17.5" />
                    <path className="st0" d="M1.5,1.5c0,0,1.2,16.7,15.3,29.3" />
                </g>
            </svg>
        </div>
    );
};
export const EntryCostArrow = () => {
    return (
        <div className="svg-arrow-overlay">
            <svg
                style={ { enableBackground: 'new 0 0 56.2 124' } }
                version="1.1"
                viewBox="0 0 56.2 124"
                x="0px"
                xmlSpace="preserve"
                xmlns="http://www.w3.org/2000/svg"
                y="0px"
            >
                <g>
                    <polyline className="st0" points="54.7,110.1 45.5,122.5 35,111.2" />
                    <path className="st0" d="M1.5,1.5c0,0,44,49.4,44,120" />
                </g>
            </svg>
        </div>
    );
};
export const GameBeginsArrow = () => {
    return (
        <div className="svg-arrow-overlay">
            <svg
                style={ { enableBackground: 'new 0 0 19.6 123.7' } }
                version="1.1"
                viewBox="0 0 19.6 123.7"
                x="0px"
                xmlSpace="preserve"
                xmlns="http://www.w3.org/2000/svg"
                y="0px"
            >
                <g>
                    <polyline className="st0" points="1.5,107.5 1.5,122.2 16.1,119.4" />
                    <path className="st0" d="M3.5,1.5c28.7,44,7.3,113,0,118.4" />
                </g>
            </svg>
        </div>
    );
};
export const TopRightArrow = () => {
    return (
        <div className="svg-arrow-overlay">
            <svg
                style={ { enableBackground: 'new 0 0 24.2 54.1' } }
                version="1.1"
                viewBox="0 0 24.2 54.1"
                x="0px"
                xmlSpace="preserve"
                xmlns="http://www.w3.org/2000/svg"
                y="0px"
            >
                <g>
                    <polyline className="st0" points="9.6,4.1 22.7,1.5 20.3,15.6" />
                    <path className="st0" d="M1.5,52.6c0,0-0.3-30.3,20.3-50.3" />
                </g>
            </svg>
        </div>
    );
};
export const FirstPlaceArrow = () => {
    return (
        <div className="svg-arrow-overlay">
            <svg
                style={ { enableBackground: 'new 0 0 19.6 133.7' } }
                version="1.1"
                viewBox="0 0 19.6 133.7"
                x="0px"
                xmlSpace="preserve"
                xmlns="http://www.w3.org/2000/svg"
                y="0px"
            >
                <g>
                    <polyline className="st0" points="18.1,16.2 15.1,1.5 1.5,8.3" />
                    <path className="st0" d="M17.1,132.2c-28.7-44-10.3-123-3-128.4" />
                </g>
            </svg>
        </div>
    );
};

export const AddFavoritesArrow = () => {
    return (
        <AvgArrow />
    );
};
