import React, { Component, PropTypes } from 'react';

import LineupInfo from '../../wizard/components/LineupInfo';
import whistleImage from './images/whistle.png';
import teamImage from './images/team.png';
import cupImage from './images/cup.png';

import './Onboarding.scss';
import { ArrowLeftToRight, ArrowRightToLeft, ArrowShort } from './SvgOnboarding';

class WelcomeOnboarding extends Component {

    static propTypes = {
        salarycapBudget: PropTypes.number,
        _t: PropTypes.func.isRequired,
    };

    componentDidMount () {
        document.body.classList.toggle( 'no-scroll' );
    }

    componentWillUnmount () {
        document.body.classList.toggle( 'no-scroll' );
    }
    render () {
        const { _t, salarycapBudget } = this.props;
        return (
            <article className="welcome-onboarding">
                <div className="inner">
                    <section className="title">
                        <h1>{ _t( 'onboarding.welcome.title.welcome' ) }</h1>
                        <p>{ _t( 'onboarding.welcome.title.fs' ) }</p>
                    </section>
                    <section className="item whistle">
                        <img alt="" className="image" src={ whistleImage } />
                        <p className="desc">
                            { _t( 'onboarding.welcome.item.manager' ) }
                        </p>
                        <ArrowLeftToRight />
                    </section>
                    <section className="item team">
                        <p className="desc">
                            { _t( 'onboarding.welcome.item.team' ) }
                        </p>
                        <img alt="" className="image" src={ teamImage } />
                        <ArrowRightToLeft />
                    </section>
                    <section className="item cup">
                        <img alt="" className="image" src={ cupImage } />
                        <p className="desc">
                            { _t( 'onboarding.welcome.item.cup' ) }
                        </p>
                        <ArrowLeftToRight />
                    </section>
                    <section className="item budget">
                        <p className="desc">
                            { _t( 'onboarding.welcome.item.budget' ) }
                        </p>
                        <div className="onboarding-budget-wrap">
                            <LineupInfo
                                _t={ _t }
                                avgFPPG={ 12.9 }
                                budgetLeft={ 30600 }
                                budgetMax={ salarycapBudget }
                                className="wizard-header__row"
                                wizardViewMode="pitch"
                            />
                        </div>
                        <ArrowShort />
                    </section>
                    <section className="bottom-title">
                        <p className="desc">
                            { _t( 'onboarding.welcome.bottom-title.top' ) }
                        </p>
                        <h1>{ _t( 'onboarding.welcome.bottom-title.win' ) }</h1>
                    </section>
                </div>
            </article>
        );
    }
}

export default WelcomeOnboarding;
