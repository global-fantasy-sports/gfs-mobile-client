import React, { PropTypes } from 'react';

import { AddFavoritesArrow } from './SvgOnboarding';
import withoutFavoritesImage from './images/without-favotites.png';
import './Onboarding.scss';

export const WithoutFavoritesOnboarding = ( { _t } ) => {
    return (
        <div className="without-favorites">
            <p className="tip tip-head">{ _t( 'onboarding.without-favorites.tip-head' ) }</p>
            <img
                alt="No favorite players yet"
                className="no-favorites-img"
                src={ withoutFavoritesImage }
            />
            <div className="tip tip-bottom">
                <AddFavoritesArrow />
                <p>{ _t( 'onboarding.without-favorites.tip-bottom' ) }</p>
            </div>
        </div>
    );
};
WithoutFavoritesOnboarding.propTypes = {
    _t: PropTypes.func,
};
