import React, { Component } from 'react';

import './style.scss';

export default class Page extends Component {
    render () {
        const { className } = this.props;
        return (
            <div className={ `page ${className || ''}` }>
                { this.props.children }
            </div>
        );
    }
}

Page.propTypes = {
    children: React.PropTypes.any.isRequired,
    className: React.PropTypes.string.isRequired,
};
