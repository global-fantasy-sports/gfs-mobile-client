import React, { Component, PropTypes } from 'react';
import Page from '../page';

import RoutesConstants from '../../constants/routes';
import './style.scss';
import BtnLink from '../../ui/btn/BtnLink';

class Page404Component extends Component {
    static defaultProps = {
        showLobbyButton: true,
    };

    static propTypes = {
        _t: PropTypes.func.isRequired,
        showLobbyButton: PropTypes.bool,
    };

    constructor ( props ) {
        if ( !( 'showLobbyButton' in props ) ) {
            props.showLobbyButton = true;
        }
        super( props );
    }

    render () {
        const { _t, showLobbyButton } = this.props;
        return (
            <Page className="page-404">
                <div className="splash">
                    <h1>404</h1>
                    <p>{ _t( 'page_404.page-not-found' ) }</p>
                    { showLobbyButton ?
                    <BtnLink
                        label={ _t( 'page_404.btn-lobby' ) }
                        relativeToLeague
                        size="monster"
                        to={ RoutesConstants.get( 'lobbyPage' ) }
                        type="primary"
                    /> :
                    null }
                </div>
            </Page>
        );
    }
}

export default Page404Component;
