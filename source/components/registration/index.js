import React, { Component } from 'react';
import { Link } from 'react-router';

import RoutesConstants from '../../constants/routes';
import { Countries, USAStates } from '../../constants/countries';
import Page from '../page';
import { Input, Checkbox, Select } from '../input';

import './style.scss';

class RegistrationComponent extends Component {
    renderWithError ( iBlock, iValue ) {
        return (
            <div className={ `${iValue.touched && iValue.invalid ? 'error' : ''}` }>
                <div className="error-text">{ iValue.touched ? iValue.error : '' }</div>
                { iBlock }
            </div>
        );
    }

    renderCountrySelectors ( iCountry, iState ) {
        const wrapperError = ( iCountry.touched && iCountry.invalid ) || ( iState.touched && iState.invalid );
        const errorText = iCountry.error || iState.error;
        return (
            <div className={ `${wrapperError ? 'error' : ''}` }>
                <div className="selector-group">
                    <div className="error-text">{ iCountry.touched ? errorText : '' }</div>
                    <Select optionsList={ Countries } placeholder="Select country" { ...iCountry } />
                    <Select optionsList={ USAStates } placeholder="Select state" { ...iState } />
                </div>
            </div>
        );
    }

    render () {
        const {
            fields: { email, password, isEnabledTerms, name, country, state },
            handleSubmit,
            registerUser,
            processing,
            serverError,
            facebookAPIPath,
            googleAPIPath,
        } = this.props;

        return (
            <Page className="registration">
                <div className="header">
                    <div className="logo" />
                    <div>JOIN NOW USED</div>
                </div>
                <div className="substrate">
                    <div className="buttons-block">
                        <a className="btn btn-regular btn-blue" href={ facebookAPIPath } >FACEBOOK</a>
                        <a className="btn btn-regular btn-blue" href={ googleAPIPath } >GOOGLE</a>
                        <a className="btn btn-regular btn-blue">TWITTER</a>
                    </div>

                    <form className="form-block" onSubmit={ handleSubmit( registerUser ) } >
                        <span>OR</span>
                        <br />
                        <div className={ `${ serverError ? 'error' : ''}` }>
                            <div className="error-text">{ serverError || '' }</div>
                        </div>
                        { this.renderWithError(
                            <Input extraProps={ email } maxLength="50" placeholder="EMAIL" type="email" />,
                            email
                        ) }
                        { this.renderWithError(
                            <Input extraProps={ name } maxLength="50" placeholder="NAME" />,
                            name
                        ) }
                        { this.renderWithError(
                            <Input extraProps={ password } maxLength="20" placeholder="PASSWORD" type="password" />,
                            password
                        ) }

                        { this.renderCountrySelectors( country, state ) }

                        { this.renderWithError(
                            <Checkbox extraProps={ isEnabledTerms }>
                                <div className="checkboxLabel">
                                    <div>I agree to Global Fantasy Sports Golf</div>
                                    <div>
                                        Contest Rules, Terms&Conditions and Privacy Policy,
                                        and that I am of legal age to play Fantasy Sports in my state of residency.
                                    </div>
                                </div>
                            </Checkbox>,
                            isEnabledTerms
                        ) }
                        <br />
                        <br />
                        <button
                            className="btn btn-wide btn-blue"
                            disabled={ processing ? 'loading' : '' }
                            type="submit"
                        >
                            JOIN
                        </button>
                    </form>
                    <br />
                </div>
                <Link className="btn btn-wide" to={ RoutesConstants.get( 'loginPage' ) }>Already a member?</Link>
            </Page>
        );
    }
}

RegistrationComponent.propTypes = {
    fields: React.PropTypes.object.isRequired,
    handleSubmit: React.PropTypes.func.isRequired,
    registerUser: React.PropTypes.func.isRequired,
    processing: React.PropTypes.bool,
    serverError: React.PropTypes.string,
    facebookAPIPath: React.PropTypes.string.isRequired,
    googleAPIPath: React.PropTypes.string.isRequired,
    country: React.PropTypes.string,
    state: React.PropTypes.string,
};

export default RegistrationComponent;
