import React, { PropTypes } from 'react';

import PlaceholderImage from '../common/PlaceholderImage';
import playerPlaceholder from '../../images/room/player-placeholder.png';

const Dst = ( { defenceTeam, selectPlayerIndex, sport } ) => {
    if ( sport === 'soccer' ) {
        return ( <div /> );
    }
    const classIfActive = selectPlayerIndex === 'dst' ? 'active' : '';
    return (
        <div
            className={ `item dst ${ classIfActive }` }
            data-index="dst"
            key="dst"
            onClick={ this.selectPlayer }
        >
            <div className="item-border" />
            <PlaceholderImage
                alt={ `Player ${ defenceTeam.nameFull }` }
                height={ 55 }
                placeholder={ playerPlaceholder }
                src={ defenceTeam.smallPhoto } width={ 140 }
            />
            <i>{ defenceTeam.fantasyPoints }</i>
        </div>
    );
};

Dst.propTypes = {
    defenceTeam: PropTypes.object,
    selectPlayerIndex: PropTypes.oneOfType( [
        PropTypes.number,
        PropTypes.string,
    ] ),
    sport: PropTypes.string,
};

export default Dst;
