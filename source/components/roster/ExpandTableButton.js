import React, { PropTypes } from 'react';

const ExpandTableButton = ( { _t, expandScoresTable, expandTable, showTable } ) => {
    if ( !showTable ) {
        return ( <div /> );
    }
    return (
        <button className="btn btn-info-table" onClick={ expandTable }>
            { expandScoresTable ?
                _t( 'roster.table.btn_show-scored-act' ) :
                _t( 'roster.table.btn_show-all-act' )
            }
        </button>
    );
};

ExpandTableButton.propTypes = {
    _t: PropTypes.func,
    expandTable: PropTypes.func.isRequired,
    expandScoresTable: PropTypes.bool.isRequired,
    showTable: PropTypes.bool,
};

export default ExpandTableButton;
