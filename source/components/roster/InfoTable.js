import React, { Component, PropTypes } from 'react';

import InfoTableRow from './InfoTableRow';


class InfoTable extends Component {
    constructor ( props ) {
        super( props );
        this.renderExpandedTable = ::this.renderExpandedTable;
        this.renderNotExpandedTable = ::this.renderNotExpandedTable;
    }

    renderExpandedTable ( scoresData ) {
        return scoresData.map( ( score, key ) => score.points ?
            <InfoTableRow
                _t={ this.props._t }
                expandScoresTable={ this.props.expandScoresTable }
                forKey={ key }
                key={ key }
                scoreData={ score }
            />
            : <div key={ key } /> );
    }
    renderNotExpandedTable ( scoresData ) {
        return scoresData.map( ( score, key ) =>
            <InfoTableRow
                _t={ this.props._t }
                expandScoresTable={ this.props.expandScoresTable }
                forKey={ key }
                key={ key }
                scoreData={ score }
            />
        );
    }

    renderScoresTableData ( scoresData, totalPoints ) {

        let scoresDataMap = this.props.expandScoresTable ?
            this.renderNotExpandedTable( scoresData ) :
            this.renderExpandedTable( scoresData );

        let params = {
            name: 'Total Score',
            points: totalPoints,
        };
        scoresDataMap.push(
            <InfoTableRow
                _t={ this.props._t }
                expandScoresTable={ this.props.expandScoresTable }
                isEmpty
                key={ -1 }
                scoreData={ params }
            />
        );

        return scoresDataMap;
    }

    render () {
        const { showTable, scoresData, totalPoints, _t } = this.props;
        if ( !showTable ) {
            return ( <div /> );
        }
        return (
            <article className="info-table">
                <div className="info-table-row head">
                    <div className="activity head">
                        { _t( 'roster.table.activity' ) }
                    </div>
                    <div className="pts head">
                        { _t( 'roster.table.fantasy_pts' ) }
                    </div>
                </div>
                { this.renderScoresTableData( scoresData, totalPoints ) }
            </article>
        );
    }
}

InfoTable.propTypes = {
    _t: PropTypes.func,
    expandScoresTable: PropTypes.bool.isRequired,
    scoresData: PropTypes.oneOfType( [
        PropTypes.array,
        PropTypes.object,
    ] ),
    totalPoints: PropTypes.number.isRequired,
    showTable: PropTypes.bool,
    sport: PropTypes.string.isRequired,
};

export default InfoTable;
