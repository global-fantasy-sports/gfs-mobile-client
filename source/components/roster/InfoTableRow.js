import React, { PropTypes } from 'react';

import cx from 'classnames';

const InfoTableRow = ( { _t, expandScoresTable, scoreData, forKey, isEmpty } ) => {
    const displayPlaceholder = isEmpty ? '' : 0;
    const rules = scoreData.rules ? scoreData.rules.replace( /PTS/gi, '' ) : displayPlaceholder;
    const points = scoreData.points && scoreData.points !== 0;
    const rowClass = cx( 'info-table-row', {
        highlighted: points && expandScoresTable,
    } );
    const pts = cx( 'pts', {
        highlighted: points && !expandScoresTable,
    } );
    return (
        <div className={ rowClass } key={ forKey }>
            <div className="activity">
                <span>{ _t( `roster.table.score-title.${ scoreData.name }` ) }</span>
                <span className="num">
                    { rules }
                </span>
            </div>
            <div className={ pts }>{ scoreData.points ? scoreData.points : 0 }</div>
        </div>
    );
};

InfoTableRow.propTypes = {
    _t: PropTypes.func,
    expandScoresTable: PropTypes.bool.isRequired,
    isEmpty: PropTypes.bool,
    scoreData: PropTypes.oneOfType( [
        PropTypes.string,
        PropTypes.object,
    ] ).isRequired,
    forKey: PropTypes.any,
};

export default InfoTableRow;
