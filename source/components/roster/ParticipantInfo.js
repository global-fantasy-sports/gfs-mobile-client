import React, { PropTypes } from 'react';

const ParticipantInfo = ( { _t, awayTeamPhoto, homeTeamPhoto, match, nameFull, position } ) => {
    return (
        <div className="player-info">
            <div className="player-name">
                <span>{ _t( `roster.position_label.${position}` ) }</span>&nbsp;
                <span>{ _t( nameFull ) }</span>
            </div>
            <div className="game-info">
                <span>{ match.homeScore }</span>
                <i style={ { background: `url(${ homeTeamPhoto }) center/contain no-repeat` } } />
                <span className="vs">VS</span>
                <i style={ { background: `url(${ awayTeamPhoto }) center/contain no-repeat` } } />
                <span>{ match.awayScore }</span>
            </div>
        </div>
    );
};

ParticipantInfo.propTypes = {
    _t: PropTypes.func,
    awayTeamPhoto: PropTypes.string.isRequired,
    homeTeamPhoto: PropTypes.string.isRequired,
    match: PropTypes.object.isRequired,
    nameFull: PropTypes.string.isRequired,
    position: PropTypes.string.isRequired,
};

export default ParticipantInfo;
