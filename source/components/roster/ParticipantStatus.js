import React, { PropTypes } from 'react';

import CounterContainer from '../../containers/CounterContainer';

const ParticipantStatus = ( { _t, played, selectedParticipant, match, notPlay } ) => {
    if ( !match || ( played && match.status === 'closed' ) || match.status === 'in_progress' ) {
        return ( <div /> );
    } else if ( notPlay ) {
        return ( <div className="status-timer"><p className="game-status">
            { _t( 'roster.status.did_not_play' ) }
        </p></div> );
    }
    return (
        <div className="status-timer">
            <p className="title-timer">{ _t( 'roster.status.match_starts' ) }</p>
            <div className="timer">
                <CounterContainer
                    key={ selectedParticipant.id }
                    range={ [match.startTimeUtc] }
                    timestamp={ match.startTimeUtc || 0 }
                />
            </div>
        </div>
    );
};

ParticipantStatus.propTypes = {
    _t: PropTypes.func,
    played: PropTypes.bool,
    selectedParticipant: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    notPlay: PropTypes.bool,
};

export default ParticipantStatus;
