import _ from 'lodash';
import React, { PropTypes } from 'react';

import cx from 'classnames';

import Dst from './DST';
import CounterContainer from '../../containers/CounterContainer';
import PlaceholderImage from '../common/PlaceholderImage';
import playerPlaceholder from '../../images/room/player-placeholder.png';


const TotalScore = ( { _t, totalPoints } ) => (
    <div className="item total">
        <h6>{ _t( 'roster.table.score-title.Total Score' ) }</h6>
        <p>{ _.round( totalPoints, 2 ) }</p>
    </div>
);

TotalScore.propTypes = {
    _t: PropTypes.func,
    totalPoints: PropTypes.number.isRequired,
};

const ScoreCardList = ( {
                            _t,
                            defenceTeam,
                            gameSportsmen,
                            gameParticipants,
                            competition,
                            game,
                            matches,
                            selectPlayerIndex,
                            selectPlayer,
                            sport,
                        } ) => {
    let block = [];

    gameSportsmen.forEach( ( gameSportsman, index ) => {
        const participant = gameParticipants[gameSportsman.id];
        if ( !( participant && participant.id ) ) {
            return;
        }

        const matchTimeBeforeStart = matches[participant.contestId].startTimeUtc;
        const matchStatus = matches[participant.contestId].status;
        const showCounter = matchStatus === 'not_started';
        let playerPoints = !participant.played && matchStatus === 'closed' ? 'DNP' : participant.fantasyPoints || 0;

        if ( ( matches[participant.contestId].startTimeUtc < competition.startDate ) ||
             ( matches[participant.contestId].startTimeUtc > competition.endDate ) ) {
            playerPoints = 0;
        }

        const itemClass = cx( 'item', {
            active: index === selectPlayerIndex,
            closed: matchStatus === 'closed',
            'in-progress': matchStatus === 'in_progress',
        } );

        block.push(
            <div
                className={ itemClass }
                data-index={ index }
                key={ gameSportsman.id }
                onClick={ selectPlayer }
            >
                <div className="item-border" />
                <PlaceholderImage
                    alt={ `Player ${ _t( gameSportsman.nameKnown ) }` }
                    height={ 50 }
                    placeholder={ playerPlaceholder }
                    src={ gameSportsman.smallPhoto } width={ 65 }
                />
                <div className="item-overlay">
                    { !showCounter ? <i className="points">{ playerPoints }</i>
                        : <i>
                          <CounterContainer
                              range={ [matchTimeBeforeStart] }
                              timestamp={ matchTimeBeforeStart || 0 }
                              type="score-card"
                          />
                      </i>
                    }
                    <div className="item-name">
                        { _t( gameSportsman.name ) }
                    </div>
                </div>
            </div>
        );
    } );

    return (
        <article className="players-list">
            { block }
            <Dst
                defenceTeam={ defenceTeam }
                selectPlayerIndex={ selectPlayerIndex }
                sport={ sport }
            />
            <TotalScore _t={ _t } totalPoints={ game.points } />
        </article>
    );
};

ScoreCardList.propTypes = {
    _t: PropTypes.func.isRequired,
    defenceTeam: PropTypes.object,
    gameParticipants: PropTypes.object.isRequired,
    gameSportsmen: PropTypes.array.isRequired,
    competition: PropTypes.object.isRequired,
    game: PropTypes.object.isRequired,
    matches: PropTypes.object.isRequired,
    selectPlayerIndex: PropTypes.oneOfType( [
        PropTypes.number,
        PropTypes.string,
    ] ).isRequired,
    selectPlayer: PropTypes.func.isRequired,
    sport: PropTypes.string,
};

export default ScoreCardList;
