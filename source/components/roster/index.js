import _ from 'lodash';
import React, { Component, PropTypes } from 'react';
import { StickyContainer, Sticky } from 'react-sticky';
import classname from 'classnames';

import Page from '../page';
import PlaceholderImage from '../common/PlaceholderImage';
import ScrollLoaderComponent from '../scroll-loader';

import ParticipantStatus from './ParticipantStatus';
import InfoTable from './InfoTable';
import ExpandTableButton from './ExpandTableButton';
import ScoreCardList from './ScoreCardList';
import ParticipantInfo from './ParticipantInfo';
import Btn from '../../ui/btn/Btn';

import playerPlaceholder from '../../images/room/player-placeholder.png';


import './style.scss';

const initialState = {
    selectPlayerIndex: 0,
    expandScoresTable: false,

};
class RosterComponent extends Component {

    constructor () {
        super();
        this.competitionUpdateRequestSent = false;
        this.competitionGamesUpdateRequestSent = false;
        this.competition = false;
        this.state = initialState;

        this.selectPlayer = ::this.selectPlayer;
        this.expandTable = ::this.expandTable;
        this.onExit = ::this.onExit;
    }

    componentWillMount () {
        this.props.getGame();
        this.props.getParticipantResults();
        this.props.getResultsLegend();
    }

    getGameSportsmen ( iSportsmen, iGame ) {
        return this.props.playerPositions
            .filter( iPosition => iPosition !== 'dst' )
            .map( iPosition => iSportsmen[iGame[iPosition]] );
    }

    findParticipant ( iParticipants, iSportsmen, iSportEventId ) {
        let participantReturn = {};
        Object.values( iParticipants ).forEach( iParticipan => {
            if ( iSportsmen.id === iParticipan.athleteId && iSportEventId === iParticipan.sportEventId ) {
                participantReturn = iParticipan;
            }
        } );
        return participantReturn;
    }

    getParticipants ( iParticipants, iGameSportsmen, iSportEventId ) {
        let participants = {};
        Object.values( iGameSportsmen ).forEach( iSportsmen => {
            participants[iSportsmen.id] = this.findParticipant( iParticipants, iSportsmen, iSportEventId );
        } );

        return participants;
    }

    selectPlayer ( iEvent ) {
        const dataIndex = iEvent.currentTarget.getAttribute( 'data-index' );
        this.setState( { selectPlayerIndex: dataIndex === 'dst' ? 'dst' : parseInt( dataIndex, 10 ) } );
    }

    expandTable () {
        this.setState( { expandScoresTable: !this.state.expandScoresTable } );
    }

    getDefenceTeam () {
        if ( this.props.sport === 'nfl' ) {
            const { defenceTeams, game, teams } = this.props;
            const dst = _.find( defenceTeams, { team: game.dst } );
            if ( dst ) {
                return {
                    ...teams[dst.team],
                    ...dst,
                    nameFull: `${ dst.city } ${ dst.name } DST`,
                };
            }
        }
        return null;
    }

    renderTitle ( userId ) {
        if ( this.props.userId !== userId ) {
            return '';
        }
        return (
            <h2 className="title">
                <span>{ this.props._t( 'roster.top.title.my' ) }</span>
                <span>{ this.props._t( 'roster.top.title.draft' ) }</span>
            </h2>
        );
    }

    onExit () {
        this.props.onExit();
    }

    render () {
        const {
            _t,
            competition,
            game,
            participants,
            matches,
            participantsResultsLegend,
            sport,
            sportsmen,
            teams,
        } = this.props;

        if ( !( competition && game && ( Object.keys( sportsmen ).length > 0 ) &&
             ( Object.keys( teams ).length > 0 ) ) ) {
            return ( <ScrollLoaderComponent /> );
        }

        const gameSportsmen = this.getGameSportsmen( sportsmen, game );
        const gameParticipants = this.getParticipants(
            participants,
            gameSportsmen,
            competition.sportEventId
        );
        const selectedPlayer = gameSportsmen[this.state.selectPlayerIndex];

        const selectedParticipant = gameParticipants[selectedPlayer.id];
        const teamLogo = _.get( teams, [selectedParticipant.teamId, 'largeIcon'] );

        const match = matches[selectedParticipant.contestId];

        if ( !match ) {
            return (
                <ScrollLoaderComponent />
            );
        }

        const participantPlayed = selectedParticipant.played;

        const showTable = participantPlayed && match.status !== 'not_started';
        const participantDidNotPlay = !participantPlayed && ['closed'].includes( match.status );
        const showZeroPoints = match.startTimeUtc < competition.startDate ||
            match.startTimeUtc > competition.endDate;

        const scoresData = selectedParticipant.calcInfo && !showZeroPoints ?
            selectedParticipant.calcInfo : participantsResultsLegend[selectedParticipant.position];


        let participantPoints = 0;
        if ( !showZeroPoints ) {
            participantPoints = selectedParticipant.fantasyPoints || 0;
        }

        return (
            <StickyContainer>
                <Page
                    className={ classname( 'roster', {
                        'not-started': !competition.started,
                        'with-table': showTable,
                    } ) }
                >
                    <div className="photo">
                        <div
                            className="team-logo"
                            style={ { background: `url(${ teamLogo }) center 5px/contain no-repeat` } }
                        />
                        <Sticky
                            className="roster-top-wrap"
                            stickyClassName="roster-top-sticky"
                            stickyStyle={ { left: 0, top: '60px', width: '100%' } }
                            topOffset={ -170 }
                        >
                            <div className="roster-top">
                                <Btn
                                    icon="arrow-left"
                                    label={ _t( 'roster.top.btn_back' ) }
                                    onClick={ this.onExit }
                                    type="transparent"
                                />
                                { this.renderTitle( game.userId ) }
                            </div>
                        </Sticky>
                        <PlaceholderImage
                            alt={ `Player ${ selectedPlayer.name }` }
                            className="player-photo"
                            height={ 130 }
                            placeholder={ playerPlaceholder }
                            src={ selectedPlayer.largePhoto }
                            width={ 175 }
                        />
                    </div>

                    <ParticipantInfo
                        _t={ _t }
                        awayTeamPhoto={ teams[match.awayTeam].smallIcon }
                        homeTeamPhoto={ teams[match.homeTeam].smallIcon }
                        match={ match }
                        nameFull={ selectedPlayer.nameKnown }
                        position={ selectedParticipant.position }
                    />
                    <ParticipantStatus
                        _t={ _t }
                        match={ match }
                        notPlay={ participantDidNotPlay }
                        played={ participantPlayed }
                        selectedParticipant={ selectedPlayer }
                    />
                    <InfoTable
                        _t={ _t }
                        expandScoresTable={ this.state.expandScoresTable }
                        scoresData={ scoresData }
                        showTable={ showTable }
                        sport={ sport }
                        totalPoints={ participantPoints }
                    />
                    <ExpandTableButton
                        _t={ _t }
                        expandScoresTable={ this.state.expandScoresTable }
                        expandTable={ this.expandTable }
                        showTable={ showTable }
                    />
                    <ScoreCardList
                        _t={ _t }
                        competition={ competition }
                        defenceTeam={ this.getDefenceTeam() }
                        game={ game }
                        gameParticipants={ gameParticipants }
                        gameSportsmen={ gameSportsmen }
                        matches={ matches }
                        selectPlayer={ this.selectPlayer }
                        selectPlayerIndex={ this.state.selectPlayerIndex }
                        sport={ sport }
                    />
                </Page>
            </StickyContainer>
        );
    }
}

RosterComponent.propTypes = {
    _t: PropTypes.func,
    competition: PropTypes.object,
    competitionId: PropTypes.number.isRequired,
    defenceTeams: PropTypes.object.isRequired,
    game: PropTypes.object,
    gameId: PropTypes.number.isRequired,
    getGame: PropTypes.func.isRequired,
    getParticipantResults: PropTypes.func.isRequired,
    getResultsLegend: PropTypes.func.isRequired,
    matches: PropTypes.object.isRequired,
    onExit: PropTypes.func.isRequired,
    participants: PropTypes.object.isRequired,
    participantsResultsLegend: PropTypes.oneOfType( [
        PropTypes.array,
        PropTypes.object,
    ] ),
    playerPositions: PropTypes.array,
    positionsNamesList: PropTypes.array,
    sport: PropTypes.string,
    sportsmen: PropTypes.object.isRequired,
    teams: PropTypes.object.isRequired,
    userId: PropTypes.number,
};

export default RosterComponent;
