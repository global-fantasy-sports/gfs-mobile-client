import React, { Component, PropTypes } from 'react';
import _ from 'lodash';
import classnames from 'classnames';

import FooterContainer from '../../containers/FooterContainer';
import TabPanel, { Tab } from '../athlete-info/TabPanel';
import ScheduleItem from './ScheduleItem';
import PoweredBy from '../footer/PoweredBy';

import './style.scss';

function getStatusIconClass ( hourMatches ) {
    const allFinal = hourMatches.every( ( matchData ) => {
        const status = matchData.match.status;
        return status === 'closed' || status === 'complete';
    } );

    if ( allFinal ) {
        return 'schedule__icon--final';
    }

    const allNotStarted = hourMatches.every( ( matchData ) => matchData.match.status === 'not_started' );

    return allNotStarted ? 'schedule__icon--not-started' : 'schedule__icon--live';
}


const TabLabel = ( { date, day } ) => (
    <div>
        <div>{ date }</div>
        <div>{ day }</div>
    </div>
);

TabLabel.propTypes = {
    date: PropTypes.string.isRequired,
    day: PropTypes.string.isRequired,
};


class ScheduleComponent extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        matchDays: PropTypes.array.isRequired,
        groupedMatches: PropTypes.object.isRequired,
    };

    componentDidMount () {
        document.body.scrollTop = 0;
    }

    renderTabContent ( matches, _t ) {
        const rawMatchesByHour = _.groupBy( matches, 'hour' );
        const matchesByHour = {};
        Object.keys( rawMatchesByHour ).sort().forEach( function ( key ) {
            matchesByHour[key] = rawMatchesByHour[key];
        } );
        const groupCount = Object.values( matchesByHour ).length;
        let counter = 0;

        const groups = _.map( matchesByHour, ( hourMatches, hour ) => {
            const [isFirst, isLast] = [counter === 0, counter === groupCount - 1];
            counter++;
            return (
                <div className="schedule__timeblock" key={ hour }>
                    <div className="schedule__timeblock-title">
                        {hour}
                    </div>
                    <div className="schedule__timeline">
                        { !isFirst && <div className="schedule__divider-padding" /> }
                        <i
                            className={ classnames( 'schedule__icon', getStatusIconClass( hourMatches ), {
                                'schedule__icon--first': isFirst,
                            } ) }
                        />
                        { !isLast && <div className="schedule__timeline-divider" /> }
                    </div>
                    <div className="schedule__connector" />
                    <div className="schedule__match-wrapper" key={ hour }>
                        { hourMatches.map( ( { match }, index ) =>
                            <ScheduleItem _t={ _t } key={ index } match={ match } />
                        ) }
                    </div>
                </div>
            );
        } );

        return (
            <div className="schedule__timeblocks">
                {groups}
                <PoweredBy className="powered-by" />
            </div>
        );
    }

    render () {
        const { _t, matchDays, groupedMatches } = this.props;
        return (
            <section className="schedule">
                <TabPanel
                    scrollTop
                    topStick={ 64 }
                >
                    { matchDays.map( ( props, index ) =>
                        <Tab key={ index } label={ <TabLabel { ...props } /> }>
                            { this.renderTabContent( groupedMatches[props.date], _t ) }
                        </Tab>
                    ) }
                </TabPanel>
                <FooterContainer />
            </section>
        );
    }
}

export default ScheduleComponent;
