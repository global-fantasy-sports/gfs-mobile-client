import cx from 'classnames';
import React, { PropTypes } from 'react';
import itemPlaceholder from '../../images/schedule/schedule-item-placeholder-icon.png';


const ScheduleTeam = ( { _t, logo = itemPlaceholder, name } ) => (
    <div className="schedule-team">
        <div className="schedule-team__logo">
            <img src={ logo } />
        </div>
        <div className="schedule-team__name">
            { _t( name ) }
        </div>
    </div>
);

ScheduleTeam.propTypes = {
    _t: PropTypes.func.isRequired,
    logo: PropTypes.string,
    name: PropTypes.string,
};


const ScheduleItem = ( { _t, match } ) => {
    const isLive = match.status === 'in_progress' || match.status === 'live';
    const isFinished = match.status === 'finished' || match.status === 'closed';

    return (
        <article className="schedule__item">
            <ScheduleTeam
                _t={ _t }
                logo={ match.homeTeam.smallIcon }
                name={ match.homeTeam.id }
            />

            { ( isLive || isFinished )
                ? <div
                    className={ cx( 'schedule__item-divider schedule__score', {
                        'schedule__score--final': isFinished,
                        'schedule__score--live': isLive,
                    } ) }
                  >
                      <div className="schedule__score-value">
                          { match.homeScore } &ndash; { match.awayScore }
                      </div>
                      <div className="schedule__score-label">
                          { isLive
                              ? _t( 'schedule.item.live' )
                              : _t( 'schedule.item.final' )
                          }
                      </div>
                  </div>
                : <div className="schedule__item-divider schedule__vs">{ _t( 'schedule.item.vs' ) }</div>
            }

            <ScheduleTeam
                _t={ _t }
                logo={ match.awayTeam.smallIcon }
                name={ match.awayTeam.id }
            />
        </article>
    );
};

ScheduleItem.propTypes = {
    _t: PropTypes.func.isRequired,
    match: PropTypes.object.isRequired,
};

export default ScheduleItem;
