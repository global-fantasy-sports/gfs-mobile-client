import React, { Component } from 'react';

import Page from '../page';
import PoweredBy from '../footer/PoweredBy';
import './style.scss';


class ScrollLoaderComponent extends Component {

    render () {
        return (
            <Page className="scroll-loader">
                <PoweredBy className="powered-by" />
                <div className="spinner">
                    <div className="svg-overlay" />
                    {/* eslint-disable react/jsx-sort-props */}
                    <svg
                        style={ { enableBackground: 'new 0 0 101 101' } }
                        version="1.1"
                        viewBox="0 0 101 101"
                        x="0px"
                        xmlns="http://www.w3.org/2000/svg"
                        xmlnsXlink="http://www.w3.org/1999/xlink"
                        xmlSpace="preserve"
                        y="0px"
                    >
                        {/* eslint-enable react/jsx-sort-props */}
                        <g>
                            <path
                                className="svg-line"
                                d="M50.5,101C22.7,101,0,78.3,0,50.5S22.7,0,50.5,0c3,0,5.5,2.5,5.5,5.5S53.5,
                                11,50.5,11C28.7,11,11,28.7,11,50.5
                                S28.7,90,50.5,90S90,72.3,90,50.5c0-3,
                                2.5-5.5,5.5-5.5s5.5,2.5,5.5,5.5C101,78.3,78.3,101,50.5,101z"
                            />
                        </g>
                    </svg>
                </div>
            </Page>
        );
    }
}

ScrollLoaderComponent.propTypes = {};

export default ScrollLoaderComponent;
