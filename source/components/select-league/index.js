import React, { Component, PropTypes } from 'react';

import Page from '../page';
import Btn from '../../ui/btn/Btn';
import Countdown from '../countdown/Countdown';
import OnboardingContainer from '../../containers/OnboardingContainer';
import WelcomeOnboarding from '../onboarding/WelcomeOnboarding';
import PoweredBy from '../footer/PoweredBy';

import './style.scss';

const MatchTimer = ( { _t, currentTime, startTime } ) => {
    return (
        <div className="match-timer">
            <h4>{ _t( 'select.league.nextin' ) }</h4>

            <Countdown
                className="clock"
                from={ currentTime * 1000 }
                horizontal
                labels={ {
                    day: _t( 'select.league.d' ),
                    hour: _t( 'select.league.h' ),
                    minute: _t( 'select.league.min' ),
                    second: _t( 'select.league.sec' ),
                } }
                to={ startTime * 1000 }
            />
        </div>
    );
};
MatchTimer.propTypes = {
    _t: PropTypes.func.isRequired,
    currentTime: PropTypes.number.isRequired,
    startTime: PropTypes.number.isRequired,
};

class SelectLeagueComponent extends Component {

    constructor ( props ) {
        super( props );

        this.chooseLeague = ::this.chooseLeague;
        this.renderLeague = ::this.renderLeague;
        document.body.scrollTop = 0;
    }

    chooseLeague ( leagueName ) {
        this.props.selectLeague( leagueName );
    }

    renderFooter ( leagueName, isEnabled, timeInfo ) {
        if ( !isEnabled ) {
            return (
                <div className="status-soon">{ this.props._t( 'select.league.comingsoon' ) }</div>
            );
        }

        return (
            <div>
                { timeInfo && timeInfo.nextEvent && timeInfo.nextEvent.startTime ?
                <div className="time-status">
                    <MatchTimer
                        _t={ this.props._t }
                        currentTime={ timeInfo.currentTime }
                        startTime={ timeInfo.nextEvent.startTime }
                    />
                </div>
                :
                <div className="time-status">
                    <div className="match-timer">
                        <h4>{ this.props._t( 'select.league.norooms' ) }</h4>
                    </div>
                </div>
                }
                <div className="btn-select">
                    <Btn
                        block
                        label={ this.props._t( 'select.league.selectbtn' ) }
                        onClick={ this.chooseLeague }
                        size="large"
                        type="primary"
                        value={ leagueName }
                    />
                </div>
                <br />
            </div>
        );
    }

    renderLeague ( league, index ) {
        const { _t } = this.props;
        const [leagueName, settings] = league;

        if ( !settings.mobile.available ) {
            return '';
        }

        return (
            <section className={ `league-type ${ leagueName }-bg` } key={ index }>
                <div className="head">
                    <h1 className="title">
                        <span>
                            { _t( `select.league.title_${ settings.title }` ) }
                            <i className={ `flag ${ leagueName }` } />
                        </span>
                    </h1>
                    <p className="subtitle">
                        { _t( `select.league.subtitle_${ settings.subtitle }` ) }
                    </p>
                </div>
                { this.renderFooter( leagueName, settings.mobile.enabled, settings.timeInfo ) }
            </section>
        );
    }

    render () {
        return (
            <Page className="leagues">
                { this.props.leagues.map( this.renderLeague ) }
                <PoweredBy className="powered-by" />
                <OnboardingContainer
                    label={ this.props._t( 'onboarding.welcome.btn_start' ) }
                    name="leagues"
                >
                    <WelcomeOnboarding _t={ this.props._t } />
                </OnboardingContainer>
            </Page>
        );
    }
}

SelectLeagueComponent.propTypes = {
    leagues: React.PropTypes.array.isRequired,
    selectLeague: React.PropTypes.func.isRequired,
    _t: React.PropTypes.func.isRequired,
};

export default SelectLeagueComponent;
