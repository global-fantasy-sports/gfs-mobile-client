import React, { Component } from 'react';
import cx from 'classnames';

import Page from '../page';
import TabsComponent from '../tabs';
import BtnLink from '../../ui/btn/BtnLink';

import './style.scss';

const initialStates = {
    selectedTab: 1,
    countdown: {},
};

class SelectSportComponent extends Component {

    constructor ( props ) {
        super( props );

        props.sportsArray.forEach( ( iSportArray, iIndex ) => {
            if ( iSportArray[1] === props.currentSport ) {
                initialStates.selectedTab = iIndex + 1;
            }
        } );

        this.state = initialStates;

        this.changeTab = ::this.changeTab;
        this.sportTimers = {};
    }

    changeTab ( iNextSelectedTabIndex ) {
        this.setState( { selectedTab: parseInt( iNextSelectedTabIndex, 10 ) } );
    }

    renderTabSportBody ( sportName ) {

        // if ( !showSportInfo.includes( sportName ) ) {
        //     let sportClass = cx( 'select-pane', sportName );
        //     return (
        //         <article className={ sportClass }>
        //             <p className="info-text">
        //                 { this.props._t( 'sport_info.blocked.daily' ) }
        //                 <i> { this.props._t( `common.sport_${sportName}` ) } </i>
        //                 { this.props._t( 'sport_info.blocked.unavailable' ) }</p>
        //             <p className="info-text">
        //                 { this.props._t( 'sport_info.blocked.text_launch' ) }
        //             </p>
        //             <p className="info-text">
        //                 { this.props._t( 'sport_info.blocked.see_soon' ) }
        //             </p>
        //         </article>
        //     );
        // }

        let sportClass = cx( 'select-pane', sportName );
        return (
            <article className={ sportClass }>
                <br /><br /><br />
                <p className="game-title">{ this.props._t( `common.sport_${sportName}` ) }</p>

                <br /><br /><br /><br /><br /><br /><br />
                <BtnLink
                    label={ this.props._t( 'sport_info.btn_enter' ) }
                    size="monster"
                    to={ `/${sportName}/` }
                    type="primary"
                />
            </article>
        );
    }

    renderTabBody () {
        return (
            <section>
                { this.props.sportsArray.map( ( sport, index ) => {
                    return (
                        <div key={ index }>
                            { this.renderTabSportBody( sport ) }
                        </div>
                    );
                } ) }
            </section>
        );
    }

    renderTabHeader () {
        return (
            <section>
                { this.props.sportsArray.map( ( sport, index ) => {
                    return (
                        <article
                            className={ sport }
                            data-sport-name={ sport }
                            key={ index }
                        />
                    );
                } ) }
            </section>
        );
    }

    render () {
        return (
            <Page className="select" >
                <TabsComponent onSelect={ this.changeTab } selectedTab={ this.state.selectedTab }>
                    { this.renderTabHeader() }
                    { this.renderTabBody() }
                    <br />
                </TabsComponent>
            </Page>
        );
    }
}

SelectSportComponent.propTypes = {
    currentSport: React.PropTypes.string.isRequired,
    sportsArray: React.PropTypes.array.isRequired,
    _t: React.PropTypes.func.isRequired,
};

export default SelectSportComponent;
