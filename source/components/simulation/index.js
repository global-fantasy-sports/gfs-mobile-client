import React, { Component, PropTypes } from 'react';

import ScrollLoaderComponent from '../scroll-loader';
import front from '../../images/header/car_front.png';
import back from '../../images/header/car_back.png';
import { Toggle } from '../input';

import { TIME_FORMAT } from '../../constants/app';

import './style.scss';
import ErrorNotification from '../../wizard/components/ErrorNotification';

const initialState = {
    hours: 0,
    minutes: 0,
};

class Simulation extends Component {

    constructor () {
        super();
        this.handleHoursInputChange = ::this.handleHoursInputChange;
        this.handleMinutesInputChange = ::this.handleMinutesInputChange;
        this.state = initialState;
        this.onCheck = ::this.onCheck;
        this.onRestart = ::this.onRestart;
        this.onKickOff = ::this.onKickOff;
        this.onMid = ::this.onMid;
        this.onResults = ::this.onResults;
        this.plusMinutes = ::this.plusMinutes;
        this.plusOneHour = ::this.plusOneHour;
        this.plusSixHours = ::this.plusSixHours;
        this.plusOneDay = ::this.plusOneDay;
        this.plusOneWeek = ::this.plusOneWeek;
    }

    onRestart () {
        this.props.restartSimulation();
        this.showSpinner();
    }

    onCheck () {
    }

    onBracketize () {
        localStorage.setItem( 'i18nDebug', localStorage.getItem( 'i18nDebug' ) === 'yes' ? 'no' : 'yes' );
        setTimeout( () => window.location.reload(), 250 );
    }

    onKickOff () {
        this.props.changePointSimulationKickOff();
        this.showSpinner();
    }

    onMid () {
        this.props.changePointsSimulationMid();
        this.showSpinner();
    }

    onResults () {
        this.props.changePointsSimulationResults();
        this.showSpinner();
    }

    handleHoursInputChange ( event ) {
        this.setState( { hours: event.target.value } );
    }

    handleMinutesInputChange ( event ) {
        this.setState( { minutes: event.target.value } );
    }

    plusMinutes () {
        this.props.changeTimeSimulationMinutes( this.state.hours, this.state.minutes );
        this.showSpinner();
    }

    plusOneHour () {
        this.props.changeTimeSimulationOneHour();
        this.showSpinner();
    }

    plusSixHours () {
        this.props.changeTimeSimulationSixHours();
        this.showSpinner();
    }

    plusOneDay () {
        this.props.changeTimeSimulationOneDay();
        this.showSpinner();
    }

    plusOneWeek () {
        this.props.changeTimeSimulationOneWeek();
        this.showSpinner();
    }

    showSpinner () {
        this.setState( { loader: true } );
    }

    renderTime ( dateTime ) {
        const date = dateTime.format( TIME_FORMAT.datetime_full );
        const time = dateTime.format( TIME_FORMAT.datetime_time );

        return (
            <article className="datetime-wrapper">
                <div className="datetime">
                    <div className="column">
                        <div className="date">
                            { date }
                        </div>
                        <div className="time">
                            { time }
                        </div>
                    </div>
                </div>
            </article>
        );
    }

    render () {
        const { onClearSimError, simLoader, simError } = this.props;

        if ( simLoader ) {
            return (
                <ScrollLoaderComponent />
            );
        }

        return (
            <section className="simulation">
                <ErrorNotification
                    error={ simError }
                    onHide={ onClearSimError }
                />
                { this.renderTime( this.props.currentDateObject ) }
                <div
                    className="simulation-button"
                    onClick={ this.onRestart }
                >
                    <img src={ front } />
                    GREAT SCOTT!
                    <img src={ back } />
                </div>
                <div className="lock">
                    <span>Simulation Environment:</span>
                    <Toggle
                        offLabel="UNLOCK"
                        onCheck={ this.onCheck }
                        onLabel="LOCK"
                    />
                </div>
                <div className="lock">
                    <span>Bracketize translations:</span>
                    <Toggle
                        isChecked={ localStorage.getItem( 'i18nDebug' ) === 'yes' }
                        offLabel="NO"
                        onCheck={ this.onBracketize }
                        onLabel="YES"
                    />
                </div>
                <div>
                    <div className="btn btn-white btn-small button" onClick={ this.onKickOff }>KICK-OFF</div>
                    <div className="btn btn-white btn-small button" onClick={ this.onMid }>MID</div>
                    <div className="btn btn-white btn-small button" onClick={ this.onResults }>RESULTS</div>
                    <br />
                    <div className="btn btn-white btn-small button" onClick={ this.plusOneHour }>+1H</div>
                    <div className="btn btn-white btn-small button" onClick={ this.plusSixHours }>+6H</div>
                    <div className="btn btn-white btn-small button" onClick={ this.plusOneDay }>+1D</div>
                    <div className="btn btn-white btn-small button" onClick={ this.plusOneWeek }>+1W</div>
                    <br />
                    <input onChange={ this.handleHoursInputChange } placeholder="hours" style={ { width: '50px' } } />
                    <input
                        onChange={ this.handleMinutesInputChange }
                        placeholder="minutes"
                        style={ { width: '50px' } }
                    />
                    <div className="btn btn-white btn-small button" onClick={ this.plusMinutes }>travel</div>
                </div>
            </section>
        );
    }
}

Simulation.propTypes = {
    currentDateObject: PropTypes.object.isRequired,
    changeTimeSimulationMinutes: PropTypes.func.isRequired,
    changeTimeSimulationOneHour: PropTypes.func.isRequired,
    changeTimeSimulationOneDay: PropTypes.func.isRequired,
    changeTimeSimulationOneWeek: PropTypes.func.isRequired,
    restartSimulation: PropTypes.func.isRequired,
    changeTimeSimulationSixHours: PropTypes.func.isRequired,
    changePointSimulationKickOff: PropTypes.func.isRequired,
    changePointsSimulationMid: PropTypes.func.isRequired,
    changePointsSimulationResults: PropTypes.func.isRequired,
    simLoader: PropTypes.bool.isRequired,
    simError: PropTypes.string.isRequired,
    onClearSimError: PropTypes.func.isRequired,
};


export default Simulation;
