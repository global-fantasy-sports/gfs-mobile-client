import cx from 'classnames';
import React, { Component, PropTypes } from 'react';
import { Motion, spring } from 'react-motion';

import './Swipeable.scss';


const UNCERTAINTY_THRESHOLD = 3;

let nodeThatClaimedTheEvent = null;

function translate ( offset ) {
    offset = Math.round( offset );
    return {
        transform: `translate(${offset}px, 0)`,
        MozTransform: `translate(${offset}px, 0)`,
        MsTransform: `translate(${offset}px, 0)`,
        WebkitTransform: `translate(${offset}px, 0)`,
    };
}

class Swipeable extends Component {

    static propTypes = {
        action: PropTypes.node.isRequired,
        children: PropTypes.node.isRequired,
        className: PropTypes.string,
        containerClassName: PropTypes.string,
        disabled: PropTypes.bool,
        height: PropTypes.number.isRequired,
        onSwipeEnd: PropTypes.func,
        onSwipeStart: PropTypes.func,
        onTouchTap: PropTypes.func,
        open: PropTypes.bool,
        springConfig: PropTypes.object,
    };

    static defaultProps = {
        disabled: false,
        open: false,
        onTouchTap: () => {},
        onSwipeEnd: () => {},
        onSwipeStart: () => {},
        springConfig: {
            stiffness: 300,
            damping: 30,
            dimension: 0.1,
        },
    };

    constructor ( props ) {
        super( props );

        this.state = {
            isOpen: props.open === true,
            isDragging: false,
        };

        this.maxOffset = null;
        this.startX = 0;
        this.startY = 0;
        this.lastX = 0;
        this.startY = 0;
        this.started = false;
        this.isSwiping = null;

        this.handleTouchEnd = ::this.handleTouchEnd;
        this.handleTouchMove = ::this.handleTouchMove;
        this.handleTouchStart = ::this.handleTouchStart;
        this.handleTouchTap = ::this.handleTouchTap;
        this.setActionNode = ::this.setActionNode;
        this.setRootNode = ::this.setRootNode;
    }

    componentWillReceiveProps ( nextProps ) {
        if ( !this.started && this.props.open !== nextProps.open ) {
            this.setState( { isOpen: nextProps.open } );
        }
    }

    setActionNode ( node ) {
        this.actionNode = node;
    }

    setRootNode ( node ) {
        this.node = node;
    }

    handleTouchStart ( event ) {
        const { pageX, pageY } = event.touches[0];

        const { width } = this.actionNode.getBoundingClientRect();
        this.maxOffset = width * -1;
        this.startX = pageX;
        this.lastX = pageX;
        this.startY = pageY;
        this.started = true;
        this.isSwiping = null;
    }

    handleTouchMove ( event ) {
        if ( !this.started ) {
            this.handleTouchStart( event );
            return;
        }

        const { pageX, pageY } = event.touches[0];

        // We don't know yet.
        if ( this.isSwiping === null ) {
            const dx = Math.abs( this.startX - pageX );
            const dy = Math.abs( this.startY - pageY );

            const isSwiping = dx > dy && dx > UNCERTAINTY_THRESHOLD;

            // We are likely to be swiping, let's prevent the scroll event.
            if ( dx > dy ) {
                event.preventDefault();
                this.props.onSwipeStart();
            }

            if ( isSwiping === true || dy > UNCERTAINTY_THRESHOLD ) {
                this.isSwiping = isSwiping;
                this.startX = pageX; // Shift the starting point.
                return; // Let's wait the next touch event to move something.
            }
        }

        if ( this.isSwiping !== true ) {
            return;
        }

        event.preventDefault();

        this.lastX = pageX;

        if ( nodeThatClaimedTheEvent === null ) {
            nodeThatClaimedTheEvent = this.node;
        }

        this.setState( {
            isDragging: true,
        } );
    }

    handleTouchEnd ( event ) {
        nodeThatClaimedTheEvent = null;

        if ( !this.started ) {
            return;
        }

        this.started = false;

        if ( this.isSwiping !== true ) {
            return;
        }

        event.preventDefault();

        const { isOpen } = this.state;
        const dx = ( this.lastX - this.startX ) * ( isOpen ? 1 : -1 );
        const minX = Math.abs( this.maxOffset / 4 );

        this.setState( {
            isOpen: dx > minX ? !isOpen : isOpen,
            isDragging: false,
        }, () => {
            this.props.onSwipeEnd( this.state.isOpen );
        } );
    }

    handleTouchTap ( event ) {
        const { isDragging, isOpen } = this.state;
        if ( !isOpen && !isDragging ) {
            this.props.onTouchTap( event );
        }
    }

    getOffset () {
        let offset = this.state.isOpen ? this.maxOffset : 0;

        if ( this.state.isDragging ) {
            offset += this.lastX - this.startX;
        }

        return offset;
    }

    render () {
        const { isDragging } = this.state;
        const {
            action,
            children,
            className,
            containerClassName,
            disabled,
            height,
            springConfig,
        } = this.props;

        const styles = {
            offset: isDragging ? this.getOffset() : spring( this.getOffset(), springConfig ),
        };

        const touchEvents = disabled ? {} : {
            onTouchEnd: this.handleTouchEnd,
            onTouchMove: this.handleTouchMove,
            onTouchStart: this.handleTouchStart,
        };

        return (
            <div
                { ...touchEvents }
                ref={ this.setRootNode }
            >
                <Motion style={ styles }>{ ( { offset } ) =>
                    <article
                        className={ cx( 'Swipeable', containerClassName ) }
                        style={ { height } }
                    >
                        <aside ref={ this.setActionNode }>
                            { action }
                        </aside>
                        <section
                            className={ className }
                            onTouchTap={ this.handleTouchTap }
                            style={ translate( offset ) }
                        >
                            { children }
                        </section>
                    </article>
                }</Motion>
            </div>
        );
    }
}

export default Swipeable;
