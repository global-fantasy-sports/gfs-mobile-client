import React, { Component } from 'react';

import './style.scss';

class TabsComponent extends Component {
    constructor () {
        super();
        this.onTabHeadClick = ::this.onTabHeadClick;
    }

    renderHead () {
        const headElements = this.headBlock.props.children;
        const countOfChildren = React.Children.count( headElements );
        const elementWidth = parseInt( 100 / countOfChildren, 10 );
        const widthAdjustment = 100 - elementWidth * countOfChildren;
        const selectedTab = this.props.selectedTab;

        let className, newElementWidth;
        return React.Children.map( headElements, ( iChild, iIndex ) => {
            newElementWidth = ( countOfChildren <= iIndex + 1 ) ? elementWidth + widthAdjustment : elementWidth;
            className = iChild.props.className;
            return React.cloneElement(
                iChild,
                {
                    className: 'tab-header-item '
                    + ` ${ selectedTab === iIndex + 1 ? 'active-tab' : '' } ${ className || '' } `,
                    'data-index': iIndex + 1,
                    style: { width: newElementWidth + '%' },
                }
            );
        } );
    }

    renderBody () {
        const bodyElements = this.bodyBlock.props.children;
        const selectedTab = this.props.selectedTab;

        let className;
        return React.Children.map( bodyElements, ( iChild, iIndex ) => {
            className = iChild.props.className;
            return React.cloneElement(
                iChild,
                {
                    className: `tab-body-item
                                ${ selectedTab === iIndex + 1 ? 'active-tab' : '' } ${ className || '' }`,
                    'data-index': iIndex + 1,
                }
            );
        } );
    }

    onTabHeadClick ( iEvent ) {
        const tabIndex = iEvent.target.getAttribute( 'data-index' );
        this.props.onSelect( tabIndex );
        iEvent.preventDefault();
        iEvent.stopPropagation();
    }

    render () {
        this.headBlock = this.props.children[0];
        this.bodyBlock = this.props.children[1];

        return (
            <div className={ `${ this.props.class || '' } tabs` }>
                <div className="tab-header" onClick={ this.onTabHeadClick }>
                    { this.renderHead() }
                </div>
                <div className="tab-body">
                    { this.renderBody() }
                </div>
            </div>
        );
    }
}

TabsComponent.propTypes = {
    children: React.PropTypes.array.isRequired,
    class: React.PropTypes.string,
    onSelect: React.PropTypes.func.isRequired,
    selectedTab: React.PropTypes.number.isRequired,
};

export default TabsComponent;
