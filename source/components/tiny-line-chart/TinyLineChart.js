import React, { Component, PropTypes } from 'react';
import { AreaChart, Area, CartesianGrid, ReferenceLine } from 'recharts';

import './TinyLineChart.scss';

class TinyLineChart extends Component {

    static propTypes = {
        color: PropTypes.string,
        data: PropTypes.array.isRequired,
        fill: PropTypes.string,
        height: PropTypes.number,
        width: PropTypes.number,
    };

    static defaultProps = {
        color: '#0000ff',
        height: 50,
        width: 150,
    };

    render () {
        const { color, data, fill, height, width } = this.props;
        return (
            <div className="TinyLineChart">
                <AreaChart
                    data={ data.map( value => ( { value } ) ) }
                    height={ height }
                    width={ width }
                >
                    <Area
                        connectNulls
                        dataKey="value"
                        dot={ { fill: 'white', fillOpacity: 1, stroke: color, strokeWidth: 2 } }
                        fill={ fill || color }
                        isAnimationActive={ false }
                        margin={ { top: 0, right: 0, bottom: 0, left: 0 } }
                        stroke={ color }
                        strokeWidth={ 3 }
                        type="linear"
                    />
                    <CartesianGrid horizontal={ false } />
                    <ReferenceLine alwaysShow isFront y={ 0 } />
                </AreaChart>
            </div>
        );
    }
}

export default TinyLineChart;
