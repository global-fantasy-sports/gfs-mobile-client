export const CoreDomain = '/';

export const WSRoutesConstants = new Map();

export const WSChannelConstants = new Map();
WSChannelConstants.set( 'login', 'login' );
WSChannelConstants.set( 'notifications', 'user-notification' );
WSChannelConstants.set( 'notificationsRead', 'user-notifications-read' );
WSChannelConstants.set( 'games', 'games' );
WSChannelConstants.set( 'competitionChanged', 'competition-room-changed' );
WSChannelConstants.set( 'getCompetitionRoom', 'competition-room' );
WSChannelConstants.set( 'userBalance', 'user-info' );

export const API = {
    appInfo: '/api/v2/auth/service/app-info/',
    lineups: '/api/v2/:sport/:league/lineups/',
    notificationsMarkAll: '/api/v2/auth/notifications/all/mark-read/',
    notificationsRecent: '/api/v2/auth/notifications/recent/',
    notificationsNext: '/api/v2/auth/notifications/?start_id=:id&limit=10',
    session: '/api/v2/auth/session/',
    sportAthleteHistory: '/api/v2/:sport/:league/athletes/:athleteId/history/',
    sportAthletes: '/api/v2/:sport/:league/athletes/',
    sportFavoriteAthletes: '/api/v2/:sport/:league/athletes/favorites/',
    sportFavoriteAthleteDelete: '/api/v2/:sport/:league/athletes/favorites/:id/',
    sportMatches: '/api/v2/:sport/:league/contests/',
    sportEvent: '/api/v2/:sport/:league/sport-events/open/',
    sportGameCreate: '/api/v2/:sport/:league/games/:gameType/',
    sportInfo: '/api/v2/:sport/:league/info/selector/',
    sportMyGames: '/api/v2/:sport/:league/games/my/',
    sportParticipants: '/api/v2/:sport/:league/participants/',
    sportParticipantsResults: '/api/v2/:sport/:league/participants/results/',
    sportParticipantsResultsLegend: '/api/v2/:sport/:league/participants/results/legend/',
    sportRoom: '/api/v2/:sport/:league/rooms/:roomId/',
    sportRoomGames: '/api/v2/:sport/:league/rooms/:roomId/games/',
    sportRooms: '/api/v2/:sport/:league/rooms/',
    sportTeams: '/api/v2/:sport/:league/teams/',
    translations: '/api/v2/auth/service/translations/',
    userInfo: '/api/v2/auth/users/me/',
    sportSimulationSetDate: '/api/v2/:sport/:league/simulation/set-current-date/',
    sportSimulationGetDate: '/api/v2/:sport/:league/simulation/current-date/',
    sportSimulationRestart: '/api/v2/:sport/:league/simulation/restart/',
    sportSimulationSetPoint: '/api/v2/:sport/:league/simulation/set-point-to/',
};
