export const sportTypeKey = 'sportType';
export const leagueTypeKey = 'leagueType';
export const SESSION_ID_KEY = 'session';
export const SESSION_ID_PARAM = 'session_id';
export const SESSION_ID_HEADER = 'x-session-id';

export const defaultSport = 'soccer';
export const defaultLeague = 'csl';

export const TIME_FORMAT = {
    athlete_info: 'llll',
    athlete_info_log_date: 'MMM Do',
    athlete_info_log_year: 'YYYY',
    athlete_info_salary: 'MMM Do',
    competition_room: 'MMM Do',
    competition_room_card: 'MMM Do',
    competition_room_header: 'MMM Do LT',
    counter: 'MMM Do',
    datetime_full: 'ddd, ll',
    datetime_time: 'LT',
    game_card_date: 'MMM Do',
    game_card_day: 'Do',
    notification: 'llll',
    schedule_date: 'MMM Do',
    schedule_time: 'LT',
};

export default {
    started: 'APP::STARTED',
    initialised: 'APP::INITIALISED',
    sportChanged: 'APP::SPORT_CHANGED',
    leagueChanged: 'APP::LEAGUE_CHANGED',
    getInfo: 'APP::INFO',
    preload: 'APP::LEAGUE_DATA_PRELOAD',
};
