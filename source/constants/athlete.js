export default {
    getAll: 'ATHLETES::ALL',
    getFavorites: 'ATHLETES::FAVORITES',
    history: 'ATHLETE::HISTORY',
};
