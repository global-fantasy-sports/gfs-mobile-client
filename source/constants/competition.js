export const GET_COMPETITIONS_REQUEST = 'GET_COMPETITIONS_REQUEST';
export const GET_COMPETITIONS_REQUEST_FAIL = 'GET_COMPETITIONS_REQUEST_FAIL';
export const GET_COMPETITIONS_REQUEST_SUCCESS = 'GET_COMPETITIONS_REQUEST_SUCCESS';

export const RESET_COMPETITION_ROOMS_FILTER = 'RESET_COMPETITION_ROOMS_FILTER';
export const SET_MY_GAMES_FILTER = 'SET_MY_GAMES_FILTER';
export const TOGGLE_COMPETITION_ROOMS_FILTER_PANEL = 'TOGGLE_COMPETITION_ROOMS_FILTER_PANEL';
export const UPDATE_COMPETITIONS_ROOMS = 'UPDATE_COMPETITIONS_ROOMS';
export const UPDATE_COMPETITION_ROOM = 'UPDATE_COMPETITION_ROOM';
export const UPDATE_COMPETITION_ROOMS_ENTRY_FEE_RANGE = 'UPDATE_COMPETITION_ROOMS_ENTRY_FEE_RANGE';
export const UPDATE_COMPETITION_ROOMS_FILTER = 'UPDATE_COMPETITION_ROOMS_FILTER';
export const UPDATE_COMPETITION_ROOM_REQUEST = 'UPDATE_COMPETITION_ROOM_REQUEST';

export const GET_COMPETITION_ROOM_REQUEST = 'GET_COMPETITION_ROOM_REQUEST';
export const GET_COMPETITION_ROOM_SUCCESS = 'GET_COMPETITION_ROOM_SUCCESS';

export const GAME_TYPE_STRINGS = {
    'Top 15%': 'top15',
    'Head 2 head': 'head2head',
    '50/50': 'fifty2fifty',
    Quadruple: 'quadruple',
    'Double Up': 'doubleUp',
    'Top 20%': 'top20',
    'Free-roll': 'freeRoll',
};

export const COMPETITION_ROOM_GROUPS = {
    'Free-roll': 'freeRoll',
    '50/50': 'fifty',
    'Top 20%': 'twenty',
    'Double Up': 'double',
    'Head 2 head': 'head',
    'Quadruple': 'quadruple',  // eslint-disable-line
    'Top 15%': 'fifteen',
};

export const COMPETITION_ROOM_TYPE_EXPLANATIONS = {
    'Free-roll': {
        title: 'Here you can play for free!',
        text: 'All you need to do is to enter with your lineup and you can begin your DFS adventure!',
    },
    '50/50': {
        title: 'Uncomplicated! Get started easily!',
        text: 'One half of the competition room are winners - the other half are not!',
    },
    'Top 20%': {
        title: 'High stakes game with big payouts!',
        text: 'The best 20% of all players are winners!',
    },
    'Double Up': {
        title: 'Double or nothing!',
        text: 'You double-up your entry fee!',
    },
    'Head 2 head': {
        title: 'Who’s the best?!',
        text: 'Two players face-off with one man left standing!',
    },
    'Quadruple': { // eslint-disable-line
        title: 'It’s time to make a 4x return on investment!',
        text: 'If you win you quadruple your entry fee!',
    },
    'Top 15%': {
        title: 'High stakes game with big payouts!',
        text: 'The best 15% of all players are winners!',
    },
};

export default {
    getAll: 'COMPETITIONS',
    getWithGames: 'COMPETITION::GAMES',
    changed: 'COMPETITION::CHANGED',
};
