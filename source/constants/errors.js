const ErrorConstants = new Map();
ErrorConstants.set( 'emailIsEmpty', 'Enter email' );
ErrorConstants.set( 'emailIsNotCorrect', 'Enter correct email' );
ErrorConstants.set( 'emailIsVeryLong', 'Email is very long' );
ErrorConstants.set( 'passwordIsEmpty', 'Enter password' );
ErrorConstants.set( 'passwordIsVeryLong', 'Password is very long' );
ErrorConstants.set( 'passwordIsVeryShort', 'Password is very short' );
ErrorConstants.set( 'nameIsEmpty', 'Enter name' );
ErrorConstants.set( 'nameIsVeryShort', 'Name is very short' );
ErrorConstants.set( 'termsShouldBeChosen', 'Please accept Terms & Conditions' );
ErrorConstants.set( 'countryIsEmpty', 'Country is required' );
ErrorConstants.set( 'stateIsEmpty', 'State is required' );

export default ErrorConstants;
