export default {
    create: 'GAME::CREATE',
    edit: 'GAME::EDIT',
    forgetNewGames: 'GAME::FORGET_NEW',
    getMy: 'GAMES::MY',
    remove: 'GAME::REMOVE',
    changed: 'GAME::CHANGED',
};
