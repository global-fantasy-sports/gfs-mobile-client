export default {
    root: 'NOTIFICATIONS',
    markAll: 'NOTIFICATIONS::MARK_READ::ALL',
    received: 'NOTIFICATIONS::RECEIVED',
    toggleNotifications: 'NOTIFICATIONS::TOGGLE',
};
