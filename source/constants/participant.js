export default {
    getAll: 'PARTICIPANTS::ALL',
    getAllResults: 'PARTICIPANTS::ALL::RESULTS',
    getResultsLegend: 'PARTICIPANTS::RESULTS::LEGEND',
    pointsChanged: 'PARTICIPANTS::POINTS::CHANGED',
};
