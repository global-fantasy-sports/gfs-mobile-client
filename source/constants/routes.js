import { defaultSport } from '../constants/app';

const RoutesConstants = new Map();
RoutesConstants.set( 'homePage', '/' );
RoutesConstants.set( 'lineups', 'lineups/' );
RoutesConstants.set( 'loginPage', 'login/' );
RoutesConstants.set( 'registrationPage', 'registration/' );
RoutesConstants.set( 'sportSelectPage', 'select-sport/' );
RoutesConstants.set( 'leagueSelectPage', 'select-league/' );
RoutesConstants.set( 'competitionPage', 'competition/' );
RoutesConstants.set( 'lobbyPage', 'lobby/' );
RoutesConstants.set( 'rosterPage', 'roster/' );
RoutesConstants.set( 'gamesPage', 'games/' );
RoutesConstants.set( 'wizardPage', 'wizard/' );
RoutesConstants.set( 'athlete', 'athletes/:athleteId/' );
RoutesConstants.set( 'schedule', 'schedule/' );
RoutesConstants.set( 'comparison', 'comparison/' );

if ( process.env.WITH_SELECTOR === 'yes' ) {
    RoutesConstants.set( 'loginRedirectPage', RoutesConstants.get( 'sportSelectPage' ) );
} else if ( process.env.WITH_SELECTOR_LEAGUES === 'yes' ) {
    RoutesConstants.set( 'loginRedirectPage', `/${defaultSport}/` );
} else {
    RoutesConstants.set( 'loginRedirectPage', '/' );
}

export default RoutesConstants;
