
// Temporary object for participants score card in the NFL game
export const scoresDataNFL = [
    {
        name: 'Passing TD',
        rules: '+0.00PTs',
    },
    {
        name: '25 passing yards',
        rules: '+0.00PTs',
    },
    {
        name: '300+ yard passing game',
        rules: '+0.00PTs',
    },
    {
        name: 'Interception',
        rules: '+0.00PTs',
    },
    {
        name: '10 rushing yards',
        rules: '+0.00PTs',
    },
    {
        name: 'Rushing TD',
        rules: '+0.00PTs',
    },
    {
        name: '100+ yard rushing game',
        rules: '+0.00PTs',
    },
    {
        name: '10 receiving yards',
        rules: '+0.00PTs',
    },
    {
        name: 'Reception',
        rules: '+0.00PTs',
    },
    {
        name: 'Receiving TD',
        rules: '+0.00PTs',
    },
    {
        name: '100+ yard receiving game',
        rules: '+0.00PTs',
    },
    {
        name: 'Punt return for TD',
        rules: '+0.00PTs',
    },
    {
        name: 'Kickoff return for TD',
        rules: '+0.00PTs',
    },
    {
        name: 'Fumble lost',
        rules: '+0.00PTs',
    },
    {
        name: '2 point conversion (pass, run, or catch)',
        rules: '+0.00PTs',
    },
    {
        name: 'Offensive fumble recovery TD',
        rules: '+0.00PTs',
    },
];
