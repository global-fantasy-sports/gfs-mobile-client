export default {
    setDate: 'SIMULATION::SET_DATE',
    getDate: 'SIMULATION::GET_DATE',
    restart: 'SIMULATION::RESTART',
    restarted: 'SIMULATION::RESTARTED',
    setPoint: 'SIMULATION::SET_POINT',
    setPointSuccess: 'SIMULATION::CURRENT_TIME_CHANGED',
};
