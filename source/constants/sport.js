export const SPORT_GET_INFO_REQUEST = 'SPORT::GET_INFO_REQUEST';
export const SPORT_GET_INFO_FAIL = 'SPORT::GET_INFO_FAIL';
export const SPORT_GET_INFO_SUCCESS = 'SPORT::GET_INFO_SUCCESS';

export const UPDATE_PARTICIPANTS = 'UPDATE_PARTICIPANTS';
export const UPDATE_SPORTSMEN = 'UPDATE_SPORTSMEN';
export const UPDATE_GAMES = 'UPDATE_GAMES';
export const UPDATE_REAL_GAMES = 'UPDATE_REAL_GAMES';
export const UPDATE_TEAMS = 'UPDATE_TEAMS';
export const UPDATE_CURRENT_DATE = 'UPDATE_CURRENT_DATE';
export const UPDATE_LOCAL_DATE = 'UPDATE_LOCAL_DATE';
export const UPDATE_DST = 'UPDATE_DST';

export const SPORT_TOGGLE_FAVORITE = 'SPORT_TOGGLE_FAVORITE';
export const SPORT_ADD_FAVORITE = 'SPORT_ADD_FAVORITE';
export const SPORT_DELETE_FAVORITE = 'SPORT_DELETE_FAVORITE';

export const SPORT_SOCKET_LOGGED = 'SPORT_SOCKET_LOGGED';

export const UPDATE_PARTICIPANT_EVENT_SPORTSMAN_INDEX = 'UPDATE_PARTICIPANT_EVENT_SPORTSMAN_INDEX';
export const UPDATE_DEFENCE_TEAM_EVENT_TEAM_INDEX = 'UPDATE_DEFENCE_TEAM_EVENT_TEAM_INDEX';

export const LINEUP_POSITIONS = {
    football: ['qb', 'rb1', 'rb2', 'wr1', 'wr2', 'wr3', 'te', 'flex', 'dst'],
    soccer: ['fw1', 'fw2', 'mf1', 'mf2', 'mf3', 'df1', 'df2', 'df3', 'gk'],
};

export const LINEUP_POSITIONS_SHORT_NAMES = {
    qb: 'QB',
    rb: 'RB',
    rb1: 'RB',
    rb2: 'RB',
    wr: 'WR',
    wr1: 'WR',
    wr2: 'WR',
    wr3: 'WR',
    te: 'TE',
    flex: 'FLEX',
    dst: 'DST',
    gk: 'GK',
    fw1: 'FW',
    fw2: 'FW',
    mf1: 'MF',
    mf2: 'MF',
    mf3: 'MF',
    df1: 'DF',
    df2: 'DF',
    df3: 'DF',
};


export const getAthletesPositions = ( sport ) => LINEUP_POSITIONS[sport] || [];

export const footballPositionsNames = ['QB', 'RB', 'RB', 'WR', 'WR', 'WR', 'TE', 'FLEX', 'DST'];
export const soccerPositionsNames = ['FW', 'FW', 'MF', 'MF', 'MF', 'DF', 'DF', 'DF', 'GK'];

export const getAthletesPositionsNames = ( iSport ) => {
    switch ( iSport ) {
        case 'football':
            return footballPositionsNames;
        case 'soccer':
            return soccerPositionsNames;
        default:
            return footballPositionsNames;
    }
};

export default {
    info: {
        root: 'SPORT::INFO',
    },
};
