export const GO_BACK = 'UI::GO_BACK';
export const REPORT_STEP = 'UI::REPORT_STEP';
export const DRAFT_STEP = 'UI::DRAFT_STEP';
export const DRAFT_POSITION_STEP = 'UI::DRAFT_POSITION_STEP';
export const COMPARISON_PAGE = 'UI::COMPARISON_PAGE';
export const SET_VARIABLE = 'UI::SET_VARIABLE';
