export default {
    login: {
        root: 'LOGIN',
        redirected: 'LOGIN::REDIRECTED',
    },
    logout: {
        root: 'LOGOUT',
        clean: 'LOGOUT::CLEAN',
        redirected: 'LOGOUT::REDIRECTED',
    },
    session: {
        restore: 'SESSION::RESTORE',
        get: 'SESSION::GET',
        request: 'SESSION::REQUEST',
        error: 'SESSION::ERROR',
    },
    user: {
        clean: 'USER::CLEAN',
        info: 'USER::INFO',
        update: 'USER::UPDATE',
    },
};
