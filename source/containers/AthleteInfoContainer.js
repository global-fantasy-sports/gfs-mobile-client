import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';
import { goTo } from '../actions/router';

import AthleteInfo from '../components/athlete-info';
import { selectParticipant, unselectParticipant } from '../actions/wizard';
import { setLineupPosition, undraftAthlete } from '../actions/lineups';
import { toggleFavoriteAthlete } from '../actions/sport';
import AthleteActions from '../actions/athlete';
import { unixDateToLocal } from '../utils/date';
import { setVariable } from '../actions/ui';
import { setComparison, removeComparison, resetComparison } from '../actions/comparison';
import { getComparisonAthletes } from '../selectors/comparison';

import { getSportData } from '../selectors/sport';
import { getLineupsMap } from '../selectors/lineups';
import { getSelectedTeamsCount, getWizardLineup } from '../selectors/wizard';
import { getSportParticipants } from '../selectors/participants';
import { ATHLETE_CARD_STATS } from '../constants/wizard';
import { TIME_FORMAT } from '../constants/app';
import { translate } from '../selectors/translations';


function getSportsmanStatsHistory ( athlete, sportState ) {
    if ( !athlete.history ) {
        return [];
    }

    const { matches } = sportState;
    const { history = [] } = athlete;

    return history.map( item => {
        // still contestId on backend
        const match = matches[item.contestId];

        return {
            ...item,
            match,
            date: match.startTimeUtc * 1000,
        };
    } );
}


function getAthleteGameLogs ( athlete, sportData ) {
    if ( !athlete.history ) {
        return [];
    }

    const { matches } = sportData;
    const { history = [] } = athlete;

    return history.map( item => {
        // still contestId on backend
        const match = matches[item.contestId];
        const gameLog = item.gameLog || {};

        return {
            date: match.startTimeUtc * 1000,
            gameLog,
        };
    } );
}


const mapStateToProps = ( state, ownProps ) => {
    const sportData = getSportData( state );
    const { athleteId } = ownProps.params;
    let athlete = sportData.sportsmen[athleteId] || {};

    const participants = getSportParticipants( state );
    const participant = participants.find( ( member ) => member.id === athlete.currentParticipantId ) || {};

    athlete = {
        ...participant,
        ...athlete,
    };

    const variables = state.reducer.ui.variables;
    const lineup = variables['from'] === 'lineup'  // eslint-disable-line dot-notation
        ? getLineupsMap( state )[variables.lineupId]
        : getWizardLineup( state.reducer.wizard.lineup, participants );

    // still contestId on backend
    let match = sportData.matches[participant.contestId] || {};
    match = {
        ...match,
        time: unixDateToLocal( match.startTimeUtc, TIME_FORMAT.athlete_info ),
    };

    const comparedAthletes = getComparisonAthletes( state );
    const compared = ( comparedAthletes.left && comparedAthletes.left.athleteId === athleteId )
         || ( comparedAthletes.right && comparedAthletes.right.athleteId === athleteId ) || false;

    const isSelected = Object.values( lineup ).some( ( entry ) => entry && entry.athleteId === athleteId );
    const isFavorite = sportData.favorites.includes( athleteId );
    const _t = translate( state );

    return {
        athlete,
        averageStats: athlete.averageStats || {},
        match,
        compared,
        currentAthleteId: athleteId,
        currentSport: sportData.sport,
        currentLeague: sportData.league,
        dataLoaded: !!Object.keys( athlete ).length && !!athlete.history,
        gameLogs: getAthleteGameLogs( athlete, sportData ),
        isFavorite,
        isSelected,
        lineup,
        selectedTeams: getSelectedTeamsCount( lineup ),
        statsByPosition: ATHLETE_CARD_STATS[athlete.position] || [],
        statsHistory: getSportsmanStatsHistory( athlete, sportData ),
        team: sportData.teams[participant.teamId] || {},
        variables,
        _t,
    };
};

function mergeProps ( state, { dispatch } ) {
    const { currentAthleteId, currentSport, currentLeague, lineup, variables } = state;

    return {
        ...state,
        onGoBack () {
            dispatch( goBack() );
        },
        onDraft () {
            const { pos, participantId, lineupId } = variables;

            dispatch( resetComparison( currentSport, currentLeague ) );
            if ( variables['from'] === 'lineup' ) {  // eslint-disable-line dot-notation
                dispatch( setLineupPosition( {
                    sport: currentSport, league: currentLeague,
                    id: lineupId }, pos, currentAthleteId ) );
                dispatch( goTo( `/lineups/${lineupId}/` ) );
            } else {
                dispatch( selectParticipant( pos, participantId ) );
                dispatch( goTo( '/wizard/select/' ) );
            }
        },
        onUndraft () {
            if ( variables['from'] === 'lineup' ) {  // eslint-disable-line dot-notation
                const { lineupId } = variables;
                dispatch( undraftAthlete( lineup, currentAthleteId ) );
                dispatch( goTo( `/lineups/${lineupId}/` ) );
            } else {
                dispatch( unselectParticipant( state.athlete.currentParticipantId ) );
                dispatch( goTo( '/wizard/select/' ) );
            }
        },
        onHandleFavorite () {
            dispatch( toggleFavoriteAthlete( currentAthleteId ) );
        },
        getAthleteHistory () {
            dispatch( AthleteActions.history( currentAthleteId ) );
        },
        onCompare ( { athleteId, position } ) {
            dispatch( setVariable( { position } ) );
            dispatch( setComparison( currentSport, currentLeague, athleteId, position ) );
            dispatch( goTo( '/comparison/' ) );
        },
        onUncompare ( athleteId ) {
            dispatch( removeComparison( currentSport, currentLeague, athleteId ) );
        },
    };
}

export default connect(
    mapStateToProps,
    null,
    mergeProps
)( AthleteInfo );
