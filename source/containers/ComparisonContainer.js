import _ from 'lodash';
import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';
import { goTo } from '../actions/router';
import { getFavoriteAthletesComparison, getFavoriteParticipantsComparison } from '../selectors/athletes';
import { addFavoriteAthlete, deleteFavoriteAthlete } from '../actions/sport';
import { getChosenSport, getTeams, isSportDataReady, getSportData } from '../selectors/sport';
import { getChosenLeague, getSalarycapBudget } from '../selectors/league';
import {
    getComparisonAthletes,
    getComparisonCandidates,
    getComparisonPosition,
    getComparisonStats,
    getComparisonTableStats,
} from '../selectors/comparison';
import {
    getSelectedTeamsCount,
    getTotals,
    getWizardData,
    getWizardLineup,
} from '../selectors/wizard';
import { getLineupsMap } from '../selectors/lineups';
import { addFavoritesToParticipants, getAvailableParticipants } from '../selectors/participants';
import { setLineupPosition, undraftAthlete } from '../actions/lineups';
import { replaceParticipant, unselectParticipant } from '../actions/wizard';
import ComparisonComponent from '../components/comparison';
import { setVariable } from '../actions/ui';
import {
    removeComparison,
    resetComparison,
    selectComparisonAthlete,
    setComparison,
} from '../actions/comparison';
import { translate } from '../selectors/translations';

const mapStateToProps = ( state ) => {
    const sportData = getSportData( state );
    const wizard = getWizardData( state );
    const salarycapBudget = getSalarycapBudget( state );

    let participants = getAvailableParticipants( state );
    participants = addFavoritesToParticipants( participants, sportData.favorites );

    const variables = state.reducer.ui.variables;

    const favorites = ( variables.from === 'wizard' )
        ? getFavoriteParticipantsComparison( state )
        : getFavoriteAthletesComparison( state );

    const lineup = variables.from === 'lineup'
        ? getLineupsMap( state )[variables.lineupId] || {}
        : getWizardLineup( wizard.lineup, participants );
    const totals = getTotals( state );
    const comparisonAthletes = getComparisonAthletes( state );
    const position = getComparisonPosition( state );
    const athletes = variables.from === 'wizard'
        ? participants.filter( ( athlete ) => athlete.position === position )
        : getComparisonCandidates( state );
    const teams = getTeams( state );

    const currentBudget = ( variables.from === 'wizard' )
        ? salarycapBudget - _.get( totals, 'salary', 0 )
        : salarycapBudget - _.get( lineup, 'totals.salary', 0 );

    return {
        _t: translate( state ),
        athletes,
        budget: currentBudget,
        comparisonAthletes,
        currentSport: getChosenSport( state ),
        currentLeague: getChosenLeague( state ),
        favorites,
        isReady: isSportDataReady( state ),
        leftAthleteTeam: teams[( comparisonAthletes.left && comparisonAthletes.left.teamId )] || {},
        lineup,
        position,
        rightAthleteTeam: teams[( comparisonAthletes.right && comparisonAthletes.right.teamId )] || {},
        selectedComparison: sportData.comparison.selectedId,
        selectedIds: variables.selected,
        selectedTeams: getSelectedTeamsCount( lineup ),
        variables,
        stats: getComparisonStats( state ),
        statsTable: getComparisonTableStats( state ),
    };
};


function mergeProps ( state, { dispatch } ) {
    const { lineup, currentSport, currentLeague, variables, comparisonAthletes } = state;

    return {
        ...state,
        onParticipantClick: ( { participantId, athleteId } ) => {
            dispatch( setVariable( { participantId } ) );
            dispatch( goTo( `/athletes/${athleteId}/` ) );
        },
        onParticipantDraft: ( side )=> {
            const { pos, lineupId } = variables;
            const participantId = comparisonAthletes[side].currentParticipantId;
            const athleteId = comparisonAthletes[side].athleteId;
            const otherSide = side === 'left' ? comparisonAthletes.right || {} : comparisonAthletes.left || {};
            const replaceParticipantId = otherSide.currentParticipantId;

            dispatch( resetComparison( currentSport, currentLeague ) );
            if ( variables.from === 'lineup' ) {
                dispatch( setLineupPosition( {
                    sport: currentSport, league: currentLeague, id: lineupId }, pos, athleteId )
                );
                dispatch( goTo( `/lineups/${lineupId}/` ) );
            } else {
                dispatch( replaceParticipant( pos, replaceParticipantId, participantId ) );
                dispatch( goTo( '/wizard/select/' ) );
            }
        },
        onParticipantUndraft: ( { athleteId, participantId } ) => {
            if ( variables.from === 'lineup' ) {
                dispatch( undraftAthlete( lineup, athleteId ) );
            } else {
                dispatch( unselectParticipant( participantId ) );
            }
        },

        onAddFavoriteAthlete: ( athleteId ) => {
            dispatch( addFavoriteAthlete( athleteId, currentSport, currentLeague ) );
        },

        onDeleteFavoriteAthlete: ( athleteId ) => {
            dispatch( deleteFavoriteAthlete( athleteId, currentSport, currentLeague ) );
        },

        addComparison: ( { athleteId, position } ) => {
            dispatch( setComparison( currentSport, currentLeague, athleteId, position ) );
        },
        removeComparison: ( athleteId ) => {
            dispatch( removeComparison( currentSport, currentLeague, athleteId ) );
        },
        selectComparisonAthlete: ( athleteId ) => {
            dispatch( selectComparisonAthlete( currentSport, currentLeague, athleteId ) );
        },
        goPrevStep: () => {
            dispatch( resetComparison( currentSport, currentLeague ) );

            if ( variables.from === 'lineup' ) {
                dispatch( goTo( `/lineups/${variables.lineupId}/` ) );
            } else {
                dispatch( goTo( '/wizard/select/' ) );
            }
        },
        goPrevRouterStep: () => {
            dispatch( goBack() );
        },
    };
}

const comparisonContainer = connect(
    mapStateToProps,
    null,
    mergeProps
)( ComparisonComponent );
export default comparisonContainer;
