import { connect } from 'react-redux';

import CompetitionComponent from '../components/competition';
import { getSportData, getChosenSport } from '../selectors/sport';
import { getChosenLeague } from '../selectors/league';
import { getUserId } from '../selectors/user';
import { getAthletesPositionsNames, getAthletesPositions } from '../constants/sport';
import CompetitionActions from '../actions/competition';
import ParticipantsActions from '../actions/participant';
import { translate } from '../selectors/translations';
import { markWinners } from '../selectors/games';
import { joinCompetition } from '../actions/wizard';
import {
    addPrizesToGames,
    getCurrentCompetition,
} from '../selectors/competitions';
import { push } from 'react-router-redux';
import { goTo } from '../actions/router';

const mapDispatchToProps = {
    getCompetitionGames: CompetitionActions.getWithGames,
    getParticipantResults: ParticipantsActions.getAllResults,
    getResultsLegend: ParticipantsActions.getResultsLegend,
    onPlay: joinCompetition,
    goTo,
    push,
};

const mapStateToProps = ( state, ownProps ) => {
    const sportState = getSportData( state );
    const sport = getChosenSport( state );
    const league = getChosenLeague( state );

    const competitionId = parseInt( ownProps.params.competitionId, 10 );
    const userId = getUserId( state );
    const competition = getCurrentCompetition( competitionId )( state );
    const {
        matches,
    } = sportState;
    let gamesWithPrizes = addPrizesToGames( competitionId )( state );
    gamesWithPrizes = markWinners( state, sportState.competitionRooms, gamesWithPrizes );

    if ( competition && ( competition.started || competition.finished ) ) {
        gamesWithPrizes = gamesWithPrizes.sort( ( game1, game2 ) => game1.place - game2.place );
    }
    const disablePlayButton = state.reducer.loading.gamesLoaded === false ||
        ( competition.type === 'Free-roll' &&
          gamesWithPrizes.some( ( game ) => game.userData.id === userId ) );

    return {
        _t: translate( state ),
        competition,
        competitionId,
        currentDate: sportState.currentDate.date,
        currentUserId: userId,
        dataLoaded: Boolean( Object.keys( competition ).length ),
        disablePlayButton,
        games: gamesWithPrizes,
        league,
        localDate: sportState.currentDate.local,
        matches,
        playerPositions: getAthletesPositions( sport ),
        playerPositionsNames: getAthletesPositionsNames( sport ),
        sport,
        competitionRoomBackTo: state.routing.competitionRoomBackTo,
    };
};

function mergeProps ( state, actions ) {
    const { competitionId, sport, league, competitionRoomBackTo } = state;
    return {
        ...state,
        ...actions,
        getCompetitionGames: () => actions.getCompetitionGames( competitionId ),
        getParticipantResults: () => actions.getParticipantResults(),
        getResultsLegend: () => actions.getResultsLegend(),
        onPlay: ( actionName = '' ) => () => {
            window.piwik.push( ['trackEvent', 'Comp Room', 'Play', actionName + ' - Comp room'] );
            return actions.onPlay( sport, league, competitionId );
        },
        onExit: () => {
            if ( competitionRoomBackTo ) {
                actions.push( competitionRoomBackTo );
            } else {
                actions.goTo( '/lobby/' );
            }

        },
    };
}

const competitionContainer = connect( mapStateToProps, mapDispatchToProps, mergeProps );

export default competitionContainer( CompetitionComponent );
