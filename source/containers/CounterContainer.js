import { connect } from 'react-redux';

import CounterComponent from '../components/game-card/counter';
import { getSportData } from '../selectors/sport';

const mapStateToProps = state => {
    const dateObj = getSportData( state ).currentDate;
    return {
        serverTime: dateObj.date || 0,
        localTime: dateObj.local || 0,
    };
};

const counterContainer = connect( mapStateToProps );

export default counterContainer( CounterComponent );
