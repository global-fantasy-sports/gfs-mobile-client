import { connect } from 'react-redux';
import DialogWrapper from '../components/dialog/DialogWrapper';
import { hideModalDialog } from '../actions/dialogs';
import { CONFIRM_OK, CONFIRM_CANCEL } from '../constants/dialogs';
import { translate } from '../selectors/translations';
import { getChosenLeague } from '../selectors/league';


const mapStateToProps = state => {
    const dialogs = state.reducer.dialogs;
    return {
        ...dialogs,
        chosenLeague: getChosenLeague( state ),
        _t: translate( state ),
    };
};

const mapDispatchToProps = ( dispatch ) => {
    return {
        onHide () {
            dispatch( hideModalDialog() );
        },
        onCancel () {
            dispatch( { type: CONFIRM_CANCEL } );
            dispatch( hideModalDialog() );
        },
        onClick () {
            dispatch( { type: CONFIRM_OK } );
        },
    };
};

const DialogsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)( DialogWrapper );

export default DialogsContainer;
