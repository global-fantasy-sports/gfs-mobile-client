import { connect } from 'react-redux';

import Page404Component from '../components/page404';
import { translate } from '../selectors/translations';

const mapStateToProps = ( state ) => ( {
    _t: translate( state ),
} );

const error404Container = connect( mapStateToProps );

export default error404Container( Page404Component );
