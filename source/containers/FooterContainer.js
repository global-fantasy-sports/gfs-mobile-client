import { connect } from 'react-redux';
import { translate } from '../selectors/translations';

import FooterComponent from '../components/footer';
import { getChosenSport } from '../selectors/sport';
import { getChosenLeague } from '../selectors/league';

const mapDispatchToProps = () => ( {} );

const mapStateToProps = ( state ) => ( {
    currentLocation: state.routing.location.pathname,
    sport: getChosenSport( state ),
    league: getChosenLeague( state ),
    _t: translate( state ),
} );

const footerContainer = connect( mapStateToProps, mapDispatchToProps );

export default footerContainer( FooterComponent );
