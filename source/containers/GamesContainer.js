import { connect } from 'react-redux';
import _ from 'lodash';
import { goTo } from '../actions/router';

import GamesComponent from '../components/games';
import { getSportData } from '../selectors/sport';
import { prepareCompetitionsGames, sortGamesByDateStart } from '../selectors/games';
import { defaultFilteredRoomsSelector } from '../selectors/competitions';
import { getAthletesPositions, getAthletesPositionsNames } from '../constants/sport';
import { setMyGamesFilter } from '../actions/competition';
import GameActions from '../actions/game';
import { translate } from '../selectors/translations';
import { getChosenLeague } from '../selectors/league';
import { initFromScratch, joinCompetition } from '../actions/wizard';

const defence = 'dst';

const addSportsmenToGames = ( games, sportsmen, teams, sport ) => {
    return games.map( game => {
        game.sportsmen = {};
        getAthletesPositions( sport ).map( position => {
            const sportsmanId = game[position];
            game.sportsmen[position] = { ...sportsmen[sportsmanId] };
            return game;
        } );
        if ( sport === 'nfl' ) {
            game.sportsmen[defence] = { ...teams[game.dst] };
        }

        return game;
    } );
};

const mapStateToProps = state => {
    const sportState = getSportData( state );
    const rooms = defaultFilteredRoomsSelector( state );
    const gamesFilter = sportState.filters.myGames;
    const league = getChosenLeague( state );

    let games = prepareCompetitionsGames( state, sportState.competitionRooms, sportState.games );
    games = sortGamesByDateStart( games );
    games = addSportsmenToGames( games, sportState.sportsmen, sportState.teams, sportState.sport );
    const now = sportState.currentDate.date || 0;
    let gamesFilteredByStatus = games.filter( game => {
        const { started, finished } = game.competition;
        let status;
        if ( gamesFilter === 'all' ) {
            status = 'all';
        } else if ( !started ) {
            status = 'soon';
        } else if ( started && !finished ) {
            status = 'live';
        } else if ( finished ) {
            status = 'finished';
        }
        return status === gamesFilter;
    } );

    let gamesOrderedByDate;
    if ( gamesFilter === 'soon' ) {
        gamesOrderedByDate = _.orderBy( gamesFilteredByStatus, ['competition.startDate'], ['asc'] );
    } else if ( gamesFilter === 'finished' || gamesFilter === 'live' ) {
        gamesOrderedByDate = _.orderBy( gamesFilteredByStatus, ['competition.startDate'], ['desc'] );
    } else {
        let finishedGames = gamesFilteredByStatus.filter( game => game.competition.finished );
        let unfinishedGames = gamesFilteredByStatus.filter( game => !game.competition.finished );
        gamesOrderedByDate = _.union(
            _.orderBy( unfinishedGames, ['competition.started', 'competition.startDate'], ['asc', 'asc'] ),
            _.orderBy( finishedGames, ['competition.startDate'], ['desc'] ),
        );
    }
    return {
        games: gamesOrderedByDate,
        gamesFilter,
        gamesTotal: games.length,
        gamesLoaded: sportState.games.status !== 'blank',
        now,
        sport: sportState.sport,
        roomsCount: rooms.length,
        league,
        playerPositions: getAthletesPositions( sportState.sport ),
        playerPositionsNames: getAthletesPositionsNames( sportState.sport ),
        _t: translate( state ),
    };
};

const mergeProps = ( state, { dispatch } ) => {
    const { sport, roomsCount, league } = state;

    return {
        ...state,
        getGames () {
            dispatch( GameActions.getMy( sport, league ) );
        },
        onFilter ( filter ) {
            dispatch( setMyGamesFilter( filter, sport, league ) );
        },
        onCreate: ( button ) => () => {
            if ( roomsCount ) {
                window.piwik.push(
                    ['trackEvent', 'My Games', 'Play', `Create new game (${button})`]
                );
                dispatch( initFromScratch( sport, league ) );
            } else {
                dispatch( goTo( '/lobby/' ) );
            }
        },
        onJoinCompetition ( competitionId ) {
            dispatch( joinCompetition( sport, league, competitionId ) );
        },
        onEdit ( gameId ) {
            dispatch( GameActions.edit( sport, league, gameId ) );
        },
        onRemove ( gameId ) {
            window.piwik.push( ['trackEvent', 'My Games', 'Delete Game (non finished games, on Swipe) Click'] );
            dispatch( GameActions.remove( sport, league, gameId ) );
        },
        onForgetNewGames () {
            dispatch( GameActions.forgetNewGames( sport, league ) );
        },
    };
};

const gamesContainer = connect( mapStateToProps, null, mergeProps );

export default gamesContainer( GamesComponent );
