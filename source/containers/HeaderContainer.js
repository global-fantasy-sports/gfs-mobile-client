import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import HeaderComponent from '../components/header';
import SimulationActions from '../actions/simulation';
import { toggleNotifications } from '../actions/notification';
import { getBalance, getCurrentDateObj, getSimulation, getSimLoader, getSimError } from '../selectors/app';
import { getUserAvatar, getUserName } from '../selectors/user';

import { getNotifications } from '../selectors/notifications';
import SESSION from '../actions/session';
import { translate } from '../selectors/translations';

const setSimulationTime = ( currentDate, number, period ) => currentDate.add( number, period ).unix();

const mapDispatchToProps = {
    logout: SESSION.remove,
    changeTimeSimulation: SimulationActions.setDate,
    changePointSimulation: SimulationActions.setPoint,
    restartSimulation: SimulationActions.restart,
    onClearSimError: SimulationActions.clearError,
    toggleNotifications,
};

const mapStateToProps = createStructuredSelector( {
    avatar: getUserAvatar,
    balance: getBalance,
    name: getUserName,
    currentDateObject: getCurrentDateObj,
    notifications: getNotifications,
    showNotifications: ( state ) => state.reducer.notifications.showNotifications,
    simulation: getSimulation,
    simLoader: getSimLoader,
    simError: getSimError,
    _t: translate,
} );

const mergeProps = ( state, actions, ownProps ) => {
    const { currentDateObject } = state;

    return {
        ...state,
        ...actions,
        ...ownProps,
        restartSimulation: () => actions.restartSimulation(),
        changeTimeSimulationMinutes: ( hours, min ) => {
            return actions.changeTimeSimulation(
                setSimulationTime( currentDateObject, ( parseInt( hours, 10 ) * 60 ) + parseInt( min, 10 ), 'minute' )
            );
        },
        changeTimeSimulationOneHour: () => {
            return actions.changeTimeSimulation( setSimulationTime( currentDateObject, 1, 'hour' ) );
        },
        changeTimeSimulationSixHours: () => {
            return actions.changeTimeSimulation( setSimulationTime( currentDateObject, 6, 'hour' ) );
        },
        changeTimeSimulationOneDay: () => {
            return actions.changeTimeSimulation( setSimulationTime( currentDateObject, 1, 'day' ) );
        },
        changeTimeSimulationOneWeek: () => {
            return actions.changeTimeSimulation( setSimulationTime( currentDateObject, 1, 'week' ) );
        },
        changePointSimulationKickOff: () => actions.changePointSimulation( 'kick-off' ),
        changePointsSimulationMid: () => actions.changePointSimulation( 'mid-game' ),
        changePointsSimulationResults: () => actions.changePointSimulation( 'results' ),
    };
};


const headerContainer = connect( mapStateToProps, mapDispatchToProps, mergeProps );

export default headerContainer( HeaderComponent );
