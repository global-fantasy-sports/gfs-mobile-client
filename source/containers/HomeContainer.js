import { connect } from 'react-redux';
import { replace } from 'react-router-redux';

import HomeComponent from '../components/home';
import { getSessionId } from '../selectors/user';
import RoutesConstants from '../constants/routes';

function mapStateToProps ( state ) {
    return {
        session: getSessionId( state ),
    };
}

const mapDispatchToProps = {
    onRedirect: () => replace( RoutesConstants.get( 'loginRedirectPage' ) ),
};


// const mapDispatchToProps = (state) => ( {
//     onRedirect: () => {
//         if ( state.session ) {
//             replace( RoutesConstants.get( 'loginRedirectPage' ) );
//         } else {
//
//         }
//     }
// } );

const HomeContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)( HomeComponent );

export default HomeContainer;
