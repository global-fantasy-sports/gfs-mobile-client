import { connect } from 'react-redux';
import { goTo, redirectTo } from '../actions/router';
import LineupCandidatesComponent from '../components/lineup-candidates';
import { getChosenSport } from '../selectors/sport';
import { getChosenLeague, getSalarycapBudget } from '../selectors/league';
import { getCurrentLineup, getCandidates } from '../selectors/lineups';
import { getSelectedIds, getSelectedTeamsCount } from '../selectors/wizard';
import { setLineupPosition } from '../actions/lineups';
import { setVariable } from '../actions/ui';
import { setComparison, removeComparison } from '../actions/comparison';
import { translate } from '../selectors/translations';


const mapStateToProps = ( state, ownProps ) => {
    const id = ownProps.params.id;
    const position = ownProps.params.position;
    const lineup = getCurrentLineup( id )( state );
    const athletes = getCandidates( position )( state );
    const salarycapBudget = getSalarycapBudget( state );
    const favoriteAthletes = athletes.filter( ( athlete ) => athlete.isFavorite );
    return {
        athletes,
        currentSport: getChosenSport( state ),
        currentLeague: getChosenLeague( state ),
        favoriteAthletes,
        lineup,
        lineupId: id,
        position,
        salarycapBudget,
        selectedIds: getSelectedIds( lineup ),
        selectedTeams: getSelectedTeamsCount( lineup ),
        _t: translate( state ),
    };
};


function mergeProps ( state, { dispatch } ) {
    const { lineup, position, currentSport, currentLeague, selectedIds } = state;

    return {
        ...state,
        onParticipantClick ( { athleteId, participantId } ) {
            dispatch( setVariable( {
                lineupId: lineup.id,
                pos: position,
                participantId,
                from: 'lineup',
                selected: selectedIds,
            } ) );
            dispatch( goTo( `/athletes/${athleteId}/` ) );
        },
        onParticipantDraft ( { athleteId } ) {
            dispatch( setLineupPosition( lineup, position, athleteId ) );
            dispatch( redirectTo( `/lineups/${lineup.id}/` ) );
        },
        onParticipantUndraft ( { athleteId } ) {
            const pos = lineup.positions.find( ( p ) => lineup[p] && lineup[p].id === athleteId );
            dispatch( setLineupPosition( lineup, pos, null ) );
        },
        onCompare ( { participantId, athleteId, position: athletePosition } ) {
            dispatch( setVariable( {
                from: 'lineup',
                participantId,
                pos: position,
                lineupId: lineup.id,
                selected: selectedIds,
            } ) );

            dispatch( setComparison( currentSport, currentLeague, athleteId, athletePosition ) );
            dispatch( goTo( '/comparison/' ) );
        },
        onUncompare ( { athleteId } ) {
            dispatch( removeComparison( currentSport, currentLeague, athleteId ) );
        },
    };
}

const lineupCandidatesContainer = connect(
    mapStateToProps,
    null,
    mergeProps
)( LineupCandidatesComponent );

export default lineupCandidatesContainer;
