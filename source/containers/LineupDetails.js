import _ from 'lodash';
import { connect } from 'react-redux';
import { goTo, redirectTo } from '../actions/router';
import {
    cancelLineupEdit,
    deleteLineup,
    editLineup,
    saveLineup,
    setLineupName,
    setLineupPosition,
} from '../actions/lineups';
import { initFromLineup } from '../actions/wizard';
import { toggleWizardViewMode } from '../actions/wizardViewMode';
import { getChosenSport } from '../selectors/sport';
import { getChosenLeague, getSalarycapBudget } from '../selectors/league';
import { getSelectedIds } from '../selectors/wizard';
import {
    getCurrentLineup,
    getLineupPositions,
    getLineupTotals,
    isDataFetched,
    checkPositionAvailability,
} from '../selectors/lineups';
import { setVariable } from '../actions/ui';
import { setComparison } from '../actions/comparison';
import LineupDetailsComponent from '../components/lineup-details';
import { translate } from '../selectors/translations';
import { defaultFilteredRoomsSelector } from '../selectors/competitions';

function mapStateToProps ( state, ownProps ) {

    const currentSport = getChosenSport( state );
    const rooms = defaultFilteredRoomsSelector( state );
    const currentLeague = getChosenLeague( state );
    const salarycapBudget = getSalarycapBudget( state );

    const { id: lineupId } = ownProps.params;
    const fetched = isDataFetched( state );
    const lineup = fetched ? checkPositionAvailability( getCurrentLineup( lineupId )( state ) ) : {};
    const isEditing = lineupId === 'new' || lineup.changed === true;

    return {
        currentSport,
        currentLeague,
        isEditing,
        fetched,
        lineup,
        lineupId,
        positions: getLineupPositions( state ),
        salarycapBudget,
        selectedIds: getSelectedIds( lineup ),
        roomsCount: rooms.length,
        totals: getLineupTotals( lineup ),
        wizardViewMode: state.reducer.wizardViewMode,
        _t: translate( state ),
    };
}

function extractLineupData ( { id, name, positions, ...lineup } ) {
    const data = id === 'new' ? { name } : { id, name };
    data.lineup = positions.reduce( ( obj, pos ) => {
        obj[pos] = _.get( lineup, [pos, 'id'], null );
        return obj;
    }, {} );
    return data;
}

function mergeProps ( state, { dispatch } ) {

    const { lineup, currentSport, currentLeague, selectedIds, roomsCount } = state;

    return {
        ...state,
        onCancel: () => {
            dispatch( cancelLineupEdit() );
            if ( lineup.id === 'new' ) {
                dispatch( redirectTo( '/lineups/' ) );
            }
        },
        onCompare: ( { athleteId, participantId, pos, position } ) => {
            dispatch( setVariable( {
                from: 'lineup',
                participantId,
                pos,
                lineupId: lineup.id,
                selected: selectedIds,
            } ) );

            dispatch( setComparison( currentSport, currentLeague, athleteId, position ) );
            dispatch( goTo( '/comparison/' ) );
        },
        onCreateGame: () => {
            if ( roomsCount ) {
                window.piwik.push( ['trackEvent', 'My Line-Ups', 'Play', 'Play (from the line-up details)'] );
                dispatch( initFromLineup( currentSport, currentLeague, lineup.id ) );
                dispatch( goTo( '/wizard/' ) );
            } else {
                dispatch( goTo( '/lobby' ) );
            }
        },
        onDelete: () => {
            dispatch( deleteLineup( currentSport, currentLeague, lineup.id ) );
        },
        onGoBack: () => {
            dispatch( redirectTo( '/lineups/' ) );
        },
        onInitEditor: () => {
            dispatch( editLineup( currentSport, currentLeague, lineup.id ) );
        },
        onToggleWizardViewMode: () => {
            dispatch( toggleWizardViewMode() );
        },
        onNameChange: ( value ) => {
            dispatch( editLineup( currentSport, currentLeague, lineup.id ) );
            dispatch( setLineupName( lineup.id, value ) );
        },
        onPositionClear: ( position ) => {
            dispatch( setLineupPosition( lineup, position, null ) );
        },
        onPositionSelect: ( position ) => {
            dispatch( setVariable( {
                selected: selectedIds,
            } ) );
            dispatch( goTo( `/lineups/${lineup.id}/${position}/` ) );
        },
        onSave: () => {
            dispatch( saveLineup( currentSport, currentLeague, extractLineupData( lineup ) ) );
        },
        onShowAthleteInfo: ( position ) => {
            const athleteId = lineup[position].id;

            dispatch( setVariable( {
                from: 'lineup',
                lineupId: lineup.id,
                pos: position,
                selected: selectedIds,
            } ) );

            dispatch( goTo( `/athletes/${athleteId}/` ) );
        },
    };
}

const LineupDetailsContainer = connect(
    mapStateToProps,
    null,
    mergeProps
)( LineupDetailsComponent );

export default LineupDetailsContainer;
