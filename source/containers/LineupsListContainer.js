import { connect } from 'react-redux';
import { goTo } from '../actions/router';

import LineupsComponent from '../components/lineups';
import { getChosenSport } from '../selectors/sport';
import { getChosenLeague, getSalarycapBudget } from '../selectors/league';
import { getLineupsList } from '../selectors/lineups';
import { translate } from '../selectors/translations';
import { cancelLineupEdit, deleteLineup, duplicateLineup } from '../actions/lineups';
import { initFromLineup } from '../actions/wizard';
import { defaultFilteredRoomsSelector } from '../selectors/competitions';


const mapStateToProps = ( state ) => {
    const salarycapBudget = getSalarycapBudget( state );
    const currentSport = getChosenSport( state );
    const rooms = defaultFilteredRoomsSelector( state );

    return {
        currentSport,
        currentLeague: getChosenLeague( state ),
        roomsCount: rooms.length,
        salarycapBudget,
        lineups: getLineupsList( state ),
        _t: translate( state ),
    };
};

const mergeProps = ( props, { dispatch } ) => ( {
    ...props,
    onCreateGame ( id ) {
        if ( props.roomsCount ) {
            window.piwik.push( ['trackEvent', 'My Line-Ups', 'Play', 'Play with selected line-up (on Swipe)'] );
            dispatch( initFromLineup( props.currentSport, props.currentLeague, id ) );
            dispatch( goTo( '/wizard/' ) );
        } else {
            dispatch( goTo( '/lobby' ) );
        }
    },
    onDeleteLineup ( id ) {
        dispatch( deleteLineup( props.currentSport, props.currentLeague, id ) );
    },
    onDuplicateLineup ( id ) {
        dispatch( duplicateLineup( props.currentSport, props.currentLeague, id ) );
        dispatch( goTo( '/lineups/new/' ) );
    },
    onCreateLineup: ( button ) => {
        window.piwik.push( ['trackEvent', 'My Line-Ups', 'Create Line-up', `Create Line-up (with ${button} button)`] );
        dispatch( cancelLineupEdit() );
        dispatch( goTo( '/lineups/new/' ) );
    },
    onViewLineup ( id ) {
        dispatch( goTo( `/lineups/${ id }/` ) );
    },
} );

const lineupsContainer = connect(
    mapStateToProps,
    null,
    mergeProps,
)( LineupsComponent );

export default lineupsContainer;
