import { connect } from 'react-redux';
import LineupsRoot from '../pages/lineups/LineupsRoot';

import { fetchLineups } from '../actions/lineups';
import { isDataFetched, getFetchError } from '../selectors/lineups';
import { getChosenSport } from '../selectors/sport';
import { translate } from '../selectors/translations';

const mapStateToProps = ( state ) => {
    return {
        currentSport: getChosenSport( state ),
        error: getFetchError( state ),
        ready: isDataFetched( state ),
        _t: translate( state ),
    };
};

const mapDispatchToProps = ( dispatch ) => ( { dispatch } );

function mergeProps ( state, { dispatch }, ownProps ) {
    return {
        ...ownProps,
        ...state,
        onInit: () => dispatch( fetchLineups( state.currentSport ) ),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)( LineupsRoot );
