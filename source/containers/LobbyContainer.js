import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import LobbyComponent from '../components/lobby';
import { goToCompetitionRoom } from '../actions/router';
import {
    entryFeeRangeSelector,
    filteredCompetitionRooms,
    getAvailableCurrencies,
    isPanelOpenSelector,
    roomFiltersSelector,
    seasonSelector,
} from '../selectors/competitions';
import {
    resetCompetitionRoomsFilter,
    toggleCompetitionRoomFilterPanel,
    updateCompetitionRoomsEntryFeeRange,
    updateCompetitionRoomsFilter,
} from '../actions/competition';
import { getChosenSport } from '../selectors/sport';
import { translate } from '../selectors/translations';
import { showRulesDialog } from '../actions/dialogs';
import { getChosenLeague } from '../selectors/league';
import { hideOnboarding } from '../actions/onboarding';
import GameActions from '../actions/game';

const mapStateToProps = createStructuredSelector( {
    competitionRooms: filteredCompetitionRooms,
    currencies: getAvailableCurrencies,
    sport: getChosenSport,
    league: getChosenLeague,
    filters: roomFiltersSelector,
    entryFeeRange: entryFeeRangeSelector,
    season: seasonSelector,
    isOpen: isPanelOpenSelector,
    showExplanation: ( state ) => state.reducer.onboarding.competitionRooms,
    _t: translate,
} );

function mergeProps ( props, { dispatch } ) {
    const { sport, league } = props;
    return {
        ...props,
        ...bindActionCreators( {
            onGetGames: () => GameActions.getMy( sport, league ),
            onFilterChanged: ( payload ) => updateCompetitionRoomsFilter( payload, sport, league ),
            onFilterReset: () => resetCompetitionRoomsFilter( sport, league ),
            onGoToRoom: goToCompetitionRoom,
            onEntryFeeChanged: ( payload ) => updateCompetitionRoomsEntryFeeRange( payload, sport, league ),
            onToggleFilterPanel: () => toggleCompetitionRoomFilterPanel( sport, league ),
            onShowRulesDialog: showRulesDialog,
            hideExplanation: () => hideOnboarding( 'competitionRooms' ),
        }, dispatch ),
    };
}

const lobbyContainer = connect( mapStateToProps, null, mergeProps );

export default lobbyContainer( LobbyComponent );
