import { reduxForm } from 'redux-form';

import { validate, LoginValidator } from '../validators/user';
import LoginComponent from '../components/login';
import SESSION from '../actions/session';

const validateForm = ( { email, password } ) => ( {
    email: validate( email, ['emailIsEmpty', 'emailIsNotCorrect', 'emailIsVeryLong'], LoginValidator ),
    password: validate( password, ['passwordIsEmpty', 'passwordIsVeryLong', 'passwordIsVeryShort'], LoginValidator ),
} );

const mapDispatchToProps = {
    signIn: SESSION.create,
};

const mapStateToProps = iState => ( {
    facebookAPIPath: '',
    googleAPIPath: '',
    serverError: iState.reducer.auth.error,
} );

const loginContainer = reduxForm( {
    form: 'LoginComponentForm',
    fields: ['email', 'password'],
    validate: validateForm,
    touchOnBlur: false,
    touchOnChange: false,
}, mapStateToProps, mapDispatchToProps );

export default loginContainer( LoginComponent );
