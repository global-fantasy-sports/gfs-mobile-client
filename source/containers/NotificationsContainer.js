import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import NotificationsWrapper from '../components/notification/NotificationsWrapper';
import NotificationActions from '../actions/notification';
import { toggleNotifications } from '../actions/notification';
import { getNotifications } from '../selectors/notifications';
import { translate } from '../selectors/translations';


const mapDispatchToProps = {
    loadNextNotifications: NotificationActions.next,
    sendReadNotifications: NotificationActions.markAll,
    toggleNotifications,
};

const mapStateToProps = createStructuredSelector( {
    _t: translate,
    notifications: getNotifications,
    showNotifications: ( state ) => state.reducer.notifications.showNotifications,
} );

const notificationsContainer = connect( mapStateToProps, mapDispatchToProps );

export default notificationsContainer( NotificationsWrapper );
