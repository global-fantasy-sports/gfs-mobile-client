import { connect } from 'react-redux';
import Onboarding from '../components/onboarding/Onboarding';
import { hideOnboarding } from '../actions/onboarding';
import { translate } from '../selectors/translations';
import { getSalarycapBudget } from '../selectors/league';


const mapStateToProps = ( state, props ) => {
    const salarycapBudget = getSalarycapBudget( state );
    return {
        ...props,
        salarycapBudget,
        visibility: state.reducer.onboarding,
        _t: translate( state ),

    };
};

const mergeProps = ( state, { dispatch }, ownProps ) => {
    return {
        ...state,
        ...ownProps,
        closeOnboarding: () => {
            dispatch( hideOnboarding( ownProps.name ) );
            if ( ownProps.onClose ) {
                ownProps.onClose();
            }
        },
        visible: state.visibility[ownProps.name],
    };
};

const onboardingContainer = connect(
    mapStateToProps,
    null,
    mergeProps
)( Onboarding );

export default onboardingContainer;
