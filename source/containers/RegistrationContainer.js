import { reduxForm } from 'redux-form';

import { validate, RegistrationValidator } from '../validators/user';
import { registrationRequest } from '../actions/registration';
import RegistrationComponent from '../components/registration';
import APIConstants from '../constants/api';

const validateForm = ( { email, password, name, isEnabledTerms, country, state } ) => ( {
    email: validate( email, ['emailIsEmpty', 'emailIsNotCorrect', 'emailIsVeryLong'], RegistrationValidator ),
    name: validate( name, ['nameIsEmpty', 'nameIsVeryShort'], RegistrationValidator ),
    password: validate( password,
                        ['passwordIsEmpty', 'passwordIsVeryLong', 'passwordIsVeryShort'],
                        RegistrationValidator
                    ),
    isEnabledTerms: validate( isEnabledTerms, ['termsShouldBeChosen'], RegistrationValidator ),
    country: validate( country, ['countryIsEmpty'], RegistrationValidator ),
    state: validate( state, ['stateIsEmpty'], RegistrationValidator ),
} );

const registerUser = ( { email, password }, iDispatch ) => {
    iDispatch( registrationRequest( { email, password } ) );
};

const mapDispatchToProps = () => ( {
    registerUser,
} );

const mapStateToProps = iState => {
    let serverError = iState.reducer.registration.error;

    if ( serverError ) {
        serverError = serverError[Object.keys( serverError )[0]];
    }
    return {
        processing: iState.reducer.registration.processing,
        facebookAPIPath: APIConstants.get( 'facebookLogin' ).route,
        googleAPIPath: APIConstants.get( 'googleLogin' ).route,
        serverError,
    };
};

const loginContainer = reduxForm( {
    form: 'RegistrationComponentForm',
    initialValues: { isEnabledTerms: false },
    fields: ['email', 'password', 'isEnabledTerms', 'name', 'state', 'country'],
    validate: validateForm,
    touchOnBlur: false,
    touchOnChange: false,
}, mapStateToProps, mapDispatchToProps );

export default loginContainer( RegistrationComponent );
