import { connect } from 'react-redux';

import RosterComponent from '../components/roster';
import CompetitionActions from '../actions/competition';
import ParticipantsActions from '../actions/participant';
import { getSportData, getChosenSport } from '../selectors/sport';
import { getParticipantsResultsLegend } from '../selectors/resultsLegend';
import { getAthletesPositionsNames, getAthletesPositions } from '../constants/sport';
import { translate } from '../selectors/translations';
import { scoresDataNFL } from '../constants/scoresDataNfl';
import { goBack } from 'react-router-redux';


const mapDispatchToProps = {
    getGame: CompetitionActions.getWithGames,
    getParticipantResults: ParticipantsActions.getAllResults,
    getResultsLegend: ParticipantsActions.getResultsLegend,
    goBack,
};

const mapStateToProps = ( iState, ownProps ) => {
    const sportState = getSportData( iState );
    const competitionId = parseInt( ownProps.params.competitionId, 10 );
    const gameId = parseInt( ownProps.params.gameId, 10 );

    const sport = getChosenSport( iState );
    const competition = sportState.competitionRooms[competitionId];

    const participantsResultsLegend = sport === 'soccer' ? getParticipantsResultsLegend( iState ) : scoresDataNFL;

    let games = competition ? competition.games : [];
    if ( !games ) {
        games = [];
    }
    return {
        competition,
        competitionId,
        game: games.filter( ( game ) => game.id === gameId )[0],
        gameId,
        participants: sportState.participants,
        sportsmen: sportState.sportsmen || {},
        matches: sportState.matches,
        defenceTeams: sportState.defenceTeams,
        teams: sportState.teams,
        sport,
        userId: iState.reducer.user.id,
        participantsResultsLegend,
        playerPositions: getAthletesPositions( sport ),
        playerPositionsNames: getAthletesPositionsNames( sport ),
        _t: translate( iState ),
    };
};

const mergeProps = ( state, actions ) => {
    const { competitionId, gameId } = state;
    return {
        ...state,
        ...actions,
        getParticipantResults: () => actions.getParticipantResults(),
        getResultsLegend: () => actions.getResultsLegend(),
        getGame: () => actions.getGame( competitionId, gameId ),
        onExit: () => actions.goBack(),
    };
};

const rosterContainer = connect( mapStateToProps, mapDispatchToProps, mergeProps )( RosterComponent );

export default rosterContainer;
