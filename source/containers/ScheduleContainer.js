import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import ScheduleComponent from '../components/schedule';
import { getMatchDatesFormatted, getGroupedMatches } from '../selectors/matches';
import { translate } from '../selectors/translations';

const mapStateToProps = createStructuredSelector( {
    _t: translate,
    matchDays: getMatchDatesFormatted,
    groupedMatches: getGroupedMatches,
} );

const scheduleContainer = connect( mapStateToProps );

export default scheduleContainer( ScheduleComponent );
