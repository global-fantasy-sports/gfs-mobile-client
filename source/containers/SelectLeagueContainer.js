import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { createStructuredSelector } from 'reselect';

import SelectLeagueComponent from '../components/select-league';
import { getChosenSport } from '../selectors/sport';
import { getSportLeagues } from '../selectors/league';
import { translate } from '../selectors/translations';

function mergeProps ( state, { dispatch } ) {
    return {
        ...state,
        selectLeague: ( league ) => {
            dispatch( push( `/${state.sport}/${league}/` ) );
        },
    };
}

const mapStateToProps = createStructuredSelector( {
    sport: getChosenSport,
    leagues: getSportLeagues,
    _t: translate,
} );

const selectLeagueContainer = connect( mapStateToProps, null, mergeProps );

export default selectLeagueContainer( SelectLeagueComponent );
