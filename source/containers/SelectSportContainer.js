import { connect } from 'react-redux';

import SelectSportComponent from '../components/select-sport';
import { getChosenSport } from '../selectors/sport';
import { translate } from '../selectors/translations';

const mapStateToProps = state => {
    return {
        currentSport: getChosenSport( state ) || '',
        sportsArray: Object.keys( ( ( ( state.reducer.app || {} ).settings || {} ).sports || {} ) ),
        _t: translate( state ),
    };
};

const selectSportContainer = connect( mapStateToProps );

export default selectSportContainer( SelectSportComponent );
