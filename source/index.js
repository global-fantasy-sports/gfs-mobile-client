import React from 'react';
import { render } from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { Provider } from 'react-redux';

import getStore from './store/index';
import { WSRoutesConstants } from './constants/api';
import getRoutes from './routes';

import './scss/main.scss';

injectTapEventPlugin();

if ( window.__SOCKETS__ ) {
    Object.keys( window.__SOCKETS__ ).forEach( iKey => {
        WSRoutesConstants.set( iKey, window.__SOCKETS__[iKey] );
    } );
}

const store = getStore();
const routes = getRoutes( store );

render(
    <Provider store={ store }>{ routes }</Provider>,
    document.getElementById( 'container' )
);
