import React, { Component } from 'react';

import AthleteInfoContainer from '../../containers/AthleteInfoContainer';
import HeaderContainer from '../../containers/HeaderContainer';


class AthletePage extends Component {

    componentDidMount () {
        document.body.scrollTop = 0;
    }

    render () {
        return (
            <div>
                <HeaderContainer isFixed />
                <AthleteInfoContainer { ...this.props } />
            </div>
        );
    }
}

export default AthletePage;
