import React, { Component } from 'react';

import HeaderContainer from '../../containers/HeaderContainer';
import ComparisonContainer from '../../containers/ComparisonContainer';

class ComparisonPage extends Component {
    render () {
        return (
            <div>
                <HeaderContainer isFixed />
                <ComparisonContainer />
            </div>
        );
    }
}

export default ComparisonPage;
