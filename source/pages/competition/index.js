import React, { Component } from 'react';
import { CompetitionContainer, HeaderContainer } from '../../containers';

class CompetitionPage extends Component {

    componentDidMount () {
        document.body.scrollTop = 0;
    }

    render () {
        return (
            <div>
                <HeaderContainer isFixed />
                <CompetitionContainer { ...this.props } />
            </div>
        );
    }
}

export default CompetitionPage;
