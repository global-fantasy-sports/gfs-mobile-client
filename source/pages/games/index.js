import React, { Component } from 'react';
import { GamesContainer, HeaderContainer, FooterContainer } from '../../containers';

class GamesPage extends Component {

    componentDidMount () {
        document.body.scrollTop = 0;
    }

    render () {
        return (
            <div>
                <HeaderContainer />
                <div className="with-footer">
                    <GamesContainer />
                </div>
                <FooterContainer />
            </div>
        );
    }
}

export default GamesPage;
