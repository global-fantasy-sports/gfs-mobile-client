import React, { Component } from 'react';
import { HomeContainer } from '../../containers';

class HomePage extends Component {

    componentDidMount () {
        document.body.scrollTop = 0;
    }

    render () {
        return (
            <HomeContainer />
        );
    }
}

export default HomePage;
