import React, { Component } from 'react';

import { LineupCandidatesContainer, HeaderContainer } from '../../containers';

class LineupCandidatesPage extends Component {
    render () {
        return (
            <div>
                <HeaderContainer isFixed />
                <LineupCandidatesContainer { ...this.props } />
            </div>
        );
    }
}

export default LineupCandidatesPage;
