import React, { Component } from 'react';

import { LineupDetailsContainer, HeaderContainer } from '../../containers';

class LineupDetailsPage extends Component {
    render () {
        return (
            <div>
                <HeaderContainer isFixed />
                <LineupDetailsContainer { ...this.props } />
            </div>
        );
    }
}

export default LineupDetailsPage;
