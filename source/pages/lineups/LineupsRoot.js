import React, { Component, PropTypes } from 'react';
import Page from '../../components/page';
import { FooterContainer, HeaderContainer } from '../../containers';
import ScrollLoaderComponent from '../../components/scroll-loader/index';

const ErrorMessage = ( props ) => (
    <div className="error-message">
        <h3>{ props._t( 'lineups.error-message.error' ) }</h3>
        <p>{ props._t( 'lineups.error-message.failed_load' ) }</p>
        { props.error ? <p>{ props.error }</p> : null }
    </div>
);

ErrorMessage.propTypes = {
    _t: PropTypes.func,
    error: PropTypes.node,
};

class LineupsRoot extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        children: PropTypes.node.isRequired,
        error: PropTypes.string,
        ready: PropTypes.bool.isRequired,
        onInit: PropTypes.func.isRequired,
    };

    componentDidMount () {
        this.handleInit();
    }

    handleInit () {
        if ( !this.props.ready ) {
            this.props.onInit();
        }
    }

    render () {
        const { _t, ready, error, children } = this.props;
        if ( ready ) {
            return children;
        }
        return (
            <div style={ { position: 'fixed' } }>
                <HeaderContainer isFixed />
                <Page className="lineups lineups--loading">
                    { error ? <ErrorMessage _t={ _t } error={ error } /> : <ScrollLoaderComponent /> }
                </Page>
                <FooterContainer />
            </div>
        );
    }
}

export default LineupsRoot;
