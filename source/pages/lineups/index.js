import React, { Component } from 'react';

import { FooterContainer, HeaderContainer, LineupsContainer } from '../../containers';

class LineupsPage extends Component {
    render () {
        return (
            <div>
                <HeaderContainer isFixed />
                <div className="with-footer">
                    <LineupsContainer />
                </div>
                <FooterContainer />
            </div>
        );
    }
}

export default LineupsPage;
