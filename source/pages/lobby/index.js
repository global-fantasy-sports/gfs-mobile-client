import React, { Component } from 'react';
import { LobbyContainer, HeaderContainer, FooterContainer } from '../../containers';


class LobbyPage extends Component {

    render () {
        return (
            <div>
                <HeaderContainer />
                <div className="with-footer">
                    <LobbyContainer />
                </div>
                <FooterContainer />
            </div>
        );
    }
}

export default LobbyPage;
