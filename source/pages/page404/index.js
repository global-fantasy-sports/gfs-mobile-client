import React, { Component } from 'react';
import Error404Container from '../../containers/Error404Container';

class Error404Page extends Component {
    render () {
        return (
            <Error404Container { ...this.props } />
        );
    }
}

export default Error404Page;
