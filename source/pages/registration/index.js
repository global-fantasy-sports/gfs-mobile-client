import React, { Component } from 'react';
import { RegistrationContainer } from '../../containers';

class RegistrationPage extends Component {
    render () {
        return (
            <RegistrationContainer />
        );
    }
}

export default RegistrationPage;
