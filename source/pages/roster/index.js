import React, { Component } from 'react';
import { RosterContainer, HeaderContainer } from '../../containers';

class RosterPage extends Component {

    componentDidMount () {
        document.body.scrollTop = 0;
    }

    render () {
        return (
            <div>
                <HeaderContainer isFixed />
                <RosterContainer { ...this.props } />
            </div>
        );
    }
}

export default RosterPage;
