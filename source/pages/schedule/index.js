import React, { Component } from 'react';
import { ScheduleContainer, HeaderContainer } from '../../containers';

class SchedulePage extends Component {
    render () {
        return (
            <div>
                <HeaderContainer isFixed />
                <ScheduleContainer />
            </div>
        );
    }
}

export default SchedulePage;
