import React, { Component } from 'react';
import { SelectLeagueContainer, HeaderContainer } from '../../containers';

class SelectLeaguePage extends Component {

    render () {
        return (
            <div>
                <HeaderContainer simulation={ false } />
                <SelectLeagueContainer { ...this.props } />
            </div>
        );
    }
}

export default SelectLeaguePage;
