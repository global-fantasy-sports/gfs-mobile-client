import React, { Component } from 'react';
import { SelectSportContainer, HeaderContainer } from '../../containers';

class SelectPage extends Component {

    componentDidMount () {
        document.body.scrollTop = 0;
    }

    render () {
        return (
            <div>
                <HeaderContainer simulation={ false } />
                <SelectSportContainer />
            </div>
        );
    }
}

export default SelectPage;
