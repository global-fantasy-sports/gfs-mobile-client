import AppTypes from '../constants/app';
import SimulationTypes from '../constants/simulation';
import { SIM_CLEAR_ERROR } from '../actions/simulation';

const initialState = {
    error: '',
    started: false,
    initialised: false,
    chosenSport: null,
    reload: false,
    simulation: {
        loader: false,
        error: '',
    },
};

export default ( state = initialState, action ) => {
    const reReq = new RegExp( 'SIMULATION::(?!GET_DATE).*::REQUEST' );
    if ( reReq.test( action.type ) ) {
        state = { ...state, simulation: { ...state.simulation, loader: true, error: '' } };
    }
    const reFail = new RegExp( 'SIMULATION::(?!GET_DATE).*::FAIL' );
    if ( reFail.test( action.type ) ) {
        state = { ...state, simulation: { ...state.simulation, loader: false, error: action.payload } };
    }


    switch ( action.type ) {
        case AppTypes.started:
            return { ...state, started: true, initialised: false };
        case AppTypes.initialised:
            return { ...state, started: true, initialised: true };
        case AppTypes.sportChanged:
            return { ...state, chosenSport: action.payload };
        case AppTypes.leagueChanged:
            return { ...state, chosenLeague: action.payload };
        case `${ AppTypes.getInfo }::SUCCESS`:
            return { ...state, settings: action.payload.data, initialised: true };
        case SimulationTypes.restarted:
            return { ...state, reload: true };
        case `${ SimulationTypes.setDate }::SUCCESS`:
            return { ...state, reload: true };
        case SimulationTypes.setPointSuccess:
            return { ...state, reload: true };
        case SIM_CLEAR_ERROR:
            return { ...state, simulation: { ...state.simulation, error: '' } };
        default:
            return state;
    }
};
