import Types from '../constants/user';
import { SESSION_ID_KEY } from '../constants/app';

let initialState = {
    error: '',
    authenticated: false,
    session: null,
    redirect: false,
    purge: false,
    sessionDied: false,
};

export default ( state = initialState, action ) => {
    switch ( action.type ) {
        case Types.session.error:
            return {
                ...state,
                authenticated: false,
                session: null,
                purge: false,
                error: action.payload,
                sessionDied: true,
            };
        case `${Types.login.root}::SUCCESS`:
            return {
                ...initialState,
                authenticated: true,
                session: action.payload.data[SESSION_ID_KEY],
                redirect: true,
                purge: false,
                error: '',
            };
        case `${Types.session.get}::SUCCESS`:
            return {
                ...initialState,
                authenticated: true,
                session: action.payload.data[SESSION_ID_KEY],
                redirect: true,
                purge: false,
                error: '',
            };
        case `${Types.login.root}::FAIL`:
            return {
                ...state,
                authenticated: false,
                session: null,
                error: action.payload,
                redirect: false,
            };
        case Types.login.redirected:
            return { ...state, redirect: false };
        case `${Types.logout.root}::SUCCESS`:
            return { ...state, authenticated: false, session: null, purge: true };
        case Types.logout.clean:
            return { ...initialState };
        default:
            return state;
    }
};
