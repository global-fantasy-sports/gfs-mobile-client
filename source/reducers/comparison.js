import ATHLETE from '../constants/athlete';
import {
    SET_COMPARISON,
    RESET_COMPARISON,
    REMOVE_COMPARISON,
    SELECT_COMPARISON_ATHLETE,
} from '../constants/comparison';

const initialState = {
    position: null,
    limits: null,
    leftId: null,
    rightId: null,
    selectedId: null,
};

export default ( sport, league, state = Object.assign( {}, initialState ), action ) => {
    if ( sport !== action.sport || league !== action.league ) {
        return state;
    }

    switch ( action.type ) {
        case SET_COMPARISON:
            const { leftId, rightId, selectedId } = state;
            const { athleteId, position } = action.payload;

            const propName = ( !leftId || ( leftId && rightId && selectedId === leftId ) )
                ? 'leftId' : 'rightId';

            return {
                ...state,
                position,
                [propName]: athleteId,
                selectedId: athleteId,
            };

        case SELECT_COMPARISON_ATHLETE:
            return { ...state, selectedId: action.payload };

        case REMOVE_COMPARISON:
            if ( action.payload === state.leftId ) {
                return { ...state, leftId: null, selectedId: state.rightId };
            }

            if ( action.payload === state.rightId ) {
                return { ...state, rightId: null, selectedId: state.leftId };
            }

            return state;
        case RESET_COMPARISON:
            return {
                ...state,
                leftId: null,
                rightId: null,
                selectedId: null,
                position: null,
            };
        case `${ATHLETE.getAll}::SUCCESS`:
            return { ...state, limits: action.payload.data.limits };
        default:
            return state;
    }
};
