import CompetitionTypes from '../constants/competition';
import GameTypes from '../constants/game';

const updateRoom = (
        iCompetitions = {},
        iNewCompetition,
        iGames,
        iUsers,
        iParticipantResults,
        iParticipants,
    ) => {
    iCompetitions[iNewCompetition.id] = { ...iCompetitions[iNewCompetition.id], ...iNewCompetition };
    iGames.forEach( ( game ) => {
        game.userData = iUsers[game.userId];
    } );
    iCompetitions[iNewCompetition.id].games = iGames;
    iCompetitions[iNewCompetition.id].participantResults = {};
    iParticipants.forEach( ( participant ) => {
        let a = iParticipantResults.filter( p => p.participantId === participant.id )[0];
        iCompetitions[iNewCompetition.id].participantResults[participant.athleteId] = a;
    } );
    return { ...iCompetitions };
};

const updateRooms = ( iCompetitions, iNewCompetitions ) => {
    iNewCompetitions.forEach( ( room ) => {
        iCompetitions[room.id] = { ...iCompetitions[room.id], ...room };
    } );
    return { ...iCompetitions };
};

export default ( sport, league, state = {}, action ) => {
    if ( sport !== action.sport || league !== action.league ) {
        return state;
    }

    const { data } = action.payload || {};

    switch ( action.type ) {
        case `${ CompetitionTypes.getAll }::SUCCESS`:
            return updateRooms( state, data.rooms );
        case `${ CompetitionTypes.getWithGames }::SUCCESS`:
            return updateRoom( state, data.room, data.games, data.users, data.participantResults, data.participants );
        case `${ GameTypes.getMy }::SUCCESS`:
            return updateRooms( state, data.rooms );
        case CompetitionTypes.changed:
            return updateRooms( state, data.rooms );
        default:
            return state;
    }
};
