import Types from '../constants/simulation';

let initialState = {
    local: null,
    date: null,
};

export default ( sport, league, state = Object.assign( {}, initialState ), action ) => {
    if ( sport !== action.sport || league !== action.league ) {
        return state;
    }

    switch ( action.type ) {
        case `${Types.getDate}::SUCCESS`:
            return {
                ...state,
                local: ( new Date() ).valueOf(),
                date: action.payload.data.currentTime,
            };
        default:
            return state;
    }
};
