import { UPDATE_DST } from '../constants/sport';

export default ( sport, league, state = {}, action ) => {
    if ( sport !== action.sport || league !== action.league ) {
        return state;
    }

    switch ( action.type ) {
        case UPDATE_DST:
            return { ...state, ...action.payload };
        default:
            return state;
    }
};
