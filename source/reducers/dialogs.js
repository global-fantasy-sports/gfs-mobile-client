import { SHOW_CONFIRM_DIALOG, HIDE_DIALOG, OPEN_BOTTOM_SIDE_MENU, CLOSE_BOTTOM_SIDE_MENU } from '../constants/dialogs';

let initialState = {
    showDialog: false,
    bottomSideMenu: false,
};

export default ( state = initialState, action ) => {
    switch ( action.type ) {
        case SHOW_CONFIRM_DIALOG:
            return {
                showDialog: true,
                ...action.payload,
            };

        case HIDE_DIALOG:
            return initialState;

        case OPEN_BOTTOM_SIDE_MENU: {
            return {
                ...state,
                bottomSideMenu: true,
            };
        }

        case CLOSE_BOTTOM_SIDE_MENU: {
            return {
                ...state,
                bottomSideMenu: false,
            };
        }

        default:
            return state;
    }
};
