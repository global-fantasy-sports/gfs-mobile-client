import Types from '../constants/event';

export default ( sport, league, state = {}, action ) => {
    if ( sport !== action.sport || league !== action.league ) {
        return state;
    }

    switch ( action.type ) {
        case `${Types.open}::SUCCESS`:
            return { ...action.payload.data };
        default:
            return state;
    }
};
