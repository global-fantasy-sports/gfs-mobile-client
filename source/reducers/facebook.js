import {
    FACEBOOK_FRIENDS_FETCHED,
    FACEBOOK_INVITE_SUCCESS,
    FACEBOOK_REGISTERED_FRIENDS_FETCHED,
    FACEBOOK_SEARCH,
    FACEBOOK_STATUS,
} from '../constants/facebook';
import UserTypes from '../constants/user';

const initialState = {
    friends: [],
    processing: false,
    search: '',
    status: null,
};

function parseFriends ( friends ) {
    return friends.map( friend => ( {
        id: friend.id,
        name: friend.name,
        picture: friend.picture.data.url,
        registered: false,
        invited: false,
    } ) );
}

function updateRegistered ( friends, registeredIds ) {
    const registeredSet = new Set( registeredIds );
    return friends.map( friend => ( {
        ...friend,
        registered: registeredSet.has( friend.id ),
    } ) );
}

export default function facebook ( state = initialState, action ) {
    if ( action.type === UserTypes.logout.clean ) {
        return initialState;
    }

    switch ( action.type ) {
        case FACEBOOK_STATUS:
            return {
                ...state,
                status: action.payload,
            };
        case FACEBOOK_FRIENDS_FETCHED:
            return {
                ...state,
                friends: parseFriends( action.payload ),
            };
        case FACEBOOK_REGISTERED_FRIENDS_FETCHED:
            return {
                ...state,
                friends: updateRegistered( state.friends, action.payload ),
            };
        case FACEBOOK_INVITE_SUCCESS:
            return {
                ...state,
                friends: state.friends.map( f => f.id === action.payload ? { ...f, invited: true } : f ),
            };
        case FACEBOOK_SEARCH:
            return {
                ...state,
                search: action.payload,
            };
        default:
            return state;
    }
}
