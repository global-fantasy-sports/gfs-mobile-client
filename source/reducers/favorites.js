import { SPORT_DELETE_FAVORITE, SPORT_ADD_FAVORITE } from '../constants/sport';
import Types from '../constants/athlete';
import UserTypes from '../constants/user';


export default ( sport, league, state = [], action ) => {
    if ( action.type === UserTypes.logout.clean ) {
        return {};
    }

    if ( sport !== action.sport || league !== action.league ) {
        return state;
    }

    switch ( action.type ) {
        case `${SPORT_ADD_FAVORITE}::SUCCESS`:
        case `${SPORT_DELETE_FAVORITE}::SUCCESS`:
            return action.payload.data.map( String );
        case `${Types.getFavorites}::SUCCESS`:
            return action.payload.data.favorites.map( String );
        case UserTypes.logout.clean:
            return [];
        default:
            return state;
    }
};
