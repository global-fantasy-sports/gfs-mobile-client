import {
    RESET_COMPETITION_ROOMS_FILTER,
    SET_MY_GAMES_FILTER,
    TOGGLE_COMPETITION_ROOMS_FILTER_PANEL,
    UPDATE_COMPETITION_ROOMS_ENTRY_FEE_RANGE,
    UPDATE_COMPETITION_ROOMS_FILTER,
} from '../constants/competition';


const getInitialState = () => ( {
    competitionRooms: {
        isOpen: false,
        currency: null,
        entryFeeRange: {},
        beginner: false,
        featured: false,
        guaranteed: false,
        top15: true,
        head2head: true,
        fifty2fifty: true,
        quadruple: true,
        doubleUp: true,
        top20: true,
        freeRoll: true,
    },
    myGames: 'all',
} );

const isAllUnchecked = ( filters ) => {
    return !(
        filters.head2head ||
        filters.fifty2fifty ||
        filters.doubleUp ||
        filters.top20 ||
        filters.top15 ||
        filters.quadruple ||
        filters.freeRoll
    );
};

export default ( sport, league, state = getInitialState(), action ) => {
    if ( sport !== action.sport || league !== action.league ) {
        return state;
    }

    switch ( action.type ) {
        case RESET_COMPETITION_ROOMS_FILTER:
            const initialState = getInitialState();
            return { ...state, competitionRooms: { ...initialState.competitionRooms, isOpen: true } };
        case UPDATE_COMPETITION_ROOMS_FILTER:
            const currentFilters = state.competitionRooms;
            const payload = typeof action.payload === 'string'
                ? { [action.payload]: !currentFilters[action.payload] }
                : action.payload;
            const competitionRooms = { ...currentFilters, ...payload };

            return isAllUnchecked( competitionRooms )
                ? { ...state }
                : { ...state, competitionRooms };
        case UPDATE_COMPETITION_ROOMS_ENTRY_FEE_RANGE:
            return { ...state, competitionRooms: { ...state.competitionRooms, entryFeeRange: action.payload } };
        case TOGGLE_COMPETITION_ROOMS_FILTER_PANEL:
            const isOpen = !state.competitionRooms.isOpen;
            return { ...state, competitionRooms: { ...state.competitionRooms, isOpen } };
        case SET_MY_GAMES_FILTER:
            return { ...state, myGames: action.payload };
        default:
            return state;
    }
};
