import _ from 'lodash';
import Types from '../constants/game';
import UserTypes from '../constants/user';
import { WIZARD_SAVE } from '../actions/wizard';

const updateGames = ( games, newGames, strict ) => {
    if ( newGames ) {
        newGames.forEach( ( game ) => {
            if ( !strict || games[game.id] ) {
                games[game.id] = {
                    ...games[game.id],
                    ...game,
                };
            }
        } );
    }
    return _.omitBy( games, ( game ) => game.deleted );
};

const forgetNewGames = ( state ) =>
    _.mapValues( state, ( g ) => g.isNew ? { ...g, isNew: false } : g );

export default ( sport, league, state = {}, action ) => {
    if ( action.type === UserTypes.logout.clean ) {
        return {};
    }

    if ( sport !== action.sport || league !== action.league ) {
        return state;
    }

    switch ( action.type ) {
        case `${ Types.getMy }::REQUEST`:
            return { ...state, status: 'blank' };
        case `${ Types.getMy }::SUCCESS`:
            delete state.status;
            return updateGames( state, action.payload.data.games );
        case `${ WIZARD_SAVE }::SUCCESS`:
            return action.payload.data.games
                ? updateGames( state, action.payload.data.games.map( g => ( { ...g, isNew: true } ) ) )
                : state;
        case Types.forgetNewGames:
            return forgetNewGames( state );
        case Types.changed:
            return updateGames( state, action.payload.data.games, true );
        default:
            return state;
    }
};
