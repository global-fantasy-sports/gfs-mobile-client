import { reducer as formReducer } from 'redux-form';

import app from './app';
import auth from './auth';
import dialogs from './dialogs';
import facebook from './facebook';
import infoPopups from './infoPopups';
import lineupEdit from './lineupEdit';
import notifications from './notifications';
import comparison from './comparison';
import competitionRooms from './competitionRooms';
import currentDate from './currentDate';
import defenceTeams from './defenceTeams';
import events from './events';
import favorites from './favorites';
import filters from './filters';
import games from './games';
import lineups from './lineups';
import matches from './matches';
import participants from './participants';
import resultsLegend from './resultsLegend';
import season from './season';
import socketLogged from './socketLogged';
import sportsmen from './sportsmen';
import teams from './teams';
import translations from './translations';
import ui from './ui';
import user from './user';
import wizard from './wizard';
import wizardViewMode from './wizardViewMode';
import onboarding from './onboarding';
import info from './sport-info';
import router from './router';
import loading from './loading';

export const rootReducers = {
    form: formReducer,
    routing: router,
    reducer: {
        app,
        auth,
        dialogs,
        facebook,
        infoPopups,
        lineupEdit,
        loading,
        notifications,
        onboarding,
        ui,
        user,
        wizard,
        wizardViewMode,
    },
    translations,
};

export const sportReducers = {
    comparison,
    competitionRooms,
    currentDate,
    defenceTeams,
    events,
    favorites,
    filters,
    games,
    info,
    season,
    lineups,
    matches,
    participants,
    resultsLegend,
    socketLogged,
    sportsmen,
    teams,
};
