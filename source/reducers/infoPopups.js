import { HIDE_INFO_POPUP, SHOW_INFO_POPUP } from '../constants/infoPopups';

let initialState = {
    visibility: {
        draftStep: true,
        stakeStep: true,
        friendsStep: true,
        bottomSideMenu: true,
    },
};

export default ( state = initialState, action ) => {
    switch ( action.type ) {
        case HIDE_INFO_POPUP:
            return {
                ...state,
                visibility: {
                    ...state.visibility,
                    [action.payload]: false,
                },
            };
        case SHOW_INFO_POPUP:
            return {
                ...state,
                visibility: {
                    ...state.visibility,
                    [action.payload]: !state.visibility[action.payload],
                },
            };
        default:
            return state;
    }
};
