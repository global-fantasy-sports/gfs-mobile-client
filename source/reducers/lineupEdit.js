import _ from 'lodash';
import {
    LINEUPS_EDIT_CANCEL,
    LINEUPS_EDIT_START,
    LINEUPS_REMOVE_ATHLETE,
    LINEUPS_SAVE,
    LINEUPS_SET_NAME,
    LINEUPS_SET_POSITION,
} from '../actions/lineups';
import UserTypes from '../constants/user';

const initialState = {
    id: null,
};

function handleSetPosition ( state, payload ) {
    const { lineup, position, value } = payload;
    const { id, sport, league } = lineup;
    const nextState = state.id === id ? state : { id, sport, league };
    return {
        ...nextState,
        [position]: value,
    };
}

function handleRemoveAthlete ( state, payload ) {
    const {
        lineup: { id, sport, league, ...lineup },
        id: athleteId,
    } = payload;
    const nextState = state.id === id ? state : { id, sport, league };
    const position = _.findKey( lineup, { athleteId } );
    return position
        ? { ...nextState, [position]: null }
        : state;
}

function lineupEditReducer ( state = initialState, action ) {
    if ( action.type === UserTypes.logout.clean ) {
        return initialState;
    }

    switch ( action.type ) {
        case LINEUPS_EDIT_START:
            return state.id !== action.payload.id
            || state.sport !== action.payload.sport || state.league !== action.payload.league
                ? action.payload
                : { ...state };
        case LINEUPS_EDIT_CANCEL:
            return initialState;
        case `${LINEUPS_SAVE}::SUCCESS`:
            return initialState;
        case LINEUPS_SET_POSITION:
            return handleSetPosition( state, action.payload );
        case LINEUPS_REMOVE_ATHLETE:
            return handleRemoveAthlete( state, action.payload );
        case LINEUPS_SET_NAME:
            return { ...state, name: action.payload.name };
        default:
            return state;
    }
}

export default lineupEditReducer;
