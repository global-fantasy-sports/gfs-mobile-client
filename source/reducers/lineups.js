import _ from 'lodash';
import { LINEUPS_DELETE, LINEUPS_FETCH, LINEUPS_SAVE, LINEUPS_SAVE_WO_REDIRECT } from '../actions/lineups';
import { LINEUP_POSITIONS } from '../constants/wizard';
import UserTypes from '../constants/user';


function parseLineups ( sport, data ) {
    const lineups = data.map( ( item ) => ( {
        ...item,
        sport,
        positions: LINEUP_POSITIONS[sport],
    } ) );
    return _.keyBy( lineups, 'id' );
}

const initialState = {
    error: null,
    fetched: false,
    byId: {},
};

export default ( sport, league, state = Object.assign( {}, initialState ), action ) => {

    if ( action.type === UserTypes.logout.clean ) {
        return {};
    }

    if (
        _.get( action, 'payload.sport', sport ) !== sport ||
        _.get( action, 'payload.league', league ) !== league
    ) {
        return state;
    }

    switch ( action.type ) {
        case LINEUPS_FETCH:
            return {
                ...state,
                error: null,
            };
        case `${LINEUPS_FETCH}::SUCCESS`:
            return {
                ...state,
                fetched: true,
                byId: parseLineups( sport, action.payload.data.lineups ),
            };
        case `${LINEUPS_FETCH}::FAIL`:
            return {
                ...state,
                error: action.payload,
            };
        case `${LINEUPS_SAVE_WO_REDIRECT}::SUCCESS`:
        case `${LINEUPS_SAVE}::SUCCESS`:
            return {
                ...state,
                byId: {
                    ...state.byId,
                    ...parseLineups( sport, action.payload.data.lineups ),
                },
            };
        case `${LINEUPS_SAVE}::FAIL`:
            return {
                ...state,
                error: action.payload,
            };
        case `${LINEUPS_DELETE}::SUCCESS`:
            return {
                ...state,
                byId: _.omit( state.byId, action.payload.data ),
            };
        case `${LINEUPS_DELETE}::FAIL`:
            return {
                ...state,
                error: action.payload,
            };
        case UserTypes.logout.clean:
            return initialState;
        default:
            return state;
    }
};
