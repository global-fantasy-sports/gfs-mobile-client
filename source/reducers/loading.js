import Types from '../constants/competition';

const initialState = {
    gamesLoaded: false,
};

export default ( state = initialState, action ) => {
    switch ( action.type ) {
        case `${Types.getWithGames}::REQUEST`:
            return { ...state, gamesLoaded: false };
        case `${Types.getWithGames}::SUCCESS`:
            return { ...state, gamesLoaded: true };
        default:
            return state;
    }
};
