import Types from '../constants/match';
import { arrayToHash } from '../utils/common';

const mergeRooms = ( state, contests ) => {
    contests.forEach( contest => {
        state[contest.id] = { ...state[contest.id], ...contest };
    } );
    return { ...state };
};

export default ( sport, league, state = {}, action ) => {
    if ( sport !== action.sport || league !== action.league ) {
        return state;
    }

    switch ( action.type ) {
        case `${Types.getAll}::SUCCESS`:
            // still contests on backend
            return arrayToHash( action.payload.data.contests );
        case Types.changed:
            return mergeRooms( state, action.payload.data.contests );
        default:
            return state;
    }
};
