import _ from 'lodash';
import Types from '../constants/notification';
import UserTypes from '../constants/user';


const initialState = {
    notificationsList: [],
    showNotifications: false,
};

function mergeNotifications ( old, newNote ) {
    const existing = new Set( old.map( item => item.id ) );
    const updated = [
        ...newNote.filter( item => !existing.has( item.id ) ),
        ...old,
    ];
    return _.reverse( _.sortBy( updated, 'id' ) );
}

function markAsRead ( notifications ) {
    return notifications.map( ( notification ) => ( {
        ...notification,
        read: true,
    } ) );
}

export default ( state = initialState, action ) => {
    const { type, payload } = action;
    switch ( type ) {
        case `${ Types.root }::SUCCESS`:
            return {
                ...state,
                notificationsList: mergeNotifications( state.notificationsList, payload.data.notifications ),
            };
        case Types.received:
            return { ...state, notificationsList: mergeNotifications( state.notificationsList, [payload] ) };
        case `${ Types.markAll }::SUCCESS`:
            return { ...state, notificationsList: markAsRead( state.notificationsList ) };
        case Types.toggleNotifications:
            if ( !state.showNotifications ) {
                window.piwik.push( ['trackEvent', 'Notifications', 'Open notifications list'] );
            }
            return { ...state, showNotifications: !state.showNotifications };
        case UserTypes.logout.clean:
            return initialState;
        default:
            return state;
    }
};
