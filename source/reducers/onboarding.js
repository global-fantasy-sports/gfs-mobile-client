import { HIDE_ONBOARDING } from '../constants/onboarding';


const initialState = {
    dfs: true,
    draft: true,
    leagues: true,
    lobby: true,
    competitionRooms: true,
    select: true,
    entryFee: true,
};

export default ( state = initialState, action ) => {
    switch ( action.type ) {
        case HIDE_ONBOARDING:
            return {
                ...state,
                [action.payload]: false,
            };
        default:
            return state;
    }
};
