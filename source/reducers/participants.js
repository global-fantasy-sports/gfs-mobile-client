import ParticipantTypes from '../constants/participant';
import CompetitionTypes from '../constants/competition';

const updateParticipants = ( participants, newData ) => {
    newData.forEach( ( participant )=> {
        participants[ String( participant.id ) ] = {
            ...participants[ String( participant.id ) ],
            ...participant,
            id: String( participant.id ),
            athleteId: String( participant.athleteId ),
        };
    } );
    return { ...participants };
};

const updateParticipantsResults = ( participants, newData ) => {
    newData.forEach( ( participant ) => {
        participants[ String( participant.participantId ) ] = {
            ...participant,
            ...participants[ String( participant.participantId ) ],
            id: String( participant.participantId ),
        };
    } );
    return { ...participants };
};

const mergePoints = ( state, participants ) => {
    participants.forEach( participant => {
        state[participant.participantId] = { ...state[participant.participantId], ...participant };
    } );

    return { ...state };
};

export default ( sport, league, state = {}, action ) => {
    if ( sport !== action.sport || league !== action.league ) {
        return state;
    }

    let tempState;
    switch ( action.type ) {
        case `${ ParticipantTypes.getAll }::SUCCESS`:
            return updateParticipants( state, action.payload.data.participants );
        case `${ ParticipantTypes.getAllResults }::SUCCESS`:
            return updateParticipantsResults( state, action.payload.data.participantResults );
        case `${ CompetitionTypes.getWithGames }::SUCCESS`:
            tempState = updateParticipants( state, action.payload.data.participants );
            return updateParticipantsResults( tempState, action.payload.data.participantResults );
        case ParticipantTypes.pointsChanged:
            return mergePoints( state, action.payload.data.participants );
        default:
            return state;
    }
};
