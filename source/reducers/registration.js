import {
    REGISTRATION_REQUEST,
    REGISTRATION_FAIL,
    REGISTRATION_SUCCESS,
    REGISTRATION_OFF,
    REGISTRATION_REDIRECTED,
} from '../actions/registration';

let initialState = {
    error: '',
    authenticated: false,
    session: null,
    redirect: false,
};

export default ( state = initialState, action ) => {
    switch ( action.type ) {
        case REGISTRATION_REQUEST:
            return { ...state, processing: true, error: null };
        case REGISTRATION_SUCCESS:
            return { ...state, processing: false, authenticated: true, session: action.payload, redirect: true };
        case REGISTRATION_FAIL:
            return {
                ...state,
                processing: false,
                authenticated: false,
                session: null,
                error: action.payload,
                redirect: false,
            };
        case REGISTRATION_REDIRECTED:
            return { ...state, redirect: false };
        case REGISTRATION_OFF:
            return { ...initialState };
        default:
            return state;
    }
};
