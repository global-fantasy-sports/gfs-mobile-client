import ParticipantTypes from '../constants/participant';

let initialState = {
    participant: {},
};

export default ( sport, league, state = Object.assign( {}, initialState ), action ) => {
    if ( sport !== action.sport || league !== action.league ) {
        return state;
    }

    switch ( action.type ) {
        case `${ ParticipantTypes.getResultsLegend }::SUCCESS`:
            return { ...state, participant: action.payload.data.legendTemplates };
        default:
            return state;
    }
};
