import { LOCATION_CHANGE } from 'react-router-redux';

const initialState = {
    location: {},
    locationBeforeTransitions: null,
    competitionRoomBackTo: '',
    wizardBackTo: '',

};

export default function ( state = initialState, { type, payload } = {} ) {
    if ( type === LOCATION_CHANGE ) {

        const newState = { ...state,
            locationBeforeTransitions: payload,
            location: payload,
        };

        const prevPathName = state.location.pathname || '';
        if ( ( payload.pathname.search( /\/competition\/(?!\/)\d*\/$/ ) > 0 ) &&
            ( ( prevPathName.indexOf( '/games/' ) > -1 ) ||
              ( prevPathName.indexOf( '/lobby/' ) > -1 ) ) ) {

            newState.competitionRoomBackTo = state.location.pathname;
        }

        if ( ( payload.pathname.indexOf( '/wizard/' ) > -1 ) &&
            ( ( prevPathName.indexOf( '/games/' ) > -1 ) ||
              ( prevPathName.indexOf( '/competition/' ) > -1 ) ||
              ( prevPathName.indexOf( '/lineups/' ) > -1 ) ) ) {

            newState.wizardBackTo = state.location.pathname;
        }
        return newState;
    }

    return state;
}
