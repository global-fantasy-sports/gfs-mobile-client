import CompetitionTypes from '../constants/competition';

const initialState = {
    period: 'notready',
};

export default ( sport, league, state = initialState, action ) => {
    if ( sport !== action.sport || league !== action.league ) {
        return state;
    }

    switch ( action.type ) {
        case `${ CompetitionTypes.getAll }::SUCCESS`:
            return { period: action.payload.data.midseason };
        default:
            return state;
    }
};
