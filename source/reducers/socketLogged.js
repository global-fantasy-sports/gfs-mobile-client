import { SOCKET_DISCONNECTED, SOCKET_SUBSCRIBE } from '../actions/socket';
import { SPORT_SOCKET_LOGGED } from '../constants/sport';

export default ( sport, league, state = false, action ) => {

    switch ( action.type ) {
        case SPORT_SOCKET_LOGGED:
        case `${SOCKET_SUBSCRIBE}::SUCCESS`:
            return league === action.payload.league;
        case SOCKET_DISCONNECTED:
            return false;
        default:
            return state;
    }
};
