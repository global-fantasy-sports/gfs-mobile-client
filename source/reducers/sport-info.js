import Types from '../constants/sport';

const initialState = {
    nextEvent: {},
    week: 0,
};

export default ( sport, league, state = Object.assign( {}, initialState ), action ) => {
    if ( sport !== action.sport || league !== action.league ) {
        return state;
    }

    switch ( action.type ) {
        case `${Types.info.root}::REQUEST`:
            return { ...state, processing: true, error: null };
        case `${Types.info.root}::SUCCESS`:
            return { ...state, processing: false, error: null, ...action.payload.data };
        case `${Types.info.root}::FAIL`:
            return { ...state, processing: false, error: action.payload };
        default:
            return state;
    }
};
