import _ from 'lodash';
import Types from '../constants/athlete';

const parseAthletes = ( athletes ) => {
    const mapped = athletes.map( ( athlete ) => {
        const nameFirst = athlete.nameFirst.trim();
        const nameLast = athlete.nameLast.trim();
        const nameKnown = athlete.nameKnown.trim();
        return {
            ...athlete,
            nameFirst,
            nameLast,
            name: nameKnown,
            id: String( athlete.id ),
            currentParticipantId: athlete.currentParticipantId && String( athlete.currentParticipantId ),
        };
    } );
    return _.keyBy( mapped, 'id' );
};

export default ( sport, league, state = {}, action ) => {
    if ( sport !== action.sport || league !== action.league ) {
        return state;
    }

    switch ( action.type ) {
        case `${Types.getAll}::SUCCESS`:
            return parseAthletes( action.payload.data.athletes );
        case `${Types.history}::SUCCESS`:
            state[action.payload.data.history.athleteId].history = action.payload.data.history.history;
            return { ...state };
        default:
            return state;
    }
};
