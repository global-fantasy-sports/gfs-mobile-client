import Types from '../constants/team';
import { arrayToHash } from '../utils/common';

export default ( sport, league, state = {}, action ) => {
    if ( sport !== action.sport || league !== action.league ) {
        return state;
    }

    switch ( action.type ) {
        case `${Types.getAll}::SUCCESS`:
            return arrayToHash( action.payload.data.teams );
        default:
            return state;
    }
};
