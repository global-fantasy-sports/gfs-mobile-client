import Types from '../constants/translations';

export default ( state = {}, action ) => {
    switch ( action.type ) {
        case `${Types.getAll}::SUCCESS`:
            return { words: action.payload.data.translations, locale: action.payload.data.locale };
        default:
            return state;
    }
};
