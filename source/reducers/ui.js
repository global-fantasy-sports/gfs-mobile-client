import _ from 'lodash';
import { LINEUPS_REMOVE_ATHLETE } from '../actions/lineups';
import { REPORT_STEP, GO_BACK, SET_VARIABLE } from '../constants/ui';
import { UNSELECT_PARTICIPANT } from '../actions/wizard';


const initialState = {
    step: null,
    url: null,
    prevSteps: ['root'],
    prevUrls: ['/'],
    variables: {},
};


export default ( state = initialState, action ) => {
    switch ( action.type ) {
        case REPORT_STEP:
            if ( state.step !== action.payload ) {
                return {
                    ...state,
                    prevSteps: [...state.prevSteps, state.step],
                    prevUrls: [...state.prevUrls, state.url],
                    step: action.payload.step,
                    url: action.payload.url,
                };
            }
            break;

        case LINEUPS_REMOVE_ATHLETE:
        case UNSELECT_PARTICIPANT:
            const selected = Array.from( _.get( state, 'variables.selected', [] ) );
            return {
                ...state,
                variables: {
                    ...state.variables,
                    selected: new Set( selected.filter( ( id ) => id && id !== action.payload.id ) ),
                },
            };

        case SET_VARIABLE:
            const variables = state.variables;
            const newVars = { ...variables, ...action.payload };
            return { ...state, variables: { ...newVars } };

        case GO_BACK:
            if ( state.prevSteps.length ) {
                const prevStep = state.prevSteps.pop();
                const prevUrl = state.prevUrls.pop();

                return {
                    ...state,
                    prevSteps: [...state.prevSteps],
                    prevUrls: [...state.prevUrls],
                    step: prevStep,
                    url: prevUrl,
                };
            }

            return { ...state, step: null, url: null };
        default:
    }

    return state;
};
