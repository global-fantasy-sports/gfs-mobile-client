import Types from '../constants/user';

let initialState = {
    error: '',
    processing: false,
};

export default ( state = initialState, action ) => {
    switch ( action.type ) {
        case `${Types.user.info}::REQUEST`:
            return { ...state, processing: true, error: null };
        case `${Types.user.info}::SUCCESS`:
            window.piwik.push( ['setUserId', '' + action.payload.data.user.id + ': ' + action.payload.data.user.name] );
            return {
                ...state,
                processing: false,
                ...action.payload.data.user,
            };
        case `${Types.user.info}::FAIL`:
            return { ...state, processing: false, error: action.payload };
        case Types.user.clean:
            return { ...initialState };
        case Types.user.update:
            return { ...state, ...action.payload };
        default:
            return state;
    }
};
