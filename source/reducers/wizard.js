import _ from 'lodash';

import App from '../constants/app';
import { LINEUPS_SAVE_WO_REDIRECT } from '../actions/lineups';
import {
    CLEAR_ERROR,
    CLEAR_POSITION,
    LINEUP_HIDE_SAVER,
    SELECT_COMPETITION,
    SELECT_PARTICIPANT,
    UNSELECT_PARTICIPANT,
    WIZARD_CLEAR,
    WIZARD_INIT,
    WIZARD_SAVE,
    WIZARD_ERROR,
    WIZARD_DRAFT_CLEAR,
} from '../actions/wizard';
import { LINEUP_POSITIONS } from '../constants/wizard';
import { arrayToggle } from '../utils/common';

function getInitialState ( sport = null ) {
    return {
        created: false,
        error: null,
        lineup: {},
        lineupIsSaved: false,
        lineupPositions: sport && LINEUP_POSITIONS[sport] || [],
        lineupSaverIsVisible: false,
        mode: null,
        requesting: false,
        selectedRooms: [],
        sport,
    };
}

function initHandler ( { sport, ...props } ) {
    return {
        ...getInitialState( sport ),
        mode: 'create',
        ...props,
    };
}

function selectParticipantHandler ( { lineup, ...state }, { position, id } ) {
    return {
        ...state,
        lineup: { ...lineup, [position]: id && String( id ) || null },
        lineupIsSaved: false,
        lineupSaverIsVisible: !!( Object.values( lineup ).filter( a => a ).length >= 8 && id && String( id ) ),
    };
}

function clearPositionHandler ( { lineup, ...state }, { position } ) {
    return {
        ...state,
        lineupIsSaved: false,
        lineupSaverIsVisible: false,
        lineup: { ...lineup, [position]: null },
    };
}

function unselectParticipantHandler ( state, { id } ) {
    const position = _.findKey( state.lineup, ( item ) => item === id );
    return position
        ? clearPositionHandler( state, { position } )
        : { ...state, lineupIsSaved: false, lineupSaverIsVisible: false };
}

function selectCompetitionHandler ( { selectedRooms, ...state }, { id } ) {
    return {
        ...state,
        selectedRooms: arrayToggle( selectedRooms, id ),
    };
}

function wizardReducer ( state = null, action ) {
    if ( state === null ) {
        state = getInitialState();
    }

    switch ( action.type ) {
        case App.sportChanged:
            return state.sport !== action.payload
                ? getInitialState()
                : state;
        case WIZARD_INIT:
            return initHandler( action.payload );
        case WIZARD_CLEAR:
            return getInitialState();
        case WIZARD_DRAFT_CLEAR:
            return {
                ...state,
                lineup: {},
                lineupIsSaved: false,
            };
        case WIZARD_SAVE:
            return { ...state, requesting: true };
        case `${ WIZARD_SAVE }::SUCCESS`:
            return { ...state, requesting: false, created: true };
        case `${ WIZARD_SAVE }::FAIL`:
            return { ...state, requesting: false, error: action.payload };
        case CLEAR_ERROR:
            return { ...state, error: null };
        case SELECT_PARTICIPANT:
            return selectParticipantHandler( state, action.payload );
        case CLEAR_POSITION:
            return clearPositionHandler( state, action.payload );
        case UNSELECT_PARTICIPANT:
            return unselectParticipantHandler( state, action.payload );
        case SELECT_COMPETITION:
            return selectCompetitionHandler( state, action.payload );
        case WIZARD_ERROR:
            return { ...state, error: action.payload.error };
        case LINEUP_HIDE_SAVER:
            return { ...state, lineupSaverIsVisible: false };
        case `${LINEUPS_SAVE_WO_REDIRECT}::SUCCESS`:
            return { ...state, lineupIsSaved: true };
        default:
            return state;
    }
}

export default wizardReducer;
