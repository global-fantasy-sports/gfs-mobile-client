import { TOGGLE_WIZARD_VIEW_MODE } from '../actions/wizardViewMode';

const initialState = 'pitch';

function wizardViewModeReducer ( state = initialState, { type } ) {
    switch ( type ) {
        case TOGGLE_WIZARD_VIEW_MODE:
            return state === 'pitch' ? 'list' : 'pitch';
        default:
            return state;
    }
}

export default wizardViewModeReducer;
