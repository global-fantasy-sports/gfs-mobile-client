import React from 'react';
import { browserHistory, IndexRoute, Route, Router, IndexRedirect, Redirect } from 'react-router';

import RoutesConstants from './constants/routes';
import App from './actions/app';
import AuthContainer from './components/auth-container';
import LayoutContainer from './components/layout-container';
import LineupsRootContainer from './containers/LineupsRootContainer';
import AthletePage from './pages/athlete-page';
import ComparisonPage from './pages/comparison';
import CompetitionPage from './pages/competition';
import Error404Page from './pages/page404';
import GamesPage from './pages/games';
import HomePage from './pages/home';
import LineupCandidatesPage from './pages/lineup-candidates';
import LineupDetailsPage from './pages/lineup-details';
import LineupsPage from './pages/lineups';
import LobbyPage from './pages/lobby';
import RosterPage from './pages/roster';
import SchedulePage from './pages/schedule';
import SelectPage from './pages/select';
import SelectLeaguePage from './pages/select-league';
import wizardRoutes from './wizard/routes';
import { syncHistoryWithStore } from 'react-router-redux';
import { preparePathWithLeague } from './utils/router';
import { defaultLeague, defaultSport, leagueTypeKey, sportTypeKey } from './constants/app';
import LocalStorage from './utils/local-storage';
import CompetitionActions from './actions/competition';

const onSportEnter = ( dispatch ) => ( { params } ) => {
    dispatch( App.sportChanged( params.sport ) );
};

const onLeagueEnter = ( dispatch ) => ( { params } ) => {
    dispatch( App.leagueChanged( params.league ) );
    dispatch( App.preload() );
};

const onCompetitionEnter = ( dispatch ) => ( { params } ) => {
    dispatch( CompetitionActions.getWithGames( params.competitionId ) );
};

import piwikReactRouter from 'piwik-react-router';

let piwik = {
    push: () => {},
    connectToHistory: ( history ) => history,
};

if ( 'piwikConfig' in window ) {
    piwik = piwikReactRouter( {
        url: window.piwikConfig.url,
        siteId: window.piwikConfig.siteId,
    } );
}

window.piwik = piwik;

export default function routes ( store ) {
    let history = syncHistoryWithStore( browserHistory, store );
    history = syncHistoryWithStore( piwik.connectToHistory( history ), store );

    const sport = LocalStorage.get( sportTypeKey ) || defaultSport;
    const league = LocalStorage.get( leagueTypeKey ) || defaultLeague;

    const currentLeague = preparePathWithLeague( '/', sport, league );

    return (
        <Router history={ history } >

            <Route
                component={ LayoutContainer }
                path={ RoutesConstants.get( 'homePage' ) }
            >
                <IndexRoute component={ HomePage } />

                <Route component={ AuthContainer }>
                    <Redirect from="schedule/" to={ `${currentLeague}schedule/` } />
                    <Redirect from="lobby/" to={ `${currentLeague}lobby/` } />
                    <Redirect
                        from={ `${RoutesConstants.get( 'competitionPage' )}:competitionId/` +
                               `${RoutesConstants.get( 'rosterPage' )}:gameId/` }
                        to={ `${currentLeague}` +
                             `${RoutesConstants.get( 'competitionPage' )}:competitionId/` +
                             `${RoutesConstants.get( 'rosterPage' )}:gameId/` }
                    />
                    <Redirect from="games/" to={ `${currentLeague}games/` } />
                    <Redirect
                        from={ `${RoutesConstants.get( 'competitionPage' )}:competitionId/` }
                        to={ `${currentLeague}${RoutesConstants.get( 'competitionPage' )}:competitionId/` }
                    />
                    <Redirect from="athlete/" to={ `${currentLeague}athlete/` } />
                    <Redirect from="comparison/" to={ `${currentLeague}comparison/` } />
                    <Redirect from="lineups/" to={ `${currentLeague}lineups/` } />
                    <Redirect from="lineups/:id/" to={ `${currentLeague}lineups/:id/` } />
                    <Redirect from="lineups/:id/:position/" to={ `${currentLeague}lineups/:id/:position/` } />
                    <Redirect from="wizard/" to={ `${currentLeague}wizard/` } />
                    <Redirect from="wizard/select/" to={ `${currentLeague}wizard/select/` } />
                    <Redirect from="wizard/select/:position" to={ `${currentLeague}wizard/select/:position/` } />
                    <Redirect from="wizard/stake" to={ `${currentLeague}wizard/stake/` } />
                    <Redirect from="wizard/friends" to={ `${currentLeague}wizard/friends/` } />

                    <Route
                        component={ SelectPage }
                        // onEnter={ onSelectSportEnter }
                        path={ RoutesConstants.get( 'sportSelectPage' ) }
                    />
                    <Route onEnter={ onSportEnter( store.dispatch ) } path=":sport/">
                        <IndexRoute
                            component={ SelectLeaguePage }
                        />
                        <Route onEnter={ onLeagueEnter( store.dispatch ) } path=":league/">
                            <IndexRedirect to="schedule/" />
                            <Route
                                component={ LobbyPage }
                                path="lobby/"
                            />
                            <Route
                                component={ RosterPage }
                                path={
                                    `${RoutesConstants.get( 'competitionPage' )}:competitionId/` +
                                    `${RoutesConstants.get( 'rosterPage' )}:gameId/`
                                }
                            />
                            <Route
                                component={ CompetitionPage }
                                onEnter={ onCompetitionEnter( store.dispatch ) }
                                path={ `${RoutesConstants.get( 'competitionPage' )}:competitionId/` }
                            />
                            <Route
                                component={ GamesPage }
                                path={ RoutesConstants.get( 'gamesPage' ) }
                            />
                            <Route
                                component={ AthletePage }
                                path={ RoutesConstants.get( 'athlete' ) }
                            />
                            <Route
                                component={ SchedulePage }
                                path={ RoutesConstants.get( 'schedule' ) }
                            />
                            <Route
                                component={ ComparisonPage }
                                path={ RoutesConstants.get( 'comparison' ) }
                            />
                            <Route
                                component={ LineupsRootContainer }
                                path={ RoutesConstants.get( 'lineups' ) }
                            >
                                <IndexRoute component={ LineupsPage } />
                                <Route
                                    component={ LineupDetailsPage }
                                    path=":id/"
                                />
                                <Route
                                    component={ LineupCandidatesPage }
                                    path=":id/:position/"
                                />
                            </Route>
                            <Route
                                path={ RoutesConstants.get( 'wizardPage' ) }
                            >
                                { wizardRoutes }
                            </Route>
                            <Route
                                component={ Error404Page }
                                path="*"
                                status="404"
                            />
                        </Route>
                    </Route>
                </Route>

                <Route
                    component={ Error404Page }
                    path="*"
                    status="404"
                />
            </Route>
        </Router>
    );
}
