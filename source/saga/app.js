import { fork, put, select, takeEvery } from 'redux-saga/effects';
import moment from 'moment';

import LocalStorage from '../utils/local-storage';
import Types, { sportTypeKey, leagueTypeKey } from '../constants/app';
import RouterTypes from '../constants/router';

import { getChosenSport } from '../selectors/sport';
import { getChosenLeague } from '../selectors/league';

import SportAction from '../actions/sport';
import SimulationAction from '../actions/simulation';
import RoomActions from '../actions/competition';
import MatchActions from '../actions/match';
import TeamActions from '../actions/team';
import EventActions from '../actions/event';
import ParticipantActions from '../actions/participant';
import AthleteActions from '../actions/athlete';
import { push, replace } from 'react-router-redux';


const changeSettings = ( key, { payload } ) => LocalStorage.set( key, payload );

function * preloadData () {
    const sport = yield select( getChosenSport );
    const league = yield select( getChosenLeague );

    yield put( RoomActions.getAll( sport, league ) );
    yield put( MatchActions.getAll( sport, league ) );
    yield put( TeamActions.getAll( sport, league ) );
    yield put( EventActions.open( sport, league ) );
    yield put( ParticipantActions.getAll( sport, league ) );
    yield put( AthleteActions.getAll( sport, league ) );
    yield put( AthleteActions.getFavorites( sport, league ) );
    yield put( SportAction.info( sport, league ) );
    yield put( SimulationAction.getDate( sport, league ) );
}

function setTimeFormat () {
    const htmlTag = document.querySelector( 'html' );
    moment.locale( htmlTag.lang );
}

function * preparePath ( path ) {
    const sport = yield select( getChosenSport );
    const league = yield select( getChosenLeague );
    if ( path[0] !== '/' ) {
        path = '/' + path;
    }
    if ( path[path.length - 1] !== '/' ) {
        path += '/';
    }

    path = `/${sport}/${league}${path}`;
    return path;
}


function * goTo ( action ) {
    const path = yield preparePath( action.payload );
    yield put( push( path ) );
}

function * redirectTo ( action ) {
    const path = yield preparePath( action.payload );
    yield put( replace( path ) );
}

export default function * app () {
    yield takeEvery( Types.sportChanged, changeSettings.bind( null, sportTypeKey ) );
    yield takeEvery( Types.leagueChanged, changeSettings.bind( null, leagueTypeKey ) );
    yield takeEvery( Types.preload, preloadData );
    yield takeEvery( RouterTypes.LEAGUE_GOTO, goTo );
    yield takeEvery( RouterTypes.LEAGUE_REDIRECT_TO, redirectTo );
    yield fork( setTimeFormat );
}
