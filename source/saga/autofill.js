import _ from 'lodash';
import { take, select, put } from 'redux-saga/effects';

import { LINEUP_AUTOFILL, selectParticipant, wizardError } from '../actions/wizard';

import { POSITIONS } from '../constants/wizard';

import { getChosenSport } from '../selectors/sport';
import { getSalarycapBudget } from '../selectors/league.js';
import { getAvailableParticipants } from '../selectors/participants';
import { translate } from '../selectors/translations';
import { getWizardLineup } from '../selectors/wizard';


const countTeamsParticipants = ( lineup, participants ) =>
    Object.values( lineup ).reduce( ( agg, participantId ) => {
        const participant = participants.find( ( p ) => p.id === participantId );
        const { teamId } = participant;
        return { ...agg, [teamId]: agg[teamId] ? agg[teamId] + 1 : 1 };
    }, {} );

const isValidPick = ( participant, lineup, participants ) => {
    const { id, teamId } = participant;
    const athletesPerTeam = countTeamsParticipants( lineup, participants );
    return !( ( athletesPerTeam[teamId] && athletesPerTeam[teamId] > 3 ) || Object.values( lineup ).includes( id ) );
};

const getCheapestParticipant = ( lineup, preselected, participants ) => {
    return Object.entries( lineup ).reduce( ( agg, [pos, participantId] ) => {
        if ( preselected.includes( pos ) ) {
            return agg;
        }

        const participant = participants.find( ( p ) => p.id === participantId );
        if ( participant && participant.salary < agg[1].salary ) {
            return [pos, participant];
        }

        return agg;
    }, [null, { salary: Infinity }] );
};

const getExpensiveParticipant = ( sport, lineup, preselected, participants ) => (
    Object.entries( lineup ).reduce( ( agg, [pos, participantId] ) => {
        if ( preselected.includes( pos ) ) {
            return agg;
        }

        const participant = participants.find( ( p ) => p.id === participantId );
        if ( participant && agg[1].salary < participant.salary ) {
            return [pos, participant];
        }
        return agg;

    }, [null, { salary: 0 }] )
);

const getLineupBudget = ( lineup, participants ) => (
    Object.values( lineup ).reduce( ( agg, participantId ) => {
        const participant = participants.find( ( p ) => p.id === participantId );
        return agg + participant.salary;
    }, 0 )
);

const replaceCheapest = ( sport, preselected, lineup, participants, allParticipants, salarycapBudget, participant ) => {
    let newLineup = { ...lineup }, par, pars, pos;

    if ( participant ) {
        pos = Object.entries( lineup ).find( ( entry ) => entry[1] === participant.id )[0];
        const positions = POSITIONS[sport][pos];
        pars = participants
            .filter( ( p ) => p.teamId !== participant.teamId && positions.includes( p.position ) )
            .sort( ( a, b ) => b.salary - a.salary );
    } else {
        [pos, par] = getCheapestParticipant( lineup, preselected, participants );
        const positions = POSITIONS[sport][pos];
        pars = participants
            .filter( ( p ) => p.salary > par.salary && positions.includes( p.position ) )
            .sort( ( a, b ) => b.salary - a.salary );
    }

    for ( let i in pars ) { // eslint-disable-line guard-for-in
        const top = pars[i];
        if ( isValidPick( top, newLineup, allParticipants ) ) {
            newLineup[pos] = top.id;
            if ( getLineupBudget( newLineup, allParticipants ) <= salarycapBudget ) {
                return [pos, newLineup];
            }
        }
    }

    return [pos, lineup];
};

const replaceExpensive = ( sport, preselected, lineup, participants, allParticipants ) => {
    let newLineup = { ...lineup };
    const [pos, par] = getExpensiveParticipant( sport, lineup, preselected, participants );

    const positions = POSITIONS[sport][pos];
    const pars = participants
        .filter( ( p ) => p.salary < par.salary && positions.includes( p.position ) )
        .sort( ( a, b ) => a.salary - b.salary );

    for ( let i in pars ) { // eslint-disable-line guard-for-in
        const top = pars[i];

        if ( isValidPick( top, newLineup, allParticipants ) ) {
            newLineup[pos] = top.id;
            return [pos, newLineup];
        }
    }

    return [pos, lineup];
};


export default function * autofill () {
    while ( yield take( LINEUP_AUTOFILL ) ) {

        const sport = yield select( getChosenSport );
        const salarycapBudget = yield select( getSalarycapBudget );
        const allParticipants = yield select( getAvailableParticipants );
        const participants = allParticipants.filter( ( p ) => p.FPPG > 0 );

        const wizardLineup = yield select( ( state ) => state.reducer.wizard.lineup );
        let lineup = yield select( () => getWizardLineup( wizardLineup, allParticipants ) );
        lineup = _.mapValues( lineup, ( participant ) => {
            return participant.unavailable ? null : participant.id;
        } );

        let newLineup = { ...lineup };
        let athletesPerTeam = countTeamsParticipants( lineup, allParticipants );
        const preselected = Object.entries( lineup ).filter( ( entry ) => {
            return !!entry[1] && allParticipants.find( ( p ) => p.id === entry[1] );
        } ).map( ( arr ) => arr[0] );
        let processed = [...preselected];

        for ( let pos in POSITIONS[sport] ) { // eslint-disable-line guard-for-in
            const positions = POSITIONS[sport][pos];

            if ( !preselected.includes( pos ) ) {
                const participant = _.sample( participants.filter( ( p ) => { // eslint-disable-line no-loop-func
                    return positions.includes( p.position )
                        && !Object.values( newLineup ).includes( p.id )
                        && !Object.keys( _.pickBy( athletesPerTeam, ( a ) => a > 3 ) ).includes( p.teamId );
                } ) );
                athletesPerTeam[participant.teamId] = athletesPerTeam[participant.teamId]
                                                    ? athletesPerTeam[participant.teamId] + 1
                                                    : 1;
                newLineup[pos] = participant.id;
            }
        }

        let budget = 0, l = {}, p;
        while ( processed.length < Object.keys( POSITIONS[sport] ).length ) {

            if ( !_.isEmpty( l ) ) {
                newLineup = { ...l };
            }

            budget = getLineupBudget( newLineup, allParticipants );

            if ( budget > salarycapBudget ) {
                [p, l] = replaceExpensive( sport, preselected, newLineup, participants, allParticipants );
            } else {
                [p, l] = replaceCheapest( sport, preselected, newLineup, participants, allParticipants,
                    salarycapBudget );
            }

            processed.push( p );
        }
        newLineup = l;

        budget = getLineupBudget( newLineup, allParticipants );
        if ( budget <= salarycapBudget ) {
            const effects = Object.entries( newLineup ).map( ( [pos, id] ) => put( selectParticipant( pos, id ) ) );
            yield effects;
        } else {
            const _t = yield select( translate );
            yield put( wizardError( _t( 'saga.autofill.autofill-error' ) ) );
        }
    }
}
