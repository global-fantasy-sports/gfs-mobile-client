import { call, fork, take, put, select } from 'redux-saga/effects';
import {
    fbInitialized,
    fbStatus,
    fbFriendsFetched,
    fbInviteFail,
    fbInviteSuccess,
} from '../actions/facebook';
import { initFBApi } from '../utils/facebook';

import { FACEBOOK_LOGIN, FACEBOOK_INVITE } from '../constants/facebook';
import { USER_GET_INFO } from '../actions/user';

function isFBLoggedIn ( state ) {
    return state.reducer.facebook.status === 'connected';
}

function * login ( fbAPI ) {
    let loggedIn = yield select( isFBLoggedIn );
    while ( !loggedIn ) {
        yield take( FACEBOOK_LOGIN );
        try {
            yield call( ::fbAPI.login );
            yield put( fbStatus( 'connected' ) );
            loggedIn = true;
        } catch ( error ) {
            yield put( fbStatus( error ) );
            loggedIn = false;
        }
    }
}

function * processInviteRequests ( fbAPI ) {
    while ( true ) {
        const id = yield take( FACEBOOK_INVITE );
        try {
            const hasPermission = yield call( ::fbAPI.checkPermissions );
            if ( !hasPermission ) {
                yield call( ::fbAPI.requestPermissions );
            }
            yield call( ::fbAPI.invite, id );
            yield put( fbInviteSuccess( id ) );
        } catch ( e ) {
            yield call( ::fbAPI.clearDialogBox );
            yield put( fbInviteFail( e.message || e ) );
        }
    }
}

export default function * facebook () {
    const { payload } = yield take( `${USER_GET_INFO}::SUCCESS` );
    const fbAPI = yield call( initFBApi, payload.value.info.facebook_app_id );
    yield put( fbInitialized() );
    try {
        yield call( ::fbAPI.checkLogin );
        yield put( fbStatus( 'connected' ) );
    } catch ( error ) {
        yield put( fbStatus( error ) );
        yield * login( fbAPI );
    }
    const friends = yield call( ::fbAPI.getFriends );
    yield put( fbFriendsFetched( friends ) );
    yield fork( processInviteRequests, fbAPI );
}
