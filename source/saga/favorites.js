import { take, put, select } from 'redux-saga/effects';
import { SPORT_TOGGLE_FAVORITE } from '../constants/sport';
import { deleteFavoriteAthlete, addFavoriteAthlete } from '../actions/sport';
import { getChosenSport } from '../selectors/sport';
import { getFavoriteAthleteIds } from '../selectors/athletes';

export default function * favorites () {
    while ( true ) {
        const { payload } = yield take( SPORT_TOGGLE_FAVORITE );
        const sport = yield select( getChosenSport );
        const favorite = yield select( getFavoriteAthleteIds );

        if ( favorite.includes( payload.athleteId ) ) {
            yield put( deleteFavoriteAthlete( payload.athleteId, sport ) );
        } else {
            yield put( addFavoriteAthlete( payload.athleteId, sport ) );
        }
    }

}
