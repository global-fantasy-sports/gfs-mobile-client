import { all, fork, put, spawn } from 'redux-saga/effects';

import app from './app';
import facebook from './facebook';
import favorites from './favorites';
import sentry from './sentry';
import lineups from './lineups';
import wizard from './wizard';
import session from './session';
import autofill from './autofill';

import restWorker from './workers/rest-worker';
import socketWorker from './workers/socket-worker';
import App from '../actions/app';
import Translations from '../actions/translations';

import replaceReducers from './replaceReducers';

export default function * root () {
    yield all( [
        spawn( app ),
        spawn( restWorker ),
        spawn( socketWorker ),
        spawn( sentry ),
        fork( lineups ),
        fork( session ),
        fork( wizard ),
        fork( replaceReducers ),
        fork( autofill ),
    ] );


    if ( process.env.WITH_SOCIAL === 'yes' ) {
        yield spawn( facebook );
    }
    yield fork( favorites );

    yield put( Translations.getAll() );
    yield put( App.started() );
}
