import _ from 'lodash';
import { all, put, take, fork, race, select } from 'redux-saga/effects';
import { LINEUPS_DELETE, LINEUPS_SAVE, getLineupURL, cancelLineupEdit } from '../actions/lineups';
import rest from '../actions/rest';
import confirm from './helpers/confirm';
import { translate } from '../selectors/translations';
import { redirectTo } from '../actions/router';


function * saveLineup () {
    while ( true ) {
        const action = yield take( LINEUPS_SAVE + '::SUCCESS' );
        const id = _.get( action, 'payload.data.lineups[0].id' );
        if ( id ) {
            yield put( redirectTo( `/lineups/${ id }/` ) );
        }
    }
}

function * deleteLineup () {
    while ( true ) {
        const { payload: { id } } = yield take( LINEUPS_DELETE );

        try {
            const _t = yield select( translate );
            const result = yield * confirm(
                _t( 'Delete line-up' ),
                _t( 'Are you sure you want to delete line-up?' ),
                _t( 'Delete' ),
                _t( 'Cancel' ),
            );
            if ( result ) {
                yield put( rest.delete( getLineupURL( id ), {}, LINEUPS_DELETE ) );

                yield race( {
                    success: take( LINEUPS_DELETE + '::SUCCESS' ),
                    fail: take( LINEUPS_DELETE + '::FAIL' ),
                } );

                yield put( cancelLineupEdit() );
                yield put( redirectTo( '/lineups/' ) );
            }

        } catch ( e ) {
            // do nothing
        }
    }
}


export default function * lineups () {
    yield all( [
        fork( saveLineup ),
        fork( deleteLineup ),
    ] );
}
