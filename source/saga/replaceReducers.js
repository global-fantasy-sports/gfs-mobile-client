import _ from 'lodash';
import { takeEvery, select } from 'redux-saga/effects';
import { injectReducer } from 'redux-injector';

import Types from '../constants/app';
import { getChosenSport } from '../selectors/sport';
import { getChosenLeague } from '../selectors/league';
import { sportReducers } from '../reducers';

const combineGameReducers = ( sport, league, reducers ) => {
    return _.mapValues( reducers, ( reducer ) => reducer.bind( null, sport, league ) );
};

function * replaceReducer () {
    const sport = yield select( getChosenSport );
    const league = yield select( getChosenLeague );

    injectReducer( `reducer.sports.${ sport }.${ league }`, combineGameReducers( sport, league, sportReducers ) );
}

export default function * replaceReducers () {
    yield takeEvery( Types.leagueChanged, replaceReducer );
}
