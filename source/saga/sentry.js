/* global Raven */
import _ from 'lodash';
import { fork, race, take } from 'redux-saga/effects';

function * watchUserChanges () {
    while ( true ) {
        const { login } = yield race( {
            login: take( 'USER::INFO::SUCCESS' ),
            logout: take( ['USER::CLEAN', 'LOGOUT::CLEAN'] ),
        } );

        if ( login ) {
            const user = _.get( login, 'payload.data.user', {} );
            Raven.setUserContext( {
                id: user.id,
                email: user.email,
            } );
        } else {
            Raven.setUserContext();
        }
    }
}

export default function * sentry () {
    if ( process.env.NODE_ENV === 'production' ) {
        yield fork( watchUserChanges );
    }
}
