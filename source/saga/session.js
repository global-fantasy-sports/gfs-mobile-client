import { put, select, takeEvery } from 'redux-saga/effects';
import user from '../constants/user';
import { getSessionId } from '../selectors/user';
import { replace } from 'react-router-redux';

import SessionActions from '../actions/session';


function * redirectToLanding () {
    yield put( SessionActions.clean() );
    const homePage = window.HOME_PAGE;
    if ( homePage.indexOf( 'http' ) === 0 ) {
        window.location.href = homePage;
    } else {
        yield put( replace( homePage ) );
    }
}


export default function * session () {
    yield takeEvery( [user.session.error, `${user.logout.root}::SUCCESS`], redirectToLanding );

    const sessionId = yield select( getSessionId );
    if ( !sessionId ) {
        yield put( SessionActions.get() );
    }
}
