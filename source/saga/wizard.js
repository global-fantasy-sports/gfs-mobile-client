import React from 'react';
import _ from 'lodash';
import axios from 'axios';
import { call, all, put, race, select, take, takeEvery } from 'redux-saga/effects';

import GameActionTypes from '../constants/game';
import Routes from '../constants/routes';

import GameActions from '../actions/game';
import rest from '../actions/rest';
import confirm from './helpers/confirm';
import {
    INIT_FROM_COMPETITION,
    INIT_FROM_LINEUP,
    INIT_FROM_SCRATCH,
    WIZARD_INIT,
    WIZARD_SAVE,
    CONFIRM_REDIRECT_TO_LINEUP,
    REPLACE_PARTICIPANT,
    selectParticipant,
    unselectParticipant,
} from '../actions/wizard';

import { getLineupPositions, getLineupsMap } from '../selectors/lineups';
import { translate } from '../selectors/translations';
import { getWizardData } from '../selectors/wizard';
import { goTo, redirectTo } from '../actions/router';

const GamesAPI = {
    list: ( { sport, league } ) => `/api/v2/${sport}/${league}/games/my/`,
    create: ( { sport, league, type = 'salarycap' } ) => `/api/v2/${sport}/${league}/games/${type}/`,
    detail: ( { sport, league, gameId, type = 'salarycap' } ) => `/api/v2/${sport}/${league}/games/${type}/${gameId}/`,
};

function * deleteGame ( { payload: { gameId, sport, league } } ) {
    try {
        const _t = yield select( translate );
        const result = yield * confirm(
            _t( 'Delete game' ),
            _t( 'Are you sure you want to delete this game?' ),
            _t( 'Delete' ),
            _t( 'Cancel' ),
        );
        if ( result ) {
            window.piwik.push( ['trackEvent', 'My Games', 'Delete Game (non finished games, on Swipe) Confirm'] );
            yield put( rest.delete( GamesAPI.detail( { sport, league, gameId } ), {}, GameActionTypes.remove ) );

            const { success } = yield race( {
                success: take( GameActionTypes.remove + '::SUCCESS' ),
                fail: take( GameActionTypes.remove + '::FAIL' ),
            } );

            if ( success ) {
                window.piwik.push( ['trackEvent', 'Server', 'Request', 'Delete game by user', 'Success'] );
                yield put( GameActions.getMy( sport, league ) );
            } else {
                window.piwik.push( ['trackEvent', 'Server', 'Request', 'Delete game by user', 'Error'] );
            }
        }
    } catch ( e ) {
        // do nothing
    }
}

function * goToWizard ( payload ) {
    yield put.resolve( { type: WIZARD_INIT, payload } );
    yield put( goTo( Routes.get( 'wizardPage' ) ) );
}

function * joinCompetition ( { payload } ) {
    const { competitionId } = payload;
    yield * goToWizard( {
        sport: payload.sport,
        league: payload.league,
        predefinedCompetitionId: competitionId,
        selectedRooms: [competitionId],
    } );
}

function getGameParticipants ( state, sport, league, gameId ) {
    const { games, sportsmen } = state.reducer.sports[sport][league];
    const game = _.find( games, { id: gameId } );
    const lineup = _.pick( game, getLineupPositions( sport, league ) );
    return _.mapValues( lineup, ( id ) => _.get( sportsmen, [id, 'currentParticipantId'], null ) );
}

function * editGame ( { payload } ) {
    const { sport, league, gameId } = payload;
    const lineup = yield select( getGameParticipants, sport, league, gameId );
    yield * goToWizard( { sport, league, gameId, lineup, mode: 'edit' } );
}

function * initFromLineup ( { payload } ) {
    const lineups = yield select( getLineupsMap );
    const { sport, league, lineupId } = payload;
    const lineup = _.pick( lineups[lineupId], getLineupPositions( sport, league, lineupId ) );
    yield * goToWizard( {
        sport, league,
        lineup: _.mapValues( lineup, ( pos ) => _.get( pos, ['currentParticipantId'], null ) ),
    } );
}

function * initFromScratch ( { payload: { sport, league } } ) {
    yield * goToWizard( { sport, league } );
}

function getGameSaveRequest ( state ) {
    const { sport, league, lineup, selectedRooms, gameId, mode } = getWizardData( state );
    const { participants } = state.reducer.sports[sport][league];

    return {
        url: GamesAPI[mode === 'edit' ? 'detail' : 'create']( { sport, league, gameId } ),
        method: mode === 'edit' ? 'PUT' : 'POST',
        data: {
            lineup: _.mapValues( lineup, ( id ) => _.get( participants, [id, 'athleteId'], null ) ),
            roomIds: selectedRooms.filter( ( value, index, array ) => array.indexOf( value ) === index ),
        },
    };
}

function * saveGame () {
    window.piwik.push( ['trackEvent', 'Wizard - Select Room', 'Pay', 'Pay for the room(s) Click'] );
    const request = yield select( getGameSaveRequest );
    const { status, data } = yield call( ::axios.request, request );
    if ( [200, 201, 204, 304].includes( status ) ) {
        window.piwik.push( ['trackEvent', 'Server', 'Request', 'Pay for game', 'Success'] );
        const wizardData = yield select( getWizardData );
        yield put( {
            type: `${WIZARD_SAVE}::SUCCESS`,
            payload: { data: data.data },
            sport: wizardData.sport,
            league: wizardData.league,
        } );
        yield put( redirectTo( process.env.WITH_SOCIAL === 'yes' ? '/wizard/friends/' : '/games/' ) );
    } else {
        window.piwik.push( ['trackEvent', 'Server', 'Request', 'Pay for game', 'Error'] );
        yield put( { type: `${WIZARD_SAVE}::FAIL`, payload: data.errorMessage } );
    }
}

function * confirmRedirectToLineup () {
    const _t = yield select( translate );
    const result = yield * confirm(
        _t( 'dialogs.confirm-redirect.load-lineup-title' ),
        <p>
            {_t( 'dialogs.confirm-redirect.load-lineup' )}
            <br />
            {_t( 'dialogs.confirm-redirect.draft-will-be-lost' )}
        </p>,
        _t( 'Continue' ),
        _t( 'Cancel' ),
    );

    if ( result ) {
        yield put( goTo( Routes.get( 'lineups' ) ) );
    }
}

function * replaceParticipantInLineup ( action ) {
    if ( action.payload.replaceId ) {
        yield put( unselectParticipant( action.payload.replaceId ) );
    }
    yield put( selectParticipant( action.payload.position, action.payload.replaceWithId ) );
}

export default function * wizard () {
    yield all( [
        takeEvery( INIT_FROM_COMPETITION, joinCompetition ),
        takeEvery( INIT_FROM_LINEUP, initFromLineup ),
        takeEvery( INIT_FROM_SCRATCH, initFromScratch ),
        takeEvery( WIZARD_SAVE, saveGame ),
        takeEvery( CONFIRM_REDIRECT_TO_LINEUP, confirmRedirectToLineup ),
        takeEvery( GameActionTypes.edit, editGame ),
        takeEvery( GameActionTypes.remove, deleteGame ),
        takeEvery( REPLACE_PARTICIPANT, replaceParticipantInLineup ),
    ] );
}
