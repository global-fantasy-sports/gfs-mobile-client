import _ from 'lodash';
import moment from 'moment';
import { createSelector } from 'reselect';
import { getUser } from '../selectors/user';

import { getSportData } from '../selectors/sport';
import { getRouterPath } from '../selectors/router';

export const getCurrentDateObj = createSelector( getSportData, ( data ) => {
    const date = _.get( data, 'currentDate.date' );
    return date ? moment.unix( date ) : moment();
} );

export const getBalance = createSelector( getUser, ( user ) =>
    _.mapValues( user.balance, ( value ) => parseInt( value, 10 ) )
);

export const getSimulation = createSelector( getRouterPath, ( pathname ) => pathname !== '/select' );

export const getSimLoader = ( state ) => _.get( state, 'reducer.app.simulation.loader', false );
export const getSimError = ( state ) => _.get( state, 'reducer.app.simulation.error', '' );

