import _ from 'lodash';
import { createSelector } from 'reselect';
import { getSportData } from './sport';
import { getSportParticipants } from './participants';

export const getFavoriteAthleteIds = createSelector( getSportData, ( data ) => data.favorites || [] );

const getRawAthletes = createSelector( getSportData, ( data ) => data.sportsmen || {} );

const getRawParticipants = createSelector( getSportData, ( data ) => data.participants || {} );

export const getAthletes = createSelector(
    [
        getRawAthletes,
        getRawParticipants,
        getFavoriteAthleteIds,
    ],
    ( athletes, participants, favoriteIds ) => _.mapValues( athletes, ( props ) => {
        const participant = participants[props.currentParticipantId] || {};

        return {
            ...props,
            athleteId: props.id,
            position: participant.position,
            teamId: participant.teamId,
            oprk: participant.oprk,
            FPPG: participant.FPPG,
            salary: participant.salary,
            isFavorite: favoriteIds.includes( props.id ),
        };
    } )
);

export const getFavoriteAthletes = createSelector( getAthletes, getFavoriteAthleteIds, ( athletes, favoriteIds ) => {
    return Object.values( athletes ).filter( ( a ) => favoriteIds.includes( a.athleteId ) );
} );

export const getFavoriteAthletesComparison = createSelector( getFavoriteAthletes, getSportData, ( favorites, data ) => {
    return favorites.filter( ( f ) => f.position === data.comparison.position );
} );

export const getFavoriteParticipantsComparison = createSelector(
    getFavoriteAthletesComparison,
    getSportParticipants,
    ( favorites, participants ) => {
        const participantsIds = participants.filter( ( p ) => !p.unavailable ).map( f => f.currentParticipantId );
        return favorites.filter( ( f ) => participantsIds.includes( f.currentParticipantId ) );
    } );

export const getAthletesList = createSelector(
    getAthletes,
    ( data ) => _.orderBy( data, ['salary', 'name'], ['desc', 'asc'] )
);
