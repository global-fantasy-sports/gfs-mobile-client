import _ from 'lodash';
import { createSelector } from 'reselect';

import { getSportData } from './sport';
import { getAthletes, getAthletesList } from './athletes';

import { RADAR_CHART_STATS, TABLE_CHART_STATS } from '../constants/wizard';

export const getComparisonData = createSelector(
    getSportData,
    ( data ) => data.comparison || {}
);

export const getComparisonLimits = createSelector(
    [getComparisonData],
    ( data ) => data.limits || {}
);

export const getComparisonAthletes = createSelector(
    [
        getAthletes,
        getComparisonData,
    ],
    ( athletes, comparison ) => ( {
        left: athletes[comparison.leftId],
        right: athletes[comparison.rightId],
    } )
);


export const getComparisonPosition = createSelector(
    [getComparisonData],
    ( comparison ) => comparison.position
);

export const getComparisonCandidates = createSelector(
    [
        getAthletesList,
        getComparisonPosition,
    ],
    ( athletes, position ) => athletes.filter( ( athlete ) => athlete.position === position )
);

function getRatio ( value, max ) {
    if ( value === null ) {
        return null;
    }

    return max ? value / max : 0;
}

export const getComparisonStats = createSelector(
    [
        getComparisonAthletes,
        getComparisonPosition,
        getComparisonLimits,
    ],
    ( athletes, position, limits ) => {
        const stats = RADAR_CHART_STATS[position] || [];
        return stats.map( ( type ) => {
            const left = _.get( athletes, ['left', 'averageStats', type], null );
            const right = _.get( athletes, ['right', 'averageStats', type], null );
            const lim = limits[type] || [];
            return {
                type, unit: lim[2],
                min: lim[0], max: lim[1],
                label: { type, unit: lim[2] },
                left, right,
                leftRatio: getRatio( left, lim[1] ),
                rightRatio: getRatio( right, lim[1] ),
            };
        } );
    }
);

export const getComparisonTableStats = createSelector(
    [
        getComparisonAthletes,
        getComparisonPosition,
        getComparisonLimits,
    ],
    ( athletes, position, limits ) => {
        const stats = TABLE_CHART_STATS[position] || [];
        return stats.map( ( type ) => {
            const left = _.get( athletes, ['left', 'averageStats', type], null );
            const right = _.get( athletes, ['right', 'averageStats', type], null );
            const lim = limits[type] || [];
            return {
                type, unit: lim[2],
                min: lim[0], max: lim[1],
                left, right,
                leftRatio: getRatio( left, lim[1] ),
                rightRatio: getRatio( right, lim[1] ),
            };
        } );
    }
);
