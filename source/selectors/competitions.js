import _ from 'lodash';
import { createSelector } from 'reselect';
import { getChosenSport, getSportData } from './sport';
import { GAME_TYPE_STRINGS } from '../constants/competition';
import { getAthletesPositions } from '../constants/sport';
import { getUserId } from './user';
import { getGames } from './games';


function getRoomEntries ( room ) {
    return room.data ? room.data.length : room.gamesCount;
}

function sortingByDate ( room1, room2 ) {
    if ( room1.startDate < room2.startDate ) {
        return -1;
    }
    if ( room1.startDate > room2.startDate ) {
        return 1;
    }
    return 0;
}

function sortingByEntries ( room1, room2 ) {
    const room1Entries = getRoomEntries( room1 );
    const room2Entries = getRoomEntries( room2 );

    if ( room1Entries > room2Entries ) {
        return -1;
    }
    if ( room1Entries < room2Entries ) {
        return 1;
    }
    return 0;
}


export const roomsSelector = createSelector(
    getSportData,
    ( sportData ) => {
        let rooms = Object.values( sportData.competitionRooms );
        rooms = rooms.filter( ( room ) => !( ( room.maxEntries === room.gamesCount ) || room.started ) );
        return rooms;
    }
);

const getRooms = createSelector(
    getSportData,
    ( sportData ) => sportData.competitionRooms
);

export const roomFiltersSelector = createSelector(
    getSportData,
    ( sportData ) => sportData.filters.competitionRooms,
);

export const isPanelOpenSelector = createSelector(
    roomFiltersSelector,
    ( filters ) => filters.isOpen,
);

export const seasonSelector = createSelector(
    getSportData,
    ( sportData ) => sportData.season,
);

export const entryFeeRangeSelector = createSelector(
    roomsSelector,
    roomFiltersSelector,
    ( rooms, { entryFeeRange } ) => {
        const entryFees = _.map( rooms, ( room ) => room.entryFee || 0 );
        const min = _.min( entryFees );
        const max = _.max( entryFees );
        const steps = _.sortBy( _.uniq( entryFees ) );
        return {
            min,
            max,
            values: {
                min,
                max,
                ...entryFeeRange,
            },
            steps,
        };
    },
);

export const defaultFilteredRoomsSelector = createSelector(
    roomsSelector,
    ( rooms ) => rooms.filter( ( room ) => room.status === 'open' ),
);

export const defaultSortedRoomsSelector = createSelector( defaultFilteredRoomsSelector, rooms => {
    return rooms.sort( ( a, b ) => {
        const sortByDate = sortingByDate( a, b );
        if ( sortByDate === 0 ) {
            return sortingByEntries( a, b );
        }

        return sortByDate;
    } );
} );

export const filteredRoomsSelector = createSelector(
    defaultSortedRoomsSelector,
    roomFiltersSelector,
    entryFeeRangeSelector,
    ( rooms, filters, { values } ) => _.filter( rooms, ( room ) => _.conformsTo( room, {
        type: ( value ) => Boolean( filters[GAME_TYPE_STRINGS[value]] ),
        maxEntries: ( value ) => value > room.gamesCount,
        entryFee: ( value ) => _.inRange( value, values.min, values.max + 1 ),
        currency: ( value ) => !filters.currency || value === filters.currency,
        beginner: ( value ) => !filters.beginner || Boolean( value ),
        featured: ( value ) => !filters.featured || Boolean( value ),
    } ) )
);

const getRoomCurrencies = createSelector(
    roomsSelector,
    ( rooms ) => rooms.reduce( ( agg, room ) => agg.includes( room.currency ) ? agg : [...agg, room.currency], [] )
);

export const getAvailableCurrencies = createSelector(
    [
        ( state ) => _.get( state, 'reducer.app.settings.currencies' ),
        getRoomCurrencies,
    ],
    ( currencies, roomCurrencies ) => !currencies || !currencies.length ? roomCurrencies : currencies,
);

export const getCurrentCompetition = ( competitionRoomId ) => createSelector(
    [
        getRooms,
    ],
    ( rooms ) => rooms[competitionRoomId] || {}
);

export const fillGamesWithAthletes = ( competitionRoomId ) => createSelector(
    [
        getCurrentCompetition( competitionRoomId ),
        getSportData,
        getChosenSport,
    ],
    ( competition, sportData, sport ) => {
        const { sportsmen, participants, teams } = sportData;
        const defence = 'dst';
        const roomGames = [...competition.games || []];
        let athleteId;
        return roomGames.map( ( game ) => {
            game.sportsmen = {};
            getAthletesPositions( sport ).map( ( position ) => {
                athleteId = game[position];
                if ( sportsmen[athleteId] ) {
                    game.sportsmen[position] = { ...sportsmen[athleteId],
                        ...participants[sportsmen[athleteId].currentParticipantId] };
                } else {
                    game.sportsmen[position] = {};
                }
                return game;
            } );
            if ( sport === 'nfl' ) {
                game.sportsmen[defence] = { ...teams[game.dst] };
            }
            return game;
        } );
    }
);

export const addPrizesToGames = ( competitionRoomId ) => createSelector(
    [
        getSportData,
        fillGamesWithAthletes( competitionRoomId ),
    ],
    ( sportData, roomGames ) => {
        const games = sportData.games;
        return roomGames.map( rg => {
            return { ...rg, ...games[rg.id] };
        } );
    }
);

/* Get filtered rooms by filteredRoomsSelector and hide all FreeRoll rooms with users games */
export const filteredCompetitionRooms = createSelector(
    [
        getUserId,
        getGames,
        defaultFilteredRoomsSelector,
        filteredRoomsSelector,
    ],
    ( userid, games, defaultRooms, filteredRooms ) => {
        const myRoomIds = Object.values( games )
            .filter( game => game.userId === userid )
            .map( game => game.competitionRoomId );
        const myFreeRollRooms = defaultRooms.filter( ( room ) => (
            room.type === 'Free-roll' && myRoomIds.includes( room.id )
        ) ).map( myRoom => myRoom.id );

        return filteredRooms.filter( ( room ) => {
            return !myFreeRollRooms.includes( room.id );
        } );
    }
);

export const getNearestCompetitions = createSelector(
    filteredCompetitionRooms,
    competitions => {
        const nearestDate = competitions.length ? competitions[0].startDate : 0;
        return competitions.filter( competition => competition.startDate === nearestDate );
    },
);

const getCompetitionDate = ( state, competitionDate ) => competitionDate;

export const getCompetitionsByDate = createSelector(
    filteredCompetitionRooms,
    getCompetitionDate,
    ( competitions, competitionDate ) => {
        return competitions.filter( competition => competition.startDate === competitionDate );
    },
);
