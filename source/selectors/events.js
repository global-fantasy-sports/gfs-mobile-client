import { createSelector } from 'reselect';

import { getSportData } from './sport';

const getEvents = createSelector(
    getSportData,
    ( sportData ) => ( sportData.events || {} ).sport_events || []
);

export const getOpenEvent = createSelector(
    getEvents,
    ( events ) => events.filter( event => !event.finished )
);

export const getOpenEventsId = createSelector(
    getOpenEvent,
    events => events.map( event => event.id )
);
