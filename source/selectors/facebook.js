import { createSelector } from 'reselect';


export const getFBSearchQuery = state => state.reducer.facebook.search || '';
export const getFBStatus = state => state.reducer.facebook.status;
export const getFriends = state => state.reducer.facebook.friends || [];

export const getSearchTerms = createSelector(
    getFBSearchQuery,
    ( query ) => query.trim().toLowerCase()
);

export const getFilteredFriends = createSelector(
    [getFriends, getSearchTerms],
    ( friends, search ) => search
        ? friends.filter( f => f.name.toLowerCase().indexOf( search ) > -1 )
        : friends
);
