import _ from 'lodash';
import { createSelector } from 'reselect';
import { getSportData } from './sport';
import { getCurrentDateObj } from '../selectors/app';
import { getUserId } from '../selectors/user';
import { unixDateToLocal } from '../utils/date';

export const markWinners = ( state, competitions, games ) => {
    return Object.values( games ).map( game => (
        {
            ...game,
            imWinner: ( game.prizeAmount && ( !game.userData || getUserId( state ) === game.userData.id ) ),
            celebrateTime: getCurrentDateObj( state )
                .isBefore( unixDateToLocal(
                    ( competitions[game.competitionRoomId] ? competitions[game.competitionRoomId].endDate : 0 )
                    + ( 24 * 60 * 60 )
                ) ),
        }
    ) );
};

export const prepareCompetitionsGames = ( state, competitions, games ) => {
    return Object
            .values( markWinners( state, competitions, games ) )
            .filter( ( item ) => typeof item !== 'string' && !item.deleted )
            .map( game => ( { ...game, competition: { ...competitions[game.competitionRoomId] } } ) );
};

export const sortGamesByDateStart = ( games ) => {
    let sortedByStatus = games.map( game => {
        const started = game.competition.started;
        const finished = game.competition.finished;
        let statusIndex;
        if ( !started ) {
            statusIndex = 1;
        } else if ( started && !finished ) {
            statusIndex = 2;
        } else if ( finished ) {
            statusIndex = 3;
        }
        return { ...game, statusIndex };
    } );
    return _.orderBy( sortedByStatus, ['statusIndex', 'competition.startDate'], ['asc', 'asc'] );
};

export const getGames = createSelector(
    getSportData,
    ( sportData ) => sportData.games
);
