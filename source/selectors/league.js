import _ from 'lodash';
import { createSelector } from 'reselect';

import { getChosenSport } from './sport';
import { defaultLeague } from '../constants/app';

export const getChosenLeague = ( state ) => _.get( state, 'reducer.app.chosenLeague', defaultLeague );

export const getSportLeagues = createSelector(
    [getChosenSport, ( state ) => state],
    ( sport, state ) => {
        const settings = _.get( state, 'reducer.app.settings', undefined );
        if ( !settings ) {
            return [];
        }
        const sportData = settings.sports[sport] || {};
        const leagues = sportData.leagues || {};
        return _.sortBy( Object.entries( leagues ), '[1].order' );
    }
);

export const getSalarycapBudget = createSelector(
    [getChosenSport, getChosenLeague, ( state ) => state],
    ( sport, league, state ) => {
        return _.get( state, `reducer.app.settings.sports.${sport}.leagues.${league}.salarycapBudget`, 0 );
    }
);
