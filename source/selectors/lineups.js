import _ from 'lodash';
import { createSelector } from 'reselect';
import { getAthletes, getAthletesList } from './athletes';
import { getChosenSport, getSportData } from './sport';
import { POSITIONS, LINEUP_POSITIONS } from '../constants/wizard';

export const getLineupPositions = createSelector( getChosenSport, ( sport ) => LINEUP_POSITIONS[sport] );

export const getLineupsById = createSelector(
    getSportData,
    ( { sport, lineups } ) => _.mapValues( lineups.byId, ( props ) => ( {
        ...props,
        sport,
        positions: LINEUP_POSITIONS[sport],
        changed: false,
    } ) )
);

export const getCurrentLineupEdits = createSelector(
    ( state ) => state.reducer.lineupEdit,
    ( { id, sport, ...edits } ) =>
        ( id && sport ) ? { id, ...edits, sport, changed: _.size( edits ) > 1 || id === 'new' } : null
);

export const getLineupTotals = ( lineup ) => {
    const athletes = _.compact( _.pick( lineup, LINEUP_POSITIONS[lineup.sport] ) );
    const totals = {
        athletes: athletes.length,
        fppg: 0,
        salary: 0,
    };
    return athletes.length ? athletes.reduce( ( agg, item ) => {
        agg.salary += item.salary || 0;
        agg.fppg += item.FPPG || 0;
        return agg;
    }, totals ) : totals;
};

export const getLineupsWithEdits = createSelector(
    [getLineupsById, getCurrentLineupEdits],
    ( data, edits ) => {
        if ( edits ) {
            const oldLineup = data[edits.id] || {};
            const lineup = { ...oldLineup, ...edits };
            lineup.totals = getLineupTotals( lineup );
            return {
                ...data,
                [lineup.id]: lineup,
            };
        }
        return data;
    }
);

export const isDataFetched = createSelector(
    getSportData,
    ( { socketLogged, lineups } ) => socketLogged && lineups.fetched
);

export const getFetchError = createSelector(
    getSportData,
    ( sportData ) => sportData.lineups.error
);

export const getLineupInfo = ( athletes, positions, { id, ...data } ) => {
    const lineup = positions.map( ( pos ) => data[pos] ? athletes[data[pos]] : null );

    const totals = _.compact( lineup ).reduce( ( agg, item ) => {
        agg.salary += item.salary || 0;
        agg.fppg += item.FPPG || 0;
        agg.athletes += 1;
        return agg;
    }, { athletes: 0, fppg: 0, salary: 0 } );

    return {
        ...data,
        ..._.zipObject( positions, lineup ),
        id, positions,
        totals,
    };
};

export const getLineupsMap = createSelector(
    [getLineupsWithEdits, getAthletes, getLineupPositions],
    ( lineups, athletes, positions ) =>
        _.mapValues( lineups, _.partial( getLineupInfo, athletes, positions ) )
);

export const getLineupsList = createSelector(
    getLineupsMap,
    ( lineups ) => {
        const lineupArr = Object.values( lineups );
        if ( lineupArr ) {
            return lineupArr.filter( ( l ) => l.id !== 'new' );
        }

        return lineupArr;
    }
);

const getNewLineup = ( sport ) => ( {
    id: 'new',
    name: 'My Lineup',
    sport,
    positions: LINEUP_POSITIONS[sport] || [],
    totals: { athletes: 0, fppg: 0, salary: 0 },
} );

//  ***=> ( !lineups[id] && id === 'new' ) -- temporary removed this checking!!!***
export const getCurrentLineup = ( id ) => createSelector(
    [getLineupsMap, getChosenSport],
    ( lineups, sport ) => !lineups[id] ? getNewLineup( sport ) : lineups[id]
);

export const getCandidates = ( position ) => createSelector(
    [getAthletesList, getChosenSport],
    ( athletes, sport ) => {
        const positions = new Set( POSITIONS[sport][position] );
        return athletes.filter( ( athlete ) => {
            const athletePosition = athlete.position;
            return positions.has( athletePosition );
        } );
    }
);

export const checkPositionAvailability = createSelector(
    [getChosenSport, ( lineup ) => lineup],
    ( sport, lineup ) => {
        let updatedLineup = _.mapValues( POSITIONS[sport], ( positions, positionId ) => {
            const participant = _.get( lineup, [positionId], null );
            if ( participant ) {
                return {
                    ...participant,
                    unavailable: participant.unavailable || !positions.includes( participant.position ),
                };
            }
            return null;
        } );
        updatedLineup = _.pickBy( updatedLineup, ( p ) => p !== null );
        return { ...lineup, ...updatedLineup };
    }
);

