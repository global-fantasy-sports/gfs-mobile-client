import _ from 'lodash';
import { createSelector } from 'reselect';
import moment from 'moment';

import { getSportData } from './sport';
import { getOpenEventsId } from './events';
import { getTeams } from './teams';
import { TIME_FORMAT } from '../constants/app';

const getOpenEventMatches = createSelector(
    getSportData,
    getOpenEventsId,
    ( sportData, eventIds ) => {
        return Object.values( sportData.matches || {} )
            .filter( match => eventIds.includes( match.sportEventId ) && match.postponed === false );
    }
);

const getRoom = ( __, room ) => room;

export const getGroupedMatches = createSelector(
    getOpenEventMatches,
    getTeams,
    ( matches, teams ) => {
        const list = matches.map( ( match ) => {
            const date = moment.unix( match.startTimeUtc );
            match = {
                ...match,
                awayTeam: teams[match.awayTeam] || {},
                homeTeam: teams[match.homeTeam] || {},
            };
            return {
                date: date.format( TIME_FORMAT.schedule_date ),
                hour: date.format( TIME_FORMAT.schedule_time ),
                match,
            };
        } );

        return _.groupBy( list, 'date' );
    }
);

export const getMatchDatesFormatted = createSelector(
    getOpenEventMatches,
    ( matches ) => {
        const matchesData = matches.map( ( match ) => {
            const date = moment.unix( match.startTimeUtc );
            return { date: date.format( TIME_FORMAT.schedule_date ), day: date.format( TIME_FORMAT.schedule_time ) };
        } );
        return _.uniqBy( matchesData, 'date' );
    }
);

export const getOnGoingMatchIdsByRoom = createSelector(
    [getOpenEventMatches, getRoom],
    ( matches, competitionRoom ) => {
        const { sportEventId, startDate } = competitionRoom;
        return matches
            .filter( match => match.sportEventId === sportEventId && match.startTimeUtc >= startDate )
            .map( match => match.id );
    }
);

export const addMatchToParticipants = createSelector(
    ( participants, matches ) => ( { participants, matches } ),
    ( { participants, matches } ) => {
        return Object.values( participants ).map( ( participant ) => (
                { ...participant, match: matches[participant.contestId] }
            )
        );
    }
);
