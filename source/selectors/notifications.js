import { createSelector } from 'reselect';
import _ from 'lodash';

const getNotificationsFromState = state => state.reducer.notifications.notificationsList;

export const getNotifications = createSelector(
    [getNotificationsFromState],
    notifications => _.orderBy( notifications, ['createdAt'], ['desc'] )
);
