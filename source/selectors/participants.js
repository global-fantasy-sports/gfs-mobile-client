import { createSelector } from 'reselect';
import _ from 'lodash';
import moment from 'moment';

import { getNearestCompetitions } from './competitions';
import { getSportData } from './sport';

const getDefenseTeams = ( { defenceTeams, teams } ) =>
    Object.values( defenceTeams ).map( ( dstInfo ) => ( {
        salary: 0,
        FPPG: 0,
        oprk: 0,
        ...teams[dstInfo.team],
        ...dstInfo,
        id: dstInfo.team,
        position: 'DST',
        nameFull: `${teams[dstInfo.team].city} ${teams[dstInfo.team].name}`,
    } ) );

const getParticipants = createSelector(
    [
        ( sportData ) => _.get( sportData, 'participants', {} ),
        ( sportData ) => _.get( sportData, 'sportsmen', {} ),
    ],
    ( participants, athletes ) =>
        Object.values( participants ).map( ( participant ) => ( {
            ...participant,
            ...athletes[participant.athleteId],
            id: String( participant.id ),
        } ) )
);

export const getParticipantsByGameMatchId = createSelector(
    [getParticipants, ( __, sportEventId ) => sportEventId],
    ( participants, sportEventId ) =>
        participants.filter( ( participant ) => participant.sportEventId === sportEventId )
);

const getAthletes = createSelector(
    [
        ( state ) => getSportData( state ).sportsmen,
        ( state ) => state.translations.words || {},
    ],
    ( athletes, translations ) => _.mapValues( athletes, ( athlete ) => ( {
        ...athlete,
        nameLocal: translations[athlete.name] || athlete.name,
    } ) ),
);

export const getSportParticipants = createSelector(
    [
        getSportData,
        getAthletes,
        ( state ) => state.reducer.wizard,
        ( state ) => state,
    ],
    ( sportData, athletes, wizard, state ) => {
        let competitionId = wizard.predefinedCompetitionId || null;
        let participants = [];

        if ( !competitionId ) {
            const nearest = getNearestCompetitions( state );
            competitionId = nearest.length > 0 ? nearest[0].id : null;
        }

        if ( competitionId ) {
            const competition = sportData.competitionRooms[competitionId];
            if ( competition ) {
                let startDayTime = moment.unix( competition.startDate ).startOf( 'day' ).unix();
                let dayMatchesIds = Object.values( sportData.matches )
                    .filter( ( match ) => startDayTime <= match.startTimeUtc && match.status === 'not_started' )
                    .map( ( match ) => match.id );

                participants = Object.values( sportData.participants )
                    .map( ( participant ) => ( {
                        ...participant,
                        ...athletes[participant.athleteId],
                        id: String( participant.id ),
                        unavailable: !dayMatchesIds.includes( participant.contestId ),
                    } ) );
            }
        }

        if ( sportData.sport === 'nfl' ) {
            participants = participants.concat( getDefenseTeams( sportData ) );
        }

        return participants;
    }
);

export const getAvailableParticipants = createSelector(
    getSportParticipants,
    ( participants ) => participants.filter( ( p ) => !p.unavailable && p.showInRoster ),
);

export const addFavoritesToParticipants = createSelector(
    ( participants, favorites ) => ( { participants, favorites } ),
    ( { participants, favorites } ) => {
        const favoritesSet = new Set( favorites );
        return Object.values( participants ).map( ( participant ) => ( {
            ...participant,
            isFavorite: favoritesSet.has( participant.athleteId ),
        } ) );
    }
);
