import { createSelector } from 'reselect';

import { getSportData } from './sport';

export const getParticipantsResultsLegend = createSelector(
    getSportData,
    ( data ) => data.resultsLegend.participant || {}
);
