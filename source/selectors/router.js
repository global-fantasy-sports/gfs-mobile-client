import { createSelector } from 'reselect';


export const getRouterData = ( state ) => state.routing.location;

export const getRouterPath = createSelector(
    getRouterData,
    ( router ) => router.pathname
);

export const isActive = ( pathname ) => {
    return function ( currentLocation ) {
        if ( !currentLocation ) {
            return false;
        }
        const location = currentLocation.pathname;

        return location.indexOf( pathname ) > -1;
    };
};
