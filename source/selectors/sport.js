import _ from 'lodash';
import { createSelector } from 'reselect';

import { defaultSport, defaultLeague } from '../constants/app';

const getSportsRoot = ( state ) => _.get( state, 'reducer.sports', {} );

export const getChosenSport = ( state ) => _.get( state, 'reducer.app.chosenSport', defaultSport );
export const getChosenLeague = ( state ) => _.get( state, 'reducer.app.chosenLeague', defaultLeague );

export const getSportData = createSelector(
    [getSportsRoot, getChosenSport, getChosenLeague],
    ( sportsRoot, sport, league ) => {
        const sportData = sportsRoot[sport] || {};
        const leagueData = sportData[league] || {};
        return { ...leagueData, sport, league };
    }
);

export const isSportDataReady = createSelector(
    getSportData,
    ( data ) => data.sport && data.league && data.socketLogged
);

export const getTeams = createSelector(
    getSportData,
    ( data ) => data.teams || {}
);
