import { createSelector } from 'reselect';
import { getSportData } from './sport';

export const getTeams = createSelector(
    getSportData,
    sportData => sportData.teams || {}
);
