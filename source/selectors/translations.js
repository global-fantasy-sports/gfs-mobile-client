import _ from 'lodash';
import { createSelector } from 'reselect';

import { defaultTimeLocale } from '../constants/translations';

const getTranslations = createSelector(
    ( state ) => state.translations,
    ( translations ) => translations.words || {},
);

const I18N_DEBUG = localStorage && localStorage.getItem( 'i18nDebug' ) === 'yes';

const bracketizeTranslation = ( words, key, values ) => {
    let output = '';
    if ( !words ) {
        output = I18N_DEBUG ? `[[${key}]]` : key;
    } else {
        output = I18N_DEBUG ? `[${words}]` : words;
    }
    return values ? output.replace( /\$\(([^\)]+)?\)/g, ( m, param ) => values[param] ) : output;
};

export const translationsReady = createSelector(
    getTranslations,
    ( translations ) => Object.keys( translations ).length > 0
);

export const translate = createSelector(
    getTranslations,
    ( strings ) =>
        ( key, values ) => bracketizeTranslation( strings[key], key, values )
);

export const getTimeLocale = ( state ) => _.get( state, 'translations.locale', defaultTimeLocale );
