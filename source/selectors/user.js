import { createSelector } from 'reselect';

export const getSessionId = createSelector(
    ( state ) => state.reducer.auth,
    ( auth ) => auth.session
);

export const getUser = createSelector(
    ( state ) => state.reducer.user,
    ( user ) => user || {}
);

export const getUserId = createSelector( getUser, ( user ) => user.id );

export const getUserAvatar = createSelector( getUser, ( user ) => user.avatar || '' );

export const getUserName = createSelector( getUser, ( user ) => user.name || '' );
