import _ from 'lodash';
import { createSelector } from 'reselect';
import { getChosenSport } from './sport';

import { getSportParticipants } from './participants';
import { POSITIONS, LINEUP_POSITIONS } from '../constants/wizard';
import { checkPositionAvailability } from './lineups';
import { isActive } from './router';

export const getSelectedTeamsCount = ( lineup ) => {
    const selectedTeams = {};
    Object.values( lineup ).forEach( ( participant ) => {
        if ( participant ) {
            if ( selectedTeams[participant.teamId] ) {
                selectedTeams[participant.teamId] += 1;
            } else {
                selectedTeams[participant.teamId] = 1;
            }
        }
    } );
    return selectedTeams;
};

export const getWizardLineup = ( lineup, participants ) => {
    const wizardLineup = Object.entries( lineup ).reduce( ( agg, [position, id] ) => {
        const participant = participants.find( ( p ) => p.id === id );
        if ( participant ) {
            agg[position] = participant;
        }
        return agg;
    }, {} );
    return checkPositionAvailability( wizardLineup );
};

export const getWizardData = ( state ) => state.reducer.wizard;

export const getTotals = createSelector(
    [
        getSportParticipants,
        getWizardData,
    ],
    ( allParticipants, wizard ) => {
        const lineup = getWizardLineup( wizard.lineup, allParticipants );
        const participants = Object.values( lineup );

        return participants.reduce( ( agg, item ) => {
            agg.salary += item.salary || 0;
            agg.fppg += item.FPPG || 0;
            return agg;
        }, { fppg: 0, salary: 0 } );
    }
);

export const getSelectedIds = ( lineup ) => new Set(
    Object.values( lineup ).filter( Boolean ).map( ( item ) => item.id )
);

export const getParticipants = createSelector(
    [
        ( props ) => props.participants,
        ( props ) => POSITIONS[props.currentSport][props.params.position],
    ],
    ( participants, positions ) => participants.filter( ( p ) => positions.includes( p.position ) )
);

export const isSelectStepActive = isActive( 'wizard/select' );
export const isEntryFeeStepActive = isActive( 'wizard/stake' );
export const isFriendsStepActive = isActive( 'wizard/friends' );

export const getCurrentStep = createSelector(
    ( state ) => state.routing.location,
    ( router ) => {
        if ( isSelectStepActive( router ) ) {
            return 'draftStep';
        }
        if ( isEntryFeeStepActive( router ) ) {
            return 'stakeStep';
        }
        if ( isFriendsStepActive( router ) ) {
            return 'friendsStep';
        }
        return '';
    }
);

export const getWizardViewMode = ( state ) => state.reducer.wizardViewMode;

export const isFullLineup = createSelector(
    getWizardData,
    getChosenSport,
    getSportParticipants,
    ( wizard, sport, participants ) => {
        const lineup = getWizardLineup( wizard.lineup, participants );
        const filtered = _.pickBy( lineup, ( p ) => p.unavailable === false );
        return Object.keys( filtered ).length === LINEUP_POSITIONS[sport].length;
    }
);
