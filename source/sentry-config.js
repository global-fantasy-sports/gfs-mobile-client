/* global Raven */
if ( process.env.NODE_ENV === 'production' ) {
    if ( typeof Raven !== 'undefined' ) {
        Raven.setTagsContext( {
            'build.target': process.env.BUILD_TARGET,
            'build.branch': process.env.BUILD_BRANCH,
            'build.number': process.env.BUILD_NUMBER,
        } );
    } else {
        console.error( 'Raven not found' );  // eslint-disable-line no-console
    }
}
