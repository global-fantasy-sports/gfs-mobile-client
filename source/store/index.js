import _ from 'lodash';
import axios from 'axios';
import { applyMiddleware, compose } from 'redux';
import { routerMiddleware as createRouterMiddleware } from 'react-router-redux';
import { browserHistory } from 'react-router';
import createSagaMiddleware from 'redux-saga';
import { createLogger } from 'redux-logger';
import persistState from 'redux-localstorage';

import { createInjectStore } from 'redux-injector';

import logging from '../utils/logging';
import rootSaga from '../saga';

import Session from '../actions/session';

import { rootReducers } from '../reducers';

const connectDevToolsExtension = () =>
    process.env.NODE_ENV !== 'production' && window.devToolsExtension
        ? window.devToolsExtension()
        : ( f ) => f;

const PATHS = [
    'reducer.infoPopups',
    'reducer.wizard',
    'reducer.wizardViewMode',
    'reducer.lineupEdit',
    'reducer.onboarding',
    'routing',
];

const slicer = ( paths ) => ( state ) => _.zipObjectDeep( paths, _.at( state, paths ) );

let store = null;

export default function getStore () {
    if ( store ) {
        return store;
    }
    const logger = createLogger( { collapsed: true } );
    const sagaMiddleware = createSagaMiddleware( { onError: ::logging.error } );
    const routerMiddleware = createRouterMiddleware( browserHistory );

    const middleware = compose(
        persistState( PATHS, { key: 'redux-storage', slicer } ),
        applyMiddleware( logger, sagaMiddleware ),
        applyMiddleware( routerMiddleware ),
        connectDevToolsExtension()
    );

    store = createInjectStore( rootReducers, middleware );

    axios.defaults.baseURL = '/';
    axios.interceptors.response.use(
        ( response ) => response,
        ( error ) => {
            const { status, headers } = error.response;
            const data = Object.assign(
                { errorMessage: 'Server error. Try again later.' },
                error.response.data || {}
            );

            if ( status === 401 ) {
                store.dispatch( Session.error( data.errorMessage ) );
            }

            return { data, headers, status };
        },
    );


    sagaMiddleware.run( rootSaga );

    if ( module.hot ) {
        module.hot.accept( '../reducers', () => {
            store.replaceReducer( require( '../reducers' ).default );
        } );
    }

    return store;
}
