import cx from 'classnames';
import React, { Component, PropTypes } from 'react';
import ReactInk from 'react-ink';

import Icon from '../icon/Icon';
import './Btn.scss';

const BtnLabel = ( { label } ) => {
    if ( Array.isArray( label ) ) {
        return (
            <span className="double-row">
                <span>{ label[0] }</span>
                <span>{ label[1] }</span>
            </span>
        );
    }
    return (
        <span>{ label }</span>
    );
};

BtnLabel.propTypes = {
    label: PropTypes.oneOfType( [
        PropTypes.node,
        PropTypes.arrayOf( PropTypes.node ),
    ] ),
};


export const BTN_SIZES = [
    'monster',
    'large',
    'normal',
    'small',
];

export const BTN_TYPES = [
    'primary',
    'payment',
    'warning',
    'confirm',
    'default',
    'transparent',
    'white',
];

class Btn extends Component {

    static propTypes = {
        block: PropTypes.bool,
        className: PropTypes.string,
        children: PropTypes.node,
        disabled: PropTypes.bool,
        icon: PropTypes.string,
        iconMod: PropTypes.string,
        onClick: PropTypes.func,
        label: PropTypes.oneOfType( [
            PropTypes.node,
            PropTypes.arrayOf( PropTypes.node ),
        ] ),
        round: PropTypes.bool,
        submit: PropTypes.bool,
        size: PropTypes.oneOf( BTN_SIZES ),
        type: PropTypes.oneOf( BTN_TYPES ),
        value: PropTypes.any,
    };

    static defaultProps = {
        className: '',
        size: 'normal',
        type: 'default',
    };

    handleClick = ( event ) => {
        if ( this.props.onClick ) {
            event.preventDefault();
            this.props.onClick( this.props.value );
        }
    };

    render () {
        const { block, className, disabled, icon, size, type, label, round, submit, iconMod, children } = this.props;

        return (
            <button
                className={ cx( 'Btn', className, size, type, {
                    block,
                    disabled,
                    round,
                    square: icon && !label && !round,
                } ) }
                disabled={ !!disabled }
                onClick={ submit ? null : this.handleClick }
                onTouchTap={ submit ? null : this.handleClick }
                type={ submit ? 'submit' : 'button' }
            >
                { icon ? <Icon icon={ icon } mod={ iconMod } /> : null }
                { label ? <BtnLabel label={ label } /> : children }
                <ReactInk />
            </button>
        );
    }
}

export default Btn;
