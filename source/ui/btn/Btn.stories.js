import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { withKnobs, boolean, text, select } from '@kadira/storybook-addon-knobs';
import Btn, { BTN_SIZES, BTN_TYPES } from './Btn';

import '../../../storybook/style.scss';

storiesOf( 'Btn', module )
    .addDecorator( withKnobs )
    .add( 'showcase', () => (
        <div className="buttons-wrap">
            {BTN_SIZES.map( ( size ) =>
                <p key={ size }>
                    {BTN_TYPES.map( ( type ) =>
                        <Btn
                            key={ `${type}` }
                            label="Play Soccer"
                            onClick={ action( 'click' ) }
                            size={ size }
                            type={ type }
                        />
                    )}
                </p>
            )}
            <p>
                <Btn
                    disabled
                    label="Next"
                    onClick={ action( 'click' ) }
                    size="normal"
                    type="primary"
                />
            </p>
            <p>
                <Btn
                    icon="plus"
                    onClick={ action( 'click' ) }
                    round
                    size="normal"
                    type="primary"
                />
                <Btn
                    icon="filter"
                    onClick={ action( 'click' ) }
                    round
                    size="small"
                    type="default"
                />
            </p>
            <p>
                <Btn
                    label={ ['Selected', '2 teams'] }
                    onClick={ action( 'click' ) }
                    size="small"
                />
            </p>
            <p>
                <Btn
                    icon="list"
                    onClick={ action( 'click' ) }
                />
            </p>
            <p>
                <Btn
                    icon="arrow-left"
                    label="Back"
                    onClick={ action( 'click' ) }
                    type="transparent"
                />
            </p>
            <p>
                <Btn
                    icon="arrow-center-bottom"
                    onClick={ action( 'click' ) }
                />
            </p>
            <p>
                <Btn
                    icon="arrow-center-top"
                    onClick={ action( 'click' ) }
                />
            </p>
        </div>
    ) )
    .add( 'interactive demo', () => (
        <div className="buttons-wrap">
            <p>
                <Btn
                    disabled={ boolean( 'disabled', false ) }
                    label={ text( 'label', 'Play' ) }
                    onClick={ action( 'click' ) }
                    size={ select( 'size', BTN_SIZES, 'normal' ) }
                    type={ select( 'type', BTN_TYPES, 'default' ) }
                />
            </p>
        </div>
    ) );
