import React, { Component, Children, cloneElement, PropTypes } from 'react';
import cx from 'classnames';
import './Btn.scss';


class BtnGroup extends Component {

    static propTypes = {
        children: PropTypes.node,
        mod: PropTypes.string,
    };

    render () {
        const buttons = Children.toArray( this.props.children );
        const cs = cx( 'BtnGroup', {
            [`BtnGroup-mod-${this.props.mod}`]: this.props.mod,
        } );

        return (
            <div className={ cs }>
                { buttons.map( ( btn ) =>
                    cloneElement( btn, null ),
                )}
            </div>
        );
    }
}

export default BtnGroup;
