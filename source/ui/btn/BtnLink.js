import React, { Component, PropTypes } from 'react';
import Btn from './Btn';
import { goToWithDispatch, preparePathWithLeague } from '../../utils/router';

class BtnLink extends Component {

    static propTypes = {
        to: PropTypes.string.isRequired,
        relativeToLeague: PropTypes.bool,
    };

    handleClick = () => {
        let path = this.props.to;
        if ( ( 'relativeToLeague' in this.props ) && this.props.relativeToLeague ) {
            path = preparePathWithLeague( this.props.to );
        }
        goToWithDispatch( path );
    };

    render () {
        return (
            <Btn
                { ...this.props }
                onClick={ this.handleClick }
            />
        );
    }
}

export default BtnLink;
