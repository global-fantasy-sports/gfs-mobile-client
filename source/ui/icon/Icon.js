import React, { PropTypes } from 'react';
import cx from 'classnames';

import './icon.scss';

const Icon = ( { className, icon, mod } ) => {
    return (
        <i
            className={ cx( className, 'icon', `icon-${icon}`, {
                [`icon-mod-${mod}`]: !!mod,
            } ) }
        />
    );
};
Icon.propTypes = {
    className: PropTypes.string,
    icon: PropTypes.string,
    mod: PropTypes.string,
};

export default Icon;
