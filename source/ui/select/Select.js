import _ from 'lodash';
import cx from 'classnames';
import React, { Component, PropTypes } from 'react';
import Btn from '../../ui/btn/Btn';

import './Select.scss';

class Select extends Component {

    static propTypes = {
        className: PropTypes.string,
        value: PropTypes.string,
        label: PropTypes.string,
        options: PropTypes.arrayOf( PropTypes.string ).isRequired,
        optionFormatter: PropTypes.func,

        onBlur: PropTypes.func,
        onChange: PropTypes.func.isRequired,
        onFocus: PropTypes.func,
    };

    static defaultProps = {
        options: [],
        className: '',
        optionFormatter: _.identity,
        onBlur: _.noop,
        onFocus: _.noop,
    };

    constructor ( props ) {
        super( props );
        this.state = { focused: false };
    }

    handleFocus = ( event ) => {
        this.setState( { focused: true } );
        this.props.onFocus( event );
    }

    handleBlur = ( event ) => {
        this.setState( { focused: false } );
        this.props.onBlur( event );
    }

    handleChange = ( event ) => {
        this.props.onChange( event.target.value );
    }

    render () {
        const { className, optionFormatter, options, value, ...props } = this.props;

        // const valueIndex = options.indexOf( value );

        return (
            <div
                className={ cx( 'UI-Select', className, {
                    'UI-Select--focused': this.state.focused,
                } ) }
            >
                <select
                    { ...props }
                    className="UI-Select__input"
                    onBlur={ this.handleBlur }
                    onChange={ this.handleChange }
                    onFocus={ this.handleFocus }
                    value={ value }
                >
                    {options.map( ( option, index )=>
                        <option key={ index } value={ option }>
                            { optionFormatter( `select.option.${ option }` ) }
                        </option>
                    )}
                </select>
                <Btn
                    icon="sort"
                />
            </div>
        );
    }
}

export default Select;
