import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { withKnobs, text } from '@kadira/storybook-addon-knobs';
import Select from './Select';

const stories = storiesOf( 'Select', module );

stories.addDecorator( withKnobs );

class SelectStory extends React.Component {

    state = { value: 'one' };

    handleChange = ( value ) => {
        this.setState( { value } );
    }

    render () {
        return (
            <div>
                <section style={ { background: '#eee', padding: 10 } }>
                    <Select
                        label={ text( 'label', 'Sort' ) }
                        onBlur={ action( 'blur' ) }
                        onChange={ this.handleChange }
                        options={ ['one', 'two', 'three'] }
                        value={ this.state.value }
                    />
                </section>
                <p style={ { padding: 10 } }>
                    Selected value: { this.state.value }
                </p>
            </div>
        );
    }
}

stories.add( 'default', () => (
    <SelectStory />
) );
