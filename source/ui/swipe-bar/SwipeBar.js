import React, { Component, PropTypes } from 'react';
import cx from 'classnames';

import './SwipeBar.scss';


class SwipeBar extends Component {

    static propTypes = {
        children: PropTypes.any,
        vertical: PropTypes.bool,
    };

    render () {
        const { children, vertical } = this.props;
        const orientation = vertical ? 'vertical' : 'horizontal';

        const cs = cx( 'SwipeBar', `orientation-${orientation}` );

        return (
            <div className={ cs }>
                {children}
            </div>
        );
    }
}

export default SwipeBar;
