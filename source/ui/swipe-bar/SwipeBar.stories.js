import React from 'react';
import { action, storiesOf } from '@kadira/storybook';
import { withKnobs } from '@kadira/storybook-addon-knobs';
import SwipeBar from './SwipeBar';

const stories = storiesOf( 'SwipeBar', module );

stories.addDecorator( withKnobs );

stories.add( 'default', () => (
    <SwipeBar
        onClick={ action( 'click' ) }
    />
) );
