export function makeRequestActionTypes ( baseAction ) {
    if ( typeof baseAction === 'string' ) {
        return {
            request: `${baseAction}::REQUEST`,
            success: `${baseAction}::SUCCESS`,
            fail: `${baseAction}::FAIL`,
        };
    }

    if ( Array.isArray( baseAction ) ) {
        return {
            request: baseAction[0],
            success: baseAction[1],
            fail: baseAction[2],
        };
    }

    return baseAction;
}
