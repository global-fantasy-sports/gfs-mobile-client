const deferTime = 10;

export const defer = ( iCallback, iDelay = deferTime ) => {
    setTimeout( iCallback, iDelay );
};

export const throttle = ( iCallback, iDelay, iContext = this ) => {
    let timeout = null;

    const later = ( params ) => {
        iCallback.apply( iContext, params );
        timeout = null;
    };

    return ( ...params ) => {
        if ( !timeout ) {
            timeout = setTimeout( later.bind( iContext, params ), iDelay );
        }
    };
};

export class Deferred {
    constructor () {
        this.resolve = null;
        this.reject = null;
        this.promise = null;

        this.promise = new Promise( ( iResolve, iReject ) => {
            this.resolve = iResolve;
            this.reject = iReject;
        } );
    }

    promise () {
        return this.promise;
    }

    resolve () {
        return this.resolve;
    }

    reject () {
        return this.reject;
    }
}

export const arrayToHash = ( iList, iKey = 'id' ) => {
    if ( !iList ) {
        return iList;
    }
    let hash = {};
    iList.forEach( iListItem => {
        hash[iListItem[iKey]] = iListItem;
    } );
    return hash;
};

/**
 * Creates a new array with or without given element
 * @param {Array} iArray
 * @param {*} iValue
 * @returns {Array}
 */
export function arrayToggle ( iArray, iValue ) {
    if ( iArray.includes( iValue ) ) {
        return iArray.filter( item => item !== iValue );
    }

    return [...iArray, iValue];
}

export const createIndex = ( iRecords, iFields = [] ) => {
    const checkFileds = ( iRecord, iFieldsNames ) => iFieldsNames.some( iField => iRecord[iField] );

    let index = {}, indexKey;
    iRecords.forEach( iRecord => {
        if ( checkFileds( iRecord, iFields ) ) {
            indexKey = iFields.reduce( ( iPrev, iCurrent ) => `${ iPrev }${ iRecord[iCurrent] }|`, '' );
            index[indexKey] = iRecord.id;
        }
    } );

    return index;
};
