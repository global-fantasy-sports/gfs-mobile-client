const cookiename = 'session';
const COOKIE_RE = new RegExp( `(?:^|; )${cookiename}=([^;]+)` );

/**
 * @deprecated
 */
export const getSessionFromCookie = () => {
    const match = document.cookie.match( COOKIE_RE );
    if ( !match ) {
        return false;
    }
    return match[1];
};
