import moment from 'moment';

export const unixDateToLocal = ( iUnixDate, iFormat ) => {
    const localDate = moment.unix( iUnixDate ).toDate();
    if ( !iFormat ) {
        return moment( localDate );
    }
    return moment( localDate ).format( iFormat );
};

export const unixDateToLocalCalendar = ( iUnixDate, iFormat ) => {
    const localDate = moment.unix( iUnixDate ).toDate();
    if ( !iFormat ) {
        return moment( localDate );
    }
    return moment( localDate ).calendar( null, { lastWeek: iFormat, sameElse: iFormat } );
};
