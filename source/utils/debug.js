import { autofill } from '../actions/wizard';

const Debug = ( store ) => {
    document.onkeydown = ( e ) => {
        if ( e.which === 77 && e.ctrlKey ) { // Ctrl + M
            store.dispatch( autofill() );
        }
    };
};

export default Debug;
