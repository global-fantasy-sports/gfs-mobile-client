/* global FB */

function extractError ( error ) {
    return ( error.code === 2500 ) ? 'not_logged_in' : error.message;
}

class FacebookAPI {

    constructor ( api ) {
        this.fbApi = api;
    }

    checkLogin () {
        return new Promise( ( resolve, reject ) =>
            this.fbApi.getLoginStatus( ( response ) => {
                switch ( response.status ) {
                    case 'connected':
                        return resolve( response );
                    case 'not_authorized':
                        return reject( 'not_authorized' );
                    default:
                        return reject( 'not_logged_in' );
                }
            } )
        );
    }

    login () {
        return new Promise( ( resolve, reject ) => {
            const params = {
                scope: 'email,user_friends',
                return_scopes: true,
            };

            function callback ( { status, authResponse } ) {
                if ( status === 'connected' ) {
                    resolve( authResponse );
                } else {
                    reject( 'not_logged_in' );
                }
            }

            return this.fbApi.login( callback, params );
        } );
    }

    logout () {
        return new Promise( resolve => this.fbApi.logout( () => resolve() ) );
    }

    checkPermissions () {
        return new Promise( ( resolve, reject ) =>
            this.fbApi.api( '/me/permissions', ( { data, error } ) => {
                if ( error ) {
                    reject( extractError( error ) );
                    return;
                }

                const permission = data.find( ( item ) => item.permission === 'user_friends' );
                if ( !permission || permission.status !== 'granted' ) {
                    resolve( false );
                } else {
                    resolve( true );
                }
            } )
        );
    }

    requestPermissions () {
        const params = {
            method: 'permissions.request',
            perms: 'user_friends',
            display: 'iframe',
        };

        return new Promise( resolve => this.fbApi.ui( params, resolve ) );
    }

    getInvitableFriends () {
        return new Promise( ( resolve, reject ) =>
            this.fbApi.api( '/me/invitable_friends?fields=id,name,picture.width(80).height(80)&limit=5000',
                ( { data, error } ) => {
                    if ( error ) {
                        reject( extractError( error ) );
                    } else {
                        resolve( data );
                    }
                }
            )
        );
    }

    getAppFriends () {
        return new Promise( ( resolve, reject ) =>
            this.fbApi.api( '/me/friends?fields=id,name,picture.width(80).height(80)&limit=5000',
                ( { data, error } ) => {
                    if ( error ) {
                        reject( extractError( error ) );
                    } else {
                        resolve( data );
                    }
                }
            )
        );
    }

    getFriends () {
        return Promise.all( [this.getAppFriends(), this.getInvitableFriends()] )
            .then( ( [app, fb] ) => [...app, ...fb] );
    }

    invite ( id ) {
        return new Promise( ( resolve, reject ) => {
            const params = {
                method: 'apprequests',
                to: [id],
                message: 'Try to beat me',
            };

            this.fbApi.ui( params, response => {
                this.clearDialogBox();
                if ( response.error_code ) {
                    reject( response.error_code );
                } else {
                    resolve( response );
                }
            } );
        } );
    }

    /**
     * Removes blank dialog box that is sometimes staying
     * after invite window is closed
     */
    clearDialogBox () {
        try {
            const nodes = document.getElementsByClassName( 'fb_dialog_mobile loading' );
            Array.from( nodes ).forEach( node => node && node.remove() );
        } catch ( e ) {
            // do nothing
        }
    }
}

let __promise = null;

/**
 * Initializes Facebook SDK
 * @param {string} appId - Facebook application id
 * @return {Promise<FB>} promise that resolves with an SDK instance
 */
export function initFBApi ( appId ) {
    if ( !__promise ) {
        if ( document.getElementById( 'facebook-jssdk' ) ) {
            return Promise.reject( new Error( 'Unexpected state' ) );
        }

        __promise = new Promise( ( resolve, reject ) => {
            window.fbAsyncInit = function fbAsyncInit () {
                FB.init( {
                    appId,
                    xfbml: false,
                    version: 'v2.7',
                    status: true,
                    frictionlessRequests: true,
                } );
                resolve( new FacebookAPI( FB ) );
            };

            const scriptElement = document.createElement( 'script' );
            scriptElement.id = 'facebook-jssdk';
            scriptElement.src = '//connect.facebook.net/en_US/sdk.js';
            scriptElement.onerror = reject;

            document.head.appendChild( scriptElement );
        } );
    }

    return __promise;
}
