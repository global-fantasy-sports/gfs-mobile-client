
const localStorage = window.localStorage;

class LocalStorage {
    static set ( iKey, iValue ) {
        localStorage.setItem( iKey, iValue );
    }

    static get ( iKey ) {
        return localStorage.getItem( iKey );
    }

    static remove ( iKey ) {
        return localStorage.removeItem( iKey );
    }
}

export default LocalStorage;
