/* global Raven */
const MESSAGE_COLORS = {
    log: 'green',
    warn: 'yellow',
    error: 'tomato',
};

/**
 *
 * @param {string} name
 * @param {string} level
 * @param {Array} messages
 */
function defaultLoggerFunc ( name, level, messages ) {
    if ( typeof Raven !== 'undefined' && level === 'error' ) {
        const [message, ...extra] = messages;
        const method = message instanceof Error
            ? 'captureException'
            : 'captureMessage';
        Raven[method]( message, { extra } );
    } else if ( process.env.NODE_ENV !== 'production' ) {
        const style = `font-weight:bold;color:${MESSAGE_COLORS[level]};`;
        console[level]( `%c${name}`, style, ...messages );  // eslint-disable-line no-console
    }
}

const DEFAULT_LOGGER = 'default';

export class Logger {

    /**
     * @param {string} name
     * @param {function} logger
     * @constructor
     */
    constructor ( name, logger = defaultLoggerFunc ) {
        this._name = name;
        this._logger = logger;
    }

    log ( ...args ) {
        this._logger( this._name, 'log', args );
    }

    warn ( ...args ) {
        this._logger( this._name, 'warn', args );
    }

    error ( ...args ) {
        this._logger( this._name, 'error', args );
    }

    getLogger ( name, logger = null ) {
        return new Logger(
            this._name === DEFAULT_LOGGER ? name : `${this._name}::${name}`,
            logger || this._logger
        );
    }
}

const defaultLogger = new Logger( DEFAULT_LOGGER );

export default defaultLogger;
