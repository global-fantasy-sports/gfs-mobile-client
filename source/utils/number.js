/**
 * @param {number} iNumberString
 * @returns {string}
 */
export const numberWithPadding = iNumberString => {
    const isNegative = Math.sign( iNumberString ) === -1;
    let sNumber = String( Math.abs( iNumberString ) );

    while ( sNumber.length < 2 ) {
        sNumber = '0' + sNumber;
    }

    return isNegative ? '- ' + sNumber : sNumber;
};

/**
 * Format a number for display
 * @param {number} value - value to format
 * @param {?number} fixed - number of floating point digits
 * @param {?string} defaultValue - value to return if iValue is missing
 * @returns {string}
 */
export function format ( value, fixed = 0, defaultValue = '' ) {
    if ( !Number.isFinite( value ) ) {
        return defaultValue;
    }
    const floatingPoint = process.env.BUILD_TARGET === 'og' ? '' : ',';
    return value.toFixed( fixed ).replace( /\B(?=(\d{3})+(?!\d))/g, floatingPoint );
}

/**
 * Represent a number as height in Feet/Inches
 * @param {number} value
 * @param {?string} defaultValue
 * @return {string}
 */
export function formatHeight ( value, defaultValue = '' ) {
    if ( !Number.isFinite( value ) ) {
        return defaultValue;
    }

    const inches = value % 12;
    const feet = ( value - inches ) / 12;
    return `${feet}'${inches}"`;
}
