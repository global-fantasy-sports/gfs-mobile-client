import React from 'react';
import ReactDOM from 'react-dom';

/**
 * @param {ReactClass} Target The component that defines `onOutsideEvent` handler.
 * @param {String[]} supportedEvents A list of valid DOM event names. Default: ['mousedown'].
 * @return {ReactClass}
 */
export default ( Target, supportedEvents = ['mousedown'] ) => {
    return class ReactOutsideEvent extends React.Component {
        componentDidMount () {
            if ( !this.refs.outsideTargetRef.onOutsideEvent ) {
                throw new Error( 'Component does not define "onOutsideEvent" method.' );
            }

            supportedEvents.forEach( ( eventName ) => {
                window.addEventListener( eventName, this.handleEvent, true );
            } );
        }

        componentWillUnmount () {
            supportedEvents.forEach( ( eventName ) => {
                window.removeEventListener( eventName, this.handleEvent, true );
            } );
        }

        handleEvent = ( event ) => {
            let targetElement = ReactDOM.findDOMNode( this.refs.outsideTargetRef ); // eslint-disable-line
            if ( targetElement && !targetElement.contains( event.target ) ) {
                this.refs.outsideTargetRef.onOutsideEvent( event );
            }
        };

        render () {
            return <Target ref="outsideTargetRef" { ... this.props } />;
        }
    };
};
