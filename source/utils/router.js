import getStore from '../store/index';
import { push, replace } from 'react-router-redux';


export const preparePathWithLeague = ( path, sport = null, league = null ) => {
    if ( !( sport && league ) ) {
        const store = getStore();
        const state = store.getState();

        sport = state.reducer.app.chosenSport;
        league = state.reducer.app.chosenLeague;
    }
    if ( path[0] !== '/' ) {
        path = '/' + path;
    }
    if ( path[path.length - 1] !== '/' ) {
        path += '/';
    }

    path = `/${sport}/${league}${path}`;
    return path;
};

export const goToWithDispatch = ( path ) => {
    const store = getStore();
    store.dispatch( push( path ) );
};

export const redirectToWithDispatch = ( path ) => {
    const store = getStore();
    store.dispatch( replace( path ) );
};
