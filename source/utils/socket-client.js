import _ from 'lodash';
import SockJS from 'sockjs-client';
import EventEmitter3 from 'eventemitter3';
import { buffers, eventChannel, END } from 'redux-saga';
import { WSRoutesConstants } from '../constants/api';
import logging from './logging';

function parseMessage ( data ) {
    try {
        const message = JSON.parse( data );
        if ( Array.isArray( message.error ) ) {
            message.error = message.error[0];
        }
        return message;
    } catch ( err ) {
        return {
            error: err.message,
            id: _.uniqueId( 'error' ),
            channel: 'unknown',
        };
    }
}

class SocketClient extends EventEmitter3 {

    static instances = {};

    /**
     * Get or create a client instance
     * @param {string} channel
     * @return {SocketClient}
     */
    static get ( channel ) {
        if ( !SocketClient.instances[channel] ) {
            SocketClient.instances[channel] = new SocketClient( channel );
        }
        return SocketClient.instances[channel];
    }

    constructor ( channel ) {
        super();

        this.channel = channel;
        this.logger = logging.getLogger( `socket[${ channel }]` );
        this.client = null;
        this.promise = null;
    }

    /**
     * Check whether this socket is connected
     * @return {Promise}
     */
    isConnected () {
        if ( !this.promise ) {
            return Promise.resolve( false );
        }

        return Promise.resolve( this.promise ).then( () => true, () => false );
    }

    /**
     * Initialize socket connection
     * @param {string} sessionId
     * @return {Promise}
     */
    connect ( sessionId ) {
        if ( !this.promise ) {
            this.promise = new Promise( ( resolve, reject ) => {
                this.client = new SockJS( WSRoutesConstants.get( this.channel ) );
                this.client.onmessage = ::this.handleSocketMessage;
                this.client.onopen = () => {
                    this.logger.log( '❯❯︎ connected' );
                    this.send( 'set', 'login', { session: sessionId } )
                        .then( ( response ) => {
                            if ( response.error ) {
                                reject( response.error );
                                this.client.close();
                            } else {
                                resolve( response );
                            }
                        } );
                };
                this.client.onclose = ( event ) => {
                    this.logger.warn( '✘ disconnected' );
                    this.client = null;
                    this.promise = null;
                    this.emit( 'DISCONNECT', event );
                };
            } );
        }
        return this.promise;
    }

    /**
     * Send a message and receive server's response
     * @param {string} action
     * @param {string} channel
     * @param {?object} value
     * @return {Promise}
     */
    send ( action, channel, value = null ) {
        return new Promise( ( resolve ) => {
            const id = ( +new Date() ).toString();

            const request = { id, action, channel };
            if ( value ) {
                request.value = value;
            }

            this.logger.log( `❮❮ ${action}/${channel}@${id}`, value );

            this.client.send( JSON.stringify( request ) );
            this.once( id, resolve );
        } );
    }

    /**
     * Close socket connection
     */
    disconnect () {
        if ( this.client ) {
            this.client.close();
        }
    }

    /**
     * Create an eventChannel for broadcasts
     * @return {Channel}
     */
    subscribe () {
        return eventChannel( ( emit ) => {
            const emitEnd = () => emit( END );

            this.on( 'BROADCAST', emit );
            this.on( 'DISCONNECT', emitEnd );

            return () => {
                this.off( 'BROADCAST', emit );
                this.off( 'DISCONNECT', emitEnd );
            };
        }, buffers.expanding( 5 ) );
    }

    /**
     * @private
     */
    handleSocketMessage ( event ) {
        const response = parseMessage( event.data );
        const { channel, id, error } = response;
        this.logger[error ? 'warn' : 'log']( `❯❯ ${channel}@${id}`, response );

        const eventName = this.listeners( id, true ) ? id : 'BROADCAST';
        this.emit( eventName, response );
    }
}

export default SocketClient;
