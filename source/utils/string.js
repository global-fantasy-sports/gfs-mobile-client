export function splitName ( fullName = '' ) {
    let index = fullName.indexOf( ',' );
    if ( index < 0 ) {
        index = fullName.indexOf( ' ' );
    }
    return {
        firstName: fullName.substring( 0, index ),
        lastName: fullName.substring( index + 1 ),
    };
}
