import moment from 'moment';

import { unixDateToLocal } from './date';

class Timer {

    constructor ( iServerTime, iLocalTime, iEndDate, iCallback, iInterval = 1000 ) {
        this.serverTime = unixDateToLocal( iServerTime );

        this.localTime = moment( iLocalTime );
        this.endDate = unixDateToLocal( iEndDate );

        this.offset = moment( this.localTime ).diff( this.serverTime );
        this.endDateNow = unixDateToLocal( iEndDate ).add( this.offset, 'milliseconds' );

        this.interval = iInterval;
        this.timer = null;
        this.createTimer( this.interval, iCallback );
    }

    formatDigits ( iNumberString ) {
        const isNegative = Math.sign( iNumberString ) === -1;
        let sNumber = String( Math.abs( iNumberString ) );

        while ( sNumber.length < 2 ) {
            sNumber = '0' + sNumber;
        }

        return isNegative ? '- ' + sNumber : sNumber;
    }

    nextTick ( iCallback ) {
        const duration = moment.duration( Math.max( 0, this.endDateNow.diff( new Date() ) ) );
        const date = {
            days: this.formatDigits( duration.days() ),
            hours: this.formatDigits( duration.hours() ),
            minutes: this.formatDigits( duration.minutes() ),
            seconds: this.formatDigits( duration.seconds() ),
        };

        iCallback( date );
    }

    subscribe ( iCallback ) {
        if ( typeof iCallback !== 'function' ) {
            return false;
        }
        this.nextTick = this.nextTick.bind( this, iCallback );
        return this.nextTick;
    }

    destroy () {
        clearInterval( this.timer );
    }

    createTimer ( iInterval, iCallback ) {
        let update = ::this.update;

        if ( typeof iCallback === 'function' ) {
            update = update.bind( this, iCallback );
        }

        this.timer = setInterval( update, iInterval );
    }

    update ( iCallback ) {
        this.nextTick( iCallback );
    }
}

export default Timer;
