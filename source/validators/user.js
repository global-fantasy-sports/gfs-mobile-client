import ErrorConstants from '../constants/errors';

/* eslint-disable */
const emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
/* eslint-enable */

const emailMaxLength = 50;
const passwordMaxLength = 20;
const passwordMinLength = 5;
const nameMinLength = 3;

const emailIsEmpty = iEmail => !iEmail || !iEmail.trim().length;
const emailIsNotCorrect = iEmail => !emailPattern.test( iEmail );
const emailIsVeryLong = iEmail => String( iEmail ).length > emailMaxLength;

const passwordIsEmpty = iPassword => !iPassword || !iPassword.trim().length;
const passwordIsVeryLong = iPassword => String( iPassword ).length > passwordMaxLength;
const passwordIsVeryShort = iPassword => {
    return String( iPassword ).length <= passwordMinLength && String( iPassword ).length > 0;
};

const nameIsEmpty = iName => !iName || !iName.trim().length;
const nameIsVeryShort = iName => {
    return String( iName ).length <= nameMinLength && String( iName ).length > 0;
};

const termsShouldBeChosen = iTerms => !iTerms;

const countryIsEmpty = iCountry => !iCountry;

const stateIsEmpty = iState => !iState;

export const LoginValidator = {
    emailIsEmpty,
    emailIsNotCorrect,
    emailIsVeryLong,
    passwordIsEmpty,
    passwordIsVeryLong,
    passwordIsVeryShort,
};

export const RegistrationValidator = {
    emailIsEmpty,
    emailIsNotCorrect,
    emailIsVeryLong,
    passwordIsEmpty,
    passwordIsVeryLong,
    passwordIsVeryShort,
    nameIsEmpty,
    nameIsVeryShort,
    termsShouldBeChosen,
    countryIsEmpty,
    stateIsEmpty,
};

export const validate = ( iTestValue, iTestRules = [], iValidations = {} ) => {
    let returnMessage;

    iTestRules.some( iFunc => {
        if ( iValidations[ iFunc ]( iTestValue ) ) {
            returnMessage = ErrorConstants.get( iFunc );
            return true;
        }
        return false;
    } );

    return returnMessage;
};

