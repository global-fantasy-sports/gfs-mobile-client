import React, { Component, PropTypes } from 'react';

import ScrollLoader from '../components/scroll-loader';
import ErrorNotification from './components/ErrorNotification';
import HeaderContainer from '../containers/HeaderContainer';
import InfoPopupContainer from './containers/InfoPopupContainer';
import WizardFooterComponent from './components/wizard-footer';
import Animated from './components/wizard-footer/Animated';
import BottomSideMenu from './components/wizard-footer/BottomSideMenu';


class WizardPage extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        bottomSideMenuActive: PropTypes.bool.isRequired,
        bottomSideMenuEnabled: PropTypes.bool.isRequired,
        children: PropTypes.node.isRequired,
        dataLoaded: PropTypes.bool.isRequired,
        error: PropTypes.string,
        isEmptyLineup: PropTypes.bool.isRequired,
        isFullLineup: PropTypes.bool.isRequired,
        isPathActive: PropTypes.func.isRequired,
        league: PropTypes.string.isRequired,
        lineupIsSaved: PropTypes.bool.isRequired,
        location: PropTypes.object.isRequired,
        mode: PropTypes.oneOf( ['create', 'edit'] ).isRequired,
        onAutofill: PropTypes.func.isRequired,
        onClearDraft: PropTypes.func.isRequired,
        onClearError: PropTypes.func.isRequired,
        onGetSaved: PropTypes.func.isRequired,
        onHideBottomSideMenu: PropTypes.func.isRequired,
        onHideInfoPopup: PropTypes.func.isRequired,
        onSaveLineup: PropTypes.func.isRequired,
        onShowBottomSideMenu: PropTypes.func.isRequired,
        onShowInfoPopup: PropTypes.func.isRequired,
        onToggleWizardViewMode: PropTypes.func.isRequired,
        onWizardInit: PropTypes.func.isRequired,
        playersCount: PropTypes.number.isRequired,
        popupActive: PropTypes.bool.isRequired,
        pos: PropTypes.string,
        sport: PropTypes.string.isRequired,
        wizardViewMode: PropTypes.string.isRequired,
    };

    componentDidMount () {
        if ( !this.props.mode ) {
            this.props.onWizardInit();
        }
    }

    render () {
        const {
            _t,
            bottomSideMenuActive,
            bottomSideMenuEnabled,
            isFullLineup,
            isPathActive,
            isEmptyLineup,
            wizardViewMode,
            onToggleWizardViewMode,
            onAutofill,
            onClearDraft,
            onHideBottomSideMenu,
            onShowBottomSideMenu,
            onShowInfoPopup,
            onSaveLineup,
            onHideInfoPopup,
            popupActive,
            playersCount,
            mode,
            league,
            children,
            dataLoaded,
            error,
            onClearError,
            sport,
            onGetSaved,
            lineupIsSaved,
            pos,
            location,
        } = this.props;

        if ( !dataLoaded || !mode ) {
            return <ScrollLoader />;
        }

        return (
            <div>
                <HeaderContainer isFixed />
                { children }
                <InfoPopupContainer />
                <ErrorNotification
                    error={ error }
                    onHide={ onClearError }
                />
                <WizardFooterComponent
                    _t={ _t }
                    bottomSideMenuActive={ bottomSideMenuActive }
                    bottomSideMenuEnabled={ bottomSideMenuEnabled }
                    currentLocation={ location }
                    isPathActive={ isPathActive }
                    league={ league }
                    mode={ mode }
                    onHideBottomSideMenu={ onHideBottomSideMenu }
                    onHideInfoPopup={ onHideInfoPopup }
                    onShowBottomSideMenu={ onShowBottomSideMenu }
                    onShowInfoPopup={ onShowInfoPopup }
                    playersCount={ playersCount }
                    popupActive={ popupActive }
                    sport={ sport }
                />

                {bottomSideMenuEnabled &&
                    <Animated
                        onClose={ onHideBottomSideMenu }
                        visible={ bottomSideMenuActive }
                    >
                        <BottomSideMenu
                            _t={ _t }
                            isEmptyLineup={ isEmptyLineup }
                            isFullLineup={ isFullLineup }
                            lineupIsSaved={ lineupIsSaved }
                            mode={ wizardViewMode }
                            onAutofill={ onAutofill }
                            onClearDraft={ onClearDraft }
                            onGetSaved={ onGetSaved }
                            onHideBottomSideMenu={ onHideBottomSideMenu }
                            onSaveLineup={ onSaveLineup }
                            onShowInfoPopup={ onShowInfoPopup }
                            onToggleWizardViewMode={ onToggleWizardViewMode }
                            pos={ pos }
                        />
                    </Animated>
                }
            </div>
        );
    }
}

export default WizardPage;
