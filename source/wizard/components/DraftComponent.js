import React, { Component, PropTypes } from 'react';

import Page from '../../components/page';
import DraftComponentHeader from './DraftComponentHeader';
import ParticipantsList from './ParticipantsList';
import SearchComponent from './SearchComponent';
import { SORTING_OPTIONS } from '../../constants/wizard';
import { getParticipants } from '../../selectors/wizard';

class DraftComponent extends Component {

    static propTypes = {
        avgFPPG: PropTypes.number.isRequired,
        budgetLeft: PropTypes.number.isRequired,
        budgetMax: PropTypes.number.isRequired,
        favoriteParticipants: PropTypes.array.isRequired,
        lineup: PropTypes.object.isRequired,
        onCompare: PropTypes.func.isRequired,
        onSelectParticipant: PropTypes.func.isRequired,
        onShowAthleteInfo: PropTypes.func.isRequired,
        onUncompare: PropTypes.func.isRequired,
        onUnselectParticipant: PropTypes.func.isRequired,
        params: PropTypes.shape( {
            position: PropTypes.string.isRequired,
        } ).isRequired,
        participants: PropTypes.array.isRequired,
        playersCount: PropTypes.number.isRequired,
        playersMax: PropTypes.number.isRequired,
        selectedIds: PropTypes.object.isRequired,
        selectedTeams: PropTypes.object.isRequired,
        currentSport: PropTypes.string.isRequired,
        route: PropTypes.object.isRequired,
        _t: PropTypes.func.isRequired,
    };

    constructor ( props ) {
        super( props );
        this.state = {
            search: '',
            showFavorite: false,
            sorting: 'salary desc',
        };
    }

    handleCompare = ( { participantId, athleteId, position } ) => {
        this.props.onCompare( { participantId, athleteId, pos: this.props.params.position, position } );
    };

    handleDraftParticipant = ( { participantId } ) => {
        this.props.onSelectParticipant( this.props.params.position, participantId );
    };

    handleUndraftParticipant = ( { participantId } ) => {
        this.props.onUnselectParticipant( participantId );
    };

    handleSelectParticipant = ( { id, participantId, athleteId } ) => {
        const { position } = this.props.params;
        this.props.onShowAthleteInfo( { position, id, participantId, athleteId } );
    };

    handleFilterByFavorites = () => {
        this.setState( { showFavorite: !this.state.showFavorite } );
    };

    handleSearch = ( search ) => {
        this.setState( { search } );
    };

    handleSort = ( sorting ) => {
        this.setState( { sorting } );
    };

    render () {
        const {
            _t,
            avgFPPG,
            budgetLeft,
            budgetMax,
            favoriteParticipants,
            onUncompare,
            params: { position },
            selectedIds,
            selectedTeams,
            currentSport,
        } = this.props;

        const { search, showFavorite, sorting } = this.state;

        return (
            <Page className="wizard">
                <DraftComponentHeader
                    _t={ _t }
                    avgFPPG={ avgFPPG }
                    budgetLeft={ budgetLeft }
                    budgetMax={ budgetMax }
                    pitchActive={ null }
                    position={ position }
                    returnTo="/wizard/select/"
                />
                <SearchComponent
                    _t={ _t }
                    favoritesOnly={ showFavorite }
                    onFilterFavorites={ this.handleFilterByFavorites }
                    onSearch={ this.handleSearch }
                    onSort={ this.handleSort }
                    position={ position }
                    search={ search }
                    sorting={ sorting }
                    sortingOptions={ SORTING_OPTIONS }
                    sport={ currentSport }
                />
                <ParticipantsList
                    _t={ _t }
                    budgetLeft={ budgetLeft }
                    favorites={ favoriteParticipants }
                    favoritesOnly={ showFavorite }
                    onClick={ this.handleSelectParticipant }
                    onCompare={ this.handleCompare }
                    onDraft={ this.handleDraftParticipant }
                    onUncompare={ onUncompare }
                    onUndraft={ this.handleUndraftParticipant }
                    participants={ getParticipants( this.props ) }
                    search={ search }
                    selectedIds={ selectedIds }
                    selectedTeams={ selectedTeams }
                    sorting={ sorting }
                />
            </Page>
        );
    }
}

export default DraftComponent;
