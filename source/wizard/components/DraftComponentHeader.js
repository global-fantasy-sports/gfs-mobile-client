import React, { PropTypes } from 'react';

import LineupInfo from './LineupInfo';
import BtnLink from '../../ui/btn/BtnLink';
import { POSITION_LABELS } from '../../constants/wizard';

import './style.scss';

const noop = () => {};

const DraftComponentHeader = ( props ) => {

    const {
        avgFPPG,
        budgetLeft,
        budgetMax,
        onToggleWizardViewMode,
        position,
        wizardViewMode,
        returnTo,
        _t,
    } = props;
    return (
        <section className="wizard-header-wrap">
            <div className="wizard-header">
                <div className="wizard-header__row">
                    <BtnLink
                        icon="arrow-left"
                        label={ _t( 'wizard.draft.header.btn_back' ) }
                        to={ returnTo }
                        type="transparent"
                    />
                    <h3 className="wizard-header__title">
                        { _t( 'wizard.draft.header.text_select' ) }
                        &nbsp;
                        <span>
                            { _t( `common.position_label.${ POSITION_LABELS[position] }` ) }
                        </span>
                    </h3>
                    <div className="empty-block" />
                </div>
                <LineupInfo
                    _t={ _t }
                    avgFPPG={ avgFPPG }
                    budgetLeft={ budgetLeft }
                    budgetMax={ budgetMax }
                    className="wizard-header__row"
                    onToggleWizardViewMode={ onToggleWizardViewMode || noop }
                    wizardSelectAthleteMode
                    wizardViewMode={ wizardViewMode }
                />
            </div>
        </section>
    );
};

DraftComponentHeader.propTypes = {
    avgFPPG: PropTypes.number.isRequired,
    budgetLeft: PropTypes.number.isRequired,
    budgetMax: PropTypes.number.isRequired,
    onToggleWizardViewMode: PropTypes.func,
    position: PropTypes.string.isRequired,
    returnTo: PropTypes.string.isRequired,
    wizardViewMode: PropTypes.oneOf( ['list', 'pitch'] ),
    _t: PropTypes.func.isRequired,
};

export default DraftComponentHeader;
