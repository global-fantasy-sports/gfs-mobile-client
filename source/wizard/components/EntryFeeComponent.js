import _ from 'lodash';
import React, { Component, PropTypes } from 'react';

import Page from '../../components/page';
import EntryFeeHeader from './EntryFeeHeader';
import CompetitionRoomGroup from '../../components/competition-room-group';
import { COMPETITION_ROOM_TYPE_EXPLANATIONS, COMPETITION_ROOM_GROUPS } from '../../constants/competition';
import OnboardingContainer from '../../containers/OnboardingContainer';
import EntryFeeOnboarding from '../../components/onboarding/EntryFeeOnboarding';

class EntryFeeComponent extends Component {

    static defaultProps = {
        _t: ( s ) => s,
    };

    static propTypes = {
        _t: PropTypes.func.isRequired,
        competitionRooms: PropTypes.array.isRequired,
        created: PropTypes.bool.isRequired,
        currentSport: PropTypes.string.isRequired,
        error: PropTypes.string,
        mode: PropTypes.oneOf( ['create', 'edit'] ).isRequired,
        onCreateGame: PropTypes.func.isRequired,
        onNextStep: PropTypes.func.isRequired,
        onPrevStep: PropTypes.func.isRequired,
        onSelectCompetition: PropTypes.func.isRequired,
        onToggleFilterPanel: PropTypes.func.isRequired,
        requesting: PropTypes.bool.isRequired,
        season: PropTypes.object.isRequired,
        selectedRooms: PropTypes.arrayOf( PropTypes.number ).isRequired,
        totalsByCurrency: PropTypes.objectOf( PropTypes.number ).isRequired,
    };

    constructor ( props ) {
        super( props );
        this.state = {
            showSelectedRooms: 'all',
        };
    }

    componentDidMount () {
        document.body.scrollTop = 0;
        this.maybeRedirect();
    }

    componentDidUpdate () {
        this.maybeRedirect();
    }

    maybeRedirect () {
        if ( this.props.created ) {
            this.props.onNextStep();
        } else if ( this.props.mode !== 'create' ) {
            this.props.onPrevStep();
        }
    }

    handleSelectedRooms = ( event ) => {
        this.setState( { showSelectedRooms: event.target.value } );
    }

    filterRooms ( type, selectedOnly = false ) {
        const { selectedRooms, competitionRooms } = this.props;
        let result = _.filter( competitionRooms, { type } );
        return selectedOnly === 'selected'
            ? result.filter( ( room ) => selectedRooms.includes( room.id ) )
            : result;
    }
    findRoomForOnboarding () {
        const { competitionRooms } = this.props;
        return _.find( competitionRooms, { pot: 300, currency: 'usd' } ) || _.first( competitionRooms );
    }

    render () {
        const {
            _t,
            currentSport,
            error,
            onCreateGame,
            onSelectCompetition,
            onToggleFilterPanel,
            requesting,
            season,
            selectedRooms,
            totalsByCurrency,
        } = this.props;
        const { showSelectedRooms } = this.state;
        const onboardingRoom = this.findRoomForOnboarding();

        return (
            <Page className="wizard">
                <EntryFeeHeader
                    _t={ _t }
                    error={ error }
                    onCreateGame={ onCreateGame }
                    onToggleFilterPanel={ onToggleFilterPanel }
                    onToggleRooms={ this.handleSelectedRooms }
                    requesting={ requesting }
                    selectedRooms={ selectedRooms }
                    showSelectedRooms={ this.state.showSelectedRooms }
                    sport={ currentSport }
                    totalsByCurrency={ totalsByCurrency }
                />
                <section>
                    { Object.keys( COMPETITION_ROOM_GROUPS ).map( ( group ) => {
                        const explanation = COMPETITION_ROOM_TYPE_EXPLANATIONS[group];
                        return (
                            <CompetitionRoomGroup
                                _t={ _t }
                                explanation={ explanation }
                                key={ group }
                                onRoomClick={ onSelectCompetition }
                                page="wizard"
                                rooms={ this.filterRooms( group, showSelectedRooms ) }
                                season={ season }
                                selected={ selectedRooms }
                                type={ group }
                            />
                        );
                    } ) }
                    <OnboardingContainer label={ _t( 'onboarding.btn_got_it' ) } name="entryFee">
                        <EntryFeeOnboarding _t={ _t } onboardingRoom={ onboardingRoom } />
                    </OnboardingContainer>
                </section>
            </Page>
        );
    }
}

export default EntryFeeComponent;
