import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';

import Btn from '../../ui/btn/Btn';
import BtnLink from '../../ui/btn/BtnLink';
import Money from '../../components/common/Money';
import './style.scss';
import RoutesConstants from '../../constants/routes';


class EntryFeeHeader extends Component {

    static propTypes = {
        error: PropTypes.string,
        onToggleRooms: PropTypes.func.isRequired,
        onCreateGame: PropTypes.func.isRequired,
        onToggleFilterPanel: PropTypes.func.isRequired,
        requesting: PropTypes.bool.isRequired,
        selectedRooms: PropTypes.arrayOf( PropTypes.number ).isRequired,
        showSelectedRooms: PropTypes.string,
        sport: PropTypes.string.isRequired,
        totalsByCurrency: PropTypes.objectOf( PropTypes.number ).isRequired,
        _t: PropTypes.func.isRequired,
    };

    handleFilterToggleButton = () => {
        this.props.onToggleFilterPanel( this.props.sport );
    };

    render () {
        const {
            error,
            onCreateGame,
            onToggleRooms,
            requesting,
            selectedRooms,
            showSelectedRooms,
            totalsByCurrency,
            _t,
        } = this.props;
        const buttonAllClass = classnames( 'btn btn-blue-filter', {
            active: showSelectedRooms === 'all',
        } );
        const buttonSelectedClass = classnames( 'btn btn-blue-filter', {
            active: showSelectedRooms === 'selected',
        } );
        const totals = Object.entries( totalsByCurrency )
            .map( ( [currency, amount], index ) =>
                <Money
                    currency={ currency }
                    key={ index }
                    value={ amount }
                />
            );

        return (
            <section className="wizard-header-wrap entryfee">
                <div className="wizard-header entryfee">
                    <div className="wizard-header__row">
                        <BtnLink
                            icon="arrow-left"
                            label={ _t( 'wizard.entree_fee.header.btn_back' ) }
                            relativeToLeague
                            to={ RoutesConstants.get( 'wizardPage' ) }
                            type="transparent"
                        />
                        <h3 className="wizard-header__title">
                            { _t( 'wizard.entree_fee.header.entry' ) }
                            &nbsp;
                            { _t( 'wizard.entree_fee.header.fee' ) }
                        </h3>
                        <Btn
                            className="wizard-create-game"
                            disabled={ !selectedRooms.length || requesting || error }
                            onClick={ onCreateGame }
                            type="primary"
                        >
                            <span>{_t( 'wizard.entree_fee.header.btn_pay' ) } {totals}</span>
                        </Btn>
                    </div>
                    <div className="wizard-header__row entryfee-step">
                        <div className="wizard-header__selected-rooms-filter">
                            <button
                                className={ buttonAllClass }
                                onClick={ onToggleRooms }
                                value="all"
                            >
                                { _t( 'wizard.entree_fee.header.btn_all' ) }
                            </button>
                            <button
                                className={ buttonSelectedClass }
                                onClick={ onToggleRooms }
                                value="selected"
                            >
                                { _t( 'wizard.entree_fee.header.btn_selected' ) }:&nbsp;
                                { selectedRooms.length || '0' }
                            </button>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}


export default EntryFeeHeader;
