import React, { Component, PropTypes } from 'react';

class ErrorNotification extends Component {

    static propTypes = {
        error: PropTypes.string,
        onHide: PropTypes.func.isRequired,
    };

    render () {
        const { error, onHide } = this.props;
        if ( !error ) {
            return null;
        }

        return (
            <section className="wizard-error" onClick={ onHide }>
                <article>{ error }</article>
            </section>
        );
    }
}

export default ErrorNotification;
