import React, { Component, PropTypes } from 'react';

import Btn from '../../ui/btn/Btn';
import Page from '../../components/page';
import FriendsChallengeHeader from './FriendsChallengeHeader';
import FriendsList from './friends-list/FriendsList';


const LoginPanel = ( { _t, onLogin } ) => (
    <div className="wizard-centered">
        <div className="login-panel">
            <p>{ _t( 'wizard.friends.login.alone' ) }</p>
            <p>{ _t( 'wizard.friends.login.please_login' ) }</p>
            <Btn
                label={ _t( 'wizard.friends.login.btn_login' ) }
                onClick={ onLogin }
                size="monster"
                type="primary"
            />
        </div>
    </div>
);
LoginPanel.propTypes = {
    onLogin: PropTypes.func.isRequired,
    _t: PropTypes.func.isRequired,
};


const SearchPanel = ( { _t, fbSearch, handleSearch, handleSort } ) => (
    <div className="wizard-search-wrap">
        <div className="wizard-search">
            <div className="wizard-search-field">
                <div className="search-input-wrap">
                    <input
                        className="search-input"
                        onChange={ handleSearch }
                        placeholder={ _t( 'wizard.friends.search' ) }
                        type="text"
                        value={ fbSearch }
                    />
                </div>
                <div className="wizard-sort">
                    <a className="sort" href="#" onClick={ handleSort }>
                        { _t( 'wizard.friends.sort' ) }
                    </a>
                </div>
            </div>
        </div>
    </div>
);
SearchPanel.propTypes = {
    _t: PropTypes.func,
    fbSearch: PropTypes.string,
    handleSearch: PropTypes.func.isRequired,
    handleSort: PropTypes.func.isRequired,
};


class FriendsChallengeComponent extends Component {

    static propTypes = {
        fbSearch: PropTypes.string.isRequired,
        fbStatus: PropTypes.string,
        fbFriends: PropTypes.array.isRequired,
        onFacebookLogin: PropTypes.func.isRequired,
        onFacebookInvite: PropTypes.func.isRequired,
        onFacebookSearch: PropTypes.func.isRequired,
        onFinish: PropTypes.func.isRequired,
        _t: PropTypes.func.isRequired,
    };

    componentDidMount () {
        document.body.scrollTop = 0;
    }

    handleSort = ( event ) => {
        event.preventDefault();
        throw new Error( 'Not implemented' );
    }

    handleLogin = ( event ) => {
        event.preventDefault();
        this.props.onFacebookLogin();
    }

    handleSearch = ( event ) => {
        this.props.onFacebookSearch( event.target.value );
    }

    render () {
        const { _t, fbSearch, fbStatus, fbFriends, onFacebookInvite, onFinish } = this.props;
        return (
            <Page className="wizard">
                <FriendsChallengeHeader
                    _t={ _t }
                    onFinish={ onFinish }
                />
                { fbStatus === 'connected' ?
                    <SearchPanel
                        _t={ _t }
                        fbSearch={ fbSearch }
                        handleSearch={ this.handleSearch }
                        handleSort={ this.handleSort }
                    />
                    : <div />
                }
                { fbStatus === 'connected'
                    ? <FriendsList _t={ _t } friends={ fbFriends } onInvite={ onFacebookInvite } />
                    : <LoginPanel _t={ _t } onLogin={ this.handleLogin } />
                }
            </Page>
        );
    }
}

export default FriendsChallengeComponent;
