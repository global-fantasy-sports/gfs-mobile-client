import React, { PropTypes } from 'react';

import './style.scss';
import Btn from '../../ui/btn/Btn';

function FriendsChallengeHeader ( { _t, onFinish } ) {

    return (
        <section className="wizard-header-wrap friends">
            <section className="wizard-header">
                <div className="wizard-header__row">
                    <span className="wizard-header__spacer" />
                    <h3 className="wizard-header__title">
                        { _t( 'wizard.friends.header.challenge' ) }<br />
                        { _t( 'wizard.friends.header.friends' ) }
                    </h3>
                    <Btn
                        label={ _t( 'wizard.friends.header.btn_finish' ) }
                        onClick={ onFinish }
                        type="payment"
                    />
                </div>
            </section>
        </section>
    );
}

FriendsChallengeHeader.propTypes = {
    _t: PropTypes.func.isRequired,
    onFinish: PropTypes.func.isRequired,
};

export default FriendsChallengeHeader;
