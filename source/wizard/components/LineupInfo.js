import React, { PropTypes } from 'react';
import { format } from '../../utils/number';
import Money from '../../components/common/Money';

function LineupInfo ( { _t, avgFPPG = 0.0, budgetLeft, budgetMax, className, wizardSelectAthleteMode } ) {
    const overflow = budgetLeft < 0;
    const progressWidth = overflow ? 100 : budgetLeft / budgetMax * 100;

    return (
        <div className={ className }>
            <div className={ 'budget-display ' + ( overflow ? 'overflow' : '' ) }>
                <span className="budget-display__label">
                    { _t( 'wizard.select.lineup_info.text_budget' ) }
                </span>
                <div className="budget-display__progress">
                    <div
                        className="budget-display__progress-value"
                        style={ { width: `${progressWidth}%` } }
                    />
                </div>
                <span className="budget-display__value">
                    <Money currency="usd" value={ budgetLeft } />
                </span>
            </div>
            { wizardSelectAthleteMode ? null :
                <div className="info-section">
                    <div className="label">
                        { _t( 'wizard.select.lineup_info.fppg_avg' ) }
                    </div>
                    <div className="value">{ format( avgFPPG, 2, 'N/A' ) }</div>
                </div>
            }
        </div>
    );
}

LineupInfo.propTypes = {
    _t: PropTypes.func,
    avgFPPG: PropTypes.number.isRequired,
    budgetLeft: PropTypes.number.isRequired,
    budgetMax: PropTypes.number.isRequired,
    className: PropTypes.string.isRequired,
    wizardSelectAthleteMode: PropTypes.bool,
};

export default LineupInfo;
