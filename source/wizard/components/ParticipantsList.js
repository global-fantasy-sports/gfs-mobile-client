import _ from 'lodash';
import React, { Component, PropTypes } from 'react';
import { createSelector } from 'reselect';

import ParticipantsListEntry from './ParticipantsListEntry';
import OnboardingContainer from '../../containers/OnboardingContainer';
import DraftStepOnboarding from '../../components/onboarding/DraftStepOnboarding';
import { WithoutFavoritesOnboarding } from '../../components/onboarding/WithoutFavoritesOnboarding';

import './style.scss';

const getParticipants = createSelector(
    [
        ( props ) => props._t,
        ( props ) => props.favoritesOnly ? props.favorites : props.participants,
        ( props ) => props.search ? _.words( props.search ) : false,
        ( props ) => props.sorting ? props.sorting.split( ' ' ) : ['nameKnown', 'asc'],
    ],
    ( _t, participants, search, [sortField, sortDir] ) => {
        if ( search ) {
            participants = participants.filter( ( p ) =>
                _.words( search )
                 .every( ( term ) => _t( p.nameKnown ).toUpperCase().indexOf( term.toUpperCase() ) !== -1 ),
            );
        }
        if ( sortField === 'name' ) {
            sortField = 'nameKnown';
        }
        return _.orderBy( participants, [sortField, 'nameKnown'], [sortDir] );
    },
);

class ParticipantsList extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        budgetLeft: PropTypes.number.isRequired,
        comparison: PropTypes.object,
        favorites: PropTypes.array.isRequired,
        favoritesOnly: PropTypes.bool.isRequired,
        onClick: PropTypes.func.isRequired,
        onCompare: PropTypes.func.isRequired,
        onDraft: PropTypes.func.isRequired,
        onUncompare: PropTypes.func.isRequired,
        onUndraft: PropTypes.func.isRequired,
        participants: PropTypes.arrayOf( PropTypes.object ).isRequired,
        search: PropTypes.string,
        selectedIds: PropTypes.object,
        selectedTeams: PropTypes.object,
        sorting: PropTypes.string.isRequired,
    };

    constructor ( props ) {
        super( props );
        this.state = {
            activeId: null,
        };
    }

    handleActiveChanged = ( activeId ) => {
        this.setState( { activeId } );
    };

    handleDraft = ( ...args ) => {
        this.setState( { activeId: null } );
        this.props.onDraft( ...args );
    };

    handleUndraft = ( ...args ) => {
        this.setState( { activeId: null } );
        this.props.onUndraft( ...args );
    };

    handleCompare = ( ...args ) => {
        this.setState( { activeId: null } );
        this.props.onCompare( ...args );
    };

    handleUncompare = ( ...args ) => {
        this.setState( { activeId: null } );
        this.props.onUncompare( ...args );
    };

    render () {
        const {
            _t,
            budgetLeft,
            comparison,
            favorites,
            favoritesOnly,
            onClick,
            selectedIds,
            selectedTeams,
        } = this.props;
        const { activeId } = this.state;

        const comparedIds = _.at( comparison, [
            'left.athleteId',
            'right.athleteId',
        ] ).filter( Boolean );

        const disabled = comparedIds.length === 2;
        const participants = getParticipants( this.props );
        const firstParticipant = _.first( participants );
        return (
            <section>
                <ul className="wizard-list">
                    { favoritesOnly && favorites.length === 0 ?
                        <WithoutFavoritesOnboarding _t={ _t } />
                        : null
                    }
                    { participants.map( ( data, index ) => {
                        const compared = comparedIds.includes( data.athleteId );
                        const selected = selectedIds.has( data.athleteId ) ||
                            selectedIds.has( data.currentParticipantId );
                        return (
                            <li className="wizard-list__item" key={ index }>
                                <ParticipantsListEntry
                                    { ...data }
                                    _t={ _t }
                                    active={ data.id === activeId }
                                    budgetLeft={ budgetLeft }
                                    compared={ compared }
                                    comparisonPage={ !!comparison }
                                    count={ selectedTeams[data.teamId] || 0 }
                                    disabled={ disabled }
                                    onActiveChanged={ this.handleActiveChanged }
                                    onClick={ onClick }
                                    onCompare={ this.handleCompare }
                                    onDraft={ this.handleDraft }
                                    onUncompare={ this.handleUncompare }
                                    onUndraft={ this.handleUndraft }
                                    participantId={ data.currentParticipantId || data.participantId }
                                    selected={ selected }
                                />
                            </li>
                        );
                    } ) }
                </ul>
                <OnboardingContainer label={ _t( 'onboarding.btn_got_it' ) } name="select" >
                    <DraftStepOnboarding
                        _t={ _t }
                        count={ ( firstParticipant && selectedTeams[firstParticipant.teamId] )
                            ? selectedTeams[firstParticipant.teamId]
                            : 0
                            }
                        participant={ firstParticipant }
                    />
                </OnboardingContainer>
            </section>
        );
    }
}

export default ParticipantsList;
