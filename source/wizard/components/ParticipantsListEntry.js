import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';
import { format } from '../../utils/number';
import Money from '../../components/common/Money';
import Icon from '../../ui/icon/Icon';
import Swipeable from '../../components/swipeable';
import SwipeBar from '../../ui/swipe-bar/SwipeBar';


const TeamInfo = ( { _t, count, match, teamId } ) => {
    if ( !match ) {
        return (
            <div className="team">
                { count > 0 && <p className="team-quantity">{ count }/4</p> }
                <p className="team-name">{ _t( teamId ) }</p>
            </div>
        );
    }

    const [team, oppTeam] = match.homeTeam === teamId ?
        [match.homeTeam, match.awayTeam] : [match.awayTeam, match.homeTeam];

    return (
        <div className="team">
            <div className="team-names">
                { count > 0 && <div className="team-quantity">{ count }/4</div> }
                <div className="team-names-top">
                    <p>{ _t( team ) }</p>
                </div>
                <p className="team-names-bottom">vs { _t( oppTeam ) }</p>
            </div>
        </div>
    );
};
TeamInfo.propTypes = {
    _t: PropTypes.func.isRequired,
    count: PropTypes.number.isRequired,
    match: PropTypes.object,
    teamId: PropTypes.string.isRequired,
};


const ParticipantRow = (
    {
        FPPG,
        _t,
        count,
        isFavorite,
        match,
        moneyClass,
        nameKnown,
        onClick,
        oprk,
        salary,
        teamId,
        wizardEntryClass,
    }
) => (
    <div className={ wizardEntryClass } onTouchTap={ onClick }>
        <div className="heading">
            { isFavorite ? <i className="favorite" /> : '' }
            <div className="name">
                <p>{ _t( nameKnown ) }</p>
            </div>
            <TeamInfo
                _t={ _t }
                count={ count }
                match={ match }
                teamId={ teamId }
            />
        </div>
        <div className="column oprk">
            <div className="label">{ _t( 'wizard.draft.list_entry.oprk' ) }</div>
            <div className="value">{ oprk }</div>
        </div>
        <div className="column fppg">
            <div className="label">{ _t( 'wizard.draft.list_entry.fppg' ) }</div>
            <div className="value">{ format( FPPG, 2, 'N/A' ) }</div>
        </div>
        <div className={ moneyClass }>
            <div className="label">{ _t( 'wizard.draft.list_entry.salary' ) }</div>
            <div className="value">
                <Money currency="usd" value={ salary } />
            </div>
        </div>
    </div>
);
ParticipantRow.propTypes = {
    FPPG: PropTypes.number.isRequired,
    _t: PropTypes.func.isRequired,
    count: PropTypes.number.isRequired,
    isFavorite: PropTypes.bool,
    match: PropTypes.object,
    moneyClass: PropTypes.string.isRequired,
    nameKnown: PropTypes.string,
    onClick: PropTypes.func,
    oprk: PropTypes.number.isRequired,
    salary: PropTypes.number.isRequired,
    teamId: PropTypes.string.isRequired,
    wizardEntryClass: PropTypes.string.isRequired,
};

class ParticipantsListEntry extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        active: PropTypes.bool.isRequired,
        athleteId: PropTypes.any.isRequired,
        budgetLeft: PropTypes.number.isRequired,
        compared: PropTypes.bool.isRequired,
        count: PropTypes.number.isRequired,
        disabled: PropTypes.bool,
        isFavorite: PropTypes.bool,
        FPPG: PropTypes.number.isRequired,
        id: PropTypes.any.isRequired,
        match: PropTypes.object,
        nameFirst: PropTypes.string,
        nameKnown: PropTypes.string,
        nameLast: PropTypes.string,
        onActiveChanged: PropTypes.func.isRequired,
        onClick: PropTypes.func.isRequired,
        onCompare: PropTypes.func.isRequired,
        onDraft: PropTypes.func.isRequired,
        onUncompare: PropTypes.func.isRequired,
        onUndraft: PropTypes.func.isRequired,
        oprk: PropTypes.number.isRequired,
        participantId: PropTypes.string.isRequired,
        position: PropTypes.string.isRequired,
        salary: PropTypes.number.isRequired,
        selected: PropTypes.bool.isRequired,
        teamId: PropTypes.string.isRequired,
        comparisonPage: PropTypes.bool,
    };

    handleClick = ( event ) => {
        event.preventDefault();

        const { id, athleteId, participantId } = this.props;
        this.props.onClick( { id, athleteId, participantId } );
    };

    handleAction = () => {
        const { athleteId, participantId, selected } = this.props;
        if ( selected ) {
            this.props.onUndraft( { athleteId, participantId } );
        } else {
            this.props.onDraft( { athleteId, participantId } );
        }
    };

    handleCompare = () => {
        const { athleteId, participantId, position, compared } = this.props;
        if ( !compared ) {
            this.props.onCompare( { athleteId, participantId, position } );
        } else {
            window.scrollTo( 0, 0 );
        }
    };

    handleSwipeStart = () => {
        this.props.onActiveChanged( null );
    };

    handleSwipeEnd = ( isOpen ) => {
        this.props.onActiveChanged( isOpen ? this.props.id : null );
    };

    render () {
        const {
            _t,
            active,
            budgetLeft,
            count,
            isFavorite,
            FPPG,
            nameFirst,
            nameKnown,
            nameLast,
            oprk,
            salary,
            selected,
            match,
            teamId,
        } = this.props;

        const disabled = count >= 4 && !selected;

        const wizardEntryClass = classnames( 'wizard-entry', {
            'selected-lineup-item': selected,
            'disabled-lineup-item': count >= 4,
        } );

        const moneyClass = classnames( 'column', {
            red: salary > budgetLeft,
        } );

        const draftCs = classnames( 'draft-button', {
            disabled,
            undraft: selected,
        } );

        return (
            <Swipeable
                action={
                    <SwipeBar>
                        <div
                            className="compare-button"
                            onClick={ this.handleCompare }
                        >
                            <Icon icon="compare" mod="draft" />
                            { _t( 'wizard.draft.list_entry.btn_compare' ) }
                        </div>
                        <div
                            className={ draftCs }
                            onClick={ !disabled ? this.handleAction : () => {} }
                        >
                            <Icon icon={ selected ? 'minus-circle' : 'plus-circle' } mod="draft" />
                            { selected
                                ? _t( 'wizard.draft.list_entry.btn_undraft' )
                                : _t( 'wizard.draft.list_entry.btn_draft' )
                            }
                        </div>
                    </SwipeBar>
                }
                containerClassName="wizard-entry-wrapper"
                disabled={ this.props.comparisonPage }
                height={ 64 }
                onSwipeEnd={ this.handleSwipeEnd }
                onSwipeStart={ this.handleSwipeStart }
                onTouchTap={ this.props.comparisonPage ? this.handleCompare : this.handleClick }
                open={ active }
            >
                <ParticipantRow
                    FPPG={ FPPG }
                    _t={ _t }
                    count={ count }
                    isFavorite={ isFavorite }
                    match={ match }
                    moneyClass={ moneyClass }
                    nameFirst={ nameFirst }
                    nameKnown={ nameKnown }
                    nameLast={ nameLast }
                    oprk={ oprk }
                    salary={ salary }
                    teamId={ teamId }
                    wizardEntryClass={ wizardEntryClass }
                />
            </Swipeable>
        );
    }
}

export default ParticipantsListEntry;
