import cx from 'classnames';
import React, { Component, PropTypes } from 'react';

import Btn from '../../ui/btn/Btn';
import ReactSlider from 'react-slider';
import BtnGroup from '../../ui/btn/BtnGroup';
import './style.scss';


function getCurrencyLabel ( currencyCode, _t ) {
    switch ( currencyCode ) {
        case 'usd': return _t( 'common.competition_type.DOLLARS' );
        case 'token': return _t( 'common.competition_type.TOKENS' );
        default: return _t( `common.competition_type.${currencyCode}` );
    }
}

class CurrencyFilter extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        currencies: PropTypes.array.isRequired,
        onClick: PropTypes.func.isRequired,
        selectedCurrency: PropTypes.any,
    };

    render () {
        const { _t, currencies, onClick, selectedCurrency } = this.props;
        if ( !currencies || currencies.length < 2 ) {
            return null;
        }
        return (
            <div className="payment-type">
                <BtnGroup mod="space-between">
                    <Btn
                        className={ cx( 'noPad', { active: selectedCurrency } ) }
                        onClick={ onClick }
                        value={ { currency: null } }
                    >
                        <div className={ cx( 'button', { inactive: selectedCurrency } ) }>
                            { _t( 'common.competition_type.all' ) }
                        </div>
                    </Btn>
                    { currencies.map( ( currency ) =>
                        <Btn
                            className={ cx( 'noPad', currency, { active: selectedCurrency === currency } ) }
                            key={ currency }
                            onClick={ onClick }
                            value={ { currency } }
                        >
                            <div className={ cx( 'button', { inactive: currency !== selectedCurrency } ) }>
                                { getCurrencyLabel( currency, _t ) }
                            </div>
                        </Btn>
                    )}
                </BtnGroup>
            </div>
        );
    }
}


class RoomsFilter extends Component {

    static propTypes = {
        _t: React.PropTypes.func.isRequired,
        currencies: React.PropTypes.array.isRequired,
        entryFeeRange: React.PropTypes.object.isRequired,
        filters: React.PropTypes.object.isRequired,
        isOpen: PropTypes.bool.isRequired,
        league: React.PropTypes.string.isRequired,
        onEntryFeeChanged: React.PropTypes.func.isRequired,
        onFilterChanged: React.PropTypes.func.isRequired,
        onFilterReset: React.PropTypes.func.isRequired,
        sport: React.PropTypes.string.isRequired,
        total: React.PropTypes.number.isRequired,
    };

    values = [];

    handleFilter = ( value ) => {
        this.props.onFilterChanged( value );
    };

    handleEntryFeeRangeChange = ( values ) => {
        this.props.onEntryFeeChanged( values );
    };

    handleEntryFeeSliderChange = ( values ) => {
        const min = this.props.entryFeeRange.steps[values[0]];
        const max = this.props.entryFeeRange.steps[values[1]];
        this.values = [values[0], values[1]];
        this.props.onEntryFeeChanged( { min, max } );
    };

    handleFilterReset = () => {
        this.values = [0, this.props.entryFeeRange.steps.length - 1];
        this.props.onFilterReset();
    };

    render () {
        const { _t, filters, isOpen, currencies, entryFeeRange, total } = this.props;
        const { steps, values } = entryFeeRange;

        if ( !isOpen ) {
            return ( null );
        }

        return (
            <div className="pane-tumblers">
                <div>
                    <div className="fee">
                        <div className="fee__label">{ _t( 'common.entry_fee' ) }</div>

                        <div className="fee__min-max">
                            <div className="min">
                                <div className="value">{ values.min || 0 }</div>
                                <div>{ _t( 'min' ) }</div>
                            </div>
                            <div className="max">
                                <div className="value">{ values.max || 0 }</div>
                                <div>{ _t( 'max' ) }</div>
                            </div>
                        </div>

                        <ReactSlider
                            defaultValue={ [0, steps.length - 1] }
                            max={ steps.length - 1 }
                            min={ 0 }
                            minDistance={ 0 }
                            onChange={ this.handleEntryFeeSliderChange }
                            pearling
                            step={ 1 }
                            value={ this.values }
                            withBars
                        />
                    </div>
                    <CurrencyFilter
                        _t={ _t }
                        currencies={ currencies }
                        onClick={ this.handleFilter }
                        selectedCurrency={ filters.currency }
                    />
                    <div className="rooms-filters-wrapper">
                        <div className="rfw__col">
                            <div className="rfw__col-title">
                                { _t( 'lobby.filters.title.casual_rooms' ) }
                            </div>
                            <div className="rfw__row">
                                <BtnGroup mod="double">
                                    <Btn
                                        className="noPad right-margin"
                                        onClick={ this.handleFilter.bind( this, 'head2head' ) }
                                    >
                                        <div className={ 'button' + ( !filters.head2head ? ' inactive' : '' ) }>
                                            { _t( 'common.competition_type.Head 2 head' ) }
                                        </div>
                                    </Btn>
                                    <Btn
                                        className="noPad"
                                        inactive={ !filters.doubleUp }
                                        onClick={ this.handleFilter.bind( this, 'doubleUp' ) }
                                    >
                                        <div className={ 'button' + ( !filters.doubleUp ? ' inactive' : '' ) }>
                                            { _t( 'common.competition_type.Double Up' ) }
                                        </div>
                                    </Btn>
                                </BtnGroup>
                            </div>
                            <div className="rfw__row">
                                <BtnGroup mod="double">
                                    <Btn
                                        className="noPad right-margin"
                                        onClick={ this.handleFilter.bind( this, 'fifty2fifty' ) }
                                    >
                                        <div className={ 'button' + ( !filters.fifty2fifty ? ' inactive' : '' ) }>
                                            { _t( 'common.competition_type.50/50' ) }
                                        </div>
                                    </Btn>

                                    <Btn
                                        className="noPad"
                                        onClick={ this.handleFilter.bind( this, 'top20' ) }
                                    >
                                        <div className={ 'button' + ( !filters.top20 ? ' inactive' : '' ) }>
                                            { _t( 'common.competition_type.Top 20%' ) }
                                        </div>
                                    </Btn>
                                </BtnGroup>
                            </div>
                        </div>
                        <div className="rfw__col rfw__col--offset">
                            <div className="rfw__col-title">
                                { _t( 'lobby.filters.title.pro_rooms' ) }
                            </div>
                            <Btn
                                className="noPad"
                                onClick={ this.handleFilter.bind( this, 'top15' ) }
                            >
                                <div className={ 'button pro' + ( !filters.top15 ? ' inactive' : '' ) }>
                                    { _t( 'common.competition_type.Top 15%' ) }
                                </div>
                            </Btn>
                            <div className="rfw__space" />
                            <Btn
                                className="noPad"
                                onClick={ this.handleFilter.bind( this, 'quadruple' ) }
                            >
                                <div className={ 'button pro' + ( !filters.quadruple ? ' inactive' : '' ) }>
                                    { _t( 'common.competition_type.Quadruple' ) }
                                </div>
                            </Btn>
                        </div>
                    </div>
                    <div className="free-roll-filter">
                        <Btn
                            block
                            className="noPad"
                            onClick={ this.handleFilter.bind( this, 'freeRoll' ) }
                        >
                            <div className={ 'button pro' + ( !filters.freeRoll ? ' inactive' : '' ) }>
                                { _t( 'common.competition_type.Free-roll' ) }
                            </div>
                        </Btn>
                    </div>
                </div>

                { total === 0 ?
                    <div className="emptyrooms">
                        <div className="caption">{ _t( 'lobby.filters.msgs.empty' ) }</div>
                        <Btn
                            label={ _t( 'lobby.filters.button.reset' ) }
                            onClick={ this.handleFilterReset }
                            size="normal"
                            type="default"
                        />
                    </div>
                    : null
                }
            </div>
        );
    }
}

export default RoomsFilter;
