import React, { Component, PropTypes } from 'react';

import Btn from '../../ui/btn/Btn';
import Select from '../../ui/select';

import './style.scss';


class SearchComponent extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        favoritesOnly: PropTypes.bool.isRequired,
        onFilterFavorites: PropTypes.func.isRequired,
        onSearch: PropTypes.func.isRequired,
        onSort: PropTypes.func.isRequired,
        position: PropTypes.string.isRequired,
        search: PropTypes.string.isRequired,
        sorting: PropTypes.string.isRequired,
        sortingOptions: PropTypes.arrayOf( PropTypes.arrayOf( PropTypes.string ) ).isRequired,
        sport: PropTypes.string,
    };

    handleFavorites = () => {
        this.props.onFilterFavorites( !this.props.favoritesOnly );
    };

    handleSearch = ( event ) => {
        this.props.onSearch( event.target.value );
    };

    handleSort = ( value ) => {
        this.props.onSort( value );
    };

    formatSortOption = ( value ) => {
        const option = this.props.sortingOptions.find( ( entry ) => entry[0] === value );
        return option ? option[1] : value;
    };

    renderAllPositionsBtn () {
        const { _t, position, sport } = this.props;
        if ( sport === 'nfl' && position === 'flex' ) {
            return (
                <button className="btn btn-mariner btn-small btn-small-two-blocks">
                    <p>
                        { _t( 'wizard.select.search.text_all' ) }
                    </p>
                    <p>
                        { _t( 'wizard.select.search.text_positions' ) }
                    </p>
                </button>
            );
        }
        return null;
    }

    render () {
        const { _t, favoritesOnly, search, sorting, sortingOptions } = this.props;
        return (
            <div className="wizard-search-wrap">
                <div className="wizard-search">
                    { this.renderAllPositionsBtn() }
                    <div className="wizard-search-field">
                        <Btn
                            icon={ favoritesOnly ? 'favorite-active' : 'favorite' }
                            onClick={ this.handleFavorites }
                            square
                            type="white"
                        />
                        <div className="search-input-wrap">
                            <input
                                className="search-input"
                                onChange={ this.handleSearch }
                                placeholder={ _t( 'wizard.select.search.text_search' ) }
                                type="text"
                                value={ search }
                            />
                        </div>
                        <Select
                            label={ _t( 'wizard.select.search.sort' ) }
                            onChange={ this.handleSort }
                            optionFormatter={ _t }
                            options={ sortingOptions.map( ( item ) => item[0] ) }
                            value={ sorting }
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default SearchComponent;
