import React, { Component, PropTypes } from 'react';
import Page from '../../components/page';
import SelectStepHeader from './SelectStepHeader';
import PitchView from './pitch/PitchView';
import LineupList from '../../components/lineup-list';
import OnboardingContainer from '../../containers/OnboardingContainer';

import DraftOnboarding from '../../components/onboarding/DraftOnboarding';
import './style.scss';


class SelectStepComponent extends Component {

    static defaultProps = {
        _t: ( s ) => s,
    };

    static propTypes = {
        _t: PropTypes.func.isRequired,
        avgFPPG: PropTypes.number.isRequired,
        budgetMax: PropTypes.number.isRequired,
        error: PropTypes.string,
        lineup: PropTypes.object.isRequired,
        lineupPositions: PropTypes.array.isRequired,
        lineupValid: PropTypes.bool.isRequired,
        mode: PropTypes.oneOf( ['create', 'edit'] ).isRequired,
        onCompare: PropTypes.func.isRequired,
        onPositionClear: PropTypes.func.isRequired,
        onPositionSelect: PropTypes.func.isRequired,
        onShowAthleteInfo: PropTypes.func.isRequired,
        onToggleWizardViewMode: PropTypes.func.isRequired,
        onUncompare: PropTypes.func.isRequired,
        playersCount: PropTypes.number.isRequired,
        playersMax: PropTypes.number.isRequired,
        selectedIds: PropTypes.object.isRequired,
        wizardViewMode: PropTypes.oneOf( ['list', 'pitch'] ).isRequired,
    };

    handleShowAthleteInfo = ( position ) => {
        const { id, participantId, athleteId } = this.props.lineup[position];
        this.props.onShowAthleteInfo( { position, id, participantId, athleteId } );
    };

    render () {
        const {
            _t,
            lineup,
            lineupPositions,
            lineupValid,
            onPositionClear,
            onPositionSelect,
            onToggleWizardViewMode,
            selectedIds,
            wizardViewMode,
        } = this.props;

        return (
            <Page className="wizard">
                <SelectStepHeader
                    { ...this.props }
                    _t={ _t }
                    lineupValid={ lineupValid }
                    mod="low-pad"
                    onToggleWizardViewMode={ onToggleWizardViewMode }
                    wizardViewMode={ wizardViewMode }
                />
                { wizardViewMode === 'pitch' ?
                    <PitchView
                        _t={ _t }
                        lineup={ lineup }
                        onCompare={ this.props.onCompare }
                        onPositionSelect={ onPositionSelect }
                        onShowAthleteInfo={ this.handleShowAthleteInfo }
                        onUndraft={ onPositionClear }
                    />
                :
                [
                    <LineupList
                        _t={ _t }
                        key={ 1 }
                        lineup={ lineup }
                        onCompare={ this.props.onCompare }
                        onPositionClear={ onPositionClear }
                        onPositionSelect={ onPositionSelect }
                        onShowAthleteInfo={ this.handleShowAthleteInfo }
                        onUncompare={ this.props.onUncompare }
                        positions={ lineupPositions }
                        selectedIds={ selectedIds }
                    />,
                ]}
                <OnboardingContainer label={ _t( 'onboarding.btn_got_it' ) } name="draft" >
                    <DraftOnboarding _t={ _t } />
                </OnboardingContainer>
            </Page>
        );
    }
}

export default SelectStepComponent;
