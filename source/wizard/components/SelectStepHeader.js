import React, { Component, PropTypes } from 'react';

import Btn from '../../ui/btn/Btn';
import LineupInfo from './LineupInfo';

import './style.scss';


class SelectStepHeader extends Component {

    static propTypes = {
        _t: PropTypes.func.isRequired,
        avgFPPG: PropTypes.number.isRequired,
        budgetLeft: PropTypes.number.isRequired,
        budgetMax: PropTypes.number.isRequired,
        error: PropTypes.string,
        lineupPositions: PropTypes.array.isRequired,
        lineupValid: PropTypes.bool,
        mode: PropTypes.oneOf( ['create', 'edit'] ).isRequired,
        onExit: PropTypes.func.isRequired,
        onNext: PropTypes.func.isRequired,
        playersCount: PropTypes.number.isRequired,
        requesting: PropTypes.bool,
    };

    render () {
        const { _t, budgetMax, budgetLeft, playersCount, lineupValid, mode, error, requesting,
            lineupPositions, avgFPPG, onExit, onNext } = this.props;

        const nextButtonLabel = mode === 'edit'
            ? _t( 'wizard.select.header.btn_save' )
            : _t( 'wizard.select.header.btn_next' );

        return (
            <section className="wizard-header-wrap">
                <div className="wizard-header">
                    <div className="wizard-header__row select-step-top">
                        <Btn
                            label={ _t( 'wizard.select.header.btn_exit' ) }
                            onClick={ onExit }
                            type="default"
                        />
                        <h3 className="wizard-header__title">
                            <p className="wizard-header__title-main">
                                { _t( 'wizard.select.header.title' ) }
                                </p>
                            <p className="wizard-header__title-count">
                                { playersCount }<span>/{ lineupPositions.length }</span>
                            </p>
                        </h3>
                        <Btn
                            className="wizard-create-game"
                            disabled={ !lineupValid || requesting || error }
                            label={ nextButtonLabel }
                            onClick={ onNext }
                            type="primary"
                        />
                    </div>
                    <LineupInfo
                        _t={ _t }
                        avgFPPG={ avgFPPG }
                        budgetLeft={ budgetLeft }
                        budgetMax={ budgetMax }
                        className="wizard-header__row select-step"
                    />
                </div>
            </section>
        );
    }
}

export default SelectStepHeader;
