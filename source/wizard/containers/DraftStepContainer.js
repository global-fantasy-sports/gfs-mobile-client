import { connect } from 'react-redux';
import { goTo } from '../../actions/router';

import DraftComponent from '../components/DraftComponent';
import { translate } from '../../selectors/translations';
import { removeComparison, setComparison } from '../../actions/comparison';
import { setVariable } from '../../actions/ui';
import { toggleWizardViewMode } from '../../actions/wizardViewMode';
import { clearPosition, selectParticipant, unselectParticipant } from '../../actions/wizard';
import { getChosenSport, getSportData } from '../../selectors/sport';
import { POSITIONS } from '../../constants/wizard';
import { getChosenLeague, getSalarycapBudget } from '../../selectors/league';
import {
    getSelectedIds,
    getSelectedTeamsCount,
    getTotals,
    getWizardData,
    getWizardLineup,
    getWizardViewMode,
} from '../../selectors/wizard';
import { addMatchToParticipants } from '../../selectors/matches';
import { addFavoritesToParticipants, getAvailableParticipants } from '../../selectors/participants';

function mapStateToProps ( state, ownProps ) {
    const sport = getChosenSport( state );
    const salarycapBudget = getSalarycapBudget( state );
    const sportData = getSportData( state );
    const { matches, favorites } = sportData;
    const wizard = getWizardData( state );

    let participants = getAvailableParticipants( state );
    participants = addMatchToParticipants( participants, matches );
    participants = addFavoritesToParticipants( participants, favorites );
    const positions = POSITIONS[sport][ownProps.params.position] || [];

    // Get full lineup for N from N fantasy restrictions for Salary Cap Game
    const lineupFull = getWizardLineup( wizard.lineup, participants );

    // Filter participants by position
    participants = participants.filter( ( p ) => positions.includes( p.position ) );

    const lineup = getWizardLineup( wizard.lineup, participants );
    const selectedParticipants = Object.values( lineup );
    const playersCount = selectedParticipants.length;
    const totals = getTotals( state );
    const budgetLeft = salarycapBudget - totals.salary;
    const playersMax = wizard.lineupPositions.length;


    return {
        avgFPPG: playersCount ? totals.fppg / playersCount : 0,
        budgetMax: salarycapBudget,
        budgetLeft,
        currentSport: getChosenSport( state ),
        currenLeague: getChosenLeague( state ),
        favoriteParticipants: participants.filter( ( p ) => p.isFavorite ),
        lineup,
        lineupValid: playersCount === playersMax && budgetLeft >= 0,
        participants,
        playersCount,
        playersMax,
        selectedIds: getSelectedIds( lineup ),
        selectedTeams: getSelectedTeamsCount( lineupFull ),
        wizardViewMode: getWizardViewMode( state ),
        _t: translate( state ),
    };
}

function mergeProps ( state, { dispatch }, ownProps ) {
    const { currentSport, currenLeague, selectedIds } = state;
    return {
        ...state,
        ...ownProps,
        onToggleWizardViewMode () {
            dispatch( toggleWizardViewMode() );
        },
        onCompare ( { participantId, athleteId, position } ) {
            dispatch( setComparison( currentSport, currenLeague, athleteId, position ) );
            dispatch( setVariable( { position, from: 'wizard', participantId, selected: selectedIds } ) );
            dispatch( goTo( '/comparison/' ) );
        },
        onUncompare ( athleteId ) {
            dispatch( removeComparison( currentSport, currenLeague, athleteId ) );
        },
        onPositionSelect ( pos ) {
            dispatch( setVariable( { pos, from: 'wizard', selected: selectedIds } ) );
            dispatch( goTo( `/wizard/select/${pos}/` ) );
        },
        onPositionClear ( position ) {
            dispatch( clearPosition( position ) );
        },
        onSelectParticipant ( position, id ) {
            dispatch( selectParticipant( position, id ) );
            dispatch( goTo( '/wizard/select/' ) );
        },
        onUnselectParticipant ( id ) {
            dispatch( unselectParticipant( id ) );
        },
        onShowAthleteInfo ( { position, participantId, athleteId } ) {
            dispatch( setVariable( { participantId, pos: position, from: 'wizard', selected: selectedIds } ) );
            dispatch( goTo( `/athletes/${athleteId}/` ) );
        },
    };
}

export default connect(
    mapStateToProps,
    null,
    mergeProps,
)( DraftComponent );
