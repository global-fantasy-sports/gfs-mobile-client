import _ from 'lodash';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { goTo, redirectTo } from '../../actions/router';

import {
    clearError,
    clearPosition,
    clearWizard,
    saveGame,
    selectCompetition,
    selectParticipant,
    unselectParticipant,
} from '../../actions/wizard';
import { toggleCompetitionRoomFilterPanel } from '../../actions/competition';
import { removeComparison, setComparison } from '../../actions/comparison';
import { setVariable } from '../../actions/ui';
import { getSportData } from '../../selectors/sport';
import EntryFeeComponent from '../components/EntryFeeComponent';
import { getWizardData } from '../../selectors/wizard';
import { getCompetitionsByDate, getNearestCompetitions, seasonSelector } from '../../selectors/competitions';
import { translate } from '../../selectors/translations';

const isIdIsNumber = ( id ) => ~~id > 0;

function getCompetitions ( sportData, state, predefinedCompetitionId ) {
    if ( !isIdIsNumber( predefinedCompetitionId ) ) {
        return getNearestCompetitions( state );
    }

    if ( Object.keys( sportData.competitionRooms ).length ) {
        const { startDate } = sportData.competitionRooms[predefinedCompetitionId];
        return getCompetitionsByDate( state, startDate );
    }
    return [];
}

function mapStateToProps ( state ) {
    const season = seasonSelector( state );
    const sportData = getSportData( state );
    const wizard = getWizardData( state );

    const competitionRooms = getCompetitions( sportData, state, wizard.predefinedCompetitionId );
    const selectedRooms = wizard.selectedRooms
        .map( ( id ) => competitionRooms.find( ( c ) => c.id === id ) )
        .filter( ( room ) => room );
    const roomsByCurrency = selectedRooms.length ? _.groupBy( selectedRooms, ( room ) => room.currency ) : {};
    return {
        competitionRooms,
        created: wizard.created,
        currentSport: sportData.sport,
        currentLeague: sportData.league,
        error: wizard.error,
        mode: wizard.mode,
        predefinedCompetitionId: wizard.predefinedCompetitionId,
        requesting: wizard.requesting,
        season,
        selectedRooms: wizard.selectedRooms,
        totalsByCurrency: _.mapValues( roomsByCurrency, ( rooms ) => _.sumBy( rooms, 'entryFee' ) ),
        _t: translate( state ),
    };
}

const mapDispatchToProps = ( dispatch ) => ( {
    ...bindActionCreators( {
        onClearError: clearError,
        onSelectCompetition: selectCompetition,
        onToggleFilterPanel: toggleCompetitionRoomFilterPanel,
    }, dispatch ),
    onFinish () {
        dispatch( clearWizard() );
        dispatch( goTo( ( '/games/' ) ) );
    },
    dispatch,
} );

function mergeProps ( state, actions, ownProps ) {
    const { currentSport, currentLeague, predefinedCompetitionId, selectedIds } = state;
    const { dispatch } = actions;

    return {
        ...state,
        ...actions,
        ...ownProps,
        onCreateGame: () => dispatch( saveGame() ),
        onCompare: ( { participantId, athleteId, position } ) => {
            dispatch( setComparison( currentSport, currentLeague, athleteId, position ) );
            dispatch( setVariable( { position, from: 'wizard', participantId, selected: selectedIds } ) );
            dispatch( goTo( '/comparison/' ) );
        },
        onUncompare: ( athleteId ) => dispatch( removeComparison( currentSport, currentLeague, athleteId ) ),
        onPositionSelect: ( pos ) => {
            dispatch( setVariable( { pos, from: 'wizard', selected: selectedIds } ) );
            dispatch( goTo( `/wizard/select/${pos}/` ) );
        },
        onPositionClear: ( position ) => dispatch( clearPosition( position ) ),
        onSelectParticipant ( position, id ) {
            dispatch( selectParticipant( position, id ) );
            dispatch( redirectTo( '/wizard/select/' ) );
        },
        onUnselectParticipant ( id ) {
            dispatch( unselectParticipant( id ) );
        },
        onExit () {
            dispatch( clearWizard() );
            dispatch( goTo( predefinedCompetitionId ? '/lobby/' : '/games/' ) );
        },
        onPrevStep () {
            dispatch( redirectTo( '/wizard/select/' ) );
        },
        onNextStep () {
            return process.env.WITH_SOCIAL === 'yes'
                ? dispatch( goTo( '/wizard/friends/' ) )
                : actions.onFinish();
        },
        onShowAthleteInfo ( { position, participantId, athleteId } ) {
            dispatch( setVariable( { participantId, pos: position, from: 'wizard', selected: selectedIds } ) );
            dispatch( goTo( `/athletes/${athleteId}/` ) );
        },
    };
}

const wizardContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps,
);

export default wizardContainer( EntryFeeComponent );
