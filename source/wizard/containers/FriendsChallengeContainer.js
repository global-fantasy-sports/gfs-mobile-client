import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { goTo } from '../../actions/router';

import FriendsChallengeComponent from '../components/FriendsChallengeComponent';
import { fbInvite, fbLogin, fbSearch } from '../../actions/facebook';
import { clearWizard } from '../../actions/wizard';
import { getFBSearchQuery, getFBStatus, getFilteredFriends } from '../../selectors/facebook';
import { translate } from '../../selectors/translations';

const mapStateToProps = ( state ) => ( {
    fbFriends: getFilteredFriends( state ),
    fbSearch: getFBSearchQuery( state ),
    fbStatus: getFBStatus( state ),
    _t: translate( state ),
} );

const mapDispatchToProps = ( dispatch ) => ( {
    ...bindActionCreators( {
        onFacebookLogin: fbLogin,
        onFacebookInvite: fbInvite,
        onFacebookSearch: fbSearch,
    }, dispatch ),
    onFinish () {
        dispatch( clearWizard() );
        dispatch( goTo( ( '/games/' ) ) );
    },
} );

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)( FriendsChallengeComponent );
