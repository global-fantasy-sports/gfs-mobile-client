import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import { showRulesDialog, showScoringDialog, openBottomSideMenu } from '../../actions/dialogs';
import { hideInfoPopup } from '../../actions/infoPopup';
import { getChosenSport } from '../../selectors/sport';
import { getSalarycapBudget } from '../../selectors/league';
import DraftStepInfoPopup from '../components/popups/DraftStepInfoPopup';
import StakeStepInfoPopup from '../components/popups/StakeStepInfoPopup';
import FriendsStepInfoPopup from '../components/popups/FriendsStepInfoPopup';
import { translate } from '../../selectors/translations';
import { isSelectStepActive, isEntryFeeStepActive, isFriendsStepActive } from '../../selectors/wizard';

function mapStateToProps ( state ) {
    let PopupComponent, salarycapBudget = null, visible;

    if ( isSelectStepActive( state.routing.location ) ) {
        PopupComponent = DraftStepInfoPopup;
        visible = state.reducer.infoPopups.visibility.draftStep;
        salarycapBudget = getSalarycapBudget( state );
    }

    if ( isEntryFeeStepActive( state.routing.location ) ) {
        PopupComponent = StakeStepInfoPopup;
        visible = state.reducer.infoPopups.visibility.stakeStep;
    }

    if ( isFriendsStepActive( state.routing.location ) ) {
        PopupComponent = FriendsStepInfoPopup;
        visible = state.reducer.infoPopups.visibility.friendsStep;
    }

    const props = {
        userId: state.reducer.user.id,
        PopupComponent,
        visible,
        sport: getChosenSport( state ),
        _t: translate( state ),
        initialBottomSideMenuActive: state.reducer.infoPopups.visibility.bottomSideMenu,
    };

    if ( PopupComponent === DraftStepInfoPopup ) {
        props.salarycapBudget = salarycapBudget;
    }
    return props;
}

const mergeProps = ( state, { dispatch } ) => {
    return {
        ...state,
        showRules () {
            dispatch( showRulesDialog() );
        },
        showScoring () {
            dispatch( showScoringDialog() );
        },
        onHide: ( name ) => {
            if ( state.initialBottomSideMenuActive ) {
                setTimeout( () => dispatch( openBottomSideMenu() ), 200 );
            }
            dispatch( hideInfoPopup( name ) );
        },
    };
};

class InfoPopupContainer extends Component {
    render () {
        const { PopupComponent, ...props } = this.props;

        if ( !PopupComponent ) {
            return null;
        }

        return <PopupComponent { ...props } />;
    }
}

InfoPopupContainer.propTypes = {
    PopupComponent: PropTypes.func,
    showRules: PropTypes.func,
    showScoring: PropTypes.func,
};

export default connect( mapStateToProps, null, mergeProps )( InfoPopupContainer );
