import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { goTo } from '../../actions/router';

import SelectStepComponent from '../components/SelectStepComponent';
import { toggleWizardViewMode } from '../../actions/wizardViewMode';
import { translate } from '../../selectors/translations';
import { removeComparison, setComparison } from '../../actions/comparison';
import { setVariable } from '../../actions/ui';
import {
    clearPosition,
    clearWizard,
    selectParticipant,
    unselectParticipant,
    hideLineUpSaver,
    saveGame,
    autofill,
} from '../../actions/wizard';
import { getChosenLeague, getChosenSport, getSportData } from '../../selectors/sport';
import { getSalarycapBudget } from '../../selectors/league';
import {
    getSelectedIds,
    getSelectedTeamsCount,
    getTotals,
    getWizardData,
    getWizardLineup,
    getWizardViewMode,
} from '../../selectors/wizard';
import { addMatchToParticipants } from '../../selectors/matches';
import { addFavoritesToParticipants, getSportParticipants } from '../../selectors/participants';

import { saveLineupWORedirect } from '../../actions/lineups';

function mapStateToProps ( state ) {
    const { matches, favorites } = getSportData( state );
    const wizard = getWizardData( state );
    let participants = getSportParticipants( state );
    participants = addMatchToParticipants( participants, matches );
    participants = addFavoritesToParticipants( participants, favorites );
    const favoriteParticipants = participants.filter( ( p ) => p.isFavorite );
    const salarycapBudget = getSalarycapBudget( state );

    const lineup = getWizardLineup( wizard.lineup, participants );
    const selectedParticipants = Object.values( lineup );
    const playersCount = selectedParticipants.filter( ( p ) => !p.unavailable ).length;
    const totals = getTotals( state );
    const budgetLeft = salarycapBudget - totals.salary;
    const playersMax = wizard.lineupPositions.length;


    return {
        _t: translate( state ),
        avgFPPG: playersCount ? totals.fppg / playersCount : 0,
        budgetLeft,
        budgetMax: salarycapBudget,
        currentLeague: getChosenLeague( state ),
        currentSport: getChosenSport( state ),
        error: wizard.error,
        favoriteParticipants,
        lineup,
        lineupIsFull: playersCount === playersMax,
        lineupIsSaved: wizard.lineupIsSaved,
        lineupPositions: wizard.lineupPositions,
        lineupSaverIsVisible: wizard.lineupSaverIsVisible,
        lineupValid: playersCount === playersMax && budgetLeft >= 0,
        mode: wizard.mode,
        playersCount,
        playersMax,
        predefinedCompetitionId: wizard.predefinedCompetitionId,
        selectedIds: getSelectedIds( lineup ),
        selectedTeams: getSelectedTeamsCount( lineup ),
        wizardViewMode: getWizardViewMode( state ),
        wizardBackTo: state.routing.wizardBackTo,
    };
}

function mergeProps ( state, { dispatch }, ownProps ) {
    const { currentSport, currentLeague, lineup, mode, selectedIds, predefinedCompetitionId } = state;
    let lineupAthleteIds = {};
    for ( let key in lineup ) {
        if ( {}.hasOwnProperty.call( lineup, key ) ) {
            lineupAthleteIds[key] = lineup[key].athleteId;
        }
    }
    return {
        ...state,
        ...ownProps,
        onAutofill () {
            dispatch( autofill() );
        },
        onCloseSaveLineUpPopup () {
            dispatch( hideLineUpSaver( ) );
        },
        onToggleWizardViewMode () {
            dispatch( toggleWizardViewMode() );
        },
        onCompare ( { participantId, athleteId, position, pos } ) {
            dispatch( setComparison( currentSport, currentLeague, athleteId, position ) );
            dispatch( setVariable( { pos, position, from: 'wizard', participantId, selected: selectedIds } ) );
            dispatch( goTo( '/comparison/' ) );
        },
        onSaveLineUpPopup () {
            dispatch( saveLineupWORedirect( currentSport, currentLeague, { lineup: lineupAthleteIds } ) );
        },
        onShowAthleteInfo ( { position, participantId, athleteId } ) {
            dispatch( setVariable( { participantId, pos: position, from: 'wizard', selected: selectedIds } ) );
            dispatch( goTo( `/athletes/${athleteId}/` ) );
        },
        onUncompare ( athleteId ) {
            dispatch( removeComparison( currentSport, currentLeague, athleteId ) );
        },
        onPositionSelect ( pos ) {
            dispatch( setVariable( { pos, from: 'wizard', selected: selectedIds } ) );
            dispatch( goTo( `/wizard/select/${pos}/` ) );
        },
        onPositionClear ( position ) {
            dispatch( clearPosition( position ) );
        },
        onSelectParticipant ( position, id ) {
            dispatch( selectParticipant( position, id ) );
            dispatch( goTo( '/wizard/select/' ) );
        },
        onUnselectParticipant ( id ) {
            dispatch( unselectParticipant( id ) );
        },
        onNext () {
            if ( mode === 'edit' ) {
                dispatch( saveGame() );
            } else {
                dispatch( goTo( '/wizard/stake/' ) );
            }
        },
        onExit () {
            window.piwik.push( ['trackEvent', 'Wizard', 'Exit Wizard'] );
            dispatch( clearWizard() );
            if ( state.wizardBackTo ) {
                dispatch( push( state.wizardBackTo ) );
            } else {
                dispatch( predefinedCompetitionId ? goTo( '/lobby/' ) : goTo( '/games/' ) );
            }
        },
    };
}

export default connect(
    mapStateToProps,
    null,
    mergeProps,
)( SelectStepComponent );
