import _ from 'lodash';
import { connect } from 'react-redux';
import { goTo } from '../../actions/router';

import routes from '../../constants/routes';

import { getChosenSport, isSportDataReady } from '../../selectors/sport';
import { getChosenLeague } from '../../selectors/league';
import { getSportParticipants } from '../../selectors/participants';
import { isActive as isPathActive } from '../../selectors/router';

import WizardPage from '../WizardPage';
import { getCurrentStep, getWizardData, getWizardLineup } from '../../selectors/wizard';
import { getWizardViewMode, isFullLineup } from '../../selectors/wizard';
import { translate } from '../../selectors/translations';

import { showInfoPopup, hideInfoPopup } from '../../actions/infoPopup';
import { toggleWizardViewMode } from '../../actions/wizardViewMode';
import { openBottomSideMenu, closeBottomSideMenu } from '../../actions/dialogs';
import { saveLineupWORedirect } from '../../actions/lineups';
import {
    clearError,
    initFromScratch,
    joinCompetition,
    autofill,
    clearDraft,
    confirmRedirect } from '../../actions/wizard';

function mapStateToProps ( state, ownProps ) {
    const currentStep = getCurrentStep( state );
    const wizard = getWizardData( state );
    const lineupParticipantIds = Object.values( wizard.lineup || {} );
    const participants = getSportParticipants( state );
    const lineup = getWizardLineup( wizard.lineup, participants );
    const isEmptyLineup = lineupParticipantIds.length === 0;
    const pos = ownProps.params.position;

    return {
        _t: translate( state ),
        currentStep,
        lineup,
        lineupIsSaved: wizard.lineupIsSaved,
        dataLoaded: isSportDataReady( state ),
        isFullLineup: isFullLineup( state ),
        isEmptyLineup,
        error: wizard.error,
        league: getChosenLeague( state ),
        mode: wizard.mode,
        playersCount: lineupParticipantIds.reduce( ( count, id ) => id ? count + 1 : count, 0 ),
        popupActive: state.reducer.infoPopups.visibility[currentStep] === true,
        bottomSideMenuActive: state.reducer.dialogs.bottomSideMenu,
        bottomSideMenuEnabled: currentStep === 'draftStep',
        initialBottomSideMenuActive: state.reducer.infoPopups.visibility.bottomSideMenu,
        sport: getChosenSport( state ),
        wizardViewMode: getWizardViewMode( state ),
        pos,
    };
}

function mergeProps ( { currentStep, lineup, isEmptyLineup, ...state }, { dispatch }, ownProps ) {
    const { competitionId, sport, league } = ownProps.location.query || {};
    const handleMenuVisibility = _.debounce( ( open ) => {
        if ( open ) {
            window.piwik.push( ['trackEvent', 'Actions Bar', 'Open Actions bar'] );
            dispatch( openBottomSideMenu() );
        } else {
            dispatch( closeBottomSideMenu() );
        }
    }, 100 );

    return {
        ...state,
        ...ownProps,
        currentStep,
        lineup,
        isEmptyLineup,
        isPathActive,
        onClearError: () => dispatch( clearError() ),
        onShowInfoPopup: () => dispatch( showInfoPopup( currentStep ) ),
        onHideBottomSideMenu: () => {
            dispatch( hideInfoPopup( 'bottomSideMenu' ) );
            handleMenuVisibility( false );
        },
        onSaveLineup: () => {
            window.piwik.push( ['trackEvent', 'Actions Bar', 'Save Line-up'] );
            let lineupAthleteIds = {};
            for ( let key in lineup ) {
                if ( {}.hasOwnProperty.call( lineup, key ) ) {
                    lineupAthleteIds[key] = lineup[key].athleteId;
                }
            }

            dispatch( saveLineupWORedirect( sport, league, { lineup: lineupAthleteIds } ) );
        },
        onShowBottomSideMenu: () => {
            if ( !state.bottomSideMenuActive ) {
                handleMenuVisibility( true );
            }
        },
        onToggleWizardViewMode: () => {
            dispatch( toggleWizardViewMode() );
        },
        onAutofill: () => {
            window.piwik.push( ['trackEvent', 'Actions Bar', 'Quick Pick'] );
            dispatch( autofill() );
        },
        onClearDraft: () => {
            dispatch( clearDraft() );
        },
        onWizardInit: () => {
            if ( !state.mode ) {
                const action = competitionId
                    ? joinCompetition( state.sport, competitionId )
                    : initFromScratch( state.sport );
                dispatch( action );
            }
        },
        onGetSaved: () => {
            window.piwik.push( ['trackEvent', 'Actions Bar', 'Import Line-Up'] );
            if ( !isEmptyLineup ) {
                dispatch( confirmRedirect() );
            } else {
                dispatch( goTo( routes.get( 'lineups' ) ) );
            }
        },
        onHideInfoPopup: () => {
            dispatch( hideInfoPopup( currentStep ) );
        },
    };
}

const wizardRootContainer = connect( mapStateToProps, null, mergeProps );

export default wizardRootContainer( WizardPage );
