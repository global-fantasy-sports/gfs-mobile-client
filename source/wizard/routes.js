import React from 'react';
import { IndexRedirect, IndexRoute, Route } from 'react-router';

import WizardRootContainer from './containers/WizardRootContainer';
import SelectStepContainer from './containers/SelectStepContainer';
import DraftStepContainer from './containers/DraftStepContainer';
import EntryFeeStepContainer from './containers/EntryFeeStepContainer';
import FriendsChallengeContainer from './containers/FriendsChallengeContainer';

export default (
    <Route component={ WizardRootContainer }>
        <IndexRedirect to="select/" />
        <Route path="select/">
            <IndexRoute
                component={ SelectStepContainer }
            />
            <Route
                component={ DraftStepContainer }
                path=":position/"
            />
        </Route>
        <Route
            component={ EntryFeeStepContainer }
            path="stake/"
        />
        <Route
            component={ FriendsChallengeContainer }
            path="friends/"
        />
    </Route>
);
