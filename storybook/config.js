import { configure } from '@kadira/storybook';
import 'scss/main.scss';

const requireWithCtx = require.context( '../source', true, /\.stories\.js$/ );

function loadStories () {
    requireWithCtx.keys().forEach( ( filename ) => requireWithCtx( filename ) );
}

configure( loadStories, module );
