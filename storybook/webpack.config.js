const path = require( 'path' );

module.exports = {
    resolve: {
        root: [
            path.resolve(__dirname, '../source'),
            path.resolve(__dirname, '../storybook'),
        ],
    },
    module: {
        preLoaders: [
            {
                test: /\.(js|jsx)$/,
                loader: 'eslint-loader',
                exclude: /(node_modules|\.idea)/,
                plugins: ['transform-runtime', 'react'],
            },
        ],
        loaders: [
            {
                test: /\.(js|jsx)$/,
                loader: 'babel',
                include: [
                    path.resolve(__dirname, '../source'),
                    path.resolve(__dirname, '../storybook'),
                    path.resolve(__dirname, '../node_modules/react-smooth'),
                ],
            },
            {
                test: /\.scss$/,
                loader: 'style!css!autoprefixer!sass',
                exclude: /(node_modules|\.idea)/,
            },
            {
                test: /\.css$/,
                loader: 'css!autoprefixer',
                exclude: /(node_modules|\.idea)/,
            },
            {
                test:   /\.(png|jpg)$/,
                loader: 'file',
                exclude: /(node_modules|\.idea)/,
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                loader: 'file',
                exclude: /(node_modules|\.idea)/,
            },
        ],
    },
    eslint: {
        configFile: './.eslintrc',
        emitWarning: true,
        fix: false,
    },
};
