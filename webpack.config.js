const CircularDependencyPlugin = require( 'circular-dependency-plugin' );
const ExtractTextPlugin = require( 'extract-text-webpack-plugin' );
const path = require( 'path' );
const webpack = require( 'webpack' );
const WebpackNotifier = require( 'webpack-notifier' );

const options = {
    buildPath: './build',
    cssMinName: 'index.min.css',
    entryMinName: 'index.min.js',
    entryName: 'index.js',
    entryPath: './index.js',
    publicPath: '/static-mobile/',
    sourcePath: './source',
};

const devServer = {
    contentBase: './source',
    host: 'localhost',
    port: 3000,
    stats: 'minimal',
    headers: {
        'Access-Control-Allow-Origin': '*',
    },
};

const globals = {
    'process.env': {
        BUILD_TARGET: JSON.stringify( process.env.BUILD_TARGET || 'og' ),
        BUILD_BRANCH: JSON.stringify( process.env.BUILD_BRANCH ),
        BUILD_NUMBER: JSON.stringify( process.env.BUILD_NUMBER ),
        NODE_ENV: JSON.stringify( process.env.NODE_ENV ),
        WITH_SELECTOR: JSON.stringify( process.env.WITH_SELECTOR || 'no' ),
        WITH_SELECTOR_LEAGUES: JSON.stringify( process.env.WITH_SELECTOR_LEAGUES || 'yes' ),
        WITH_SIMULATION: JSON.stringify( process.env.WITH_SIMULATION || 'yes' ),
        WITH_SOCIAL: JSON.stringify( process.env.WITH_SOCIAL || 'no' ),
    },
};

const isBuild = process.env.NODE_ENV === 'production';

const vendorModules = [
    './sentry-config.js',
    'babel-polyfill',
];

console.log( 'Mobile build configuration\n', globals );

const plugins = [
    new webpack.DefinePlugin( globals ),
    new webpack.ContextReplacementPlugin( /node_modules\/moment\/locale/, /en-gb|zh-cn/ ),
    new CircularDependencyPlugin( {
        exclude: /node_modules/,
        failOnError: true,
    } ),
    new ExtractTextPlugin( options.cssMinName, {
        allChunks: true,
        disable: !isBuild,
    } ),
    new WebpackNotifier( {
        title: 'mobileclient',
        alwaysNotify: true,
        excludeWarnings: true,
        contentImage: './source/images/favicons/favicon_120.png',
    } ),
];

if ( isBuild ) {
    plugins.push(
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.UglifyJsPlugin( {
            sourceMap: true,
            compress: {
                'keep_fnames': true,
                'screw_ie8': true,
                'warnings': false,
            },
            mangle: {
                'keep_fnames': true,
                'screw_ie8': true,
            },
        } )
    );
}
;

const autoprefixerConf = JSON.stringify( {
    browsers: [
        'last 2 versions',
        'iOS 8',
        'IE 10',
    ],
} );

const config = {
    cache: true,
    context: path.join( __dirname, options.sourcePath ),
    entry: vendorModules.concat( [options.entryPath] ),
    devtool: isBuild ? 'source-map' : 'cheap-module-inline-source-map',
    debug: !isBuild,
    output: {
        path: path.join( __dirname, options.buildPath ),
        filename: isBuild ? options.entryMinName : options.entryName,
        publicPath: ( isBuild ? '' : 'http://' + devServer.host + ':' + devServer.port ) + options.publicPath,
        pathinfo: !isBuild,
    },
    devServer,
    plugins,
    resolve: {
        extensions: ['', '.js', '.jsx', '.css'],
    },
    module: {
        loaders: [
            {
                test: /\.(js|jsx)$/,
                loader: isBuild ? 'babel' : 'react-hot!babel',
                include: [
                    path.resolve( __dirname, './source' ),
                ],
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract( 'style', 'css!autoprefixer?' + autoprefixerConf + '!sass' ),
                exclude: /node_modules/,
            },
            {
                test: /\.(png|jpg)$/,
                loader: 'file?name=[path][name].[hash].[ext]',
                exclude: /node_modules/,
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                loader: 'file?name=[path][name].[hash].[ext]',
            },
        ],
        preLoaders: isBuild ? [] : [
            {
                test: /\.(js|jsx)$/,
                loader: 'eslint-loader',
                exclude: /node_modules/,
                plugins: ['react'],
            },
        ],
    },
    eslint: {
        configFile: './.eslintrc',
        emitWarning: true,
    },
    sassLoader: {
        includePaths: path.resolve( __dirname, './source/scss' ),
    },
};

module.exports = config;
